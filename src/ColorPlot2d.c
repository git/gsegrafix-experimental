/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* ColorPlot2d.c
*
* Plots two-dimensional samples3d information as a function of color.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "ColorPlot2d.h"
#include "Misc.h"
#include "DrawTickMarks.h"
#include "DrawGrid2d.h"
#include <math.h>
#include "gsegraf.h"
#include "InitializePlot.h"
#include <string.h>
#include <stdlib.h>  // for free

#include "gse-cairo.h"

void
ColorPlot2d (struct target *target, const struct gse_ctx *context, int iplot, int icolor, int nx, int ny )
{
   int i, j, i1, j1, in, jn, nxvalues, nyvalues, nzvalues, width, height;
   guint32 color;
   double xmin, xmax, ymin, ymax, zmin, zmax,
          xscale, yscale, zscale, dz, width_bar, height_bar,
          x1_bar, x2_bar, y1_bar, y2_bar, width_ztick_labels,
          x1, x2, y1, y2, fraction, dx1, dx2, dy1, dy2,
          *xi, *yi, *zi, x, y, z;
   char string[21];
   GdkPixbuf *pixbuf_colorbar, *pixbuf_color;
   GseCairoPoints *points;

   const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];

   /* Get minimum and maximum axis values */
   nxvalues = context->xtick_labels.nvalues;
   nyvalues = context->ytick_labels.nvalues;
   nzvalues = context->ztick_labels.nvalues;
   xmin = context->xtick_labels.values[0];
   xmax = context->xtick_labels.values[nxvalues-1];
   ymin = context->ytick_labels.values[0];
   ymax = context->ytick_labels.values[nyvalues-1];
   zmin = context->ztick_labels.values[0];
   zmax = context->ztick_labels.values[nzvalues-1];
   xmin = xmin - context->xtick_labels.offset1;
   xmax = xmax + context->xtick_labels.offset2;
   ymin = ymin - context->ytick_labels.offset1;
   ymax = ymax + context->ytick_labels.offset2;
   zmin = zmin - context->ztick_labels.offset1;
   zmax = zmax + context->ztick_labels.offset2;

   cairo_rectangle_t box;
   get_box (target, context, &box);

   /* Calculate axis scale factors */
   xscale = box.width/(xmax - xmin);
   yscale = box.height/(ymax - ymin);

   /* Draw color-bar pixbuf */
   if ( icolor == 1 )
      {
      width_bar = 20.0;
      height_bar = 0.875*box.height;
      x1_bar = (box.x + box.width) + 20.0;
      x2_bar = x1_bar + width_bar;
      y1_bar = (box.y + (box.y + box.height))/2.0 + height_bar/2.0;
      y2_bar = (box.y + (box.y + box.height))/2.0 - height_bar/2.0;
      if (context->plot_param.draw_box)
         {
         width  = roundl(width_bar);
         height = roundl(height_bar);
         pixbuf_colorbar = gdk_pixbuf_new(GDK_COLORSPACE_RGB, TRUE, 8, width, height);
         gdk_pixbuf_fill(pixbuf_colorbar, 0xFFFFFF00);

         for ( j=height; j>=1; j-- )
            {
            fraction = (j - 1.0)/(height - 1.0);
            color = interp_color_1(context, fraction);
            for ( i=1; i<=width; i++ )
               put_pixel(pixbuf_colorbar, i-1, height-j, color);
            }

	 gse_cairo_render_pixbuf (target->cr, pixbuf_colorbar,
				  x1_bar, y1_bar, GSE_ANCHOR_SOUTH_WEST);

         g_object_unref(pixbuf_colorbar);

         /* Draw vertical line */
         points = gse_cairo_points_new(4);
         points->coords[0] = x2_bar + 8.0;
         points->coords[1] = y1_bar;
         points->coords[2] = x2_bar + 8.0 + context->tick_major;
         points->coords[3] = y1_bar;
         points->coords[4] = x2_bar + 8.0 + context->tick_major;
         points->coords[5] = y2_bar;
         points->coords[6] = x2_bar + 8.0;
         points->coords[7] = y2_bar;

	 gse_cairo_render_line (target->cr, points, 4, 2, context->canvas_fg_color);
				
	 
         gse_cairo_points_unref(points);

         /* Draw tick marks and tick-mark labels */
         if ( context->plot_param.z_tick_marks )
            {
	      DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
			    x2_bar + 8.0 + context->tick_major, 0, y1_bar, y2_bar - y1_bar,
                          context->ztick_labels.nvalues, &context->ztick_labels.values[0],
                          context->ztick_labels.offset1, context->ztick_labels.offset2,
                          180*deg2rad);

            if ( context->plot_param.z_tick_labels )
               {
               zscale = height_bar/(zmax - zmin);
               dz = (zmax - zmin)/(nzvalues - 1);
               width_ztick_labels = 0.0;
               for ( i=1; i<=nzvalues; i++ )
                  {
                  memset(string, 0, sizeof(string));
                  if ( fabs(context->ztick_labels.values[i-1]) < 0.01*dz )
                     snprintf(string, sizeof(string), "%1.0f", 0.0);
                  else
                     snprintf(string, sizeof(string), "%g", context->ztick_labels.values[i-1]);

		  gse_cairo_render_text_with_extents (target->cr, string,
						      x2_bar + 8.0 + context->tick_major + 8.0,
						      y1_bar - (context->ztick_labels.values[i-1] - zmin)*zscale,
						      GSE_ANCHOR_WEST,
						      context->canvas_fg_color,
						      context->font_tick_labels, 0,
						      &x1, &y1, &x2, &y2);

		  
                  if ( x2 - x1 > width_ztick_labels )
                     width_ztick_labels = x2 - x1;
                  }
               }
            }
         }
      }


   /* Label color-bar */
   if (context->plot_param.draw_box &&
        icolor == 1 && context->zlabel != NULL)
      {
      /* Draw z-axis label */
      y = (box.y + (box.y + box.height))/2.0;
      if ( context->plot_param.z_tick_marks &&
           context->plot_param.z_tick_labels )
         x = x2_bar + 8.0 + context->tick_major + 8.0 + width_ztick_labels + 8.0;
      else
         x = x2_bar + 8.0 + context->tick_major + 8.0;



      cairo_save (target->cr);
      cairo_translate (target->cr, x, y);
      cairo_rotate (target->cr, -M_PI / 2.0);
      gse_cairo_render_text (target->cr, context->zlabel, 0, 0,
			     GSE_ANCHOR_WEST,
			     context->canvas_fg_color,
			     context->font_axis_labels);
      cairo_restore (target->cr);
      }


   /* Calculate pixbuf width and height */
   dx1 = 0.0;
   dx2 = 0.0;
   dy1 = 0.0;
   dy2 = 0.0;

   if ( xmin >= the_plot->samples3d.x[0] )
      x1 = xmin;
   else
      {
      x1 = the_plot->samples3d.x[0];
      dx1 = the_plot->samples3d.x[0] - xmin;
      }

   if ( xmax <= the_plot->samples3d.x[nx-1] )
      x2 = xmax;
   else
      {
      x2 = the_plot->samples3d.x[nx-1];
      dx2 = xmax - the_plot->samples3d.x[nx-1];
      }

   if ( ymin >= the_plot->samples3d.y[0] )
      y1 = ymin;
   else
      {
      y1 = the_plot->samples3d.y[0];
      dy1 = the_plot->samples3d.y[0] - ymin;
      }

   if ( ymax <= the_plot->samples3d.y[ny-1] )
      y2 = ymax;
   else
      {
      y2 = the_plot->samples3d.y[ny-1];
      dy2 = ymax - the_plot->samples3d.y[ny-1];
      }

   width  = roundl((box.x + box.width) - box.x - (dx1 + dx2)*xscale);
   height = roundl((box.y + box.height) - box.y - (dy1 + dy2)*yscale);


   /* Check pixbuf width and height */
   if ( width <= 0 || height <= 0 )
      return;


   /* Get interpolated values of x and y */
   xi = xmalloc(width*sizeof(double));
   yi = xmalloc(height*sizeof(double));
   zi = xmalloc(width*height*sizeof(double));
   for ( i=1; i<=width; i++ )
      xi[i-1] = x1 + (i - 1)*(x2 - x1)/(width - 1);
   for ( j=1; j<=height; j++ )
      yi[j-1] = y1 + (j - 1)*(y2 - y1)/(height - 1);


   /* Get interpolated values of z (bilinear interpolation) */
   if ( the_plot->styleflags != STYLE_BILINEAR )
      for ( i=1; i<=width; i++ )
         for ( j=1; j<=height; j++ )
            interp2(nx, ny, 1, &the_plot->samples3d.x[0], &the_plot->samples3d.y[0], &the_plot->samples3d.z[0],
                    &xi[i-1], &yi[j-1], &zi[height*(i-1)+(j-1)]);


   /* Get interpolated values of z (nearest-neighbor interpolation) */
   else if ( the_plot->styleflags != STYLE_NEAREST )
      for ( i=1; i<=width; i++ )
         {
         if ( xi[i-1] <= the_plot->samples3d.x[0] )
            i1 = 0;
         else if ( xi[i-1] >= the_plot->samples3d.x[nx-1] )
            i1 = nx - 2;
         else
	   {
	     find_indices (0, nx-1, &the_plot->samples3d.x[0], xi[i-1], &i1, NULL);
	   }

         for ( j=1; j<=height; j++ )
            {
            if ( yi[j-1] <= the_plot->samples3d.y[0] )
               j1 = 0;
            else if ( yi[j-1] >= the_plot->samples3d.y[ny-1] )
               j1 = ny - 2;
            else
	      {
		find_indices(0, ny-1, &the_plot->samples3d.y[0], yi[j-1], &j1, NULL);
	      }

            if ( fabs(xi[i-1] - the_plot->samples3d.x[i1]) < fabs(xi[i-1] - the_plot->samples3d.x[i1+1]) )
               in = i1;
            else
               in = i1 + 1;

            if ( fabs(yi[j-1] - the_plot->samples3d.y[j1]) < fabs(yi[j-1] - the_plot->samples3d.y[j1+1]) )
               jn = j1;
            else
               jn = j1 + 1;

            zi[height*(i-1)+(j-1)] = the_plot->samples3d.z[ny*in+jn];
            }
         }


   /* Create pixbuf */
   pixbuf_color = gdk_pixbuf_new(GDK_COLORSPACE_RGB, TRUE, 8, width, height);
   gdk_pixbuf_fill(pixbuf_color, 0xFFFFFF00);


   /* Draw color plot */
   zscale = 1.0/(zmax - zmin);
   for ( i=1; i<=width; i++ )
      for ( j=height; j>=1; j-- )
         {
         z = zi[height*(i-1)+(j-1)];
         fraction = (z - zmin)*zscale;
         if ( fraction < 0.0 )
            fraction = 0.0;
         else if ( fraction > 1.0 )
            fraction = 1.0;
         if ( z < the_plot->zblack )
            color = 0x000000FF;
         else if ( z > the_plot->zwhite )
            color = 0xFFFFFFFF;
         else
	   color = interp_color_1(context, fraction);
         put_pixel(pixbuf_color, i-1, height-j, color);
         }
   

   gse_cairo_render_pixbuf (target->cr, pixbuf_color,
		     (box.x + (box.x + box.width) + (dx1 - dx2)*xscale)/2.0,
		     ((box.y + box.height) + box.y + (dy2 - dy1)*yscale)/2.0,
		     GSE_ANCHOR_CENTER);
		     
   
   g_object_unref(pixbuf_color);


   /* Draw grid */
   DrawGrid2d (target, context);

   /* Free memory */
   free(xi);
   free(yi);
   free(zi);

   return;
   }
