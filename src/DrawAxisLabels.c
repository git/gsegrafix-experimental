/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* DrawAxisLabels.c
*
* Draws plot box, axis labels, and title for two-dimensional plots.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/


#include <math.h>
#include "gsegraf.h"
#include "InitializePlot.h"
#include "gse-cairo.h"
#include "DrawAxisLabels.h"

void
DrawAxisLabels (struct target *target, const struct gse_ctx *context, const struct tick_label_widths *tlw)
{
  double x, y;

  cairo_rectangle_t box;
  get_box (target, context, &box);
     
   /* Draw plot box rectangle */
   if (context->plot_param.draw_box)
     {
       gse_cairo_render_rectangle (target->cr,
				   box.x, box.y,
				   box.width,
				   box.height,
				   2,
				   context->canvas_fg_color,
				   0xFFFFFF00);
     }

   /* Draw x-axis label */
   if ( context->xlabel != NULL )
      {
      x = (box.x + (box.x +  box.width))/2.0;
      if ( context->plot_param.draw_box &&
           context->plot_param.x_tick_marks &&
           context->plot_param.x_tick_labels )
         y = (box.y + box.height) + 8.0 + context->font_size_tick_labels + 8.0;
      else
         y = (box.y + box.height) + 8.0;

      gse_cairo_render_text (target->cr, context->xlabel,
			     x, y,
			     GSE_ANCHOR_NORTH,
			     context->canvas_fg_color,
			     context->font_axis_labels);
      }


   /* Draw y-axis label */
   if ( context->ylabel != NULL)
      {
      if (context->plot_param.draw_box &&
           context->plot_param.y_tick_marks &&
           context->plot_param.y_tick_labels )
	x = box.x - 8.0 - tlw->width_ytick_labels - 8.0;
      else
         x = box.x - 8.0;
      y = box.y + box.height/2.0;


      gse_cairo_render_text_with_extents (target->cr, context->ylabel, x, y,
			     GSE_ANCHOR_WEST,
			     context->canvas_fg_color,
			     context->font_axis_labels,
					  -M_PI / 2.0, 0,0,0,0
			     );
      }


   /* Draw plot title */
   if ( context->title != NULL )
      {
      x = box.x + box.width/2.0;
      y = box.y - 8.0;
      gse_cairo_render_text (target->cr, context->title,
			     x, y,
			     GSE_ANCHOR_SOUTH,
			     context->canvas_fg_color,
			     context->font_title);
      }

   return;
   }

