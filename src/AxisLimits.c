/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* AxisLimits.c
*
* Sets axis limits to specified values.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "AxisLimits.h"
#include "AutoScale.h"
#include <math.h>
#include "gsegraf.h"

gboolean
data_limits_are_sane (const struct gse_ctx *context)
{
  if (context->plot_param.axes_type == AXES_3d)
    g_return_val_if_fail (context->data_min_max.zmin < context->data_min_max.zmax,
			  FALSE);


  g_return_val_if_fail (context->data_min_max.xmin < context->data_min_max.xmax,
			FALSE);

  g_return_val_if_fail (context->data_min_max.ymin < context->data_min_max.ymax,
			FALSE);
   
  if (context->plot_param.axes_type == AXES_linear)
    for (int p = 0; p < context->plot_param.nplots; ++p)
      {
	const struct plot_parameters *the_plot = &context->plot_parameters[p];
	if (the_plot->plot_type == PLOT_contour ||
	    the_plot->plot_type == PLOT_color)
	  g_return_val_if_fail (context->data_min_max.zmin <
				context->data_min_max.zmax, FALSE);
      }
  return TRUE;
}

void AxisLimits (struct gse_ctx *context)
   {
   /* Declare variables */
   int i, iplot, nx, ny, nz;
   double x1, x2, y1, y2, z1, z2, deltax, deltay, deltaz, epsx, epsy, epsz;

   /* Get axis limits */
   if ( context->axis_limits[0])
      context->data_min_max.xmin = context->plot_param.axis_limits[0];
   if ( context->axis_limits[1])
      context->data_min_max.xmax = context->plot_param.axis_limits[1];
   if ( context->axis_limits[2])
      context->data_min_max.ymin = context->plot_param.axis_limits[2];
   if ( context->axis_limits[3])
      context->data_min_max.ymax = context->plot_param.axis_limits[3];
   if ( context->axis_limits[4])
      context->data_min_max.zmin = context->plot_param.axis_limits[4];
   if ( context->axis_limits[5])
      context->data_min_max.zmax = context->plot_param.axis_limits[5];

   /* Check axis limits */
   if (!data_limits_are_sane (context))
     return;

   /* Get autoscale data */
   if ( context->plot_param.axes_type == AXES_linear ||
        context->plot_param.axes_type == AXES_semilogx ||
        context->plot_param.axes_type == AXES_semilogy ||
        context->plot_param.axes_type == AXES_loglog )
      {
	int flag1 = 0;
	int flag2 = 0;
      for ( iplot=1; iplot<= context->plot_param.nplots; iplot++ )
         {
	   const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];
         if ( the_plot->plot_type == PLOT_color )
            flag1 = 1;
         else if ( the_plot->plot_type == PLOT_contour &&
                   the_plot->styleflags == STYLE_AUTO )
            flag1 = 1;
         else if ( the_plot->plot_type == PLOT_contour &&
                   (the_plot->styleflags == STYLE_CHAR || the_plot->styleflags == STYLE_HEX) )
            flag2 = 1;
         }

      if ( flag1 == 1 )
	AutoScale(context, 3, 7);
      else if ( flag2 == 1 )
	AutoScale(context, 3, 10);
      else
	AutoScale(context, 2, 10);
      }

   else if ( context->plot_param.axes_type == AXES_polar )
     AutoScale(context, 2, 5);

   else if ( context->plot_param.axes_type == AXES_3d )
     AutoScale(context, 3, 7);


   /* Get old tick-mark label values */
   nx = context->xtick_labels.nvalues;
   ny = context->ytick_labels.nvalues;
   nz = context->ztick_labels.nvalues;
   x1 = context->xtick_labels.values[0];
   x2 = context->xtick_labels.values[nx-1];
   y1 = context->ytick_labels.values[0];
   y2 = context->ytick_labels.values[ny-1];
   z1 = context->ztick_labels.values[0];
   z2 = context->ztick_labels.values[nz-1];
   deltax = (x2 - x1)/(nx - 1.0);
   deltay = (y2 - y1)/(ny - 1.0);
   deltaz = (z2 - z1)/(nz - 1.0);
   epsx = (x2 - x1)*1.0e-03;
   epsy = (y2 - y1)*1.0e-03;
   epsz = (z2 - z1)*1.0e-03;


   /* Get new tick-mark label values*/
   if ( context->axis_limits[0]&&
        context->plot_param.axes_type != AXES_semilogx &&
        context->plot_param.axes_type != AXES_loglog )
      {
      if ( context->plot_param.axis_limits[0] - x1 > epsx )
         {
         nx--;
         x1 = x1 + deltax;
         context->xtick_labels.offset1 = x1 - context->plot_param.axis_limits[0];
         }
      else
         context->xtick_labels.offset1 = 0.0;
      }

   if ( context->axis_limits[1]&& 
        context->plot_param.axes_type != AXES_semilogx &&
        context->plot_param.axes_type != AXES_loglog )
      {
      if ( x2 - context->plot_param.axis_limits[1] > epsx )
         {
         nx--;
         x2 = x2 - deltax;
         context->xtick_labels.offset2 = context->plot_param.axis_limits[1] - x2;
         }
      else
         context->xtick_labels.offset2 = 0.0;
      }

   if ( context->axis_limits[2]&&
        context->plot_param.axes_type != AXES_semilogy &&
        context->plot_param.axes_type != AXES_loglog )
      {
      if ( context->plot_param.axis_limits[2] - y1 > epsy )
         {
         ny--;
         y1 = y1 + deltay;
         context->ytick_labels.offset1 = y1 - context->plot_param.axis_limits[2];
         }
      else
         context->ytick_labels.offset1 = 0.0;
      }

   if ( context->axis_limits[3]&&
        context->plot_param.axes_type != AXES_semilogy &&
        context->plot_param.axes_type != AXES_loglog )
      {
      if ( y2 - context->plot_param.axis_limits[3] > epsy )
         {
         ny--;
         y2 = y2 - deltay;
         context->ytick_labels.offset2 = context->plot_param.axis_limits[3] - y2;
         }
      else
         context->ytick_labels.offset2 = 0.0;
      }

   if ( context->axis_limits[4])
      {
      if ( context->plot_param.axis_limits[4] - z1 > epsz )
         {
         nz--;
         z1 = z1 + deltaz;
         context->ztick_labels.offset1 = z1 - context->plot_param.axis_limits[4];
         }
      else
         context->ztick_labels.offset1 = 0.0;
      }

   if ( context->axis_limits[5])
      {
      if ( z2 - context->plot_param.axis_limits[5] > epsz )
         {
         nz--;
         z2 = z2 - deltaz;
         context->ztick_labels.offset2 = context->plot_param.axis_limits[5] - z2;
         }
      else
         context->ztick_labels.offset2 = 0.0;
      }


   /* Save new tick-mark label values */
   context->xtick_labels.nvalues = nx;
   context->ytick_labels.nvalues = ny;
   context->ztick_labels.nvalues = nz;
   for ( i=1; i<=nx; i++ )
      context->xtick_labels.values[i-1] = x1 + (i - 1)*((x2 - x1)/(nx - 1.0));
   for ( i=1; i<=ny; i++ )
      context->ytick_labels.values[i-1] = y1 + (i - 1)*((y2 - y1)/(ny - 1.0));
   for ( i=1; i<=nz; i++ )
      context->ztick_labels.values[i-1] = z1 + (i - 1)*((z2 - z1)/(nz - 1.0));

   }
