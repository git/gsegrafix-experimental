/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
 *
 * PlotLines.c
 *
 * Plots lines.
 *
 * Copyright 2017 John Darrington
 * Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
 * http://savannah.gnu.org/projects/gsegrafix
 *
 * This file is part of GSEGrafix, a scientific and engineering plotting program.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#include "PlotLines.h"
#include "Misc.h"
#include "Clip.h"
#include "DrawSymbols.h"
#include "gse-cairo.h"
#include "Initialize3d.h"
#include <math.h>
#include "gsegraf.h"
#include "PolarPlot.h"
#include <string.h>
#include "InitializePlot.h"

void
PlotLines (struct target *target, const struct gse_ctx *context)
{
  int i, nx, ny, nz;
  double  lower[3], upper[3];
  double  rmin, rmax;
  double xscale, yscale, zscale;
  double rscale, xorigin, yorigin;
  double radius, phi, theta;
  double Ryz[9];
  double origin[3];

  /* Get minimum and maximum axis values */
  if ( context->plot_param.axes_type == AXES_linear )
    {
      nx = context->xtick_labels.nvalues;
      lower[0] = context->xtick_labels.values[0];
      upper[0] = context->xtick_labels.values[nx-1];
      lower[0] = lower[0] - context->xtick_labels.offset1;
      upper[0] = upper[0] + context->xtick_labels.offset2;
      ny = context->ytick_labels.nvalues;
      lower[1] = context->ytick_labels.values[0];
      upper[1] = context->ytick_labels.values[ny-1];
      lower[1] = lower[1] - context->ytick_labels.offset1;
      upper[1] = upper[1] + context->ytick_labels.offset2;
    }

  else if ( context->plot_param.axes_type == AXES_semilogx )
    {
      nx = context->xtick_labels.nvalues;
      lower[0] = floor(context->xtick_labels.values[0]);
      upper[0] = ceil(context->xtick_labels.values[nx-1]);
      nx = roundl(upper[0] - lower[0] + 1.0);
      ny = context->ytick_labels.nvalues;
      lower[1] = context->ytick_labels.values[0];
      upper[1] = context->ytick_labels.values[ny-1];
      lower[1] = lower[1] - context->ytick_labels.offset1;
      upper[1] = upper[1] + context->ytick_labels.offset2;
    }

  else if ( context->plot_param.axes_type == AXES_semilogy )
    {
      nx = context->xtick_labels.nvalues;
      lower[0] = context->xtick_labels.values[0];
      upper[0] = context->xtick_labels.values[nx-1];
      lower[0] = lower[0] - context->xtick_labels.offset1;
      upper[0] = upper[0] + context->xtick_labels.offset2;
      ny = context->ytick_labels.nvalues;
      lower[1] = floor(context->ytick_labels.values[0]);
      upper[1] = ceil(context->ytick_labels.values[ny-1]);
      ny = roundl(upper[1] - lower[1] + 1.0);
    }

  else if ( context->plot_param.axes_type == AXES_loglog )
    {
      nx = context->xtick_labels.nvalues;
      lower[0] = floor(context->xtick_labels.values[0]);
      upper[0] = ceil(context->xtick_labels.values[nx-1]);
      nx = roundl(upper[0] - lower[0] + 1.0);
      ny = context->ytick_labels.nvalues;
      lower[1] = floor(context->ytick_labels.values[0]);
      upper[1] = ceil(context->ytick_labels.values[ny-1]);
      ny = roundl(upper[1] - lower[1] + 1.0);
    }

  else if ( context->plot_param.axes_type == AXES_polar )
    {
      ny = context->ytick_labels.nvalues;
      rmin = context->ytick_labels.values[0];
      rmax = context->ytick_labels.values[ny-1];
      rmin = rmin - context->ytick_labels.offset1;
      rmax = rmax + context->ytick_labels.offset2;
    }

  else if ( context->plot_param.axes_type == AXES_3d )
    {
      nx = context->xtick_labels.nvalues;
      ny = context->ytick_labels.nvalues;
      nz = context->ztick_labels.nvalues;
      lower[0] = context->xtick_labels.values[0];
      upper[0] = context->xtick_labels.values[nx-1];
      lower[1] = context->ytick_labels.values[0];
      upper[1] = context->ytick_labels.values[ny-1];
      lower[2] = context->ztick_labels.values[0];
      upper[2] = context->ztick_labels.values[nz-1];
      lower[0] = lower[0] - context->xtick_labels.offset1;
      upper[0] = upper[0] + context->xtick_labels.offset2;
      lower[1] = lower[1] - context->ytick_labels.offset1;
      upper[1] = upper[1] + context->ytick_labels.offset2;
      lower[2] = lower[2] - context->ztick_labels.offset1;
      upper[2] = upper[2] + context->ztick_labels.offset2;
    }

  cairo_rectangle_t box;
  get_box (target, context, &box);

  /* Calculate axis scale factors */
  if ( context->plot_param.axes_type == AXES_linear ||
       context->plot_param.axes_type == AXES_semilogx ||
       context->plot_param.axes_type == AXES_semilogy ||
       context->plot_param.axes_type == AXES_loglog )
    {
      xscale = box.width/(upper[0] - lower[0]);
      yscale = box.height/(upper[1] - lower[1]);
    }

  else if (context->plot_param.axes_type == AXES_polar )
    {
      polar_get_geometry (target, &xorigin, &yorigin, &radius);
	
      rscale = radius/(rmax - rmin);
    }

  else if ( context->plot_param.axes_type == AXES_3d )
    {
      double Ry[9];
      /* Get view angles */
      phi = context->plot_param_3d.phi;
      theta = context->plot_param_3d.theta;

      get_origin (context, (int) (phi/90.0) + 1, origin);

      /* Calculate rotation matrices */
      phi = phi*deg2rad;
      theta = theta*deg2rad;

      Ry[0] = cos(-theta); Ry[1] = 0.0; Ry[2] = -sin(-theta);
      Ry[3] = 0.0; Ry[4] = 1.0; Ry[5] = 0.0;
      Ry[6] = sin(-theta); Ry[7] = 0.0; Ry[8] = cos(-theta);

      Ryz[0] = cos(phi); Ryz[1] = sin(phi); Ryz[2] = 0.0;
      Ryz[3] = -sin(phi); Ryz[4] = cos(phi); Ryz[5] = 0.0;
      Ryz[6] = 0.0; Ryz[7] = 0.0; Ryz[8] = 1.0;

      multiply_mm (Ry, Ryz);

      /* Get axis length */
      double axis_length = axis_length_3d (target);

      /* Calculate axis scale factors */
      xscale = axis_length/(upper[0] - lower[0]);
      yscale = axis_length/(upper[1] - lower[1]);
      zscale = axis_length/(upper[2] - lower[2]);
    }

  /* Draw lines */

  for (i = 0; i < context->n_lines; ++i)
    {
      const struct line_spec *ls = context->lines + i;
      double coords[6];
      memcpy (coords, ls->coords, 6 * sizeof (double));
       
      if ( context->plot_param.axes_type == AXES_linear ||
	   context->plot_param.axes_type == AXES_semilogx ||
	   context->plot_param.axes_type == AXES_semilogy ||
	   context->plot_param.axes_type == AXES_loglog )
	{
	  if ( Clip2d(lower, upper, coords) == 1 )
	    {
	      GseCairoPoints *points = gse_cairo_points_new(2);
	      points->coords[0] = box.x + (coords[0] - lower[0])*xscale;
	      points->coords[1] = (box.y + box.height) - (coords[1] - lower[1])*yscale;
	      points->coords[2] = box.x + (coords[2] - lower[0])*xscale;
	      points->coords[3] = (box.y + box.height) - (coords[3] - lower[1])*yscale;
	      DrawLine (target, context, points,
			ls->line_color, ls->line_width, ls->texture);
	      gse_cairo_points_unref(points);
	    }
	}

      else if ( context->plot_param.axes_type == AXES_polar )
	{
	  if ( ClipPolar(rmin, rmax, &coords[0]) == 1 )
	    {
	      GseCairoPoints *points = gse_cairo_points_new(2);
	      points->coords[0] = xorigin + (coords[1] - rmin)*cos(coords[0])*rscale;
	      points->coords[1] = yorigin - (coords[1] - rmin)*sin(coords[0])*rscale;
	      points->coords[2] = xorigin + (coords[3] - rmin)*cos(coords[2])*rscale;
	      points->coords[3] = yorigin - (coords[3] - rmin)*sin(coords[2])*rscale;
	      DrawLine (target, context, points, ls->line_color, ls->line_width, ls->texture);
	      gse_cairo_points_unref(points);
	    }
	}

      else if ( context->plot_param.axes_type == AXES_3d )
	{
	  if ( Clip3d(lower, upper, coords) == 1 )
	    {
	      double r1[3], r2[3];
	      r1[0] = (coords[0] - lower[0])*xscale;
	      r1[1] = (coords[1] - lower[1])*yscale;
	      r1[2] = (coords[2] - lower[2])*zscale;

	      r2[0] = (coords[3] - lower[0])*xscale;
	      r2[1] = (coords[4] - lower[1])*yscale;
	      r2[2] = (coords[5] - lower[2])*zscale;

	      multiply_mv (Ryz, r1);
	      multiply_mv (Ryz, r2);

	      GseCairoPoints *points = gse_cairo_points_new(2);
	      points->coords[0] = origin[1] + r1[1];
	      points->coords[1] = origin[2] - r1[2];
	      points->coords[2] = origin[1] + r2[1];
	      points->coords[3] = origin[2] - r2[2];
	      DrawLine (target, context, points, ls->line_color, ls->line_width, ls->texture);
	      gse_cairo_points_unref(points);
	    }
	}
    }
}
