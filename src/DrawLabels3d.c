/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* DrawLabels3d.c
*
* Draws plot box, axes, axis labels, and title for 3-dimensional plots.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/


#include <math.h>
#include "gsegraf.h"

#include "gse-cairo.h"

#include "DrawLabels3d.h"
#include "DrawAxisLabels.h"

void
DrawLabels3d (struct target *target, const struct gse_ctx *context, const struct tick_label_widths *tlw)
{
  int i;
   double axis1[3], axis2[3], axis3[3], origin[3],
          x, y, x0, y0, x1, y1, x2, y2,
          angle1, angle2, phi, theta;
   char *label;
   GseCairoPoints *points;

   /* Get quadrant */
   int quadrant = context->plot_param_3d.quadrant;
   
   /* Get azimuth and elevation */
   phi   = context->plot_param_3d.phi;
   if ( quadrant == 2 )
      phi = phi - 90.0;
   else if ( quadrant == 3 )
      phi = phi - 180.0;
   else if ( quadrant == 4 )
      phi = phi - 270.0;
   theta = context->plot_param_3d.theta;
   phi   = phi*deg2rad;
   theta = theta*deg2rad;
   
   /* Get axes */
   for ( i=1; i<=3; i++ )
      {
      axis1[i-1] = context->plot_param_3d.axis1[i-1];
      axis2[i-1] = context->plot_param_3d.axis2[i-1];
      axis3[i-1] = context->plot_param_3d.axis3[i-1];
      }


   /* Get origin */
   for ( i=1; i<=3; i++ )
      origin[i-1] = context->plot_param_3d.origin[i-1];


   /* Draw axes */
   if (context->plot_param.draw_box)
      {
      points = gse_cairo_points_new(2);

      for ( i=1; i<=3; i++ )
         {
         points->coords[0] = origin[1];
         points->coords[1] = origin[2];

         if ( i == 1 )
            {
            points->coords[2] = origin[1] + axis1[1];
            points->coords[3] = origin[2] - axis1[2];
            }

         else if ( i == 2 )
            {
            points->coords[2] = origin[1] + axis2[1];
            points->coords[3] = origin[2] - axis2[2];
            }

         else if ( i == 3 )
            {
            points->coords[2] = origin[1] + axis3[1];
            points->coords[3] = origin[2] - axis3[2];
            }

	 gse_cairo_render_line (target->cr, points, 2, 2, context->canvas_fg_color);
         }

      gse_cairo_points_unref(points);
      }


   /* Draw plot box */
   if (context->plot_param.draw_box)
      {
      points = gse_cairo_points_new(6);
      points->coords[0]  = origin[1] + axis1[1];
      points->coords[1]  = origin[2] - axis1[2];
      points->coords[2]  = origin[1] + axis1[1] + axis3[1];
      points->coords[3]  = origin[2] - axis1[2] - axis3[2];
      points->coords[4]  = origin[1] + axis3[1];
      points->coords[5]  = origin[2] - axis3[2];
      points->coords[6]  = origin[1] + axis2[1] + axis3[1];
      points->coords[7]  = origin[2] - axis2[2] - axis3[2];
      points->coords[8]  = origin[1] + axis2[1];
      points->coords[9]  = origin[2] - axis2[2];
      points->coords[10] = origin[1] + axis1[1] + axis2[1];
      points->coords[11] = origin[2] - axis1[2] - axis2[2];

      gse_cairo_render_polygon (target->cr, points, 6, 2,
				context->canvas_fg_color,
				0xFFFFFF00);
      
      gse_cairo_points_unref(points);
      }


   /* Draw axis-1 label */
   if ( quadrant == 1 || quadrant == 3 )
      label = context->xlabel;
   else
      label = context->ylabel;

   if ( label != NULL )
      {
      x1 = origin[1] + axis2[1];
      y1 = origin[2] - axis2[2];
      x2 = origin[1] + axis1[1] + axis2[1];
      y2 = origin[2] - axis1[2] - axis2[2];
      x0 = (x1 + x2)/2.0;
      y0 = (y1 + y2)/2.0;
      angle2 = atan2(axis2[2], axis2[1]);

      x = x0 + (8.0 + tlw->width_axis1_tick_labels + 8.0)*fabs(cos(angle2))*sin(phi)*sin(theta)
             + (8.0 + tlw->width_axis1_tick_labels + 8.0)*cos(phi);
      y = y0 + (8.0 + tlw->width_axis1_tick_labels + 8.0)*fabs(sin(angle2))*sin(phi)*sin(theta)
             + (8.0 + context->font_size_tick_labels + 8.0)*sin(phi)*cos(theta);
                      
      gse_cairo_render_text (target->cr, label, x, y,
			     GSE_ANCHOR_NORTH_WEST,
			     context->canvas_fg_color,
			     context->font_axis_labels);
      }


   /* Draw axis-2 label */
   if ( quadrant == 1 || quadrant == 3 )
      label = context->ylabel;
   else
      label = context->xlabel;

   if ( label != NULL )
      {
      x1 = origin[1] + axis1[1];
      y1 = origin[2] - axis1[2];
      x2 = origin[1] + axis1[1] + axis2[1];
      y2 = origin[2] - axis1[2] - axis2[2];
      x0 = (x1 + x2)/2.0;
      y0 = (y1 + y2)/2.0;
      angle1 = atan2(axis1[2], axis1[1]);

      x = x0 - (8.0 + tlw->width_axis2_tick_labels + 8.0)*fabs(cos(angle1))*cos(phi)*sin(theta)
             - (8.0 + tlw->width_axis2_tick_labels + 8.0)*sin(phi);
      y = y0 + (8.0 + tlw->width_axis2_tick_labels + 8.0)*fabs(sin(angle1))*cos(phi)*sin(theta)
             + (8.0 + context->font_size_tick_labels + 8.0)*cos(phi)*cos(theta);

      gse_cairo_render_text (target->cr, label, x, y,
			     GSE_ANCHOR_NORTH_EAST,
			     context->canvas_fg_color,
			     context->font_axis_labels);
      }


   /* Draw z-axis label */
   if ( context->zlabel != NULL)
      {
      x1 = origin[1] + axis1[1];
      y1 = origin[2] - axis1[2];
      x2 = origin[1] + axis1[1] + axis3[1];
      y2 = origin[2] - axis1[2] - axis3[2];
      y = -(y1 + y2)/2.0;
      x = x1 - 8.0 - tlw->width_axis3_tick_labels - 8.0;
      y = (y1 + y2)/2.0;

      cairo_save (target->cr);
      cairo_translate (target->cr, x, y);
      cairo_rotate (target->cr, -M_PI / 2.0);
      gse_cairo_render_text (target->cr, context->zlabel, 0, 0,
			     GSE_ANCHOR_EAST,
			     context->canvas_fg_color,
			     context->font_axis_labels);
      cairo_restore (target->cr);
      }


   /* Draw plot title */
   if ( context->title != NULL )
      {
      x = origin[1] + (axis1[1] + axis2[1])/2.0;
      y = origin[2] - axis3[2] - 8.0;


      gse_cairo_render_text (target->cr, context->title, x, y,
			     GSE_ANCHOR_SOUTH,
			     context->canvas_fg_color,
			     context->font_title);
      }

   return;
   }
