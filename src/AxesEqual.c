/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* AxesEqual.c
*
* Converts axis scaling from "normal" (units on x and y axes not necessarily
* the same length) to "equal" (units on x and y axes equal in length).
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "AxesEqual.h"
#include "AxisLimits.h"
#include "Misc.h"
#include <math.h>
#include "gsegraf.h"
#include "InitializePlot.h"

#include <string.h> // for strcmp

void AxesEqual (struct target *target, struct gse_ctx *context)
{
  /* Return if equal axes not requested */
  g_return_if_fail (context->plot_param.equal_axes);

  /* Get initial tick-mark label values */
  int nx = context->xtick_labels.nvalues;
  int ny = context->ytick_labels.nvalues;
  double xmin = context->xtick_labels.values[0];
  double xmax = context->xtick_labels.values[nx-1];
  double ymin = context->ytick_labels.values[0];
  double ymax = context->ytick_labels.values[ny-1];
  xmin -= context->xtick_labels.offset1;
  xmax += context->xtick_labels.offset2;
  ymin -= context->ytick_labels.offset1;
  ymax += context->ytick_labels.offset2;


  /* Get data minimum and maximum values */
  double xdata_min = context->data_min_max.xmin;
  double xdata_max = context->data_min_max.xmax;
  double ydata_min = context->data_min_max.ymin;
  double ydata_max = context->data_min_max.ymax;

  cairo_rectangle_t box;
  get_box (target, context, &box);

  /* Expand x data range if necessary */
  double ratio = box.height/box.width;
  if ( ratio*(xmax - xmin) < (ymax - ymin) )
    {
      double xmid = (xdata_min + xdata_max)/2.0;
      double x1 = xmid - 0.5*(ydata_max - ydata_min)/ratio;
      double x2 = xmid + 0.5*(ydata_max - ydata_min)/ratio;
      context->axis_limits[0] = TRUE;
      context->axis_limits[1] = TRUE;
      context->plot_param.axis_limits[0] = x1;
      context->plot_param.axis_limits[1] = x2;
      AxisLimits(context);
      nx = context->xtick_labels.nvalues;
      xmin = context->xtick_labels.values[0];
      xmax = context->xtick_labels.values[nx-1];
      xmin -= context->xtick_labels.offset1;
      xmax += context->xtick_labels.offset2;
    }

  /* Calculate new y-axis limits */
  double ymid = (ydata_min + ydata_max)/2.0;
  double y1 = ymid - ratio*(xmax - xmin)/2.0;
  double y2 = ymid + ratio*(xmax - xmin)/2.0;
  context->axis_limits[2] = TRUE;
  context->axis_limits[3] = TRUE;
  context->plot_param.axis_limits[2] = y1;
  context->plot_param.axis_limits[3] = y2;

  /* Calculate new tick-mark label values */
  AxisLimits(context);
}
