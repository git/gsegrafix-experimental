/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*
  This file is part of GSEGrafix, a scientific and engineering plotting 
  program.

  Copyright (C) 2017 John Darrington

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include "data-iterator.h"
#include "data.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "merge-sort.h"


#include <assert.h>

static int
cmp (const void *d0_, const void *d1_)
{
  const struct gse_datum *d0 = d0_;
  const struct gse_datum *d1 = d1_;

  if (d0->x < d1->x)
    return -1;

  return (d0->x > d1->x);
}

static void
shipout (FILE *fp, const struct gse_datum *d, const char *template)
{
  fprintf (fp, template, d->x, d->y, d->z);
  fprintf (fp, "\n");
}

/* Merge the files F0 and F1 into OUTPUT */
static bool
merge_pair (const char *f0, const char *f1, const char *output,
	    const char *template, int dimensions)
{
  struct sample_iterator *i0 = it_create_from_file (f0, template, dimensions);
  if (!i0)
    {
      printf ("%s not created\n", f0);
      return false;
    }
  
  struct sample_iterator *i1 = it_create_from_file (f1, template, dimensions);
  if (!i1)
    {
      printf ("%s Not created\n", f1);
      return false;
    }

  FILE *fp = fopen (output, "w+");
  if (! fp)
    {
      perror ("fopen");
      return false;
    }
  
  for (;;)
    {
      const struct gse_datum *s0 = it_get (i0);
      const struct gse_datum *s1 = it_get (i1);

      if (s1 && s0)
	{
	  if (cmp (s0, s1) < 0)
	    {
	      shipout (fp, s0, template);
	      it_next (i0);
	    }
	  else
	    {
	      shipout (fp, s1, template);
	      it_next (i1);
	    }
	}
      else if (s1)
	{
	  shipout (fp, s1, template);
	  it_next (i1);
	}
      else if (s0)
	{
	  shipout (fp, s0, template);
	  it_next (i0);
	}
      else
	break;
    }
  it_destroy (i0);
  it_destroy (i1);

  fclose (fp);
  return true;
}


/* Merge the *N files whose names are in the array IN,
   into (*N + 1) / 2 files. 
   IN and N will be automatically updated with the names
   of the new files, and the quantity of them */
static bool
merge_step (char ***in, int *n, const char *template, int dimensions)
{
  int x = 0;
  int i = 0;
  char **out = calloc ((*n + 1) / 2 , sizeof (char *));
  for (i = 0; i < *n - 1; i+=2)
    {
      out[x] = strdup (P_tmpdir "/outXXXXXX");
      int fd = mkstemp (out[x]);
      if (fd >= 0)
	close (fd);
      if (!merge_pair ((*in)[i], (*in)[i+1], out[x], template, dimensions))
	{
	  free (out);
	  return false;
	}
      ++x;
    }

  for (int j = 0; j < x * 2; ++j)
    {
      unlink ((*in)[j]);
    }

  if (i < *n)
    {
      assert (*n % 2); // n is odd
      assert (i == *n - 1);
      out[x++] = strdup ((*in)[i]);
    }

  for (i = 0; i < *n; ++i)
    {
      free ((*in)[i]);
    }
  free (*in);
  
  *in = out;
  
  *n = x;
  return true;
}


static char *
save_chunk (struct gse_datum *chunk, size_t n, const char *template)
{
  qsort (chunk, n, sizeof (struct gse_datum), cmp);
  char *name = strdup (P_tmpdir "/outXXXXXX");
  int fd = mkstemp (name);
  if (fd < 0)
    {
      perror ("mkstemp");
      return NULL;
    }
  FILE *fp = fdopen (fd, "w+");
  if (fp == NULL)
    {
      perror ("fdopen");
      return NULL;
    }
  for (int i = 0; i < n; ++i)
    {
      const struct gse_datum *d = chunk + i;
      fprintf (fp, template, d->x, d->y, d->z);
      fprintf (fp, "\n");
    }
  fclose (fp);
  close (fd);
  return name;
}

/* Sort INPUT_FILE.
   Returns the name of the sorted file. */
char *
merge_sort (const char *input_file, const char *template, int dimensions)
{
  const size_t CHUNKSIZE = 256;
  struct gse_datum *chunk = calloc (CHUNKSIZE, sizeof (*chunk));
  if (chunk == NULL)
    return NULL;

  FILE *fp = fopen (input_file, "r");
  if (!fp)
    {
      perror ("fopen");
      free (chunk);
      return NULL;
    }

  char **ff = NULL;
  char *lineptr = NULL;
  size_t linelen = 0;
  int i = 0;
  int c_idx = 0;
  for (;!feof (fp);)
    {
      int len = getline (&lineptr, &linelen, fp);
      if (len <= 0)
	continue;

      struct gse_datum d;
      if (dimensions != sscanf (lineptr, template, &d.x, &d.y, &d.z))
	{
	  continue;
	}
      chunk[i] = d;
      i++;
      if (i == CHUNKSIZE)
	{
	  ff = realloc (ff, sizeof (char *) * ++c_idx);
	  ff[c_idx-1] = save_chunk (chunk, i, template);
	}
      i = i%CHUNKSIZE;
    }
  if (fp)
    fclose (fp);

  free (lineptr);
  
  if (i > 0)
    {
      ff = realloc (ff, sizeof (char *) * ++c_idx);
      ff[c_idx-1] = save_chunk (chunk, i, template);
    }

  int n = c_idx;
  while (n > 1)
    {
      if (! merge_step (&ff, &n, template, dimensions))
	{
	  printf ("Merge failed\n");
	  break;
	}
    }

  char *retval = ff[0];
  
  free (ff);

  return retval;
}

