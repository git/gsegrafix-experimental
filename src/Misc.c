/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* Misc.c
*
* Miscellaneous functions.
*
* Contains functions:
*    xmalloc
*    min
*    max
*    roundl
*    dot
*    cross
*    multiply_mv
*    multiply_mm
*    interp2
*    interp_rect
*    interp_color_1
*    interp_color_2
*    find_indices
*    put_pixel
*
* Rotation matrices:
*
*    Rx = [1           0          0
*          0  cos(theta) sin(theta)
*          0 -sin(theta) cos(theta)]
*
*    Ry = [cos(theta) 0 -sin(theta)
*                   0 1           0
*          sin(theta) 0  cos(theta)]
*
*    Rz = [ cos(theta) sin(theta) 0
*          -sin(theta) cos(theta) 0
*                    0          0 1]
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "Misc.h"
#include <ctype.h>
#include <math.h>
#include "gsegraf.h"

#include <stdlib.h> // for malloc
#include <string.h>

void *
xmalloc ( size_t size )
{
  if (size == 0)
    return NULL;
  void *ptr = malloc(size);
  if ( ptr == NULL )
    {
      fprintf(stderr, "\ngsegraf out of memory.\n\n");
      abort ();
    }

  return ptr;
}

void *
xzalloc (size_t size)
{
  void *ptr = xmalloc (size);

  memset (ptr, 0, size);
  
  return ptr;
}


double min ( int n, const double *x)
   {
   int i;
   double xmin;

   xmin = x[0];
   for ( i=2; i<=n; i++ )
      if ( x[i-1] < xmin )
         xmin = x[i-1];

   return xmin;
   }


double max ( int n, const double *x)
   {
   int i;
   double xmax;

   xmax = x[0];
   for ( i=2; i<=n; i++ )
      if ( x[i-1] > xmax )
         xmax = x[i-1];

   return xmax;
   }

void
multiply_mv (const double *matrix, double *vector)
{
  /* Product of matrix and vector */
  /* Matrix elements labeled: [ 0 1 2; 3 4 5; 6 7 8 ] */

  double vector_out[3];
  
  vector_out[0] = matrix[0]*vector[0] + matrix[1]*vector[1] + matrix[2]*vector[2];
  vector_out[1] = matrix[3]*vector[0] + matrix[4]*vector[1] + matrix[5]*vector[2];
  vector_out[2] = matrix[6]*vector[0] + matrix[7]*vector[1] + matrix[8]*vector[2];

  memcpy (vector, vector_out, sizeof (double) * 3);
}


/* Premultiply matrix2 by matrix 1 */
void
multiply_mm (const double *matrix1, double *matrix2)
{
  /* Matrix elements labeled: [ 0 1 2; 3 4 5; 6 7 8 ] */

  double matrix_out[9];
  
  matrix_out[0] = matrix1[0]*matrix2[0] + matrix1[1]*matrix2[3] + matrix1[2]*matrix2[6];
  matrix_out[1] = matrix1[0]*matrix2[1] + matrix1[1]*matrix2[4] + matrix1[2]*matrix2[7];
  matrix_out[2] = matrix1[0]*matrix2[2] + matrix1[1]*matrix2[5] + matrix1[2]*matrix2[8];

  matrix_out[3] = matrix1[3]*matrix2[0] + matrix1[4]*matrix2[3] + matrix1[5]*matrix2[6];
  matrix_out[4] = matrix1[3]*matrix2[1] + matrix1[4]*matrix2[4] + matrix1[5]*matrix2[7];
  matrix_out[5] = matrix1[3]*matrix2[2] + matrix1[4]*matrix2[5] + matrix1[5]*matrix2[8];

  matrix_out[6] = matrix1[6]*matrix2[0] + matrix1[7]*matrix2[3] + matrix1[8]*matrix2[6];
  matrix_out[7] = matrix1[6]*matrix2[1] + matrix1[7]*matrix2[4] + matrix1[8]*matrix2[7];
  matrix_out[8] = matrix1[6]*matrix2[2] + matrix1[7]*matrix2[5] + matrix1[8]*matrix2[8];

  memcpy (matrix2, matrix_out, sizeof (double) * 9);
}



void
interp2 ( int nx, int ny, int ni, double *x, double *y, double *z, double *xi, double *yi, double *zi )
   {
   /********************************************************************************
   *
   * Two-dimensional linear interpolation 
   *
   * Inputs
   *    nx: number of values in x vector
   *    ny: number of values in y vector
   *    x:  vector of x values
   *    y:  vector of y values
   *    z:  matrix of z values (nx rows, ny columns)
   *    xi: vector of interpolated values of x
   *    yi: vector of interpolated values of y
   *
   * Output
   *    zi: vector of interpolated values of z
   *
   * Elements of x and y must be monotonically increasing or decreasing but need
   * not be equally spaced.
   * Number of elements of xi must be equal to number of elements of yi.
   * Elements of xi and yi must be included in the ranges of x and y, respectively.
   *
   ********************************************************************************/

   int i, i1, i2, j1, j2;
   double z11, z12, z21, z22, dx, dy, dxi, dyi, dxidx, dyidy, one_dxidx, one_dyidy,
          f11, f12, f21, f22;

   for ( i=1; i<=ni; i++ )
      {
      /* Find indices of x bracketing xi */
      if ( x[0] < x[nx-1] )
         {
         /* Elements of x increasing */
         if ( xi[i-1] <= x[0] )
            i1 = 0;
         else if ( xi[i-1] >= x[nx-1] )
            i1 = nx - 2;
         else
	   find_indices(0, nx-1, x, xi[i-1], &i1, NULL);
         i2 = i1 + 1;
         }
      else
         {
         /* Elements of x decreasing */
         if ( xi[i-1] >= x[0] )
            i1 = 0;
         else if ( xi[i-1] <= x[nx-1] )
            i1 = nx - 2;
         else
	   find_indices(0, nx-1, x, xi[i-1], &i1, NULL);
         i2 = i1 + 1;
         }

      /* Find indices of y bracketing yi */
      if ( y[0] < y[ny-1] )
         {
         /* Elements of y increasing */
         if ( yi[i-1] <= y[0] )
            j1 = 0;
         else if ( yi[i-1] >= y[ny-1] )
            j1 = ny - 2;
         else
	   find_indices(0, ny-1, y, yi[i-1], &j1, NULL);
         j2 = j1 + 1;
         }
      else
         {
         /* Elements of y decreasing */
         if ( yi[i-1] >= y[0] )
            j1 = 0;
         else if ( yi[i-1] <= y[ny-1] )
            j1 = ny - 2;
         else
	   find_indices (0, ny-1, y, yi[i-1], &j1, NULL);
         j2 = j1 + 1;
         }

      /* Get values of z bracketing solution */
      z11 = z[ny*i1+j1];
      z12 = z[ny*i1+j2];
      z21 = z[ny*i2+j1];
      z22 = z[ny*i2+j2];

      /* Calculate weighting factors for each value of z */
      dxi = x[i2] - xi[i-1];
      dyi = y[j2] - yi[i-1];
      dx  = x[i2] - x[i1];
      dy  = y[j2] - y[j1];
      dxidx = dxi/dx;
      dyidy = dyi/dy;
      one_dxidx = 1.0 - dxidx;
      one_dyidy = 1.0 - dyidy;
      f11 = dxidx*dyidy;
      f12 = dxidx*one_dyidy;
      f21 = one_dxidx*dyidy;
      f22 = one_dxidx*one_dyidy;

      /* Calculate interpolated value of z */
      zi[i-1] = f11*z11 + f12*z12 + f21*z21 + f22*z22;
      }

   return;
   }


double interp_rect ( double x1, double x2, double y1, double y2, double xi )
   {
   /* Interpolate to find where a contour line intersects side of a rectangle */

   double f1, f2, yi;

   f1 = (x2 - xi)/(x2 - x1);
   f2 = 1.0 - f1;
   yi = f1*y1 + f2*y2;

   return yi;
   }


guint32 interp_color_1 (const struct gse_ctx *context, double fraction )
   {
   /********************************************************************************
   *
   * Interpolate colors from opaque blue (0x0000FFFF, fraction = 0.0)
   * to opaque red (0xFF0000FF, fraction = 1.0)
   *
   ********************************************************************************/

   int index;
   guint32 color;

   index = roundl(fraction*(context->n_color_spectrum_1 - 1));
   color = context->color_spectrum_1[index];

   return color;
   }


guint32 interp_color_2 (const struct gse_ctx *context, double fraction )
   {
   /********************************************************************************
   *
   * Interpolate colors from opaque dark blue (0x0000C0FF, fraction = 0.0)
   * to opaque dark red (0xC00000FF, fraction = 1.0)
   *
   ********************************************************************************/

   int index;
   guint32 color;

   index = roundl(fraction*(context->n_color_spectrum_2 - 1));
   color = context->color_spectrum_2[index];

   return color;
   }


void
find_indices (int index1, int index2, const double *x, double target,
	      int *out1, int *out2)
{
  /********************************************************************************
   *
   * Finds indices of values of monotonically increasing or decreasing x array
   * that bracket xi.
   *
   * Inputs
   *    index1: lowest index of x array to consider
   *    index2: highest index of x array to consider
   *    x:      vector of x values
   *    xi:     value to be bracketed
   *
   * Output
   *    out1:     lower index of x array that brackets xi
   *    out2:     upper index of x array that brackets xi
   *
   ********************************************************************************/

  *out1 = index1;
  //  *out2 = index2; UNUSED for all our callers!
  if (index2 - index1 < 2)
    return;

  if (x[index1+1] >= target && x[index2-1] <= target)
    return;

  int cutpoint = (index2 + index1)/2;

  if (x[cutpoint] > target)
    find_indices (index1, cutpoint, x, target, out1, out2);
  else
    find_indices (cutpoint, index2, x, target, out1, out2);
}

void
put_pixel ( GdkPixbuf *pixbuf, int i, int j, guint32 color )
{
   /* Add pixel of specified color to pixbuf */

   guchar *pixels     = gdk_pixbuf_get_pixels(pixbuf);  /* pointer to pixel data of pixbuf */
   int n_channels = gdk_pixbuf_get_n_channels(pixbuf);  /* samples per pixel; n_channels = 4 */
   int rowstride  = gdk_pixbuf_get_rowstride(pixbuf);   /* bytes between start of one row and start of next row */

   guchar red   = color >> 24;
   guchar green = color >> 16;
   guchar blue  = color >> 8;
   guchar alpha = color ;

   guchar *p = pixels + i*n_channels + j*rowstride;
   p[0] = red;
   p[1] = green;
   p[2] = blue;
   p[3] = alpha;
}




