/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* DrawLegend.c
*
* Contains functions:
*    DrawLegend
*
* Draws legend.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "DrawLegend.h"
#include "DrawText.h"
#include "Misc.h"
#include "GetWindowCoords.h"
#include "DrawSymbols.h"
#include "ReadParamFile.h"

#include "gse-cairo.h"
#include "gsegraf.h"

#include <string.h>
#include <stdlib.h>

static void
draw_legend_symbol (struct target *target, const struct gse_ctx *context, const struct plot_parameters *the_plot,
      double yoffset,
      double xanchor_symbol, double yanchor_symbol)
{
  int i, index;
  double x, y, x1, y1, x2, y2;
  double dy_bar[] = { 0.0, 6.0, 0.0 };
  char *pchar;

   if ( the_plot->plot_type == PLOT_points ||
       the_plot->plot_type == PLOT_histogram )
    {
      if (the_plot->texture != NONE)
	{
	  GseCairoPoints *points = gse_cairo_points_new(2);
	  points->coords[0] = xanchor_symbol;
	  points->coords[1] = yanchor_symbol + yoffset;
	  points->coords[2] = xanchor_symbol + 60.0;
	  points->coords[3] = yanchor_symbol + yoffset;
	  DrawLine (target, context, points, the_plot->fill_colors_rgba, the_plot->stylesizes, the_plot->texture);
	  gse_cairo_points_unref(points);
	}

      /* Draw symbols in symbol_string1 ("cCtTsSiIpPhH") */
      else if ( (pchar = strchr(symbol_string1, the_plot->stylechar1)) != NULL )
	{
	  int ifunc = pchar - symbol_string1;
	  for ( i=1; i<=3; i++ )
	    {
	      x = xanchor_symbol + 10.0 + (i-1)*20.0;
	      y = yanchor_symbol + yoffset;
	      context->symbol_func1[ifunc](target, x, y, the_plot->fill_colors_rgba, the_plot->outline_colors_rgba,
					   the_plot->stylesizes);
	    }
	}

      /* Draw symbols in symbol_string2 ("+xra") */
      else if ( (pchar = strchr(symbol_string2, the_plot->stylechar1)) != NULL )
	{
	  int ifunc = pchar - symbol_string2;
	  for ( i=1; i<=3; i++ )
	    {
	      x = xanchor_symbol + 10.0 + (i-1)*20.0;
	      y = yanchor_symbol + yoffset;
	      context->symbol_func2[ifunc](target, x, y, the_plot->fill_colors_rgba, the_plot->outline_colors_rgba,
					   the_plot->stylesizes);
	    }
	}

      /* Draw bars */
      else if ( the_plot->stylechar1 == 'b' || the_plot->stylechar1 == 'B' )
	{
	  for ( i=1; i<=3; i++ )
	    {
	      x = xanchor_symbol + 10.0 + (i-1)*20.0;
	      y = yanchor_symbol + yoffset;

	      x1 = x - 10.0;
	      x2 = x + 10.0;
	      y1 = y + 6.0;
	      y2 = y - dy_bar[i-1];

	      DrawBar (target, x1+1.0, y1, x2-1.0, y2, the_plot->fill_colors_rgba, the_plot->outline_colors_rgba);
	      DrawBar (target, x1, y1+1.0, x2, y2-1.0, 0xFFFFFF00, context->canvas_bg_color);
	    }
	}
    }

  else if ( the_plot->plot_type == PLOT_mesh )
    {
      x1 = xanchor_symbol;
      y1 = yanchor_symbol + yoffset;
      x2 = xanchor_symbol + 60.0;
      y2 = yanchor_symbol + yoffset;

      if (the_plot->styleflags == STYLE_CHAR_CHAR ||
	  the_plot->styleflags == STYLE_CHAR_HEX)
	{
	  if ( (pchar = strchr(color_string, the_plot->stylechar1)) != NULL )
	    {
	      index = pchar - &color_string[0];
	      DrawMesh (target, x1, y1, x2, y2, context->color_rgba[index], the_plot->meshcolors, the_plot->styleflags);
	    }
	}
      else if ( the_plot->styleflags == STYLE_HEX_CHAR || the_plot->styleflags == STYLE_HEX_HEX )
	DrawMesh(target, x1, y1, x2, y2, the_plot->stylecolor1, the_plot->meshcolors, the_plot->styleflags);

      else if ( the_plot->styleflags == STYLE_AUTO )
	DrawMesh(target, x1, y1, x2, y2, the_plot->alphacolor, the_plot->meshcolors, the_plot->styleflags);
    }

  else if ( the_plot->plot_type == PLOT_contour )
    {
      x1 = xanchor_symbol;
      y1 = yanchor_symbol + yoffset;
      x2 = xanchor_symbol + 60.0;
      y2 = yanchor_symbol + yoffset;

      if ( the_plot->styleflags == STYLE_CHAR )
	{
	  /* 2d contour plot */
	  if ( (pchar = strchr(color_string, the_plot->stylechar1)) != NULL )
	    {
	      index = pchar - &color_string[0];
	      DrawContour (target, context,  x1, y1, x2, y2, context->color_rgba[index], 0xFFFFFF00, the_plot->styleflags);
	    }
	}

      else if ( the_plot->styleflags == STYLE_HEX )
	{
	  /* 2d contour plot */
	  DrawContour (target, context,  x1, y1, x2, y2, the_plot->stylecolor1, 0xFFFFFF00, the_plot->styleflags);
	}

      else if ( the_plot->styleflags == STYLE_AUTO )
	{
	  /* 2d contour plot */
	  DrawContour (target, context,  x1, y1, x2, y2, context->color_rgba[3], 0xFFFFFF00, the_plot->styleflags);
	}

      else if ( the_plot->styleflags == STYLE_CHAR_CHAR || the_plot->styleflags == STYLE_CHAR_HEX )
	{
	  /* 3d contour plot */
	  if ( (pchar = strchr(color_string, the_plot->stylechar1)) != NULL )
	    {
	      index = pchar - &color_string[0];
	      DrawContour (target, context,  x1, y1, x2, y2, context->color_rgba[index], the_plot->contourcolors, the_plot->styleflags);
	    }
	}

      else if ( the_plot->styleflags == STYLE_HEX_CHAR || the_plot->styleflags == STYLE_HEX_HEX )
	{
	  /* 3d contour plot */
	  DrawContour (target, context,  x1, y1, x2, y2, the_plot->stylecolor1, the_plot->contourcolors, the_plot->styleflags);
	}
    }


  else if ( the_plot->plot_type == PLOT_color )
    {
      x1 = xanchor_symbol;
      y1 = yanchor_symbol + yoffset;
      x2 = xanchor_symbol + 60.0;
      y2 = yanchor_symbol + yoffset;
      DrawColorPlot (target, context, x1, y1, x2, y2);                                            /* 2d color plot */
    }
}

void
DrawLegend (struct target *target, const struct gse_ctx *context)
{
  if (context->legend == NULL)
    return;
  
   double dx_text, dx_symbol, dy_symbol;
   double plot_coords[3], window_coords[2];
   double xanchor_text, yanchor_text;
   double xanchor, yanchor;
   int iplot;
   
   PangoRectangle rect;
   PangoLayout *layout =
     gse_cairo_create_layout (target->cr, context->legend->legend_str, -1, context->legend->font,
			      &rect);

   double width_text = rect.width / PANGO_SCALE;
   double height_text = rect.height / PANGO_SCALE;
   double width_legend = 70.0 + width_text;
   double height_legend = height_text;

   cairo_rectangle_t box;
   get_box (target, context, &box);

   switch (context->legend->anchor_text)
     {
     case GSE_ANCHOR_CENTER:
     case GSE_ANCHOR_NORTH:
     case GSE_ANCHOR_SOUTH:
       dx_text = (width_legend - width_text)/2.0;
       dx_symbol = -width_legend/2.0;
       break;

     case GSE_ANCHOR_WEST:
     case GSE_ANCHOR_NORTH_WEST:
     case GSE_ANCHOR_SOUTH_WEST:
       dx_text = width_legend - width_text;
       dx_symbol = 0.0;
       break;

     case GSE_ANCHOR_EAST:
     case GSE_ANCHOR_NORTH_EAST:
     case GSE_ANCHOR_SOUTH_EAST:
       dx_text = 0.0;
       dx_symbol = -width_legend;
       break;
     default:
       g_assert_not_reached ();
     }

   switch (context->legend->anchor_text)
     {
     case GSE_ANCHOR_CENTER:
     case GSE_ANCHOR_EAST:
     case GSE_ANCHOR_WEST:
       dy_symbol = -height_legend/2.0;
       break;

     case GSE_ANCHOR_SOUTH:
     case GSE_ANCHOR_SOUTH_EAST:
     case GSE_ANCHOR_SOUTH_WEST:
       dy_symbol = -height_legend;
       break;

     case GSE_ANCHOR_NORTH:
     case GSE_ANCHOR_NORTH_EAST:
     case GSE_ANCHOR_NORTH_WEST:
       dy_symbol = 0.0;
       break;
     default:
       g_assert_not_reached ();
     }
   
   if (context->legend->coords_ref == GSE_COORDS_ABS)
      {
	plot_coords[0] = context->legend->xlegend;
	plot_coords[1] = context->legend->ylegend;
	plot_coords[2] = context->legend->zlegend;
	GetWindowCoords (target, context, plot_coords, window_coords);
	xanchor = window_coords[0];
	yanchor = window_coords[1];
      }
   else if (context->legend->coords_ref == GSE_COORDS_REL)
     {
       xanchor = (1.0 - context->legend->xlegend)*box.x + context->legend->xlegend*(box.x + box.width);
       yanchor = (1.0 - context->legend->ylegend)*(box.y + box.height) + context->legend->ylegend*box.y;
      }

   /* Check legend is within plot box for absolute coordinates */
   if (context->legend->coords_ref == GSE_COORDS_ABS &&
        (xanchor < 0 || yanchor < 0) )
     return;

   xanchor_text = xanchor + dx_text;
   yanchor_text = yanchor;

   g_object_unref (layout);

   background_text (target, context, context->legend->legend_str,
		    xanchor_text, yanchor_text,
		    context->legend->anchor_text);

   /* Specify legend symbol coordinate parameters */
   double xanchor_symbol = xanchor + dx_symbol;
   double yanchor_symbol = yanchor + dy_symbol;
   double yinc1 = height_legend/context->legend->nlines;
   double yinc2 = yinc1/2.0;

   for ( iplot=1; iplot <= context->plot_param.nplots; iplot++ )
      {
	const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];
	draw_legend_symbol (target, context, the_plot,
	      yinc1*(iplot - 1) + yinc2,
	      xanchor_symbol, yanchor_symbol);
      }
}
