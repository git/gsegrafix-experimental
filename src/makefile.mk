################################################################################
##
## Source-directory Makefile.am
## Processing Makefile.am with automake produces Makefile.in
##
## Copyright 2017 John Darrington <john@darrington.wattle.id.au>
## Copyright © 2008, 2009, 2010 Spencer A. Buckner
## http://savannah.gnu.org/projects/gsegrafix
##
## This file is part of GSEGrafix, a scientific and engineering plotting program.
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
##
################################################################################

noinst_PROGRAMS = gsetest merge-sort

bin_PROGRAMS = gse


lib_LTLIBRARIES = libgsegrafix.la

libgsegrafix_la_CFLAGS =  $(PANGO_CAIRO_CFLAGS) $(PANGO_CFLAGS) $(GLIB_CFLAGS) $(GDK_PIXBUF_CFLAGS) $(GDK_CFLAGS) \
	-I $(top_srcdir)/src


include_HEADERS = src/gsegrafix.h


# src/data.h \
# src/DrawGraph.h \
# src/InitializePlot.h \
# src/gsegraf.h



libgsegrafix_la_SOURCES = \
src/data-iterator-private.h \
src/data.h \
src/merge-sort.h \
src/plot-iterator.c \
src/data-iterator.h \
src/file-iterator.c \
src/memory-iterator.c \
src/memory-iterator.h \
src/AutoScale.c \
src/AutoScale.h \
src/AxesEqual.c \
src/AxesEqual.h \
src/AxisLimits.c \
src/AxisLimits.h \
src/CheckParamData.c \
src/CheckParamData.h \
src/Clip.c \
src/Clip.h \
src/ColorPlot2d.c \
src/ColorPlot2d.h \
src/ColorPlot3d.c \
src/ColorPlot3d.h \
src/ContourPlot2d.c \
src/ContourPlot2d.h \
src/DataMinMax.c \
src/DataMinMax.h \
src/DrawAxisLabels.c \
src/DrawAxisLabels.h \
src/DrawBackgroundImage.c \
src/DrawBackgroundImage.h \
src/DrawContours3d.c \
src/DrawContours3d.h \
src/DrawDateTime.c \
src/DrawDateTime.h \
src/DrawGraph.c \
src/DrawGrid.c \
src/DrawGrid.h \
src/DrawGrid2d.c \
src/DrawGrid2d.h \
src/DrawGrid3d.c \
src/DrawGrid3d.h \
src/DrawImage.c \
src/DrawImage.h \
src/DrawLabels3d.c \
src/DrawLabels3d.h \
src/DrawLegend.c \
src/DrawLegend.h \
src/DrawLines.c \
src/DrawLines.h \
src/DrawSymbols.c \
src/DrawSymbols.h \
src/DrawText.c \
src/DrawText.h \
src/DrawTickLabels.c \
src/DrawTickLabels.h \
src/DrawTickLabels2d.c \
src/DrawTickLabels2d.h \
src/DrawTickMarks.c \
src/DrawTickMarks.h \
src/DrawTickMarks3d.c \
src/DrawTickMarks3d.h \
src/FileRead.c \
src/FileRead.h \
src/GetWindowCoords.c \
src/GetWindowCoords.h \
src/Histogram.c \
src/Histogram.h \
src/Initialize3d.c \
src/Initialize3d.h \
src/InitializePlot.c \
src/InitializePlot.h \
src/Misc.c \
src/Misc.h \
src/PlotData2d.c \
src/PlotData2d.h \
src/PlotData3d.c \
src/PlotData3d.h \
src/PlotEllipses.c \
src/PlotEllipses.h \
src/PlotInterp3d.c \
src/PlotInterp3d.h \
src/PlotLines.c \
src/PlotLines.h \
src/PlotNormal3d.c \
src/PlotNormal3d.h \
src/PlotPoints3d.c \
src/PlotPoints3d.h \
src/PlotRectangles.c \
src/PlotRectangles.h \
src/PlotSymbols.c \
src/PlotSymbols.h \
src/PolarPlot.c \
src/PolarPlot.h \
src/ReadParamFile.c \
src/ReadParamFile.h \
src/SurfacePlot.c \
src/SurfacePlot.h \
src/ZoomIn.c \
src/ZoomIn.h \
src/ZoomOut.c \
src/ZoomOut.h \
src/gse-cairo.c \
src/gse-cairo.h \
src/gse-context.c \
src/merge-sort.c \
src/merge-sort.h \
src/gsegraf.h


# Tests
merge_sort_LDADD = libgsegrafix.la

merge_sort_CFLAGS = -I $(top_srcdir)/src

merge_sort_LDFLAGS = $(GTK_LIBS) -lm 

merge_sort_SOURCES = src/test-merge-sort.c



# New simple application
gsetest_LDADD = libgsegrafix.la

gsetest_CFLAGS = $(GTK_CFLAGS) \
	-I $(top_srcdir)/src

gsetest_LDFLAGS = $(GTK_LIBS) -lm 

gsetest_SOURCES = src/gsetest.c


# Complicated Interactive Application
gse_LDADD = libgsegrafix.la

gse_CFLAGS = $(GTK_CFLAGS) \
	-I $(top_srcdir)/src

gse_LDFLAGS = $(GTK_LIBS) -lm 

gse_SOURCES = src/gse.c

gsegrafix.pc: gsegrafix.pc.in
	$(SED) -e 's/%glib_dep%/$(GLIB_MODULES)/' \
	-e 's/%gdk_dep%/$(GDK_MODULES)/' \
	-e 's/%gdk_pixbuf_dep%/$(GDK_PIXBUF_MODULES)/' \
	-e 's/%pango_cairo_dep%/$(PANGO_MODULES)/' \
	-e 's|%prefix%|$(prefix)|' $< > $@

install-exec-local: gsegrafix.pc
	$(MKDIR_P) $(DESTDIR)$(libdir)
	chmod u+w $(DESTDIR)$(libdir)
	$(INSTALL) -D $< $(DESTDIR)$(libdir)/pkgconfig/gsegrafix.pc

uninstall-local:
	$(RM) $(DESTDIR)$(libdir)/pkgconfig/gsegrafix.pc

EXTRA_DIST+=gsegrafix.pc.in

CLEANFILES = gsegrafix.pc

