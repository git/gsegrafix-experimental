/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* CheckParamData.c
*
* Checks plot-parameter data read from plot-parameter file.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "CheckParamData.h"

#include "InitializePlot.h"

#include "Misc.h"

#include <ctype.h>
#include "gsegraf.h"

#include <errno.h>
#include <string.h>
#include <stdlib.h> // for free

#define _(X) X

gboolean
CheckParamData (struct gse_ctx *context, struct data_parser *parser, GError **err)
   {
   /* Declare variables */
   int iplot, nplots, iformat, index_formats,
       i, j, k;
   char *pchar;
   const char *error_str[] =
      { "Window size greater than screen size.",
        "Invalid or missing axis_type parameter.",
        "Invalid or missing plot_type parameter.",
        "Only points, histogram, contour, and color plot_type's\ncompatible with linear axis_type.",
        "Only points plot_type compatible with semilogx axis_type.",
        "Only points plot_type compatible with semilogy axis_type.",
        "Only points plot_type compatible with loglog axis_type.",
        "Only points plot_type compatible with polar axis_type.",
        "Only points, mesh, contour, and color plot_type's\ncompatible with 3d axis_type.",
        "Invalid or missing plot_style parameter.",
        "Invalid axis_scale parameter.",
        "Invalid grid parameter.",
        "Invalid bin_value parameter.",
        "Invalid bin_ref parameter.",
        "View-direction elevation angle out of range.",
        "Invalid date_time parameter.",
        "Invalid plot_box parameter.",
        "Invalid x_tick_marks parameter.",
        "Invalid y_tick_marks parameter.",
        "Invalid z_tick_marks parameter.",
        "Invalid x_tick_labels parameter.",
        "Invalid y_tick_labels parameter.",
        "Invalid z_tick_labels parameter.",
        "Invalid or missing data format;\nformat must specify double data type;\none data column read for histogram plot type.",
        "Invalid or missing data format;\nformat must specify double data type;\ntwo data columns read for 2d points plot type.",
        "Invalid or missing data format;\nformat must specify double data type;\nthree data columns read for 3d points plot type."};
   FILE *fptr;


   /* Get number of plots */
   nplots = context->plot_param.nplots;


   /* Check data filenames */
   for ( iplot=1; iplot<=nplots; iplot++ )
      {
	const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];

      if ( (fptr = fopen(the_plot->filename, "r")) == NULL )
         {
	   const char *why = strerror (errno);
	   g_set_error (err, parser->domain, 0,
			_("Cannot open data file \"%s\": %s"),
			the_plot->filename, why);
	   return FALSE;
         }
      else
         {
         fclose(fptr);
         }
      }


   /* Check axis_type parameter */
   if ( context->plot_param.axes_type != AXES_linear &&
        context->plot_param.axes_type != AXES_semilogx &&
        context->plot_param.axes_type != AXES_semilogy &&
        context->plot_param.axes_type != AXES_loglog &&
        context->plot_param.axes_type != AXES_polar &&
        context->plot_param.axes_type != AXES_3d )
      {
      g_set_error_literal (err, parser->domain, 0, error_str[1]);
      return FALSE;
      }


   /* Check plot_type parameters */
   for ( iplot=1; iplot<=nplots; iplot++ )
      {
	const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];


      if ( the_plot->plot_type != PLOT_points &&
           the_plot->plot_type != PLOT_histogram &&
           the_plot->plot_type != PLOT_mesh &&
           the_plot->plot_type != PLOT_contour &&
           the_plot->plot_type != PLOT_color )
         {
         g_set_error_literal (err, parser->domain, 0, error_str[2]);
         return FALSE;
         }
      }


   /* Check compatibility of axis_type and plot_type parameters */
   for ( iplot=1; iplot<=nplots; iplot++ )
      {
		   const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];

      if ( context->plot_param.axes_type == AXES_linear &&
           the_plot->plot_type != PLOT_points &&
           the_plot->plot_type != PLOT_histogram &&
           the_plot->plot_type != PLOT_contour &&
           the_plot->plot_type != PLOT_color )
         {
         g_set_error_literal (err, parser->domain, 0, error_str[3]);
         return FALSE;
         }

      else if ( context->plot_param.axes_type == AXES_semilogx &&
                the_plot->plot_type != PLOT_points )
         {
         g_set_error_literal (err, parser->domain, 0, error_str[4]);
         return FALSE;
         }

      else if ( context->plot_param.axes_type == AXES_semilogy &&
                the_plot->plot_type != PLOT_points )
         {
         g_set_error_literal (err, parser->domain, 0, error_str[5]);
         return FALSE;
         }

      else if ( context->plot_param.axes_type == AXES_loglog &&
                the_plot->plot_type != PLOT_points )
         {
         g_set_error_literal (err, parser->domain, 0, error_str[6]);
         return FALSE;
         }

      else if ( context->plot_param.axes_type == AXES_polar &&
                the_plot->plot_type != PLOT_points )
         {
         g_set_error_literal (err, parser->domain, 0, error_str[7]);
         return FALSE;
         }

      else if ( context->plot_param.axes_type == AXES_3d &&
                the_plot->plot_type != PLOT_points &&
                the_plot->plot_type != PLOT_mesh &&
                the_plot->plot_type != PLOT_contour &&
                the_plot->plot_type != PLOT_color )
         {
         g_set_error_literal (err, parser->domain, 0, error_str[8]);
         return FALSE;
         }
      }


   /* Check compatibility of plot_type and plot_style parameters */
   for ( iplot=1; iplot<=nplots; iplot++ )
      {
		   const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];

      if ( the_plot->plot_type == PLOT_points )
         {
         if ( the_plot->styleflags != STYLE_CHAR_CHAR &&
              the_plot->styleflags != STYLE_CHAR_HEX )
            {
            g_set_error_literal (err, parser->domain, 0, error_str[9]);
            return FALSE;
            }
         }

      else if ( the_plot->plot_type == PLOT_histogram )
         {
         if ( the_plot->styleflags != STYLE_CHAR_CHAR &&
              the_plot->styleflags != STYLE_CHAR_HEX )
            {
            g_set_error_literal (err, parser->domain, 0, error_str[9]);
            return FALSE;
            }
         }

      else if ( the_plot->plot_type == PLOT_contour &&
                context->plot_param.axes_type == AXES_linear )
         {
         if ( the_plot->styleflags != STYLE_CHAR &&
              the_plot->styleflags != STYLE_HEX &&
              the_plot->styleflags != STYLE_AUTO )
            {
            g_set_error_literal (err, parser->domain, 0, error_str[9]);
            return FALSE;
            }
         }

      else if ( the_plot->plot_type == PLOT_color &&
                context->plot_param.axes_type == AXES_linear )
         {
         if ( the_plot->styleflags != STYLE_BILINEAR &&
              the_plot->styleflags != STYLE_NEAREST )
            {
            g_set_error_literal (err, parser->domain, 0, error_str[9]);
            return FALSE;
            }
         }

      else if ( the_plot->plot_type == PLOT_mesh )
         {
         if ( the_plot->styleflags != STYLE_CHAR_CHAR &&
              the_plot->styleflags != STYLE_CHAR_HEX &&
              the_plot->styleflags != STYLE_HEX_CHAR &&
              the_plot->styleflags != STYLE_HEX_HEX &&
              the_plot->styleflags != STYLE_AUTO )
            {
            g_set_error_literal (err, parser->domain, 0, error_str[9]);
            return FALSE;
            }
         }

      else if ( the_plot->plot_type == PLOT_contour &&
                context->plot_param.axes_type == AXES_3d )
         {
         if ( the_plot->styleflags != STYLE_CHAR_CHAR &&
              the_plot->styleflags != STYLE_CHAR_HEX &&
              the_plot->styleflags != STYLE_HEX_CHAR &&
              the_plot->styleflags != STYLE_HEX_HEX )
            {
            g_set_error_literal (err, parser->domain, 0, error_str[9]);
            return FALSE;
            }
         }

      else if ( the_plot->plot_type == PLOT_color &&
                context->plot_param.axes_type == AXES_3d )
         {
         if ( the_plot->styleflags != STYLE_AUTO )
            {
            g_set_error_literal (err, parser->domain, 0, error_str[9]);
            return FALSE;
            }
         }
      }


   /* Check context->stylechar1 data */
   for ( iplot=1; iplot<=nplots; iplot++ )
      {
	const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];

      if ( the_plot->plot_type == PLOT_points )
         {
         if ( (pchar = strchr(symbol_string, the_plot->stylechar1)) == NULL )
            {
	      g_set_error (err, parser->domain, 0,
			   _("Invalid or missing plot symbol for file \"%s\""),
			   the_plot->filename);
	      return FALSE;
            }
         }

      else if ( the_plot->plot_type == PLOT_histogram )
         {
         if ( (pchar = strchr("lbB", the_plot->stylechar1)) == NULL )
            {
	      g_set_error (err, parser->domain, 0,
			   _("Invalid or missing plot symbol for file \"%s\""),
			   the_plot->filename);
	      
	      return FALSE;
            }
         }

      else if ( the_plot->plot_type == PLOT_mesh )
         {
         if ( the_plot->styleflags == STYLE_CHAR_CHAR ||
              the_plot->styleflags == STYLE_CHAR_HEX )
            {
            if ( (pchar = strchr(color_string, the_plot->stylechar1)) == NULL )
               {
               g_set_error (err, parser->domain, 0,
			    _("Invalid or missing plot color for file \"%s\""),
			    the_plot->filename);
               return FALSE;
               }
            }
         }

      else if ( context->plot_param.axes_type == AXES_3d &&
                the_plot->plot_type == PLOT_contour )
         {
         if ( the_plot->styleflags == STYLE_CHAR_CHAR ||
              the_plot->styleflags == STYLE_CHAR_HEX )
            {
            if ( (pchar = strchr(color_string, the_plot->stylechar1)) == NULL )
               {
		 g_set_error (err, parser->domain, 0,
			      _("Invalid or missing plot color for file \"%s\""),
			      the_plot->filename);
		 return FALSE;
               }
            }
         }
      }


   /* Check context->stylechar2 data */
   for ( iplot=1; iplot<=nplots; iplot++ )
      {
	const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];

      if ( the_plot->plot_type == PLOT_points ||
           the_plot->plot_type == PLOT_histogram ||
           the_plot->plot_type == PLOT_mesh ||
           the_plot->plot_type == PLOT_contour )
         {
         if ( the_plot->styleflags == STYLE_CHAR_CHAR ||
              the_plot->styleflags == STYLE_HEX_CHAR )
            if ( (pchar = strchr(color_string, the_plot->stylechar2)) == NULL )
               {
               g_set_error (err, parser->domain, 0,
			    _("Invalid or missing plot color for file \"%s\""),
			    the_plot->filename);
	       return FALSE;
               }
         }
      }


   /* Check grid parameters */
   if ( strcmp(context->plot_param.grid, "on1") != 0 &&
        strcmp(context->plot_param.grid, "on2") != 0 &&
        strcmp(context->plot_param.grid, "off") != 0 )
      {
      g_set_error_literal (err, parser->domain, 0, error_str[11]);
      return FALSE;
      }

   if ( strcmp(context->plot_param.grid, "on1") == 0 )
      {
      if ( (context->gridchar1 != 'l' && context->gridchar1 != 'd' && context->gridchar1 != '.') ||
           (pchar = strchr(color_string, context->gridchar2)) == NULL )
         {
         g_set_error_literal (err, parser->domain, 0, error_str[11]);
         return FALSE;
         }
      }
   else if ( strcmp(context->plot_param.grid, "on2") == 0 )
      {
      if ( context->gridchar1 != 'l' && context->gridchar1 != 'd' )
         {
         g_set_error_literal (err, parser->domain, 0, error_str[11]);
         return FALSE;
         }
      }

   /* Check view-direction angles for 3d plots */
   if ( context->plot_param.axes_type == AXES_3d )
      if ( context->plot_param_3d.theta <  0.0 ||
           context->plot_param_3d.theta > 90.0 )
         {
         g_set_error_literal (err, parser->domain, 0, error_str[14]);
         return FALSE;
         }


   return TRUE;
   }
