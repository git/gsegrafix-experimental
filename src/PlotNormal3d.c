/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* PlotNormal3d.c
*
* Plots 3-dimensional mesh data for polygons that are not truncated.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "PlotNormal3d.h"
#include "Misc.h"

#include <math.h>
#include "gsegraf.h"


#include "gse-cairo.h"

void
PlotNormal3d (struct target *target, const struct gse_ctx *context, int iplot, 
	      double xmin, double ymin, double zmin, double zmax,
	      double xscale, double yscale, double zscale,
	      const double *origin, const double *Ryz, const guint32 *fill_color,
	      double *xpoints, double *ypoints, double *zpoints )
{
   const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];

   /* Declare variables */
   int i, ifill;
   guint32 color_polygon;
   double cross_prod, r[4][3], zmin_polygon, zmax_polygon, zavg_polygon, fraction;
   GseCairoPoints *points_polygon, *points_line;

   /* Specify polygon position vectors */
   for ( i=1; i<=4; i++ )
      {
      r[i-1][0] = (xpoints[i-1] - xmin)*xscale;
      r[i-1][1] = (ypoints[i-1] - ymin)*yscale;
      r[i-1][2] = (zpoints[i-1] - zmin)*zscale;
      }


   /* Rotate polygon position vectors */
   for ( i=1; i<=4; i++ )
      {
	multiply_mv  (Ryz, r[i-1]);
      }


   /* Specify points structure components */
   points_polygon = gse_cairo_points_new(4);
   for ( i=1; i<=4; i++ )
      {
      points_polygon->coords[2*i-2] = origin[1] + r[i-1][1];
      points_polygon->coords[2*i-1] = origin[2] - r[i-1][2];
      }


   /* Calculate line perpendicular to polygon */
   cross_prod = (r[2][1] - r[0][1])*(r[3][2] - r[1][2]) - (r[2][2] - r[0][2])*(r[3][1] - r[1][1]);


   /* Draw polygon */
   if ( the_plot->plot_type == PLOT_mesh )
      {
      if ( the_plot->styleflags == STYLE_AUTO )
         {
         zmin_polygon = zmax;
         zmax_polygon = zmin;
         for ( i=1; i<=4; i++ )
            {
            if ( zpoints[i-1] < zmin_polygon )
               zmin_polygon = zpoints[i-1];
            if ( zpoints[i-1] > zmax_polygon )
               zmax_polygon = zpoints[i-1];
            }
         zavg_polygon = (zmin_polygon + zmax_polygon)/2.0;

         fraction = (zavg_polygon - zmin)/(zmax - zmin);
         if ( cross_prod >= 0.0 )
	   color_polygon = interp_color_1(context, fraction);
         else
	   color_polygon = interp_color_2(context, fraction);
         color_polygon = color_polygon - 0xFF + the_plot->alphacolor;
	 gse_cairo_render_polygon (target->cr, points_polygon, 4,
				   1,
				   the_plot->meshcolors,
				   color_polygon);
         }
      else
         {
         if ( cross_prod >= 0.0 )
            ifill = 0;
         else
            ifill = 1;

	 gse_cairo_render_polygon (target->cr, points_polygon, 4,
				   1,
				   the_plot->meshcolors,
				   fill_color[ifill]);

         }
      }

   else if ( the_plot->plot_type == PLOT_contour )
      {
      if ( cross_prod >= 0.0 )
         ifill = 0;
      else
         ifill = 1;

      gse_cairo_render_polygon (target->cr, points_polygon, 4, 1,
				fill_color[ifill],
				fill_color[ifill]);
      
      points_line = gse_cairo_points_new(3);
      for ( i=1; i<=3; i++ )
         {
         points_line->coords[2*i-2] = origin[1] + r[i-1][1];
         points_line->coords[2*i-1] = origin[2] - r[i-1][2];
         }

      gse_cairo_render_line (target->cr, points_line, 3, 1, fill_color[ifill]);

      gse_cairo_points_unref(points_line);
      }


   /* Free points structure */
   gse_cairo_points_unref(points_polygon);

   return;
   }
