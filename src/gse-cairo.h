/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*
  This file is part of GSEGrafix, a scientific and engineering plotting 
  program.

  Copyright (C) 2017 John Darrington

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef GSE_CAIRO_H
#define GSE_CAIRO_H

#include <cairo.h>
#include <glib.h>

#include "gsegraf.h"

typedef struct
{
  int num_points;
  double *coords;
} GseCairoPoints;

GseCairoPoints *gse_cairo_points_new (int);
void gse_cairo_points_unref (GseCairoPoints *pts);


void gse_cairo_render_text_with_extents (cairo_t *cr, const char *text, int x, int y,
					 enum GseAnchorType anchor,
					 guint32 color,
					 const PangoFontDescription *font_desc,
					 gdouble angle,
					 double *x1, double *y1, double *x2, double *y2);

void gse_cairo_render_text (cairo_t *cr, const char *text, int x, int y,
			    enum GseAnchorType anchor,
			    guint32 color,
			    const PangoFontDescription *font_desc);

void gse_cairo_render_circle (cairo_t *cr,
			      double x, double y, double r,
			      gint linewidth,
			      guint32 outline_color, guint32 fill_color);

void gse_cairo_render_pixbuf (cairo_t *cr, GdkPixbuf *pixbuf,
			 double x, double y, enum GseAnchorType anchor);


void gse_cairo_render_ellipse (cairo_t *cr, double x, double y, double width, double height,
			  gint linewidth, 
			  guint32 outline_color, guint32 fill_color);

void gse_cairo_render_rectangle (cairo_t *cr, double x, double y,
				 double width, double height,
				 gint linewidth, 
				 guint32 outline_color, guint32 fill_color);


void gse_cairo_render_line (cairo_t *cr, GseCairoPoints *points, int n_points,
			    gint line_width,
			    guint32 color);

void gse_cairo_render_polygon (cairo_t *cr, GseCairoPoints *points, int n_points,
			       gint line_width,
			       guint32 outline_color, guint32 fill_color);


void gse_cairo_render_arrowhead (cairo_t *cr, double size,  guint32 fill_color);

PangoLayout * gse_cairo_create_layout (cairo_t *cr,
				       const char *text, int len,
				       const PangoFontDescription *font_desc,
				       PangoRectangle *rect);
				       

void gse_cairo_render_layout (cairo_t *cr, PangoLayout *layout,
			      const PangoRectangle *rect,
			      double x, double y,
			      enum GseAnchorType anchor,
			      guint32 color);


void get_shift_for_anchor (enum GseAnchorType anchor, double width, double height,
			   double *xshift, double *yshift);



/* 
   A simple wrapper around cairo_set_source_rgba.
   COLOR is a 32 bit value with the format 0xRRGGBBAA
 */
static inline void
gse_cairo_set_source_rgba (cairo_t *cr, guint32 color)
{
  cairo_set_source_rgba (cr,
			 ((color >> 24) &0xFF) / (double) 0xFF,
			 ((color >> 16) &0xFF) / (double) 0xFF,
			 ((color >> 8) & 0xFF) / (double) 0xFF,
			 ((color >> 0) & 0xFF) / (double) 0xFF);
}


#endif
