/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*
  This file is part of GSEGrafix, a scientific and engineering plotting 
  program.

  Copyright (C) 2017 John Darrington

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdbool.h>

#include "data-iterator.h"
#include "gsegraf.h"

#include "Misc.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>
#include <errno.h>

#include <math.h>

struct file_sample_iterator
{
  struct sample_iterator base;
  
  FILE *fp;
  struct gse_datum current_sample;

  char *filename;
  char *template;
  
  bool end_of_data;
  int dimensions;
  bool segmented;
  char *buffer;
  size_t bufsize;
  int line;
  int records;
  long bytes;
};

void
file_destroy (struct sample_iterator *it_)
{
  struct file_sample_iterator *it = (struct file_sample_iterator *) it_;

  free (it->filename);
  free (it->template);
  free (it->buffer);
  fclose (it->fp);
  free (it);
}

struct sample_iterator *
file_clone (const struct sample_iterator *in_)
{
  const struct file_sample_iterator *in = (const struct file_sample_iterator *) in_;
  
  if (in == NULL)
    return NULL;

  fpos_t pos;
  if (0 != fgetpos (in->fp, &pos))
    {
      perror ("Clone fgetpos");
      return NULL;
    }

  if (feof (in->fp))
    g_print ("EOF for source\n");

  int fd = fileno (in->fp);
  if (fd < 0)
    return NULL;
  
  struct file_sample_iterator *it = xzalloc (sizeof *it);

  memcpy (it, in, sizeof *it);

  it->buffer = NULL;
  it->bufsize = 0;

  it->fp = fopen (it->filename, "r");
  if (it->fp == NULL)
    {
      perror ("Cannot dup");
      free (it);
      return NULL;
    }

  if (0 != fsetpos (it->fp, &pos))
    {
      perror ("Cannot seek");
      free (it);
      return NULL;
    }
  
  return (struct sample_iterator *) it;
}


bool
file_last (const struct sample_iterator *it_)
{
  const struct file_sample_iterator *it = (const struct file_sample_iterator *) it_;

  return it->end_of_data || feof (it->fp);
}


void
file_next (struct sample_iterator *it_)
{
  struct file_sample_iterator *it = (struct file_sample_iterator *) it_;

  struct gse_datum d;

  int len = getline (&it->buffer, &it->bufsize, it->fp);
  if (len < 0)
    return;
  it->line++;
  it->records++;
  it->bytes += len;
  int n = sscanf (it->buffer, it->template, &d.x, &d.y, &d.z);
  if (n > 0)
    {
      double mean = it->bytes / (double) it->records;
      if (fabs (log10 (mean/(double)len)) > 0.2)
	{
	  g_warning ("%s:%d  Line length differs greatly from average (corrupt file?)\n",
		     it->filename, it->line);
	}
    }
  
  /* In the non-segmented case skip over the * * * line */
  if (n == 0 && ! feof (it->fp) && !it->segmented)
    {
      it->records = 0;
      it->bytes = 0;
      getline (&it->buffer, &it->bufsize, it->fp);
      it->line++;
      n = sscanf (it->buffer, it->template, &d.x, &d.y, &d.z);
    }

  if (n != it->dimensions)
    {
      if (n < it->dimensions && n > 0)
	{
	  g_warning ("%s:%d Expected %d values but only %d are present.",
		     it->filename, it->line, it->dimensions, n);
	}
      it->end_of_data = true;
    }
  else
    {
      it->current_sample = d;
    }
}

struct sample_iterator *
file_next_segment (struct sample_iterator *in_)
{
  struct file_sample_iterator *in = (struct file_sample_iterator *) in_;

  if (!in->segmented || feof (in->fp))
    {
      it_destroy (in_);
      return NULL;
    }
  
  struct file_sample_iterator *it = (struct file_sample_iterator*) file_clone (in_);
  it->end_of_data = false;
  it->records = 0;
  it->bytes = 0;

  it_next ((struct sample_iterator *) it);
  
  it_destroy (in_);

  return (struct sample_iterator *) it;
}

const struct gse_datum *
file_get (const struct sample_iterator *it_)
{
  const struct file_sample_iterator *it = (const struct file_sample_iterator *) it_;
  
  if (it_last (it_))
    return NULL;

  return &it->current_sample;
}

struct sample_iterator *
it_create_from_file (const char *filename, const char *template, int dimensions)
{
  struct file_sample_iterator *it = xzalloc (sizeof *it);
  it->base.clone = file_clone;
  it->base.next_segment = file_next_segment;
  it->base.destroy = file_destroy;
  it->base.last = file_last;
  it->base.next = file_next;
  it->base.get = file_get;

  it->dimensions = dimensions;
  it->end_of_data = false;
  it->segmented = false;
  it->filename = strdup (filename);
  it->template = strdup (template);

  it->fp = fopen (it->filename, "r");
  if (it->fp == NULL)
    return NULL;

  it_next ( (struct sample_iterator *) it);
  
  return (struct sample_iterator *) it;
}
