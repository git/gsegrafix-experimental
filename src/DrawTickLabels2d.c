/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* DrawTickLabels2d.c
*
* Draws tick-mark labels for linear axes of two-dimensional plots.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "DrawTickLabels2d.h"
#include "DrawTickLabels.h"
#include "DrawAxisLabels.h"
#include "InitializePlot.h"
#include <math.h>
#include "gsegraf.h"

void
DrawTickLabels2d (struct target *target, const struct gse_ctx *context, struct tick_label_widths *tlw)
{
  /* Check plot_box parameter */
  if (!context->plot_param.draw_box)
    return;

  cairo_rectangle_t box;
  get_box (target, context, &box);
  
  /* Draw x-axis tick-mark labels */
  if ( context->plot_param.x_tick_marks &&
       context->plot_param.x_tick_labels)

    {
      if (context->plot_param.axes_type == AXES_linear ||
	  context->plot_param.axes_type == AXES_semilogy)
	{
	  DrawTickLabels (target, context, "linear",
			  box.x, box.width, box.y + box.height, 0,
			  context->xtick_labels.nvalues, &context->xtick_labels.values[0],
			  context->xtick_labels.offset1, context->xtick_labels.offset2,
			  0.0, 8.0, GSE_ANCHOR_NORTH);
	}

      if (context->plot_param.axes_type == AXES_semilogx ||
	  context->plot_param.axes_type == AXES_loglog)
	{
	  DrawTickLabels (target, context, "log",
			  box.x, box.width, box.y + box.height, 0,
			  context->xtick_labels.nvalues, &context->xtick_labels.values[0],
			  context->xtick_labels.offset1, context->xtick_labels.offset2,
			  0.0, 8.0, GSE_ANCHOR_NORTH);
	}
    }

  /* Draw y-axis tick-mark labels */
  if ( context->plot_param.y_tick_marks &&
       context->plot_param.y_tick_labels)
    {
      if (context->plot_param.axes_type == AXES_linear ||
	  context->plot_param.axes_type == AXES_semilogx)
	{
	  tlw->width_ytick_labels =
	    DrawTickLabels (target, context, "linear",
			    box.x, 0, box.y + box.height, -box.height,
			    context->ytick_labels.nvalues, &context->ytick_labels.values[0],
			    context->ytick_labels.offset1, context->ytick_labels.offset2,
			    -8.0, 0.0,
			    GSE_ANCHOR_EAST);
	}
       

      if (context->plot_param.axes_type == AXES_semilogy ||
	  context->plot_param.axes_type == AXES_loglog)
	{
	  tlw->width_ytick_labels =
	    DrawTickLabels (target, context, "log",
			    box.x, 0, box.y + box.height, -box.height,
			    context->ytick_labels.nvalues, &context->ytick_labels.values[0],
			    context->ytick_labels.offset1, context->ytick_labels.offset2,
			    -8.0, 0.0, GSE_ANCHOR_EAST);
	}
    }
}
