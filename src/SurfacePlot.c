/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
 *
 * ContourPlot3d.c
 *
 * Plots a two-dimensional projection of three-dimensional contour data.
 *
* Copyright 2017 John Darrington
 * Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
 * http://savannah.gnu.org/projects/gsegrafix
 *
 * This file is part of GSEGrafix, a scientific and engineering plotting program.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#include "PlotNormal3d.h"
#include "DrawContours3d.h"
#include "PlotInterp3d.h"
#include "Initialize3d.h"
#include <string.h> //for strchr
#include <stdbool.h>
#include "gsegraf.h"
#include <math.h>

static inline void
qq_begin (int *i, int limit, bool descending)
{
  if (descending)
    *i = limit - 1;
  else
    *i = 1;
}

static inline bool
qq_last (const int *i, int limit, bool descending)
{
  return descending ? (*i <= 0) : (*i >= limit);
}

static inline void
qq_next (int *i, bool descending)
{
  if (descending)
    (*i)--;
  else
    (*i)++;
}

typedef void fill_surface_func (struct target *, const struct gse_ctx *context, int icontour,
				  const double *xpoints, const double *ypoints,
				  const double *zpoints);


static void
no_fill (struct target *target, const struct gse_ctx *context, int icontour,
	 const double *xpoints, const double *ypoints, const double *zpoints)
{
  /* Do nothing */
}


void SurfacePlot (struct target *, const struct gse_ctx *context, int iplot, int nx, int ny, fill_surface_func *fill);

void
ContourPlot3d (struct target *target, const struct gse_ctx *context, int iplot, int nx, int ny)
{
  SurfacePlot (target, context, iplot, nx, ny, DrawContours3d);
}

void
MeshPlot3d (struct target *target, const struct gse_ctx *context, int iplot, int nx, int ny)
{
  SurfacePlot (target, context, iplot, nx, ny, no_fill);
}


void
SurfacePlot (struct target *target, const struct gse_ctx *context, int iplot, int nx, int ny, fill_surface_func *fill)
{
  /* Declare variables */
  int i, j, nxvalues, nyvalues, nzvalues;
  unsigned int index;
  guint32 fill_color[2];
  double xmin, xmax, ymin, ymax, zmin, zmax, xscale, yscale, zscale, 
    axis_length, origin[3], xpoints[4], ypoints[4], zpoints[4];
  char *pchar;

  /* Get minimum and maximum axis values */
  nxvalues = context->xtick_labels.nvalues;
  nyvalues = context->ytick_labels.nvalues;
  nzvalues = context->ztick_labels.nvalues;
  xmin = context->xtick_labels.values[0];
  xmax = context->xtick_labels.values[nxvalues-1];
  ymin = context->ytick_labels.values[0];
  ymax = context->ytick_labels.values[nyvalues-1];
  zmin = context->ztick_labels.values[0];
  zmax = context->ztick_labels.values[nzvalues-1];
  xmin = xmin - context->xtick_labels.offset1;
  xmax = xmax + context->xtick_labels.offset2;
  ymin = ymin - context->ytick_labels.offset1;
  ymax = ymax + context->ytick_labels.offset2;
  zmin = zmin - context->ztick_labels.offset1;
  zmax = zmax + context->ztick_labels.offset2;

  double phi = context->plot_param_3d.phi;
  if (phi >= 180.0)
    phi -= 360.0;

  /* Get axis length */
  axis_length = axis_length_3d (target);

  /* Calculate axis scale factors */
  xscale = axis_length/(xmax - xmin);
  yscale = axis_length/(ymax - ymin);
  zscale = axis_length/(zmax - zmin);

  get_origin (context, context->plot_param_3d.quadrant, origin);

  const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];

  /* Get fill colors */
  if ( the_plot->styleflags == STYLE_CHAR_CHAR )
    {
      if ( (pchar = strchr(color_string, the_plot->stylechar1)) != NULL )   /* get pointer to color character 1 */
	index = pchar - &color_string[0];                                 /* get index to color character 1   */
      fill_color[0] = context->color_rgba[index];
      if ( (pchar = strchr(color_string, the_plot->stylechar2)) != NULL )   /* get pointer to color character 2 */
	index = pchar - &color_string[0];                                 /* get index to color character 2   */
      fill_color[1] = context->color_rgba[index];
    }
  else if ( the_plot->styleflags == STYLE_CHAR_HEX )
    {
      if ( (pchar = strchr(color_string, the_plot->stylechar1)) != NULL )   /* get pointer to color character 1 */
	index = pchar - &color_string[0];                                 /* get index to color character 1   */
      fill_color[0] = context->color_rgba[index];
      fill_color[1] = the_plot->stylecolor2;
    }
  else if ( the_plot->styleflags == STYLE_HEX_CHAR )
    {
      fill_color[0] = the_plot->stylecolor1;
      if ( (pchar = strchr(color_string, the_plot->stylechar2)) != NULL )   /* get pointer to color character 2 */
	index = pchar - &color_string[0];                                 /* get index to color character 2   */
      fill_color[1] = context->color_rgba[index];
    }
  else if ( the_plot->styleflags == STYLE_HEX_HEX )
    {
      fill_color[0] = the_plot->stylecolor1;
      fill_color[1] = the_plot->stylecolor2;
    }

  /* Plot data */
  for (qq_begin (&i, nx, (fabs(phi) > 90)); !qq_last (&i, nx, (fabs(phi) > 90)); qq_next (&i, (fabs(phi) > 90)))
    {
      xpoints[0] = the_plot->samples3d.x[i-1];
      xpoints[1] = the_plot->samples3d.x[i];
      xpoints[2] = the_plot->samples3d.x[i];
      xpoints[3] = the_plot->samples3d.x[i-1];

      /* All x coordinates within range */
      if ( (xmin <= xpoints[0] && xpoints[0] <= xmax) &&
	   (xmin <= xpoints[1] && xpoints[1] <= xmax) )
	{
	  for (qq_begin (&j, ny, (phi < 0)); !qq_last (&j, ny, (phi < 0)); qq_next (&j, (phi < 0)))
	    {
	      ypoints[0] = the_plot->samples3d.y[j-1];
	      ypoints[1] = the_plot->samples3d.y[j-1];
	      ypoints[2] = the_plot->samples3d.y[j];
	      ypoints[3] = the_plot->samples3d.y[j];

	      /* All y coordinates within range */
	      if ( (ymin <= ypoints[1] && ypoints[1] <= ymax) &&
		   (ymin <= ypoints[2] && ypoints[2] <= ymax) )
		{
		  zpoints[0] = the_plot->samples3d.z[ny*(i-1)+j-1];
		  zpoints[1] = the_plot->samples3d.z[ny*i+j-1];
		  zpoints[2] = the_plot->samples3d.z[ny*i+j];
		  zpoints[3] = the_plot->samples3d.z[ny*(i-1)+j];

		  /* All z coordinates within range */
		  if ( (zmin <= zpoints[0] && zpoints[0] <= zmax) &&
		       (zmin <= zpoints[1] && zpoints[1] <= zmax) &&
		       (zmin <= zpoints[2] && zpoints[2] <= zmax) &&
		       (zmin <= zpoints[3] && zpoints[3] <= zmax) )
		    {
		      PlotNormal3d (target, context, iplot, xmin, ymin, zmin, zmax, xscale, yscale, zscale,
				    &origin[0], &context->plot_param_3d.Ryz[0], &fill_color[0],
				    &xpoints[0], &ypoints[0], &zpoints[0]);
		      fill (target, context, iplot, &xpoints[0], &ypoints[0], &zpoints[0]);
		    }

		  /* Not all z coordinates within range */
		  else
		    {
		      PlotInterp3d (target, context, iplot, xmin, ymin, zmin, zmax, xscale, yscale, zscale,
				    &origin[0], &context->plot_param_3d.Ryz[0], &fill_color[0],
				    &xpoints[0], &ypoints[0], &zpoints[0]);
		      fill (target, context, iplot, &xpoints[0], &ypoints[0], &zpoints[0]);
		    }
		}
	    }
	}

    }
}
