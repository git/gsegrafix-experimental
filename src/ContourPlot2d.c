/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* ContourPlot2d.c
*
* Plots two-dimensional contour lines using contour lines of the same color or
* contour lines which vary in color.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "ContourPlot2d.h"
#include "Misc.h"
#include "DrawTickMarks.h"
#include <math.h>
#include "gsegraf.h"
#include "InitializePlot.h"
#include <string.h> // for strcmp
#include <stdlib.h> // for free

#include "gse-cairo.h"

void
ContourPlot2d (struct target *target, const struct gse_ctx *context, int iplot, int icontour, int nx, int ny )
{
   int i, j, k, nxvalues, nyvalues, nzvalues, width, height, nc, linewidth;
   guint32 color, *color0 = NULL;
     double xmin, xmax, ymin, ymax, zmin, zmax,
          xscale, yscale, zscale, dz, width_bar, height_bar,
          x1_bar, x2_bar, y1_bar, y2_bar,
          width_ztick_labels, x1, x2, y1, y2, fraction,
          dx1, dx2, dy1, dy2, *contours, *xi, *yi, *zi, x, y, dzdx, dzdy, dr, grad, dr0;
   char string[21];
   GdkPixbuf *pixbuf_contour;
   GseCairoPoints *points;

   const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];

   /* Get minimum and maximum axis values */
   nxvalues = context->xtick_labels.nvalues;
   xmin = context->xtick_labels.values[0];
   xmax = context->xtick_labels.values[nxvalues-1];
   nyvalues = context->ytick_labels.nvalues;
   ymin = context->ytick_labels.values[0];
   ymax = context->ytick_labels.values[nyvalues-1];
   nzvalues = context->ztick_labels.nvalues;
   zmin = context->ztick_labels.values[0];
   zmax = context->ztick_labels.values[nzvalues-1];
   xmin = xmin - context->xtick_labels.offset1;
   xmax = xmax + context->xtick_labels.offset2;
   ymin = ymin - context->ytick_labels.offset1;
   ymax = ymax + context->ytick_labels.offset2;
   zmin = zmin - context->ztick_labels.offset1;
   zmax = zmax + context->ztick_labels.offset2;

   cairo_rectangle_t box;
   get_box (target, context, &box);
   
   /* Calculate axis scale factors */
   xscale = box.width/(xmax - xmin);
   yscale = box.height/(ymax - ymin);


   /* Get number of contour lines */
   if ( context->ncontours < 2 )
      nc = 2*nzvalues - 1;
   else
      nc = context->ncontours;
   contours = xmalloc(nc*sizeof(double));


   /* Calculate contour-line values */
   for ( i=1; i<=nc; i++ )
      contours[i-1] = zmin + (i - 1)*(zmax - zmin)/(nc - 1);


   /* Contour plot with variable-color contour lines */
   if ( the_plot->styleflags == STYLE_AUTO )
      {
      /* Calculate contour-line colors */
      color0 = xmalloc(nc*sizeof(guint32));
      for ( i=1; i<=nc; i++ )
         {
         fraction = (double)(i - 1)/(double)(nc - 1);
         color0[i-1] = interp_color_1(context, fraction);   /* alpha = 0xFF */
         }


      /* Draw color bar */
      if ( icontour == 1 )
         {
         width_bar = 20.0;
         height_bar = 0.875*box.height;
         x1_bar = (box.x + box.width) + 20.0;
         x2_bar = x1_bar + width_bar;
         y1_bar = (box.y + (box.y + box.height))/2.0 + height_bar/2.0;
         y2_bar = (box.y + (box.y + box.height))/2.0 - height_bar/2.0;
         if (context->plot_param.draw_box)
            {
            points = gse_cairo_points_new(2);
            for ( i=1; i<=nc; i++ )
               {
               points->coords[0] = x1_bar;
               points->coords[1] = y1_bar + (i - 1.0)*(y2_bar - y1_bar)/(nc - 1.0);
               points->coords[2] = x2_bar;
               points->coords[3] = points->coords[1];
	       gse_cairo_render_line (target->cr, points, 2, 2, color0[i-1]);
               }
            gse_cairo_points_unref(points);
            }


         /* Draw and label color-bar tick marks */
         if (context->plot_param.draw_box)
            {
            /* Draw vertical line */
            points = gse_cairo_points_new(4);
            points->coords[0] = x2_bar + 8.0;
            points->coords[1] = y1_bar;
            points->coords[2] = x2_bar + 8.0 + context->tick_major;
            points->coords[3] = y1_bar;
            points->coords[4] = x2_bar + 8.0 + context->tick_major;
            points->coords[5] = y2_bar;
            points->coords[6] = x2_bar + 8.0;
            points->coords[7] = y2_bar;
	    gse_cairo_render_line (target->cr, points, 4, 2, context->canvas_fg_color);
            gse_cairo_points_unref(points);

            /* Draw tick marks and tick-mark labels */
            if ( context->plot_param.z_tick_marks )
               {
		 DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
			       x2_bar + 8.0 + context->tick_major, 0, y1_bar, y2_bar - y1_bar,
			       context->ztick_labels.nvalues, &context->ztick_labels.values[0],
                             context->ztick_labels.offset1, context->ztick_labels.offset2,
                             180*deg2rad);

               if ( context->plot_param.z_tick_labels )
                  {
                  zscale = height_bar/(zmax - zmin);
                  dz = (zmax - zmin)/(nzvalues - 1);
                  width_ztick_labels = 0.0;
                  for ( i=1; i<=nzvalues; i++ )
                     {
                     memset(string, 0, sizeof(string));
                     if ( fabs(context->ztick_labels.values[i-1]) < 0.01*dz )
                        snprintf(string, sizeof(string), "%1.0f", 0.0);
                     else
                        snprintf(string, sizeof(string), "%g", context->ztick_labels.values[i-1]);
		     
		     gse_cairo_render_text_with_extents (target->cr, string, 
							 x2_bar + 8.0 + context->tick_major + 8.0,
							 y1_bar - (context->ztick_labels.values[i-1] - zmin)*zscale,
							 GSE_ANCHOR_WEST,
							 context->canvas_fg_color,
							 context->font_tick_labels,
							 0,
							 &x1, &y1, &x2, &y2);
                     if ( x2 - x1 > width_ztick_labels )
                        width_ztick_labels = x2 - x1;
                     }
                  }
               }
            }
         }

      /* Label color-bar */
      if (context->plot_param.draw_box &&
           icontour == 1 && context->zlabel != NULL)
         {
         /* Draw z-axis label */
         y = (box.y + (box.y + box.height))/2.0;
	 x = x2_bar + 8.0 + context->tick_major + 8.0;
         if ( context->plot_param.z_tick_marks &&
              context->plot_param.z_tick_labels )
	   x += width_ztick_labels + 8.0;
	 gse_cairo_render_text_with_extents (target->cr, context->zlabel,
					     x, y, GSE_ANCHOR_WEST,
					     context->canvas_fg_color,
					     context->font_axis_labels,
					     -M_PI / 2.0,
					     0, 0, 0, 0);
         }
      }


   /* Calculate pixbuf width and height */
   dx1 = 0.0;
   dx2 = 0.0;
   dy1 = 0.0;
   dy2 = 0.0;

   if ( xmin >= the_plot->samples3d.x[0] )
      x1 = xmin;
   else
      {
      x1 = the_plot->samples3d.x[0];
      dx1 = the_plot->samples3d.x[0] - xmin;
      }

   if ( xmax <= the_plot->samples3d.x[nx-1] )
      x2 = xmax;
   else
      {
      x2 = the_plot->samples3d.x[nx-1];
      dx2 = xmax - the_plot->samples3d.x[nx-1];
      }

   if ( ymin >= the_plot->samples3d.y[0] )
      y1 = ymin;
   else
      {
      y1 = the_plot->samples3d.y[0];
      dy1 = the_plot->samples3d.y[0] - ymin;
      }

   if ( ymax <= the_plot->samples3d.y[ny-1] )
      y2 = ymax;
   else
      {
      y2 = the_plot->samples3d.y[ny-1];
      dy2 = ymax - the_plot->samples3d.y[ny-1];
      }

   width  = roundl((box.x + box.width) - box.x + 1.0 - (dx1 + dx2)*xscale);
   height = roundl((box.y + box.height) - box.y + 1.0 - (dy1 + dy2)*yscale);


   /* Check pixbuf width and height */
   if ( width <= 0 || height <= 0 )
      return;


   /* Get interpolated values of x and y */
   xi = xmalloc(width*sizeof(double));
   yi = xmalloc(height*sizeof(double));
   zi = xmalloc(width*height*sizeof(double));
   for ( i=1; i<=width; i++ )
      xi[i-1] = x1 + (i - 1)*(x2 - x1)/(width - 1);
   for ( j=1; j<=height; j++ )
      yi[j-1] = y1 + (j - 1)*(y2 - y1)/(height - 1);


   /* Get interpolated values of z (bilinear interpolation) */
   for ( i=1; i<=width; i++ )
      for ( j=1; j<=height; j++ )
         interp2(nx, ny, 1,
		 &the_plot->samples3d.x[0], &the_plot->samples3d.y[0], &the_plot->samples3d.z[0],
                 &xi[i-1], &yi[j-1], &zi[height*(i-1)+(j-1)]);


   /* Create pixbuf */
   pixbuf_contour = gdk_pixbuf_new(GDK_COLORSPACE_RGB, TRUE, 8, width, height);
   gdk_pixbuf_fill(pixbuf_contour, 0xFFFFFF00);


   /* Contour plot with variable-color contour lines */
   if ( the_plot->styleflags == STYLE_AUTO )
      {
      /* Draw contour lines */
      linewidth = the_plot->stylesizes;
      dr0 = (linewidth + 1.0)/2.0;
      for ( i=2; i<width; i++ )
         for ( j=height-1; j>1; j-- )
            {
            dzdx = (zi[height*(i)+(j-1)] - zi[height*(i-2)+(j-1)])/2.0;
            dzdy = (zi[height*(i-1)+(j)] - zi[height*(i-1)+(j-2)])/2.0;
            grad = sqrt(dzdx*dzdx + dzdy*dzdy);
            for ( k=1; k<=nc; k++ )
               {
               dz = fabs(zi[height*(i-1)+(j-1)] - contours[k-1]);
               dr = dz/grad;
               if ( dr <= dr0 - 1.0 )
                  {
                  color = color0[k-1];                         /* alpha = 0xFF */
                  put_pixel(pixbuf_contour, i-1, height-j, color);
                  }
               else if ( dr <= dr0 )
                  {
                  color = color0[k-1] - (1 - dr0 + dr)*0xFF;   /* alpha = (dr0 - dr)*0xFF */
                  put_pixel(pixbuf_contour, i-1, height-j, color);
                  }
               }
            }
      }

   /* Contour plot with constant-color contour lines */
   else if ( the_plot->styleflags == STYLE_CHAR || the_plot->styleflags == STYLE_HEX )
      {
      /* Draw contour lines */
      linewidth = the_plot->stylesizes;
      dr0 = (linewidth + 1.0)/2.0;
      for ( i=2; i<width; i++ )
         for ( j=height-1; j>1; j-- )
            {
            dzdx = (zi[height*(i)+(j-1)] - zi[height*(i-2)+(j-1)])/2.0;
            dzdy = (zi[height*(i-1)+(j)] - zi[height*(i-1)+(j-2)])/2.0;
            grad = sqrt(dzdx*dzdx + dzdy*dzdy);
            for ( k=1; k<=nc; k++ )
               {
               dz = fabs(zi[height*(i-1)+(j-1)] - contours[k-1]);
               dr = dz/grad;
               if ( dr <= dr0 - 1.0 )
                  {
                  color = the_plot->stylecolor1;                           /* alpha = 0xFF */
                  put_pixel(pixbuf_contour, i-1, height-j, color);
                  }
               else if ( dr <= dr0 )
                  {
                  color = the_plot->stylecolor1 - (1.0 - dr0 + dr)*0xFF;   /* alpha = (dr0 - dr)*0xFF */
                  put_pixel(pixbuf_contour, i-1, height-j, color);
                  }
               }
            }
      }


   /* Draw pixbuf on canvas */
   x = box.x + dx1*xscale;
   y = (box.y + box.height) - dy1*yscale;

   gse_cairo_render_pixbuf (target->cr, pixbuf_contour, x, y,
			    GSE_ANCHOR_SOUTH_WEST);
   
   
   g_object_unref(pixbuf_contour);


   /* Free memory */
   free(contours);
   free(color0);
   free(xi);
   free(yi);
   free(zi);

   return;
   }
