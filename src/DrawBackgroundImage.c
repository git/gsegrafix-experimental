/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* DrawBackgroundImage.c
*
* Draws plot background image.
*
* Copyright 2017 John Darrington
* Copyright © 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "DrawBackgroundImage.h"
#include "Misc.h"
#include <math.h>
#include "gsegraf.h"
#include "InitializePlot.h"

#include "gse-cairo.h"

#include "PolarPlot.h"

gboolean
DrawBackgroundImage (struct target *target, const struct gse_ctx *context, GError **err)
   {
   /* Declare variables */
   int i, j, width_plot, height_plot, width_image, height_image,
       x_image, y_image, width_image_copied, height_image_copied,
       width_image_scaled, height_image_scaled, width, height;
   double xcenter, ycenter,
          radius, ratio_plot, ratio_image, i0, j0, rsq;


   /* Check context->background_image_file */
   if ( context->background_image_file == NULL )
      return TRUE;


   /* Get plot box minimum and maximum values */
   if ( context->plot_param.axes_type == AXES_linear ||
        context->plot_param.axes_type == AXES_semilogx ||
        context->plot_param.axes_type == AXES_semilogy ||
        context->plot_param.axes_type == AXES_loglog )
      {
	cairo_rectangle_t box;
	get_box (target, context, &box);
	xcenter = box.x +  box.width / 2.0;
	ycenter = box.y +  box.height / 2.0;
	width_plot = roundl (box.width);
	height_plot = roundl (box.height);
	ratio_plot = box.height/box.width;
      }

   else if ( context->plot_param.axes_type == AXES_polar )
      {
	polar_get_geometry (target, &xcenter, &ycenter, &radius);
	
	width_plot = roundl(2.0*radius);
	height_plot = width_plot;
	ratio_plot = 1.0;
      }


   /* Draw background image */
   GdkPixbuf *image = NULL;
   GdkPixbuf *image_file = gdk_pixbuf_new_from_file(context->background_image_file, err);
   if (!image_file)
     {
       g_assert (err == NULL || *err != NULL);
       return FALSE;
     }
   g_assert (err == NULL || *err == NULL);
   width_image = gdk_pixbuf_get_width(image_file);
   height_image = gdk_pixbuf_get_height(image_file);
   ratio_image = (double) height_image/(double) width_image;

   if ( context->background_image_style == CENTER )
      {
      if ( width_image > width_plot && height_image <= height_plot )
         {
         width_image_copied = width_plot;
         height_image_copied = height_image;
         x_image = roundl((width_image - width_image_copied)/2.0);
         y_image = 0;
         }
      else if ( width_image <= width_plot && height_image > height_plot )
         {
         width_image_copied = width_image;
         height_image_copied = height_plot;
         x_image = 0;
         y_image = roundl((height_image - height_image_copied)/2.0);
         }
      else if ( width_image > width_plot && height_image > height_plot )
         {
         width_image_copied = width_plot;
         height_image_copied = height_plot;
         x_image = roundl((width_image - width_image_copied)/2.0);
         y_image = roundl((height_image - height_image_copied)/2.0);
         }
      else
         {
         width_image_copied = width_image;
         height_image_copied = height_image;
         x_image = 0;
         y_image = 0;
         }

      image = gdk_pixbuf_new(GDK_COLORSPACE_RGB,
                                    TRUE,
                                    8,
                                    width_image_copied,
                                    height_image_copied);

      gdk_pixbuf_copy_area(image_file,
                           x_image,
                           y_image,
                           width_image_copied,
                           height_image_copied,
                           image,
                           0,
                           0);

      width = width_image_copied;
      height = height_image_copied;
      }

   else if ( context->background_image_style == FILL )
      {
      width_image_scaled = width_plot;
      height_image_scaled = height_plot;

      image = gdk_pixbuf_scale_simple(image_file,
                                             width_image_scaled,
                                             height_image_scaled,
                                             GDK_INTERP_BILINEAR);

      width = width_image_scaled;
      height = height_image_scaled;
      }

   else if ( context->background_image_style == SCALE )
      {
      if ( ratio_image <= ratio_plot )
         {
         width_image_scaled = width_plot;
         height_image_scaled = roundl(width_image_scaled*ratio_image);
         }
      else
         {
         height_image_scaled = height_plot;
         width_image_scaled = roundl(height_image_scaled/ratio_image);
         }

      image = gdk_pixbuf_scale_simple(image_file,
                                             width_image_scaled,
                                             height_image_scaled,
                                             GDK_INTERP_BILINEAR);

      width = width_image_scaled;
      height = height_image_scaled;
      }

   else if ( context->background_image_style == ZOOM )
      {
      if ( ratio_image <= ratio_plot )
         {
         height_image_copied = height_image;
         width_image_copied = roundl(height_image_copied/ratio_plot);
         x_image = roundl((width_image - width_image_copied)/2.0);
         y_image = 0;
         }
      else
         {
         width_image_copied = width_image;
         height_image_copied = roundl(width_image_copied*ratio_plot);
         x_image = 0;
         y_image = roundl((height_image - height_image_copied)/2.0);
         }

      GdkPixbuf *image_o = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
                                    TRUE,
                                    8,
                                    width_image_copied,
                                    height_image_copied);

      gdk_pixbuf_copy_area (image_file,
			    x_image,
			    y_image,
			    width_image_copied,
			    height_image_copied,
			    image_o,
			    0,
			    0);

      width_image_scaled = width_plot;
      height_image_scaled = height_plot;
      image = gdk_pixbuf_scale_simple (image_o,
				      width_image_scaled,
				      height_image_scaled,
				      GDK_INTERP_BILINEAR);

      width = width_image_scaled;
      height = height_image_scaled;
      g_object_unref (image_o);
      }


   if ( context->plot_param.axes_type == AXES_polar )
      {
      /* Make image pixels outside plot circle transparent */
      i0 = (double) width/2.0;
      j0 = (double) height/2.0;
      for ( i=1; i<=width; i++ )
         for ( j=1; j<=height; j++ )
            {
            rsq = (i - i0)*(i - i0) + (j - j0)*(j - j0);
            if ( rsq >= radius*radius )
               put_pixel(image, i-1, j-1, 0xFFFFFF00);
            }
      }

   gse_cairo_render_pixbuf (target->cr, image, xcenter, ycenter,
			    GSE_ANCHOR_CENTER);
   
   if ( context->plot_param.axes_type == AXES_polar )
     {
     gse_cairo_render_rectangle (target->cr, 
				 xcenter - radius,
				 ycenter - radius,
				 2*radius, 2*radius,
				 2,
				 context->canvas_bg_color,
				 0xFFFFFF00);
     }

   g_object_unref (image);
   g_object_unref (image_file);
   return TRUE;
   }

