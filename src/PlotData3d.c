/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
 *
 * PlotData3d.c
 *
 * Plots a two-dimensional projection of three-dimensional data.
 *
* Copyright 2017 John Darrington
 * Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
 * http://savannah.gnu.org/projects/gsegrafix
 *
 * This file is part of GSEGrafix, a scientific and engineering plotting program.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#include "PlotData3d.h"
#include "PlotPoints3d.h"
#include "ColorPlot3d.h"
#include "SurfacePlot.h"

#include "gsegraf.h"


void
PlotData3d (struct target *target, const struct gse_ctx *context)
{
  int iplot, nplots;
  /* Plot data */
  nplots = context->plot_param.nplots;
  for ( iplot=1; iplot<=nplots; iplot++ )
    {
      const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];
      if ( the_plot->plot_type == PLOT_points )
	{
	  /* Plot 3d line data */
	  PlotPoints3d (target, context, iplot);
	}
      else if ( the_plot->plot_type == PLOT_mesh )
	{
	  /* Plot 3d mesh data */
	  int nx = the_plot->samples3d.nx;
	  int ny = the_plot->samples3d.ny;
	  MeshPlot3d (target, context, iplot, nx, ny);
	}

      else if ( the_plot->plot_type == PLOT_contour )
	{
	  /* Plot 3d contour data */
	  int nx = the_plot->samples3d.nx;
	  int ny = the_plot->samples3d.ny;
	  ContourPlot3d (target, context, iplot, nx, ny);
	}


      else if ( the_plot->plot_type == PLOT_color )
	{
	  /* Plot 3d color data */
	  int nx = the_plot->samples3d.nx;
	  int ny = the_plot->samples3d.ny;
	  ColorPlot3d (target, context, iplot, nx, ny);
	}
    }
}
