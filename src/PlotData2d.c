/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
 *
 * PlotData2d.c
 *
 * Plots data points.
 *
* Copyright 2017 John Darrington
 * Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
 * http://savannah.gnu.org/projects/gsegrafix
 *
 * This file is part of GSEGrafix, a scientific and engineering plotting program.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#include "PlotData2d.h"
#include "Misc.h"
#include "DrawSymbols.h"
#include "Histogram.h"
#include "ContourPlot2d.h"
#include "ColorPlot2d.h"
#include "DrawLines.h"
#include "InitializePlot.h"
#include <math.h>
#include "gsegraf.h"

#include <string.h>

#include "data-iterator.h"

static void
DrawLineSegments (struct target *target, const struct gse_ctx *context, const struct plot_parameters *the_plot,
		  const double *lower, const double *upper,
		  double xscale, double yscale, enum line_texture texture)
{
  /* Draw all line segments */
  int iseg = 0;
  struct sample_iterator *it = it_create (context, the_plot, true);
  for (; it; ++iseg)
    {
      DrawLines2d(target, context,
		  it,
		  lower, upper,
		  xscale, yscale, the_plot->fill_colors_rgba,
		  the_plot->stylesizes,
		  texture);
      it = it_next_segment (it);
    }
}


void
PlotData2d (struct target *target, const struct gse_ctx *context)
{
  int iplot;
  double lower[3], upper[3];
  char *pchar;
   
  /* Get minimum and maximum axis values */
  if ( context->plot_param.axes_type == AXES_linear )
    {
      int nx = context->xtick_labels.nvalues;
      lower[0] = context->xtick_labels.values[0];
      upper[0] = context->xtick_labels.values[nx-1];
      int ny = context->ytick_labels.nvalues;
      lower[1] = context->ytick_labels.values[0];
      upper[1] = context->ytick_labels.values[ny-1];
      lower[0] = lower[0] - context->xtick_labels.offset1;
      upper[0] = upper[0] + context->xtick_labels.offset2;
      lower[1] = lower[1] - context->ytick_labels.offset1;
      upper[1] = upper[1] + context->ytick_labels.offset2;
    }

  else if ( context->plot_param.axes_type == AXES_semilogx )
    {
      int nx = context->xtick_labels.nvalues;
      lower[0] = floor(context->xtick_labels.values[0]);
      upper[0] = ceil(context->xtick_labels.values[nx-1]);
      int ny = context->ytick_labels.nvalues;
      lower[1] = context->ytick_labels.values[0];
      upper[1] = context->ytick_labels.values[ny-1];
      lower[1] = lower[1] - context->ytick_labels.offset1;
      upper[1] = upper[1] + context->ytick_labels.offset2;
    }

  else if ( context->plot_param.axes_type == AXES_semilogy )
    {
      int nx = context->xtick_labels.nvalues;
      lower[0] = context->xtick_labels.values[0];
      upper[0] = context->xtick_labels.values[nx-1];
      int ny = context->ytick_labels.nvalues;
      lower[1] = floor(context->ytick_labels.values[0]);
      upper[1] = ceil(context->ytick_labels.values[ny-1]);
      lower[0] = lower[0] - context->xtick_labels.offset1;
      upper[0] = upper[0] + context->xtick_labels.offset2;
    }

  else if ( context->plot_param.axes_type == AXES_loglog )
    {
      int nx = context->xtick_labels.nvalues;
      lower[0] = floor(context->xtick_labels.values[0]);
      upper[0] = ceil(context->xtick_labels.values[nx-1]);
      int ny = context->ytick_labels.nvalues;
      lower[1] = floor(context->ytick_labels.values[0]);
      upper[1] = ceil(context->ytick_labels.values[ny-1]);
    }

  cairo_rectangle_t box;
  get_box (target, context, &box);

  /* Calculate axis scale factors */
  double xscale = box.width/(upper[0] - lower[0]);
  double yscale = box.height/(upper[1] - lower[1]);

  /* Plot data */
  int icontour = 0;
  int icolor = 0;
  for (iplot=1; iplot <= context->plot_param.nplots; iplot++)
    {
      const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];

      if ( the_plot->plot_type == PLOT_points )
	{
	  /* Draw stem lines */
	  if (the_plot->stem_type != STEM_OFF)
            {
	      double y = 0;
	      GseCairoPoints *points = gse_cairo_points_new(2);

	      /* Calculate y coordinate of stem point 1 */
	      if (the_plot->stem_type == STEM_VALUE)
		{
		  if ( lower[1] <= the_plot->stemvalues && the_plot->stemvalues <= upper[1] )
		    y = (box.y + box.height) - (the_plot->stemvalues - lower[1])*yscale;
		  else if ( the_plot->stemvalues < lower[1] )
		    y = (box.y + box.height);
		  else if ( the_plot->stemvalues > upper[1] )
		    y = box.y;
		}
	      else
		y = (box.y + box.height);
	      points->coords[1] = y;

	      struct sample_iterator *it = it_create (context, the_plot, false);
	      for (; ! it_last(it); it_next (it))
		{
		  const struct gse_datum *sample = it_get (it);
		  if ( lower[0] <= sample->x && sample->x <= upper[0] &&
		       lower[1] <= sample->y && sample->y <= upper[1] )
		    {
		      /* Calculate x coordinate of stem point 1 */
		      double x = box.x + (sample->x - lower[0])*xscale;
		      points->coords[0] = x;

		      /* Calculate x and y coordinates of stem point 2 */
		      x = box.x + (sample->x - lower[0])*xscale;
		      y = (box.y + box.height) - (sample->y - lower[1])*yscale;
		      points->coords[2] = x;
		      points->coords[3] = y;

		      DrawLine (target, context, points, the_plot->outline_colors_rgba, 1, SOLID);
		    }
		}
	      it_destroy (it);

	      gse_cairo_points_unref(points);
            }


	  /* Draw lines */
	  if (the_plot->texture != NONE)
	    DrawLineSegments (target, context, the_plot,
			     lower, upper,
                             xscale, yscale, the_plot->texture);

	  /* Draw symbols in symbol_string1 ("cCtTsSiIpPhH") */
	  else if ( (pchar = strchr(symbol_string1, the_plot->stylechar1)) != NULL )
            {
	      int ifunc = pchar - symbol_string1;
	      struct sample_iterator *it = it_create (context, the_plot, false);
	      for (; ! it_last(it); it_next (it))
		{
		  const struct gse_datum *sample = it_get (it);
		  if ( lower[0] <= sample->x && sample->x <= upper[0] &&
		       lower[1] <= sample->y && sample->y <= upper[1] )
		    {
		      double x = box.x + (sample->x - lower[0])*xscale;
		      double y = (box.y + box.height) - (sample->y - lower[1])*yscale;
		      context->symbol_func1[ifunc](target, x, y, the_plot->fill_colors_rgba,
						   the_plot->outline_colors_rgba,
						   the_plot->stylesizes);
		    }
		}
	      it_destroy (it);
	    }

	  /* Draw symbols in symbol_string2 ("+xra") */
	  else if ( (pchar = strchr(symbol_string2, the_plot->stylechar1)) != NULL )
            {
	      int ifunc = pchar - symbol_string2;
	      struct sample_iterator *it = it_create (context, the_plot, false);
	      for (; ! it_last(it); it_next (it))
		{
		  const struct gse_datum *sample = it_get (it);
		  if ( lower[0] <= sample->x && sample->x <= upper[0] &&
		       lower[1] <= sample->y && sample->y <= upper[1] )
		    {
		      double x = box.x + (sample->x - lower[0])*xscale;
		      double y = (box.y + box.height) - (sample->y - lower[1])*yscale;
		      context->symbol_func2[ifunc](target, x, y, the_plot->fill_colors_rgba,
						   the_plot->outline_colors_rgba,
						   the_plot->stylesizes);
		    }
		}
	      it_destroy (it);
            }
	}

      else if ( the_plot->plot_type == PLOT_histogram )
	{
	  PlotHistogram (target, context, iplot);
	}

      else if ( the_plot->plot_type == PLOT_contour )
	{
	  icontour++;
	  int nxcontourplot = the_plot->samples3d.nx;
	  int nycontourplot = the_plot->samples3d.ny;
	  ContourPlot2d (target, context, iplot, icontour, nxcontourplot, nycontourplot);
	}

      else if ( the_plot->plot_type == PLOT_color )
	{
	  icolor++;
	  int nxcolorplot = the_plot->samples3d.nx;
	  int nycolorplot = the_plot->samples3d.ny;
	  ColorPlot2d (target, context, iplot, icolor, nxcolorplot, nycolorplot);
	}
    }
}

