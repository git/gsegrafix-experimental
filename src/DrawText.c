/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* DrawText.c
*
* Contains functions:
*    DrawText
*    background_text
*
* Draws text.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#define _GNU_SOURCE
#include "DrawText.h"
#include "GetWindowCoords.h"
#include "InitializePlot.h"
#include "gsegraf.h"

#include "gse-cairo.h"
#include <string.h>

void
DrawText (struct target *target, const struct gse_ctx *context)
{
  /* Draw texts */
  for (int i = 0; i < context->n_texts; ++i)
    {
      const struct text_spec *ts = context->texts +i;

      if ( ts->text == NULL )
	return;

      double xanchor = -1;
      double yanchor = -1;

      cairo_rectangle_t box;
      get_box (target, context, &box);
      
      switch (ts->coords_ref)
	{
	case GSE_COORDS_ABS:
	  {
	    double plot_coords[3];
	    double window_coords[2];
	    plot_coords[0] = ts->x;
	    plot_coords[1] = ts->y;
	    plot_coords[2] = ts->z;
	    GetWindowCoords (target, context, plot_coords, window_coords);
	    xanchor = window_coords[0];
	    yanchor = window_coords[1];
	  }
	  break;
	case GSE_COORDS_REL:
	  xanchor = box.x + ts->x * box.width;
	  yanchor = box.y + box.height * (1.0 - ts->y);
	  break;
	default:
	  g_assert_not_reached ();
	}

      /* Check text is within plot box for absolute coordinates */
      if ( ts->coords_ref == GSE_COORDS_ABS &&
	   (xanchor < 0 || yanchor < 0) )
	return;

      /* Draw text on a background of translucent rectangles */
      background_text (target, context, ts->text, xanchor, yanchor,
		       ts->anchor);
    }
}

void
background_text (struct target *target, const struct gse_ctx *context, const char *text_str,
		 double x1, double y1, enum GseAnchorType anchor)
{
  const char *start = text_str;
  double voffset = 0;
  for (;;)
    {
      const char *end = strchrnul (start, '\n');

      PangoRectangle rect;
      PangoLayout *layout =
	gse_cairo_create_layout (target->cr,
				 start, end-start, context->font_text,
				 &rect);
      double x1rec = rect.x / PANGO_SCALE;
      double x2rec = (rect.x + rect.width) / PANGO_SCALE;
      double y1rec = rect.y / PANGO_SCALE;
      double y2rec = (rect.y + rect.height) / PANGO_SCALE;
      
      double x11 = x1rec - 1.0;
      double x22 = x2rec + 1.0;
      double y11 = y1rec + voffset;
      double y22 = y11 +  + y2rec + 1.0;
      
      if (end - start > 0) /* Plot nothing for zero length lines */
	{
	  double xshift, yshift;
	  get_shift_for_anchor (anchor, x22 - x11, y22 - y11,
				&xshift, &yshift);

      
	  gse_cairo_render_rectangle (target->cr,
				      x1 + x11 + xshift,
				      y1 + y11 + yshift,
				      x22 - x11,
				      y22 - y11,
				      1,
				      context->canvas_bg_color & 0xFFFFFFC0,
				      context->canvas_bg_color & 0xFFFFFFC0);

	  gse_cairo_render_layout (target->cr, layout,
				   &rect, x1 + x11, y1 + y11,
				   anchor, context->canvas_fg_color);
	}
      
      g_object_unref (layout);

      voffset += y22 - y11;
      start = end + 1;
      if (*end == '\0')
	break;
    }
}
