/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* AutoScale.c
*
* Calculates "nice" tick-mark labels for linear plot axes.
*
* Labels are constrained to increment as:
*
*       0.1 0.2 0.3 ...   1  2  3 ...   10   20   30 ... etc.
*    or 0.2 0.4 0.6 ...   2  4  6 ...   20   40   60 ... etc.
*    or 0.5 1.0 1.5 ...   5 10 15 ...   50  100  150 ... etc.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "AutoScale.h"
#include "Misc.h"
#include <math.h>
#include "gsegraf.h"


void AutoScale (struct gse_ctx *context, int naxes, int nincmax )
   {
   /* Declare variables */
   int iaxis, i, n, nf, n1, n2, f[] = { 1, 2, 5, 10, 20 }, nticks;
   double datamin, datamax, incmin, inc, axismin, axismax;


   /* Calculate tick-mark labels for each axis */
   for ( iaxis=1; iaxis<=naxes; iaxis++ )
      {
      /* Get minimum and maximum values of plot data */
      if ( iaxis == 1 )
         {
         datamin = context->data_min_max.xmin;
         datamax = context->data_min_max.xmax;
         }
      else if ( iaxis == 2 )
         {
         datamin = context->data_min_max.ymin;
         datamax = context->data_min_max.ymax;
         }
      else if ( iaxis == 3 )
         {
         datamin = context->data_min_max.zmin;
         datamax = context->data_min_max.zmax;
         }


      /* Calculate minimum tick-mark increment */
      incmin = (datamax - datamin)/nincmax;
      if ( incmin <= 0.0 )
         continue;


      /* Calculate "nice" tick-mark increment */
      n = roundl(floor(log10(fabs(incmin))));
      if ( 0.5*pow(10.0, (double) n) < incmin && incmin <= 1.0*pow(10.0, (double) n) )
         nf = 0;
      else if ( 1.0*pow(10.0, (double) n) < incmin && incmin <= 2.0*pow(10.0, (double) n) )
         nf = 1;
      else if ( 2.0*pow(10.0, (double) n) < incmin && incmin <= 5.0*pow(10.0, (double) n) )
         nf = 2;
      else
         nf = 3;
      inc = f[nf]*pow(10.0, (double) n);
      n1 = roundl(floor(datamin/inc));
      n2 = roundl(ceil(datamax/inc));
      axismin = n1*inc;
      axismax = n2*inc;


      /* Increase tick-mark increment if too many increments */
      if ( n2 - n1 > nincmax)
         {
         nf = nf + 1;
         inc = f[nf]*pow(10.0, (double) n);
         n1 = roundl(floor(datamin/inc));
         n2 = roundl(ceil(datamax/inc));
         axismin = n1*inc;
         axismax = n2*inc;
         }


      /* Save tick-mark label data */
      nticks = n2 - n1 + 1;
      if ( iaxis == 1 )
         {
         context->xtick_labels.nvalues = nticks;
         for ( i=1; i<=nticks; i++ )
            context->xtick_labels.values[i-1] = axismin + (i - 1)*((axismax - axismin)/(nticks - 1));
         }
      else if ( iaxis == 2 )
         {
         context->ytick_labels.nvalues = nticks;
         for ( i=1; i<=nticks; i++ )
            context->ytick_labels.values[i-1] = axismin + (i - 1)*((axismax - axismin)/(nticks - 1));
         }
      else if ( iaxis == 3 )
         {
         context->ztick_labels.nvalues = nticks;
         for ( i=1; i<=nticks; i++ )
            context->ztick_labels.values[i-1] = axismin + (i - 1)*((axismax - axismin)/(nticks - 1));
         }
      }

   return;
   }
