/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*
  This file is part of GSEGrafix, a scientific and engineering plotting 
  program.

  Copyright (C) 2017 John Darrington

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <glib.h>

struct gse_ctx;
#include "gsegraf.h"

#include "gse-cairo.h"
#include "cairo.h"

void texture_to_dash (const struct gse_ctx *context, enum line_texture texture,
		      double *dash, int *n_dash);

void DrawLine (struct target *, const struct gse_ctx *context, GseCairoPoints *points, guint32 color,
	       unsigned int line_width, enum line_texture texture);


void     DrawCircle (const struct target *, double x, double y, guint32 fill_color_rgba, guint32 outline_color_rgba, int size );
void     DrawTriangle (const struct target *, double x, double y, guint32 fill_color_rgba, guint32 outline_color_rgba, int size );
void     DrawSquare (const struct target *, double x, double y, guint32 fill_color_rgba, guint32 outline_color_rgba, int size );
void     DrawDiamond (const struct target *, double x, double y, guint32 fill_color_rgba, guint32 outline_color_rgba, int size );
void     DrawPentagon (const struct target *, double x, double y, guint32 fill_color_rgba, guint32 outline_color_rgba, int size );
void     DrawHexagon (const struct target *, double x, double y, guint32 fill_color_rgba, guint32 outline_color_rgba, int size );

void     DrawPlus (const struct target *, double x, double y, guint32 fill_color_rgba, guint32 dummy, int size );
void     DrawX (const struct target *,  double x, double y, guint32 fill_color_rgba, guint32 dummy, int size );
void     DrawStar (const struct target *,  double x, double y, guint32 fill_color_rgba, guint32 dummy, int size );
void     DrawAsterisk (const struct target *,  double x, double y, guint32 fill_color_rgba, guint32 dummy, int size );

void     DrawBar (const struct target *,  double x1, double y1, double x2, double y2,
                   guint32 fill_color_rgba, guint32 outline_color_rgba );
void     DrawMesh (const struct target *,  double x1, double y1, double x2, double y2,
                    guint32 fill_color_rgba, guint32 outline_color_rgba, int flag );
void     DrawContour (struct target *, const struct gse_ctx *, double x1, double y1, double x2, double y2,
                       guint32 fill_color_rgba, guint32 outline_color_rgba, int flag );
void     DrawColorPlot (struct target *, const struct gse_ctx *, double x1, double y1, double x2, double y2 );

