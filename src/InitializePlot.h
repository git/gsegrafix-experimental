/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*
  This file is part of GSEGrafix, a scientific and engineering plotting 
  program.

  Copyright (C) 2017 John Darrington

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INITIALIZE_PLOT
#define INITIALIZE_PLOT

#include <stdbool.h>
#include <glib.h>
#include <cairo.h>

struct gse_ctx;
struct target;

struct data_parser
{
  GQuark domain;

  int maxline;
  char *line;
  char *string_get;
  char *param_file;

  bool close_after_save;
  char *save_filename;
  
  int window_width;
  int window_height;
};


void get_box_from_coords (const struct gse_ctx *context, double width, double height, cairo_rectangle_t *rect);
void get_box (const struct target *, const struct gse_ctx *context, cairo_rectangle_t *rect);


#endif
