/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*
  This file is part of GSEGrafix, a scientific and engineering plotting 
  program.

  Copyright (C) 2017 John Darrington

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GSEGRAFIX_H
#define GSEGRAFIX_H 1

struct gse_ctx;
struct data_parser;

typedef void (*message_handler) (const char *message, void *aux);

struct gse_ctx * InitializePlot (const char *filename, struct data_parser **p,
				 message_handler msg, gpointer msg_aux);

void destroy_parser (struct data_parser *p);
void gse_context_destroy (struct gse_ctx *context);

void DrawGraph (cairo_t *cr, int width, int height, struct gse_ctx *context);

void get_box_from_coords (const struct gse_ctx *context, double width, double height, cairo_rectangle_t *rect);


#endif
