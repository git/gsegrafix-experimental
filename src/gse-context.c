/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*
  This file is part of GSEGrafix, a scientific and engineering plotting 
  program.

  Copyright (C) 2017 John Darrington
  Copyright © 2008, 2009, 2010, 2011 Spencer A. Buckner

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gsegraf.h"

#include <Misc.h>
#include "DrawSymbols.h"

#include <string.h>
#include <stdlib.h>

struct gse_ctx *
gse_context_create (void)
{
  unsigned int size;
  time_t time_current;

  struct gse_ctx *context = xzalloc (sizeof *context);

  /* Define function arrays */
  context->symbol_func1[0]  = DrawCircle;
  context->symbol_func1[1]  = DrawCircle;
  context->symbol_func1[2]  = DrawTriangle;
  context->symbol_func1[3]  = DrawTriangle;
  context->symbol_func1[4]  = DrawSquare;
  context->symbol_func1[5]  = DrawSquare;
  context->symbol_func1[6]  = DrawDiamond;
  context->symbol_func1[7]  = DrawDiamond;
  context->symbol_func1[8]  = DrawPentagon;
  context->symbol_func1[9]  = DrawPentagon;
  context->symbol_func1[10] = DrawHexagon;
  context->symbol_func1[11] = DrawHexagon;

  context->symbol_func2[0] = DrawPlus;
  context->symbol_func2[1] = DrawX;
  context->symbol_func2[2] = DrawStar;
  context->symbol_func2[3] = DrawAsterisk;

  /* Get date and time */
  memset(context->date_time, 0, sizeof(context->date_time));
  time_current = time(NULL);
  strftime(context->date_time,
	   sizeof(context->date_time),
	   "%d-%b-%Y %H:%M:%S",
	   localtime(&time_current));

  context->plot_param.equal_axes = FALSE;
  strcpy(context->plot_param.grid,             "off");
  strcpy(context->plot_param.minor_ticks,      "off");
  context->plot_param.page_note_anchor = GSE_ANCHOR_NONE;
  context->plot_param.draw_box = TRUE;
  context->plot_param.x_tick_marks  =    TRUE;
  context->plot_param.y_tick_marks  =    TRUE;
  context->plot_param.z_tick_marks  =    TRUE;
  context->plot_param.x_tick_labels =    TRUE;
  context->plot_param.y_tick_labels =    TRUE;
  context->plot_param.z_tick_labels =    TRUE;

  context->canvas_bg_color = 0xFFFFFFFF;   /* white */
  context->canvas_fg_color = 0x000000FF;   /* black */
  context->gridchar1 = 'l';                /* solid line */
  context->gridchar2 = 's';                /* silver */
  context->gridcolor = 0xC0C0C0FF;         /* silver */
  context->background_image_style = FILL;     /* fill */

  context->data_min_max.xmin = DBL_MAX;
  context->data_min_max.xmax = -DBL_MAX;
  context->data_min_max.ymin = DBL_MAX;
  context->data_min_max.ymax = -DBL_MAX;
  context->data_min_max.zmin = DBL_MAX;
  context->data_min_max.zmax = -DBL_MAX;

  context->plot_param_3d.phi   = 30.0;
  context->plot_param_3d.theta = 30.0;

  context->minor_ticks_flag = 0;

  /* Specify default font name */
  size = sizeof("FreeSans");
  context->font_name = xmalloc(size + 1);
  strcpy(context->font_name, "FreeSans");
   
  /* Specify font style and weight */
  context->font_date_time = pango_font_description_new();
  pango_font_description_set_style(context->font_date_time, PANGO_STYLE_NORMAL);
  pango_font_description_set_weight(context->font_date_time, PANGO_WEIGHT_NORMAL);

  context->font_text = pango_font_description_new();
  pango_font_description_set_style(context->font_text, PANGO_STYLE_NORMAL);
  pango_font_description_set_weight(context->font_text, PANGO_WEIGHT_NORMAL);

  context->font_tick_labels = pango_font_description_new();
  pango_font_description_set_style(context->font_tick_labels, PANGO_STYLE_NORMAL);
  pango_font_description_set_weight(context->font_tick_labels, PANGO_WEIGHT_NORMAL);

  context->font_axis_labels = pango_font_description_new();
  pango_font_description_set_style(context->font_axis_labels, PANGO_STYLE_NORMAL);
  pango_font_description_set_weight(context->font_axis_labels, PANGO_WEIGHT_NORMAL);

  context->font_title = pango_font_description_new();
  pango_font_description_set_style(context->font_title, PANGO_STYLE_NORMAL);
  pango_font_description_set_weight(context->font_title, PANGO_WEIGHT_NORMAL);


  /* Specify default font point sizes */
  context->font_size_date_time   = 12;
  context->font_size_text        = 14;
  context->font_size_tick_labels = 14;
  context->font_size_axis_labels = 16;
  context->font_size_title       = 18;


  /* Tabulate color array for colors specified by color characters */
  context->color_rgba[0]  = 0x000000FF;   /* k black   (black)        */
  context->color_rgba[1]  = 0x808080FF;   /* a gray    (gray50)       */
  context->color_rgba[2]  = 0xC0C0C0FF;   /* s silver  (gray75)       */
  context->color_rgba[3]  = 0xFFFFFFFF;   /* w white   (white)        */
  context->color_rgba[4]  = 0xFF0000FF;   /* r red     (red)          */
  context->color_rgba[5]  = 0xFFFF00FF;   /* y yellow  (yellow)       */
  context->color_rgba[6]  = 0x00FF00FF;   /* l lime    (green)        */
  context->color_rgba[7]  = 0x00FFFFFF;   /* q aqua    (cyan)         */
  context->color_rgba[8]  = 0x0000FFFF;   /* b blue    (blue)         */
  context->color_rgba[9]  = 0xFF00FFFF;   /* f fuchsia (magenta)      */
  context->color_rgba[10] = 0x800000FF;   /* m maroon  (dark red)     */
  context->color_rgba[11] = 0x808000FF;   /* o olive   (dark yellow)  */
  context->color_rgba[12] = 0x008000FF;   /* g green   (dark green)   */
  context->color_rgba[13] = 0x008080FF;   /* t teal    (dark cyan)    */
  context->color_rgba[14] = 0x000080FF;   /* n navy    (dark blue)    */
  context->color_rgba[15] = 0x800080FF;   /* p purple  (dark magenta) */
  context->color_rgba[16] = 0xFFFFFF00;   /* x transparent            */


  /****************************************************************************
   *
   * Calculate all colors with maximum saturation from blue to green to red
   *    used for:
   *       color plots
   *       contour-line colors of 2d contour plots with plot_style = "auto"
   *       upper-surface colors of 3d mesh plots with plot_style = "auto"
   *
   ****************************************************************************/
  context->n_color_spectrum_1 = 1021;
  context->color_spectrum_1[0] = 0x0000FFFF;                        /*               0x0000FFFF; index =           0 */
  unsigned int i;
  for ( i=1; i<= 255; i++ )
    context->color_spectrum_1[i]     = 0x0000FFFF + 0x10000*i;     /* 0x0001FFFF to 0x00FFFFFF; index =   1 to  255 */
  for ( i=1; i<= 255; i++ )
    context->color_spectrum_1[255+i] = 0x00FFFFFF - 0x100*i;       /* 0x00FFFEFF to 0x00FF00FF; index = 256 to  510 */
  for ( i=1; i<= 255; i++ )
    context->color_spectrum_1[510+i] = 0x00FF00FF + 0x1000000*i;   /* 0x01FF00FF to 0xFFFF00FF; index = 511 to  765 */
  for ( i=1; i<= 255; i++ )
    context->color_spectrum_1[765+i] = 0xFFFF00FF - 0x10000*i;     /* 0xFFFE00FF to 0xFF0000FF; index = 766 to 1020 */


  /****************************************************************************
   *
   * Calculate all colors with 75% maximum saturation from blue to green to red
   *    used for lower-surface colors of 3d mesh plots with plot_style = "auto"
   *
   ****************************************************************************/

  context->n_color_spectrum_2 = 769;
  context->color_spectrum_2[0] = 0x0000C0FF;                        /*               0x0000C0FF; index =          0 */
  for ( i=1; i<= 192; i++ )
    context->color_spectrum_2[i]     = 0x0000C0FF + 0x10000*i;     /* 0x0001C0FF to 0x00C0C0FF; index =   1 to 192 */
  for ( i=1; i<= 192; i++ )
    context->color_spectrum_2[192+i] = 0x00C0C0FF - 0x100*i;       /* 0x00C0BFFF to 0x00C000FF; index = 193 to 384 */
  for ( i=1; i<= 192; i++ )
    context->color_spectrum_2[384+i] = 0x00C000FF + 0x1000000*i;   /* 0x01C000FF to 0xC0C000FF; index = 385 to 576 */
  for ( i=1; i<= 192; i++ )
    context->color_spectrum_2[576+i] = 0xC0C000FF - 0x10000*i;     /* 0xC0BF00FF to 0xC00000FF; index = 577 to 768 */

  for (i = 0; i < context->n_texts; ++i)
    free (context->texts[i].text);
  free (context->texts);
  
  return context;
}


void
gse_context_destroy (struct gse_ctx *context)
{
   free(context->xlabel);
   free(context->ylabel);
   free(context->zlabel);
   free(context->title);
   free(context->background_image_file);

   free(context->font_name);

   int i;
   for (i = 0; i < context->plot_param.nplots; ++i)
     {
       struct plot_parameters *plot = &context->plot_parameters[i];

       free(plot->samples3d.x);
       free(plot->samples3d.y);
       free(plot->samples3d.z);

       free(plot->yhist);
       free(plot->filename);
       free(plot->template);
     }
   free(context->plot_parameters);

   for (i = 0; i < context->n_texts; ++i)
     free (context->texts[i].text);
   free (context->texts);
   
   free (context->images);
   free (context->lines);
   free (context->symbols);
   free (context->rectangles);
   free (context->ellipses);

   if (context->legend)
     free (context->legend->legend_str);
   free (context->legend);

   
   pango_font_description_free(context->font_date_time);

   if (context->legend)
     pango_font_description_free(context->legend->font);
   pango_font_description_free(context->font_text);
   pango_font_description_free(context->font_tick_labels);
   pango_font_description_free(context->font_axis_labels);
   pango_font_description_free(context->font_title);

   free (context);
}
