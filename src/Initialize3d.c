/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
 *
 * Initialize3d.c
 *
 * Calculates quadrants, view azimuth angles, rotation matrices, origin, and
 * axes for 3-dimensional plots.
 *
* Copyright 2017 John Darrington
 * Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
 * http://savannah.gnu.org/projects/gsegrafix
 *
 * This file is part of GSEGrafix, a scientific and engineering plotting program.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#include "Initialize3d.h"
#include "Misc.h"

#include <math.h>
#include "gsegraf.h"

void
Initialize3d (const struct target *target, struct gse_ctx *context)
{
  {
    double Ry[9];
    /* Get view angles */
    double phi   = context->plot_param_3d.phi;     /* view-direction azimuth (deg) from x axis in x-y plane */
    double theta = context->plot_param_3d.theta;   /* view-direction elevation (deg) from x-y plane */

    /* Calculate rotation matrices for general case */
    phi *= deg2rad;
    theta *= deg2rad;

    Ry[0] = cos(-theta);
    Ry[1] = 0.0;
    Ry[2] = -sin(-theta);
    Ry[3] = 0.0;
    Ry[4] = 1.0;
    Ry[5] = 0.0;
    Ry[6] = sin(-theta);
    Ry[7] = 0.0;
    Ry[8] = cos(-theta);

    context->plot_param_3d.Ryz[0] = cos(phi);
    context->plot_param_3d.Ryz[1] = sin(phi);
    context->plot_param_3d.Ryz[2] = 0.0;
    context->plot_param_3d.Ryz[3] = -sin(phi);
    context->plot_param_3d.Ryz[4] = cos(phi);
    context->plot_param_3d.Ryz[5] = 0.0;
    context->plot_param_3d.Ryz[6] = 0.0;
    context->plot_param_3d.Ryz[7] = 0.0;
    context->plot_param_3d.Ryz[8] = 1.0;

    multiply_mm (Ry, context->plot_param_3d.Ryz);
  }

  {
    int i, quadrant;
    double phi, theta;

      double Ry1[9], Ryz1[9];

    /* Define quadrants */
    phi   = context->plot_param_3d.phi;
    theta = context->plot_param_3d.theta;

    phi = fmod(phi, 360.0);
    if ( phi < 0.0 )
      phi = phi + 360.0;
    if ( 0.0 <= phi && phi < 90.0 )
      quadrant = 1;
    else if ( 90.0 <= phi && phi < 180.0 )
      quadrant = 2;
    else if ( 180.0 <= phi && phi < 270.0 )
      quadrant = 3;
    else
      quadrant = 4;

    /* Adjust view azimuth-angle */
    if ( quadrant == 2 )
      phi = phi - 90.0;
    else if ( quadrant == 3 )
      phi = phi - 180.0;
    else if ( quadrant == 4 )
      phi = phi - 270.0;

    /* Save quadrant */
    context->plot_param_3d.quadrant = quadrant;
    
    /* Calculate rotation matrices for quadrant 1 */
    phi *= deg2rad;
    theta *= deg2rad;

    Ry1[0] = cos(-theta);
    Ry1[1] = 0.0;
    Ry1[2] = -sin(-theta);
    Ry1[3] = 0.0;
    Ry1[4] = 1.0;
    Ry1[5] = 0.0;
    Ry1[6] = sin(-theta);
    Ry1[7] = 0.0;
    Ry1[8] = cos(-theta);

    Ryz1[0] = cos(phi);
    Ryz1[1] = sin(phi);
    Ryz1[2] = 0.0;
    Ryz1[3] = -sin(phi);
    Ryz1[4] = cos(phi);
    Ryz1[5] = 0.0;
    Ryz1[6] = 0.0;
    Ryz1[7] = 0.0;
    Ryz1[8] = 1.0;

    multiply_mm (Ry1, Ryz1);


    {
      double axis1[3], axis2[3], axis3[3];
      double axis_length = axis_length_3d (target);
      double origin[3];
      /* Calculate axes */
      axis1[0] = axis_length;
      axis1[1] = 0.0;
      axis1[2] = 0.0;
      multiply_mv (Ryz1, axis1);

      axis2[0] = 0.0;
      axis2[1] = axis_length;
      axis2[2] = 0.0;
      multiply_mv (Ryz1, axis2);

      axis3[0] = 0.0;
      axis3[1] = 0.0;
      axis3[2] = axis_length;
      multiply_mv (Ryz1, axis3);

      /* Calculate origin and plot-box width and height */
      double plot_width  = axis2[1] - axis1[1];
      double plot_height = axis3[2] - axis1[2] - axis2[2];
      origin[0] = 0.0;
      origin[1] = (target->window_width - plot_width)/2.0 - axis1[1];
      origin[2] = (target->window_height - 30 - plot_height)/2.0 + axis3[2];

      /* Save axis and origin */
      for ( i=1; i<=3; i++ )
	{
	  context->plot_param_3d.axis1[i-1] = axis1[i-1];
	  context->plot_param_3d.axis2[i-1] = axis2[i-1];
	  context->plot_param_3d.axis3[i-1] = axis3[i-1];
	  context->plot_param_3d.origin[i-1] = origin[i-1];
	}
    }
  }
}
  
void
get_origin (const struct gse_ctx *context, int quadrant, double *origin)
{
  switch (quadrant)
    {
    case 1:
      origin[0] = context->plot_param_3d.origin[0];
      origin[1] = context->plot_param_3d.origin[1];
      origin[2] = context->plot_param_3d.origin[2];
      break;
    case 2:
      origin[0] = context->plot_param_3d.origin[0] + context->plot_param_3d.axis2[0];
      origin[1] = context->plot_param_3d.origin[1] + context->plot_param_3d.axis2[1];
      origin[2] = context->plot_param_3d.origin[2] - context->plot_param_3d.axis2[2];
      break;
    case 3:
      origin[0] = context->plot_param_3d.origin[0] + context->plot_param_3d.axis1[0] + context->plot_param_3d.axis2[0];
      origin[1] = context->plot_param_3d.origin[1] + context->plot_param_3d.axis1[1] + context->plot_param_3d.axis2[1];
      origin[2] = context->plot_param_3d.origin[2] - context->plot_param_3d.axis1[2] - context->plot_param_3d.axis2[2];
	  
      break;
    case 0:
    case 4:
      origin[0] = context->plot_param_3d.origin[0] + context->plot_param_3d.axis1[0];
      origin[1] = context->plot_param_3d.origin[1] + context->plot_param_3d.axis1[1];
      origin[2] = context->plot_param_3d.origin[2] - context->plot_param_3d.axis1[2];
      break;
    default:
      g_assert_not_reached ();
      break;
    }
}
