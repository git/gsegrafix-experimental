/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
 *
 * DrawDateTime.c
 *
 * Draws date-time string.
 *
* Copyright 2017 John Darrington
 * Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
 * http://savannah.gnu.org/projects/gsegrafix
 *
 * This file is part of GSEGrafix, a scientific and engineering plotting program.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#include "DrawDateTime.h"
#include "gsegraf.h"

#include "gse-cairo.h"

void
DrawDateTime (struct target *target, const struct gse_ctx *context)
{
  if (context->plot_param.page_note_anchor == GSE_ANCHOR_NONE)
    return;

  double xposition = 0;
  double yposition = 0;

  double xshift = 0;
  double yshift = 0;
  
  get_shift_for_anchor (context->plot_param.page_note_anchor,
			target->window_width, target->window_height, &xshift, &yshift);
  
  xposition -= xshift;
  yposition -= yshift;
  
  gse_cairo_render_text (target->cr, context->date_time,
			 xposition, yposition,
			 context->plot_param.page_note_anchor,
			 context->canvas_fg_color,
			 context->font_date_time);
}
