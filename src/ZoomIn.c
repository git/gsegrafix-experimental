/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* ZoomIn.c
*
* Causes plot to zoom to indicated rectangle.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "ZoomIn.h"
#include "AxisLimits.h"
#include "AxesEqual.h"
#include "InitializePlot.h"
#include <math.h>
#include "gsegraf.h"

gboolean 
ZoomIn (struct gse_ctx *context,
	double width, double height,
	double x1_window, double y1_window,
	double x2_window, double y2_window,
	struct tick_labels *original_tick_labels)
{
  if (x1_window == x2_window)
    return FALSE;
  
  if (y1_window == y2_window)
    return FALSE;
     
   /* Get axis minimum and maximum values */
   int nxvalues = context->xtick_labels.nvalues;
   int nyvalues = context->ytick_labels.nvalues;
   int nzvalues = context->ztick_labels.nvalues;
   double xmin = context->xtick_labels.values[0];
   double xmax = context->xtick_labels.values[nxvalues-1];
   double ymin = context->ytick_labels.values[0];
   double ymax = context->ytick_labels.values[nyvalues-1];
   double zmin = context->ztick_labels.values[0];
   double zmax = context->ztick_labels.values[nzvalues-1];
   xmin -= context->xtick_labels.offset1;
   xmax += context->xtick_labels.offset2;
   ymin -= context->ytick_labels.offset1;
   ymax += context->ytick_labels.offset2;
   zmin -= context->ztick_labels.offset1;
   zmax += context->ztick_labels.offset2;

   if (original_tick_labels)
   {
     original_tick_labels[0] = context->xtick_labels;
     original_tick_labels[1] = context->ytick_labels;
     original_tick_labels[2] = context->ztick_labels;
   }
   
   if ( context->plot_param.axes_type == AXES_semilogx ||
        context->plot_param.axes_type == AXES_loglog )
      {
      xmin = floor(xmin);
      xmax = ceil(xmax);
      }

   if ( context->plot_param.axes_type == AXES_semilogy ||
        context->plot_param.axes_type == AXES_loglog )
      {
      ymin = floor(ymin);
      ymax = ceil(ymax);
      }


   cairo_rectangle_t box;
   get_box_from_coords (context, width, height, &box);
   
   /* Calculate new data minimum and maximum values */
   double xscale = box.width/(xmax - xmin);
   double yscale = box.height/(ymax - ymin);
   double x1 = xmin + (x1_window - box.x)/xscale;
   double x2 = xmin + (x2_window - box.x)/xscale;
   double y1 = ymin - (y1_window - (box.y + box.height))/yscale;
   double y2 = ymin - (y2_window - (box.y + box.height))/yscale;

   /* Save new data minimum and maximum values */
   context->data_min_max.xmin = MIN (x1, x2);
   context->data_min_max.xmax = MAX (x1, x2);

   context->data_min_max.ymin = MIN (y1, y2);
   context->data_min_max.ymax = MAX (y1, y2);

   context->data_min_max.zmin = zmin;
   context->data_min_max.zmax = zmax;

   /* Set tick-label offsets to zero */
   context->xtick_labels.offset1 = 0.0;
   context->xtick_labels.offset2 = 0.0;
   context->ytick_labels.offset1 = 0.0;
   context->ytick_labels.offset2 = 0.0;

   int i;
   /* Turn axis limits off */
   for ( i=1; i<=4; i++ )
      context->axis_limits[i-1] = FALSE;
   for ( i=5; i<=6; i++ )
      context->axis_limits[i-1] = TRUE;
   context->plot_param.axis_limits[4] = zmin;
   context->plot_param.axis_limits[5] = zmax;

   /* Draw plot */
   if ( context->plot_param.axes_type == AXES_linear ||
        context->plot_param.axes_type == AXES_semilogx ||
        context->plot_param.axes_type == AXES_semilogy ||
        context->plot_param.axes_type == AXES_loglog )
      {
	AxisLimits (context);
      }

   return TRUE;
}
