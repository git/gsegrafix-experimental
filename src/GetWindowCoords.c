/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* GetWindowCoords.c
*
* Calculate window coordinates from plot coordinates.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "GetWindowCoords.h"
#include "Misc.h"

#include <math.h>
#include "gsegraf.h"
#include "InitializePlot.h"
#include "Initialize3d.h"
#include "PolarPlot.h"

void
GetWindowCoords (const struct target *target, const struct gse_ctx *context, double *plot_coords, double *window_coords )
   {
   /* Declare variables */
     int nx, ny, nz;
     double
          xmin, xmax, ymin, ymax, zmin, zmax, rmin, rmax,
          xscale, yscale, zscale, 
          rscale, xorigin, yorigin, radius, theta, phi,
     axis_length;
   double origin[3], Ry[9], Ryz[9], r[3];

   /* Get minimum and maximum axis values */
   if ( context->plot_param.axes_type == AXES_linear )
      {
      nx = context->xtick_labels.nvalues;
      xmin = context->xtick_labels.values[0];
      xmax = context->xtick_labels.values[nx-1];
      xmin = xmin - context->xtick_labels.offset1;
      xmax = xmax + context->xtick_labels.offset2;
      ny = context->ytick_labels.nvalues;
      ymin = context->ytick_labels.values[0];
      ymax = context->ytick_labels.values[ny-1];
      ymin = ymin - context->ytick_labels.offset1;
      ymax = ymax + context->ytick_labels.offset2;
      }

   else if ( context->plot_param.axes_type == AXES_semilogx )
      {
      nx = context->xtick_labels.nvalues;
      xmin = floor(context->xtick_labels.values[0]);
      xmax = ceil(context->xtick_labels.values[nx-1]);
      nx = roundl(xmax - xmin + 1.0);
      ny = context->ytick_labels.nvalues;
      ymin = context->ytick_labels.values[0];
      ymax = context->ytick_labels.values[ny-1];
      ymin = ymin - context->ytick_labels.offset1;
      ymax = ymax + context->ytick_labels.offset2;
      }

   else if ( context->plot_param.axes_type == AXES_semilogy )
      {
      nx = context->xtick_labels.nvalues;
      xmin = context->xtick_labels.values[0];
      xmax = context->xtick_labels.values[nx-1];
      xmin = xmin - context->xtick_labels.offset1;
      xmax = xmax + context->xtick_labels.offset2;
      ny = context->ytick_labels.nvalues;
      ymin = floor(context->ytick_labels.values[0]);
      ymax = ceil(context->ytick_labels.values[ny-1]);
      ny = roundl(ymax - ymin + 1.0);
      }

   else if ( context->plot_param.axes_type == AXES_loglog )
      {
      nx = context->xtick_labels.nvalues;
      xmin = floor(context->xtick_labels.values[0]);
      xmax = ceil(context->xtick_labels.values[nx-1]);
      nx = roundl(xmax - xmin + 1.0);
      ny = context->ytick_labels.nvalues;
      ymin = floor(context->ytick_labels.values[0]);
      ymax = ceil(context->ytick_labels.values[ny-1]);
      ny = roundl(ymax - ymin + 1.0);
      }

   else if ( context->plot_param.axes_type == AXES_polar )
      {
      ny = context->ytick_labels.nvalues;
      rmin = context->ytick_labels.values[0];
      rmax = context->ytick_labels.values[ny-1];
      rmin = rmin - context->ytick_labels.offset1;
      rmax = rmax + context->ytick_labels.offset2;
      }

   else if ( context->plot_param.axes_type == AXES_3d )
      {
      nx = context->xtick_labels.nvalues;
      ny = context->ytick_labels.nvalues;
      nz = context->ztick_labels.nvalues;
      xmin = context->xtick_labels.values[0];
      xmax = context->xtick_labels.values[nx-1];
      ymin = context->ytick_labels.values[0];
      ymax = context->ytick_labels.values[ny-1];
      zmin = context->ztick_labels.values[0];
      zmax = context->ztick_labels.values[nz-1];
      xmin = xmin - context->xtick_labels.offset1;
      xmax = xmax + context->xtick_labels.offset2;
      ymin = ymin - context->ytick_labels.offset1;
      ymax = ymax + context->ytick_labels.offset2;
      zmin = zmin - context->ztick_labels.offset1;
      zmax = zmax + context->ztick_labels.offset2;
      }

   cairo_rectangle_t box;
   get_box (target, context, &box);
   
   /* Calculate axis scale factors */
   if ( context->plot_param.axes_type == AXES_linear ||
        context->plot_param.axes_type == AXES_semilogx ||
        context->plot_param.axes_type == AXES_semilogy ||
        context->plot_param.axes_type == AXES_loglog )
      {
      xscale = ((box.x + box.width) - box.x) / (xmax - xmin);
      yscale = ((box.y + box.height) - box.y) / (ymax - ymin);
      }

   else if ( context->plot_param.axes_type == AXES_polar )
      {
      polar_get_geometry (target, &xorigin, &yorigin, &radius);
      
      rscale = radius/(rmax - rmin);
      }

   else if ( context->plot_param.axes_type == AXES_3d )
      {
      /* Get view angles */
      phi = context->plot_param_3d.phi;
      theta = context->plot_param_3d.theta;

      get_origin (context, (int) (phi/90.0) + 1, origin);

      /* Calculate rotation matrices */
      phi = phi*deg2rad;
      theta = theta*deg2rad;

      Ry[0] = cos(-theta); Ry[1] = 0.0; Ry[2] = -sin(-theta);
      Ry[3] = 0.0;         Ry[4] = 1.0; Ry[5] = 0.0;
      Ry[6] = sin(-theta); Ry[7] = 0.0; Ry[8] = cos(-theta);

      Ryz[0] = cos(phi);  Ryz[1] = sin(phi); Ryz[2] = 0.0;
      Ryz[3] = -sin(phi); Ryz[4] = cos(phi); Ryz[5] = 0.0;
      Ryz[6] = 0.0;       Ryz[7] = 0.0;      Ryz[8] = 1.0;

      multiply_mm (Ry, Ryz);

      /* Get axis length */
      axis_length = axis_length_3d (target);

      /* Calculate axis scale factors */
      xscale = axis_length/(xmax - xmin);
      yscale = axis_length/(ymax - ymin);
      zscale = axis_length/(zmax - zmin);
      }


   /* Modify coordinates for logarithmic axes */
   if ( context->plot_param.axes_type == AXES_semilogx )
      plot_coords[0] = log10(fabs(plot_coords[0]));

   else if ( context->plot_param.axes_type == AXES_semilogy )
      plot_coords[1] = log10(fabs(plot_coords[1]));

   else if ( context->plot_param.axes_type == AXES_loglog )
      {
      plot_coords[0] = log10(fabs(plot_coords[0]));
      plot_coords[1] = log10(fabs(plot_coords[1]));
      }


   /* Modify coordinates for polar axes */
   if ( context->plot_param.axes_type == AXES_polar )
      plot_coords[0] = plot_coords[0]*deg2rad;


   /* Initialize window coordinates */
   window_coords[0] = -1.0;
   window_coords[1] = -1.0;


   /* Calculate plot coordinates */
   if ( context->plot_param.axes_type == AXES_linear ||
        context->plot_param.axes_type == AXES_semilogx ||
        context->plot_param.axes_type == AXES_semilogy ||
        context->plot_param.axes_type == AXES_loglog )
      {
      if ( xmin <= plot_coords[0] && plot_coords[0] <= xmax &&
           ymin <= plot_coords[1] && plot_coords[1] <= ymax )
         {
         window_coords[0] = box.x + (plot_coords[0] - xmin)*xscale;
         window_coords[1] = (box.y + box.height) - (plot_coords[1] - ymin)*yscale;
         }
      }

   else if ( context->plot_param.axes_type == AXES_polar )
      {
      if ( rmin <= plot_coords[1] && plot_coords[1] <= rmax )
         {
         window_coords[0] = xorigin + (plot_coords[1] - rmin)*cos(plot_coords[0])*rscale;
         window_coords[1] = yorigin - (plot_coords[1] - rmin)*sin(plot_coords[0])*rscale;
         }
      }

   else if ( context->plot_param.axes_type == AXES_3d )
      {
      if ( xmin <= plot_coords[0] && plot_coords[0] <= xmax &&
           ymin <= plot_coords[1] && plot_coords[1] <= ymax &&
           zmin <= plot_coords[2] && plot_coords[2] <= zmax )
         {
         r[0] = (plot_coords[0] - xmin)*xscale;
         r[1] = (plot_coords[1] - ymin)*yscale;
         r[2] = (plot_coords[2] - zmin)*zscale;

         multiply_mv (Ryz, r);

         window_coords[0] = origin[1] + r[1];
         window_coords[1] = origin[2] - r[2];
         }
      }

   return;
   }
