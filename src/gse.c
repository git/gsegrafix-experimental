/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*
  This file is part of GSEGrafix, a scientific and engineering plotting 
  program.

  Copyright (C) 2017 John Darrington

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <config.h>

#define _GNU_SOURCE
#include <math.h>

#include <gtk/gtk.h>
#include <cairo.h>

#include "gsegraf.h"
#include "AxisLimits.h"
#include "InitializePlot.h"
#include "ZoomIn.h"
#include "ZoomOut.h"
#include "gse-cairo.h"

#include <string.h>

#define _(X) X

struct _GseApp
{
  GtkApplication parent_instance;

  GtkPrintSettings *settings;
  GtkWidget *window;
  GtkWidget *drawing_area;
  char *filename;
  GSimpleAction *about_action;
  GSimpleAction *quit_action;
  GSimpleAction *open_action;
  GSimpleAction *save_action;
  GSimpleAction *print_action;

  GSimpleAction *coords_action;
  GSimpleAction *view_action;

  gboolean zoomed;
  
  struct gse_ctx *context;
  char coords_string[256];
  struct data_parser *parser;

  /* Saved tick labels when zooming */
  struct tick_labels saved_tick_labels[3];

  GtkWidget *sb[6];
};

typedef struct _GseApp GseApp;
  
typedef GtkApplicationClass GseAppClass;


G_DEFINE_TYPE (GseApp, gse_app, GTK_TYPE_APPLICATION)

#define GSE_TYPE_APP            (gse_app_get_type ())

#define GSE_APP(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GSE_TYPE_APP, GseApp))


static void
gse_app_init (GseApp *app)
{
  app->context = NULL;
  app->about_action = g_simple_action_new ("about", NULL);
  app->quit_action = g_simple_action_new ("quit", NULL);
  app->open_action = g_simple_action_new ("open", NULL);
  app->save_action = g_simple_action_new ("save", NULL);
  g_simple_action_set_enabled (app->save_action, FALSE);

  app->print_action = g_simple_action_new ("print", NULL);
  g_simple_action_set_enabled (app->print_action, FALSE);

  app->coords_action =
    g_simple_action_new_stateful ("coords",
				  NULL,
				  g_variant_new_boolean (FALSE));


  app->view_action = g_simple_action_new ("view", NULL);

  
  app->settings = NULL;
}

static void gse_app_activate (GApplication *app);

static void
gse_app_open (GApplication *app,
	  GFile **      files,
	  gint          n_files,
	      const gchar        *hint)
{
  GseApp *gse = GSE_APP (app);
  gse->filename = g_file_get_path (files[0]);
  g_application_activate (G_APPLICATION (app));
}


static void
gse_app_class_init (GseAppClass *class)
{
  GApplicationClass *application_class = G_APPLICATION_CLASS (class);
  //  GObjectClass *object_class = G_OBJECT_CLASS (class);
  
  application_class->open = gse_app_open;
  application_class->activate = gse_app_activate;
}

GseApp *
gse_new (void)
{
  GseApp *gse;

  g_set_application_name ("Gse");

  gse = g_object_new (gse_app_get_type (),
		      "application-id", "org.gnu.gse",
		      "flags", G_APPLICATION_HANDLES_OPEN,
		      NULL);

  return gse;
}





GMenu *
create_menu ()
{
  GMenu *menu = g_menu_new ();
  GMenu *view = g_menu_new ();
  g_menu_append (view, _("Display _Coordinates"), "win.coords");
  g_menu_append (view, _("Change _Viewpoint"), "win.view");
  
  GMenu *help = g_menu_new ();
  g_menu_append (help, _("_About"), "win.about");

  GMenu *file = g_menu_new ();
  g_menu_append (file, _("_Open"), "win.open");
  g_menu_append (file, _("_Save"), "win.save");
  g_menu_append (file, _("_Print"), "win.print");
  g_menu_append (file, _("_Quit"), "win.quit");

  g_menu_append_submenu (menu, _("_File"), G_MENU_MODEL (file));
  g_menu_append_submenu (menu, _("_View"), G_MENU_MODEL (view));
  g_menu_append_submenu (menu, _("_Help"), G_MENU_MODEL (help));
  
  return menu;
}

static gboolean version = FALSE;


static gboolean
motion_notify (GtkWidget *widget,
	       GdkEventMotion  *event,
	       gpointer   user_data)
{
  GseApp *gse = GSE_APP (user_data);
  struct gse_ctx *ctx = gse->context;
  GVariant *coords = g_action_get_state (G_ACTION (gse->coords_action));
  if (g_variant_get_boolean (coords))
    {
      cairo_rectangle_t box;
      get_box_from_coords (ctx, gtk_widget_get_allocated_width (widget),
			   gtk_widget_get_allocated_height (widget),
			   &box);
      double x = event->x - box.x;
      double y = box.y + box.height - event->y;

      if ((x < 0)
	  ||
	  (y < 0)
	  ||
	  (x > (box.x + box.width) - box.x)
	  ||
	  (y > (box.y + box.height) - box.y))
	{
	  gse->coords_string[0] = '\0';
	  goto done;
	}

      /* Get minimum and maximum axis values */
      int nxvalues = ctx->xtick_labels.nvalues;
      int nyvalues = ctx->ytick_labels.nvalues;
      int nzvalues = ctx->ztick_labels.nvalues;
      double xmin = ctx->xtick_labels.values[0];
      double xmax = ctx->xtick_labels.values[nxvalues-1];
      double ymin = ctx->ytick_labels.values[0];
      double ymax = ctx->ytick_labels.values[nyvalues-1];
      double zmin = ctx->ztick_labels.values[0];
      double zmax = ctx->ztick_labels.values[nzvalues-1];
      xmin -=  ctx->xtick_labels.offset1;
      xmax +=  ctx->xtick_labels.offset2;
      ymin -=  ctx->ytick_labels.offset1;
      ymax +=  ctx->ytick_labels.offset2;
      zmin -=  ctx->ztick_labels.offset1;
      zmax +=  ctx->ztick_labels.offset2;

      /* Calculate axis scale factors */
      double xscale = box.width / (xmax - xmin);
      double yscale = box.height / (ymax - ymin);

      snprintf (gse->coords_string, 256, _("%g, %g"), x/xscale + xmin,
		y/yscale + ymin);
    }

 done:
  gtk_widget_queue_draw (gse->drawing_area);
  
  return FALSE;
}

gboolean
draw (GtkWidget *w, cairo_t *cr, gpointer ud)
{
  GseApp *gse = GSE_APP (ud);

  if (gse->context == NULL)
    return FALSE;

  GdkRGBA *color;
  GtkStyleContext *sc = gtk_widget_get_style_context (w);
  gtk_style_context_get (sc,
			 GTK_STATE_FLAG_SELECTED,
			 "background-color",
			 &color, NULL);

  GtkCssProvider *cp = gtk_css_provider_new ();
  gchar *css = g_strdup_printf ("* {background-color: rgba(%d, %d, %d, 0.25);}",
				(int) (100.0 * color->red),
				(int) (100.0 * color->green),
				(int) (100.0 * color->blue));

  gtk_css_provider_load_from_data (cp, css, strlen (css), 0);

  gtk_style_context_add_provider (sc, GTK_STYLE_PROVIDER (cp),
				  GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  DrawGraph (cr, gtk_widget_get_allocated_width (w),
	     gtk_widget_get_allocated_height (w),
	     gse->context);
  
  cairo_rectangle_t *zoom = g_object_get_data (G_OBJECT (w), "zoom-area");

  if (zoom)
  {
    gtk_render_background (sc, cr,
		     zoom->x, zoom->y, zoom->width, zoom->height);
  }

  if (gse->coords_string[0])
    {
      gse_cairo_render_text (cr,
			     gse->coords_string,
			     10.0,
			     gtk_widget_get_allocated_height (w) - 10.0,
			     GSE_ANCHOR_SOUTH_WEST,
			     gse->context->canvas_fg_color,
			     gse->context->font_text
			     );
    }

  gtk_style_context_remove_provider (sc, GTK_STYLE_PROVIDER (cp));
  g_free (css);
  g_object_unref (cp);
  gdk_rgba_free (color);
  
  return FALSE;
}

static GOptionEntry entries[] =
{
  { "version", 'v', 0, G_OPTION_ARG_NONE, &version, "Show version and exit", NULL },
  { NULL }
};

void
du (GtkGestureDrag *gest, gdouble offsetx, gdouble offsety, gpointer ud)
{
  GseApp *gse = GSE_APP (ud);
  GtkWidget *drawing_area = gse->drawing_area;

  gdouble startx, starty;
  gtk_gesture_drag_get_start_point (gest, &startx, &starty);

  cairo_rectangle_t *zoom = g_malloc (sizeof (*zoom));
  zoom->x = startx;
  zoom->y = starty;
  zoom->width = offsetx;
  zoom->height = offsety;

  if (offsetx < 0)
    {
      zoom->width = -offsetx;
      zoom->x += offsetx;
    }

  if (offsety < 0)
    {
      zoom->height = -offsety;
      zoom->y += offsety;
    }

  g_object_set_data (G_OBJECT (drawing_area), "zoom-area", zoom);
  gtk_widget_queue_draw (drawing_area);
}

void
de (GtkGestureDrag *gest, gdouble offsetx, gdouble offsety, gpointer ud)
{
  GseApp *gse = GSE_APP (ud);
  GtkWidget *drawing_area = gse->drawing_area;
  struct gse_ctx *context = gse->context;
  gdouble startx, starty;
  gtk_gesture_drag_get_start_point (gest, &startx, &starty);

  g_free (g_object_get_data (G_OBJECT (drawing_area), "zoom-area"));
  g_object_set_data (G_OBJECT (drawing_area), "zoom-area", NULL);

  gse->zoomed = ZoomIn (context,
			gtk_widget_get_allocated_width (drawing_area),
			gtk_widget_get_allocated_height (drawing_area),
			startx, starty, startx + offsetx, starty + offsety,
			gse->zoomed ? NULL : gse->saved_tick_labels);
  
  gtk_widget_queue_draw (drawing_area);
}

gboolean
released (GtkWidget *drawing_area, GdkEventButton *event, gpointer ud)
{
  GseApp *gse = GSE_APP (ud);
  struct gse_ctx *context = gse->context;

  if (event->button != 3)
    return FALSE;
  if (gse->zoomed)
    {
    ZoomOut (context, gse->saved_tick_labels);
    gtk_widget_queue_draw (drawing_area);
    gse->zoomed = FALSE;
    }

  return FALSE;
}

static void
on_about (GSimpleAction *sa, GVariant *parameter, gpointer user_data)
{
  char *authors[] = { "Spencer A. Buckner", "John Darrington", NULL};
  GtkWidget *dialog = gtk_about_dialog_new ();
  gtk_show_about_dialog (GTK_WINDOW (dialog),
			 "comments", _("This is an experimental fork of GNU GseGrafix."),
			 "website", "http://sv.gnu.org/p/gsegrafix-experimental/",
			 "version", PACKAGE_VERSION,
			 "authors", authors,
			 "license-type", GTK_LICENSE_GPL_3_0,
			 NULL);
}


static void
draw_page (GtkPrintOperation *operation,
           GtkPrintContext   *context,
           gint               page_nr,
           gpointer           user_data)
{
  GseApp *gse = GSE_APP (user_data);
  cairo_t *cr = gtk_print_context_get_cairo_context (context);
  gdouble width = gtk_print_context_get_width (context);
  gdouble height = gtk_print_context_get_height (context);

  DrawGraph (cr, width, height, gse->context);
}


static void
begin_print (GtkPrintOperation *op, GtkPrintContext *pctx, gpointer user_data)
{
  gtk_print_operation_set_n_pages (op, 1);
}
  
static void
on_print (GSimpleAction *sa, GVariant *parameter, gpointer user_data)
{
  GseApp *gse = GSE_APP (user_data);

  GtkPrintOperation *print = gtk_print_operation_new ();

  if (gse->settings != NULL)
    gtk_print_operation_set_print_settings (print, gse->settings);

  g_signal_connect (print, "begin_print", G_CALLBACK (begin_print), NULL);
  g_signal_connect (print, "draw_page", G_CALLBACK (draw_page), gse);

  GtkPrintOperationResult res =
    gtk_print_operation_run (print, GTK_PRINT_OPERATION_ACTION_PRINT_DIALOG,
			     GTK_WINDOW (gse->window), NULL);

  if (res == GTK_PRINT_OPERATION_RESULT_APPLY)
    {
      if (gse->settings != NULL)
        g_object_unref (gse->settings);
      gse->settings = g_object_ref (gtk_print_operation_get_print_settings (print));
    }

  g_object_unref (print);
}

static void
on_save (GSimpleAction *sa, GVariant *parameter, gpointer user_data)
{
  GseApp *gse = GSE_APP (user_data);
  GtkWidget *dialog = gtk_file_chooser_dialog_new (_("Save Plot"),
						   GTK_WINDOW (gse->window),
						   GTK_FILE_CHOOSER_ACTION_SAVE,
						   _("Cancel"), GTK_RESPONSE_CANCEL,
						   _("Save"), GTK_RESPONSE_ACCEPT,
						   NULL);

  if (gse->parser->save_filename)
    gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER (dialog), gse->parser->save_filename);
  
  if (GTK_RESPONSE_ACCEPT == gtk_dialog_run (GTK_DIALOG (dialog)))
    {
      gint width = gtk_widget_get_allocated_width (gse->drawing_area);
      gint height = gtk_widget_get_allocated_height (gse->drawing_area);

      cairo_surface_t *surface =
	cairo_image_surface_create (CAIRO_FORMAT_RGB24,
				    width, height);

      /* Set the background to white */
      cairo_t *cr = cairo_create (surface);
      cairo_set_source_rgb (cr, 1.0, 1.0, 1.0);
      cairo_paint (cr);

      DrawGraph (cr, width, height, gse->context);
      cairo_surface_write_to_png (surface,
				  gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog)));
    }
  
  gtk_widget_destroy (dialog);
}

static void
gse_message_handler (const char *message, gpointer aux)
{
  GseApp *gse = GSE_APP (aux);
  GtkWidget *dialog = gtk_message_dialog_new (GTK_WINDOW (gse->window),
					      GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE,
					      message);
  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
}



static void
on_open (GSimpleAction *sa, GVariant *parameter, gpointer user_data)
{
  GseApp *gse = GSE_APP (user_data);
  GtkWidget *dialog = gtk_file_chooser_dialog_new (_("Open Parameter File"),
						   GTK_WINDOW (gse->window),
						   GTK_FILE_CHOOSER_ACTION_OPEN,
						   _("Cancel"), GTK_RESPONSE_CANCEL,
						   _("Open"), GTK_RESPONSE_ACCEPT,
						   NULL);
  if (GTK_RESPONSE_ACCEPT == gtk_dialog_run (GTK_DIALOG (dialog)))
    {
      gchar *filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

      if (filename)
	{
	  if (gse->context)
	    gse_context_destroy (gse->context);

	  destroy_parser (gse->parser);
	  gse->context = InitializePlot (filename, &gse->parser, gse_message_handler, gse);
	  g_simple_action_set_enabled (gse->save_action, TRUE);
	  g_simple_action_set_enabled (gse->print_action, TRUE);
	  gtk_widget_queue_draw (gse->drawing_area);
	}
    }

  gtk_widget_destroy (dialog);
}

static void
on_azimuth_change (GtkSpinButton *sb, GseApp *gse)
{
  gdouble value = gtk_spin_button_get_value (sb);

  gse->context->plot_param_3d.phi = value;
  gtk_widget_queue_draw (gse->drawing_area);
}

static void
on_elevation_change (GtkSpinButton *sb, GseApp *gse)
{
  gdouble value = gtk_spin_button_get_value (sb);

  gse->context->plot_param_3d.theta = value;
  gtk_widget_queue_draw (gse->drawing_area);
}


static void
on_limit_change (GtkSpinButton *sb, GseApp *gse)
{
  for (int i = 0; i < 6; ++i)
    {
      gse->context->plot_param.axis_limits[i] =
	gtk_spin_button_get_value (GTK_SPIN_BUTTON (gse->sb[i]));
      gse->context->axis_limits[i] = TRUE;
    }

  AxisLimits (gse->context);
  gtk_widget_queue_draw (gse->drawing_area);
}

/* Ensure that upper limit spin buttons always display a value
   not less than its corresponding lower limit spin button. */
static void
gang_limits (GtkSpinButton *sb, GtkSpinButton **spinners)
{
  gdouble value = gtk_spin_button_get_value (sb);

  gint index = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (sb), "index"));
  g_assert (sb == spinners[index]);

  if (index % 2)
    {
      /* sb is an upper limit spinner */
      GtkSpinButton *partner = spinners[index-1];
      GtkAdjustment *adj = gtk_spin_button_get_adjustment (partner);
      gdouble step = gtk_adjustment_get_step_increment (adj);
      double pvalue = gtk_spin_button_get_value (partner);
      if (value - step < pvalue)
	gtk_spin_button_set_value (partner, value - step);
    }
  else
    {
      /* sb is a lower limit spinner */
      GtkSpinButton *partner = spinners[index+1];
      GtkAdjustment *adj = gtk_spin_button_get_adjustment (partner);
      gdouble step = gtk_adjustment_get_step_increment (adj);
      double pvalue = gtk_spin_button_get_value (partner);
      if (value + step > pvalue)
	gtk_spin_button_set_value (partner, value + step);
    }
}


static void
on_viewpoint (GSimpleAction *sa, GVariant *parameter, gpointer user_data)
{
  GseApp *gse = GSE_APP (user_data);

  double original_theta = gse->context->plot_param_3d.theta;
  double original_phi  = gse->context->plot_param_3d.phi;
  
  GtkWidget *dialog =
    gtk_dialog_new_with_buttons (_("Viewpoint"),
				 GTK_WINDOW (gse->window),
				 GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
				 _("_OK"),
				 GTK_RESPONSE_ACCEPT,
				 _("_Cancel"),
				 GTK_RESPONSE_REJECT,
				 NULL);

  GtkWidget *ca = gtk_dialog_get_content_area (GTK_DIALOG (dialog));

  {
    int dimensions = 3;
    GtkWidget *frame = gtk_frame_new (_("Axis Limits"));
    GtkWidget *grid = gtk_grid_new ();
    gtk_grid_set_column_homogeneous (GTK_GRID (grid), FALSE);
    gtk_grid_set_row_homogeneous (GTK_GRID (grid), TRUE);
    gtk_grid_set_column_spacing (GTK_GRID (grid), 5);
    gtk_grid_attach (GTK_GRID (grid), gtk_label_new (_("Lower")), 1, 0, 1, 1);
    gtk_grid_attach (GTK_GRID (grid), gtk_label_new (_("Upper")), 2, 0, 1, 1);

    if (gse->context->plot_param.axes_type == AXES_polar)
      {
	char string[7];
	memset (string, '\0', 7);
	const gunichar rho = 0x03C1; /* GREEK SMALL LETTER RHO */
	g_unichar_to_utf8 (rho, string);
	gtk_grid_attach (GTK_GRID (grid), gtk_label_new (string), 0, 2, 1, 1);
	dimensions = 2;
      }
    else
      {
	gtk_grid_attach (GTK_GRID (grid), gtk_label_new (_("x")), 0, 1, 1, 1);
	gtk_grid_attach (GTK_GRID (grid), gtk_label_new (_("y")), 0, 2, 1, 1);
	gtk_grid_attach (GTK_GRID (grid), gtk_label_new (_("z")), 0, 3, 1, 1);
      }
    

    for (int i = 0; i < dimensions * 2; ++i)
      {
	gse->sb[i] = gtk_spin_button_new_with_range (-DBL_MAX, +DBL_MAX, 1);
	gtk_grid_attach (GTK_GRID (grid), gse->sb[i], 1 + i % 2, 1 + i / 2, 1, 1);
	g_object_set_data (G_OBJECT (gse->sb[i]), "index", GINT_TO_POINTER (i));
	g_signal_connect (gse->sb[i], "value-changed", G_CALLBACK (gang_limits), gse->sb);
      }

    gtk_spin_button_set_value (GTK_SPIN_BUTTON (gse->sb[0]), gse->context->data_min_max.xmin);
    gtk_spin_button_set_value (GTK_SPIN_BUTTON (gse->sb[1]), gse->context->data_min_max.xmax);
    gtk_spin_button_set_value (GTK_SPIN_BUTTON (gse->sb[2]), gse->context->data_min_max.ymin);
    gtk_spin_button_set_value (GTK_SPIN_BUTTON (gse->sb[3]), gse->context->data_min_max.ymax);
    gtk_spin_button_set_value (GTK_SPIN_BUTTON (gse->sb[4]), gse->context->data_min_max.zmin);
    gtk_spin_button_set_value (GTK_SPIN_BUTTON (gse->sb[5]), gse->context->data_min_max.zmax);

    for (int i = 0; i < dimensions; ++i)
      {
	double v0 = gtk_spin_button_get_value (GTK_SPIN_BUTTON (gse->sb[i * 2]));
	double v1 = gtk_spin_button_get_value (GTK_SPIN_BUTTON (gse->sb[i * 2 + 1]));
	double digits = floor(log10(fabs(v1 - v0))) - 1.0;
	double step = exp10 (digits);
	GtkAdjustment *adj = gtk_spin_button_get_adjustment (GTK_SPIN_BUTTON (gse->sb[i * 2]));
	gtk_adjustment_set_step_increment (adj, step);
	adj = gtk_spin_button_get_adjustment (GTK_SPIN_BUTTON (gse->sb[i * 2 + 1]));
	gtk_adjustment_set_step_increment (adj, step);
	gtk_spin_button_set_digits (GTK_SPIN_BUTTON (gse->sb[i * 2]), MAX (0, -digits));
	gtk_spin_button_set_digits (GTK_SPIN_BUTTON (gse->sb[i * 2 + 1]), MAX (0, -digits));
      }
    
    for (int i = 0; i < dimensions * 2; ++i)
      g_signal_connect (gse->sb[i], "value-changed", G_CALLBACK (on_limit_change), gse);
    g_object_set (grid, "margin", 3, NULL);
    gtk_container_add (GTK_CONTAINER (frame), grid);
    g_object_set (frame, "margin", 5, NULL);
    gtk_box_pack_start (GTK_BOX (ca), frame, TRUE, TRUE, 5);
  }

  if (gse->context->plot_param.axes_type == AXES_polar)
    {
      gtk_widget_set_no_show_all (gse->sb[0], TRUE);
      gtk_widget_set_no_show_all (gse->sb[1], TRUE);
    }
  
  if (gse->context->plot_param.axes_type == AXES_3d)
  {
    /* Set up the viewing angle widgets */
    GtkWidget *frame = gtk_frame_new (_("Observation Angle"));
    GtkWidget *grid = gtk_grid_new ();
    gtk_grid_set_column_homogeneous (GTK_GRID (grid), FALSE);
    gtk_grid_set_row_homogeneous (GTK_GRID (grid), TRUE);
    gtk_grid_set_column_spacing (GTK_GRID (grid), 5);
    {
      GtkWidget *label = gtk_label_new (_("Azimuth"));
      gtk_grid_attach (GTK_GRID (grid), label, 0, 0, 1, 1);
      GtkWidget *sb = gtk_spin_button_new_with_range (0, 90, 1);
      gtk_grid_attach (GTK_GRID (grid), sb, 1, 0, 1, 1);
      gtk_spin_button_set_value (GTK_SPIN_BUTTON (sb),
				 gse->context->plot_param_3d.phi);
      g_signal_connect (sb, "value-changed", G_CALLBACK (on_azimuth_change), gse);
    }

    {
      GtkWidget *label = gtk_label_new (_("Elevation"));
      gtk_grid_attach (GTK_GRID (grid), label, 0, 1, 1, 1);

      GtkWidget *sb = gtk_spin_button_new_with_range (0, 90, 1);
      gtk_grid_attach (GTK_GRID (grid), sb, 1, 1, 1, 1);

      gtk_spin_button_set_value (GTK_SPIN_BUTTON (sb),
				 gse->context->plot_param_3d.theta);
      g_signal_connect (sb, "value-changed", G_CALLBACK (on_elevation_change), gse);
    }

    g_object_set (grid, "margin", 3, NULL);
    gtk_container_add (GTK_CONTAINER (frame), grid);
    g_object_set (frame, "margin", 5, NULL);
    gtk_box_pack_start (GTK_BOX (ca), frame, TRUE, TRUE, 5);
  }
  gtk_widget_show_all (ca);

  if (GTK_RESPONSE_REJECT == gtk_dialog_run (GTK_DIALOG (dialog)))
    {
      gse->context->plot_param_3d.theta = original_theta;
      gse->context->plot_param_3d.phi = original_phi;
      gtk_widget_queue_draw (gse->drawing_area);
    }
  
  gtk_widget_destroy (dialog);
}

static void
gse_app_activate (GApplication *app)
{
  GseApp *gse = GSE_APP (app);

  g_signal_connect (gse->about_action, "activate", G_CALLBACK (on_about), NULL);
  g_signal_connect (gse->open_action,  "activate", G_CALLBACK (on_open), gse);
  g_signal_connect (gse->save_action,  "activate", G_CALLBACK (on_save), gse);
  g_signal_connect (gse->print_action, "activate", G_CALLBACK (on_print), gse);

  g_signal_connect (gse->view_action,  "activate", G_CALLBACK (on_viewpoint), gse);

  g_signal_connect_swapped (gse->quit_action, "activate", G_CALLBACK (g_application_quit), app);

  gse->window = gtk_application_window_new (GTK_APPLICATION (app));

  g_action_map_add_action (G_ACTION_MAP (gse->window), G_ACTION (gse->about_action));
  g_action_map_add_action (G_ACTION_MAP (gse->window), G_ACTION (gse->quit_action));
  g_action_map_add_action (G_ACTION_MAP (gse->window), G_ACTION (gse->coords_action));
  g_action_map_add_action (G_ACTION_MAP (gse->window), G_ACTION (gse->view_action));
  g_action_map_add_action (G_ACTION_MAP (gse->window), G_ACTION (gse->open_action));
  g_action_map_add_action (G_ACTION_MAP (gse->window), G_ACTION (gse->save_action));
  g_action_map_add_action (G_ACTION_MAP (gse->window), G_ACTION (gse->print_action));
  
  g_signal_connect_swapped (gse->window, "destroy", G_CALLBACK (g_application_quit), app);

  GtkWidget *vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
  GtkWidget *menu_bar = gtk_menu_bar_new_from_model (G_MENU_MODEL (create_menu ()));
  gse->drawing_area = gtk_drawing_area_new ();

  gtk_box_pack_start (GTK_BOX (vbox), menu_bar, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), gse->drawing_area, TRUE, TRUE, 0);

  gtk_container_add (GTK_CONTAINER (gse->window), vbox);

  if (gse->filename)
    {
      gse->context = InitializePlot(gse->filename, &gse->parser, gse_message_handler, gse);
      g_simple_action_set_enabled (gse->save_action, TRUE);
      g_simple_action_set_enabled (gse->print_action, TRUE);
    }

  GtkGesture *drag = gtk_gesture_drag_new (gse->drawing_area);
  g_signal_connect (drag, "drag-update", G_CALLBACK (du), gse);
  g_signal_connect (drag, "drag-end", G_CALLBACK (de), gse);

  g_signal_connect (gse->drawing_area, "button-release-event",
		    G_CALLBACK (released), gse);
  
  g_signal_connect (gse->drawing_area, "draw", G_CALLBACK (draw), gse);

  g_signal_connect (gse->drawing_area, "motion-notify-event",
		    G_CALLBACK (motion_notify), gse);
  gtk_widget_add_events (gse->drawing_area, GDK_POINTER_MOTION_MASK);

  gtk_widget_show_all (gse->window);
}


int
main (int argc, char **argv)
{
  GError *error = NULL;
  GOptionContext *optctx = g_option_context_new ("- test gsegrafix library");
  g_option_context_add_main_entries (optctx, entries, NULL);
  if (!g_option_context_parse (optctx, &argc, &argv, &error))
    {
      g_print ("option parsing failed: %s\n", error->message);
      return 1;
    }
  g_option_context_free (optctx);

  if (version)
    {
      g_print ("Version: %s\n", PACKAGE_VERSION);
      return 0;
    }

  GseApp *app = gse_new ();
  int status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);
  return status;
}
