/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*
  This file is part of GSEGrafix, a scientific and engineering plotting 
  program.

  Copyright (C) 2017 John Darrington

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <glib.h>

#include "gsegraf.h"

struct sample_iterator;
struct target;

void DrawLines2d (struct target *, const struct gse_ctx *context,
	     struct sample_iterator *it,
	     const double *lower, const double *upper,
	     double xscale, double yscale,
	     guint32 color, unsigned int line_width, enum line_texture texture);


void DrawLinesPolar (struct target *, const struct gse_ctx *context,
		struct sample_iterator *it,
		double xorigin, double yorigin,
		double rmin, double rmax, double rscale,
		guint32 color, unsigned int line_width, enum line_texture texture);

void DrawLines3d (struct target *, const struct gse_ctx *context, 
	     struct sample_iterator *it,
	     const double *origin, const double *Ryz,
	     const double *lower, const double *upper,
	     double xscale, double yscale, double zscale, guint32 color,
	     unsigned int line_width, enum line_texture texture);

