/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
 *
 * PlotRectangles.c
 *
 * Plots rectangles.
 *
 * Copyright 2017 John Darrington
 * Copyright © 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
 * http://savannah.gnu.org/projects/gsegrafix
 *
 * This file is part of GSEGrafix, a scientific and engineering plotting program.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#include "PlotRectangles.h"
#include "DrawLines.h"
#include "data-iterator.h"
#include <math.h>
#include "gsegraf.h"
#include "InitializePlot.h"

void
PlotRectangles (struct target *target, const struct gse_ctx *context)
{
  if (context->plot_param.axes_type != AXES_linear)
    return;

  double lower[3], upper[3];

  int nx = context->xtick_labels.nvalues;
  lower[0] = context->xtick_labels.values[0];
  upper[0] = context->xtick_labels.values[nx-1];
  int ny = context->ytick_labels.nvalues;
  lower[1] = context->ytick_labels.values[0];
  upper[1] = context->ytick_labels.values[ny-1];
  lower[0] = lower[0] - context->xtick_labels.offset1;
  upper[0] = upper[0] + context->xtick_labels.offset2;
  lower[1] = lower[1] - context->ytick_labels.offset1;
  upper[1] = upper[1] + context->ytick_labels.offset2;

  cairo_rectangle_t box;
  get_box (target, context, &box);
  
  double xscale = box.width / (upper[0] - lower[0]);
  double yscale = box.height / (upper[1] - lower[1]);

  /* Draw rectangles */
  int i;
  for (i = 0; i < context->n_rectangles; ++i)
    {
      const struct shape_spec *rs = context->rectangles + i;
      double R[4];
      struct gse_datum data[5];
	      
      /* Calculate rectangle centered at offset point */
      double a = rs->width/2.0;    /* half long dimension */
      double b = rs->height/2.0;   /* half short dimension */
	      
      /* Calculate rotation matrix elements; */
      /* R elements labeled: [ 0 1; 2 3 ]    */
      R[0] =  cos(rs->angle);
      R[1] =  sin(rs->angle);
      R[2] = -sin(rs->angle);
      R[3] =  cos(rs->angle);

      /* Calculate rotated rectangle */
      data[0].x = rs->x0 + (- R[0]*a - R[1]*b);
      data[0].y = rs->y0 - (- R[2]*a - R[3]*b);
      data[1].x = rs->x0 + (+ R[0]*a - R[1]*b);
      data[1].y = rs->y0 - (+ R[2]*a - R[3]*b);
      data[2].x = rs->x0 + (+ R[0]*a + R[1]*b);
      data[2].y = rs->y0 - (+ R[2]*a + R[3]*b);
      data[3].x = rs->x0 + (- R[0]*a + R[1]*b);
      data[3].y = rs->y0 - (- R[2]*a + R[3]*b);
      data[4].x = data[0].x;
      data[4].y = data[0].y;

      struct sample_iterator *it = it_create_from_data (data, 5);
      DrawLines2d (target, context, 
		  it,
		  lower, upper,
		  xscale, yscale,
		  rs->line_color, rs->line_width, rs->texture);

      it_destroy (it);
    }
}
