/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* DrawContours3d.c
*
* Plots 3-dimensional contour data for polygons that are not truncated.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "DrawContours3d.h"
#include "Misc.h"
#include <math.h>
#include "gsegraf.h"
#include "Initialize3d.h"
#include "gse-cairo.h"

void
DrawContours3d (struct target *target, const struct gse_ctx *context, int icontour,
		     const double *xpoints, const double *ypoints,
		     const double *zpoints)
{
     int i, ic, nc, nxvalues, nyvalues, nzvalues;
   double axis_length;
   double origin[3], Ryz[9],
          xmin, xmax, ymin, ymax, zmin, zmax,
          xscale, yscale, zscale,
          xtest[8], ytest[8], ztest[8],
          zmin_polygon, zmax_polygon, contour,
          x1, y1, z1, x2, y2, z2, r1[3], r2[3],
          length, dx, dy;
   GseCairoPoints *points;

   get_origin (context, context->plot_param_3d.quadrant, origin);

   /* Get rotation matrix */
   for ( i=1; i<=9; i++ )
      Ryz[i-1] = context->plot_param_3d.Ryz[i-1];

   /* Get axis length */
   axis_length = axis_length_3d (target);

   /* Get minimum and maximum axis values */
   nxvalues = context->xtick_labels.nvalues;
   nyvalues = context->ytick_labels.nvalues;
   nzvalues = context->ztick_labels.nvalues;
   xmin = context->xtick_labels.values[0];
   xmax = context->xtick_labels.values[nxvalues-1];
   ymin = context->ytick_labels.values[0];
   ymax = context->ytick_labels.values[nyvalues-1];
   zmin = context->ztick_labels.values[0];
   zmax = context->ztick_labels.values[nzvalues-1];
   xmin = xmin - context->xtick_labels.offset1;
   xmax = xmax + context->xtick_labels.offset2;
   ymin = ymin - context->ytick_labels.offset1;
   ymax = ymax + context->ytick_labels.offset2;
   zmin = zmin - context->ztick_labels.offset1;
   zmax = zmax + context->ztick_labels.offset2;


   /* Calculate axis scale factors */
   xscale = axis_length/(xmax - xmin);
   yscale = axis_length/(ymax - ymin);
   zscale = axis_length/(zmax - zmin);


   /* Define polygon test points */
   for ( i=1; i<=4; i++ )
      {
      xtest[i-1] = xpoints[i-1];
      ytest[i-1] = ypoints[i-1];
      ztest[i-1] = zpoints[i-1];
      xtest[i+3] = xpoints[i-1];
      ytest[i+3] = ypoints[i-1];
      ztest[i+3] = zpoints[i-1];
      }

   /* Find polygon zmin and zmax */
   zmin_polygon = min(4, zpoints);
   zmax_polygon = max(4, zpoints);

   /* Calculate number of contour lines */
   if ( context->ncontours < 2 )
      nc = 2*nzvalues - 1;
   else
      nc = context->ncontours;

   /* Calculate contour-line coordinates */
   points = gse_cairo_points_new(2);
   for ( ic=1; ic<=nc; ic++ )
      {
      /* Calculate contour value */
      contour = zmin + (ic-1)*(zmax - zmin)/(nc - 1);

      /* Check if all points equal */
      if ( ztest[0] == ztest[1] && ztest[0] == ztest[2] && ztest[0] == ztest[3] )
         continue;

      /* Check if contour line goes through polygon */
      if ( zmin_polygon < contour && contour < zmax_polygon )
         {
         /* Find coordinates of intersecting contour line */
	   gboolean flag = FALSE;
         for ( i=1; i<=4; i++ )
            {
            /* Case 1: two adjacent sides */
            if ( ztest[i-1] < contour && ztest[i] > contour && ztest[i+1] < contour && ztest[i+2] < contour )
               {
               x1 = interp_rect(ztest[i-1], ztest[i], xtest[i-1], xtest[i], contour);
               y1 = interp_rect(ztest[i-1], ztest[i], ytest[i-1], ytest[i], contour);
               z1 = contour;
               x2 = interp_rect(ztest[i], ztest[i+1], xtest[i], xtest[i+1], contour);
               y2 = interp_rect(ztest[i], ztest[i+1], ytest[i], ytest[i+1], contour);
               z2 = contour;
               flag = TRUE;;
               break;
               }

            else if ( ztest[i-1] > contour && ztest[i] < contour && ztest[i+1] > contour && ztest[i+2] > contour )
               {
               x1 = interp_rect(ztest[i-1], ztest[i], xtest[i-1], xtest[i], contour);
               y1 = interp_rect(ztest[i-1], ztest[i], ytest[i-1], ytest[i], contour);
               z1 = contour;
               x2 = interp_rect(ztest[i], ztest[i+1], xtest[i], xtest[i+1], contour);
               y2 = interp_rect(ztest[i], ztest[i+1], ytest[i], ytest[i+1], contour);
               z2 = contour;
               flag = TRUE;;
               break;
               }


            /* Case 2: two opposite sides */
            else if ( ztest[i-1] < contour && ztest[i] > contour && ztest[i+1] > contour && ztest[i+2] < contour )
               {
               x1 = interp_rect(ztest[i-1], ztest[i], xtest[i-1], xtest[i], contour);
               y1 = interp_rect(ztest[i-1], ztest[i], ytest[i-1], ytest[i], contour);
               z1 = contour;
               x2 = interp_rect(ztest[i+1], ztest[i+2], xtest[i+1], xtest[i+2], contour);
               y2 = interp_rect(ztest[i+1], ztest[i+2], ytest[i+1], ytest[i+2], contour);
               z2 = contour;
               flag = TRUE;;
               break;
               }

            else if ( ztest[i-1] > contour && ztest[i] < contour && ztest[i+1] < contour && ztest[i+2] > contour )
               {
               x1 = interp_rect(ztest[i-1], ztest[i], xtest[i-1], xtest[i], contour);
               y1 = interp_rect(ztest[i-1], ztest[i], ytest[i-1], ytest[i], contour);
               z1 = contour;
               x2 = interp_rect(ztest[i+1], ztest[i+2], xtest[i+1], xtest[i+2], contour);
               y2 = interp_rect(ztest[i+1], ztest[i+2], ytest[i+1], ytest[i+2], contour);
               z2 = contour;
               flag = TRUE;;
               break;
               }


            /* Case 3: both pairs opposite sides */
            else if ( ztest[i-1] < contour && ztest[i] > contour && ztest[i+1] < contour && ztest[i+2] > contour )
               {
               x1 = interp_rect(ztest[i-1], ztest[i], xtest[i-1], xtest[i], contour);
               y1 = interp_rect(ztest[i-1], ztest[i], ytest[i-1], ytest[i], contour);
               z1 = contour;
               x2 = interp_rect(ztest[i+1], ztest[i+2], xtest[i+1], xtest[i+2], contour);
               y2 = interp_rect(ztest[i+1], ztest[i+2], ytest[i+1], ytest[i+2], contour);
               z2 = contour;
               flag = TRUE;;
               break;
               }

            else if ( ztest[i-1] > contour && ztest[i] < contour && ztest[i+1] > contour && ztest[i+2] < contour )
               {
               x1 = interp_rect(ztest[i-1], ztest[i], xtest[i-1], xtest[i], contour);
               y1 = interp_rect(ztest[i-1], ztest[i], ytest[i-1], ytest[i], contour);
               z1 = contour;
               x2 = interp_rect(ztest[i+1], ztest[i+2], xtest[i+1], xtest[i+2], contour);
               y2 = interp_rect(ztest[i+1], ztest[i+2], ytest[i+1], ytest[i+2], contour);
               z2 = contour;
               flag = TRUE;;
               break;
               }


            /* Case 4: one point */
            else if ( ztest[i-1] == contour && ztest[i] > contour && ztest[i+1] < contour && ztest[i+2] < contour )
               {
               x1 = xtest[i-1];
               y1 = ytest[i-1];
               z1 = contour;
               x2 = interp_rect(ztest[i], ztest[i+1], xtest[i], xtest[i+1], contour);
               y2 = interp_rect(ztest[i], ztest[i+1], ytest[i], ytest[i+1], contour);
               z2 = contour;
               flag = TRUE;;
               break;
               }

            else if ( ztest[i-1] == contour && ztest[i] < contour && ztest[i+1] > contour && ztest[i+2] > contour )
               {
               x1 = xtest[i-1];
               y1 = ytest[i-1];
               z1 = contour;
               x2 = interp_rect(ztest[i], ztest[i+1], xtest[i], xtest[i+1], contour);
               y2 = interp_rect(ztest[i], ztest[i+1], ytest[i], ytest[i+1], contour);
               z2 = contour;
               flag = TRUE;;
               break;
               }

            else if ( ztest[i-1] == contour && ztest[i] < contour && ztest[i+1] < contour && ztest[i+2] > contour )
               {
               x1 = xtest[i-1];
               y1 = ytest[i-1];
               z1 = contour;
               x2 = interp_rect(ztest[i+1], ztest[i+2], xtest[i+1], xtest[i+2], contour);
               y2 = interp_rect(ztest[i+1], ztest[i+2], ytest[i+1], ytest[i+2], contour);
               z2 = contour;
               flag = TRUE;;
               break;
               }

            else if ( ztest[i-1] == contour && ztest[i] > contour && ztest[i+1] > contour && ztest[i+2] < contour )
               {
               x1 = xtest[i-1];
               y1 = ytest[i-1];
               z1 = contour;
               x2 = interp_rect(ztest[i+1], ztest[i+2], xtest[i+1], xtest[i+2], contour);
               y2 = interp_rect(ztest[i+1], ztest[i+2], ytest[i+1], ytest[i+2], contour);
               z2 = contour;
               flag = TRUE;;
               break;
               }


            /* Case 5: two adjacent points */
            else if ( ztest[i-1] == contour && ztest[i] == contour && ztest[i+1] != contour && ztest[i+2] != contour )
               {
               x1 = xtest[i-1];
               y1 = ytest[i-1];
               z1 = contour;
               x2 = xtest[i];
               y2 = ytest[i];
               z2 = contour;
               flag = TRUE;;
               break;
               }


            /* Case 6: two opposite points */
            else if ( ztest[i-1] == contour && ztest[i] != contour && ztest[i+1] == contour && ztest[i+2] != contour )
               {
               x1 = xtest[i-1];
               y1 = ytest[i-1];
               z1 = contour;
               x2 = xtest[i+1];
               y2 = ytest[i+1];
               z2 = contour;
               flag = TRUE;;
               break;
               }


            /* Case 7: three points */
            else if ( ztest[i-1] == contour && ztest[i] == contour && ztest[i+1] == contour && ztest[i+2] != contour )
               {
               x1 = xtest[i-1];
               y1 = ytest[i-1];
               z1 = contour;
               x2 = xtest[i+1];
               y2 = ytest[i+1];
               z2 = contour;
               flag = TRUE;;
               break;
               }
            }

	 g_assert (flag);

         /* Calculate contour-line position vectors */
         r1[0] = (x1 - xmin)*xscale;
         r1[1] = (y1 - ymin)*yscale;
         r1[2] = (z1 - zmin)*zscale;

         r2[0] = (x2 - xmin)*xscale;
         r2[1] = (y2 - ymin)*yscale;
         r2[2] = (z2 - zmin)*zscale;


         /* Rotate contour-line position vectors */
         multiply_mv (Ryz, r1);
         multiply_mv (Ryz, r2);


         /* Draw contour line */
         x1 = origin[1] + r1[1];
         y1 = origin[2] - r1[2];
         x2 = origin[1] + r2[1];
         y2 = origin[2] - r2[2];
         length = sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));
         dx = (x2 - x1)/length;
         dy = (y2 - y1)/length;
         points->coords[0] = x1 - dx;
         points->coords[1] = y1 - dy;
         points->coords[2] = x2 + dx;
         points->coords[3] = y2 + dy;
	 gse_cairo_render_line (target->cr, points, 2, 1,
				context->plot_parameters[icontour-1].contourcolors);
	 
         }
      }

   gse_cairo_points_unref(points);
   }
