/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*
  This file is part of GSEGrafix, a scientific and engineering plotting 
  program.

  Copyright (C) 2017 John Darrington

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gsegraf.h"
#include "memory-iterator.h"
#include "data-iterator.h"
#include <stdlib.h>
#include <Misc.h>
#include <string.h>

struct mem_sample_iterator
{
  struct sample_iterator base;
  const struct gse_datum *data;
  int ndata;
  int i;
};


void
mem_destroy (struct sample_iterator *it)
{
  free (it);
}

struct sample_iterator *
mem_next_segment (struct sample_iterator *in_)
{
  it_destroy (in_);
  return NULL;
}

struct sample_iterator *
mem_clone (const struct sample_iterator *in)
{
  if (in == NULL)
    return NULL;
  struct mem_sample_iterator *it = xzalloc (sizeof *it);

  memcpy (it, in, sizeof *it);
  return (struct sample_iterator *) it;
}

bool
mem_last (const struct sample_iterator *it_)
{
  const struct mem_sample_iterator *it = (const struct mem_sample_iterator *) it_;
  return it->i >= it->ndata;
}

void
mem_next (struct sample_iterator *it_)
{
  struct mem_sample_iterator *it = (struct mem_sample_iterator *) it_;
  it->i++;
}

const struct gse_datum *
mem_get (const struct sample_iterator *it_)
{
  const struct mem_sample_iterator *it = (const struct mem_sample_iterator *) it_;
  
  if (it_last (it_))
    return NULL;
  return it->data + it->i;
}


struct sample_iterator *
it_create_from_data (const struct gse_datum *data, int ndata)
{
  struct mem_sample_iterator *it = xzalloc (sizeof *it);
  it->base.clone = mem_clone;
  it->base.next_segment = mem_next_segment;
  it->base.destroy = mem_destroy;
  it->base.last = mem_last;
  it->base.next = mem_next;
  it->base.get = mem_get;

  it->data = data;
  it->ndata = ndata;
  it->i = 0;
  return (struct sample_iterator *) it;
}
