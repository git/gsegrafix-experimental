/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* InitializePlot.c
*
* Contains functions:
*    InitializePlot
*    PlotBox
*
* Initializes plot variables, reads plot-parameter file, checks plot-parameter
* values, calculates various plot variables, and analyzes various plot
* variables.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "InitializePlot.h"

#include "ReadParamFile.h"
#include "CheckParamData.h"
#include "FileRead.h"
#include "DataMinMax.h"
#include "AxisLimits.h"
#include "AxesEqual.h"
#include "Misc.h"
#include "PolarPlot.h"

#include <math.h>
#include "gsegraf.h"

#include <string.h>
#include <stdlib.h>

struct gse_ctx *gse_context_create (void);

void
destroy_parser (struct data_parser *p)
{
  if (!p)
    return;
  free (p->save_filename);
  free (p);
}

#include <glib/gprintf.h>

static void
emit_warning (message_handler hdlr, gpointer aux, const char *fmt, ...)
{
  va_list args;
  va_start (args, fmt);
  
  gchar *string = NULL;
  g_vasprintf (&string, fmt, args);
  
  if (hdlr)
    hdlr (string, aux);
  
  g_free (string);
  
  va_end (args);
}

struct gse_ctx *
InitializePlot (const char *filename, struct data_parser **p,
		message_handler msg, gpointer msg_aux)
{
  struct gse_ctx *context = gse_context_create ();
  struct data_parser *parser = xzalloc (sizeof *parser);

  parser->param_file = g_strdup (filename);
  parser->domain = g_quark_from_string ("GSE");

  /* Read plot-parameter file */
  GError *err = NULL;
  if (! ReadParamFile (context, parser, &err))
    {
      emit_warning (msg, msg_aux, "%s", err->message);
      g_clear_error (&err);
    }

   /* Check plot-parameter values */
  if (! CheckParamData (context, parser, &err))
    {
      emit_warning (msg, msg_aux, "%s", err->message);
      g_clear_error (&err);
    }

   /* Set axis_type flags */
   int flag_polar = 0;
   int flag_3d = 0;
   int flag_2d_rect = 0;
   if ( context->plot_param.axes_type == AXES_linear )
      flag_2d_rect = 1;
   else if ( context->plot_param.axes_type == AXES_semilogx )
      flag_2d_rect = 1;
   else if ( context->plot_param.axes_type == AXES_semilogy )
      flag_2d_rect = 1;
   else if ( context->plot_param.axes_type == AXES_loglog )
      flag_2d_rect = 1;
   else if ( context->plot_param.axes_type == AXES_polar )
      flag_polar = 1;
   else if ( context->plot_param.axes_type == AXES_3d )
      flag_3d = 1;

   context->tick_major = 9;
   context->tick_minor = 0.5*context->tick_major;
   context->dash = context->tick_major;
   context->space_dash = 0.5*context->dash;
   context->space_dot = context->space_dash - 1.0;
   g_assert (context->space_dot > 0);
   g_assert (context->space_dash > 0);
   g_assert (context->dash > 0);

   /* Analyze plot data */
   if (flag_2d_rect == 1 || flag_polar == 1 || flag_3d == 1)
     {
       /* Read plot data file */
       if (! FileRead (context, parser, &err))
	 {
	   emit_warning (msg, msg_aux, "%s", err->message);
	   g_clear_error (&err);
	   goto error;
	 }

       /* Find minimum and maximum values of plot data */
       DataMinMax(context);

       /* Adjust axes */
       AxisLimits (context);
     }

   if (p)
     *p = parser;
   return context;
 error:
   free (context);
   free (parser);
   return NULL;
}

void
get_box_from_coords (const struct gse_ctx *context, double width, double height, cairo_rectangle_t *rect)
{
  /* Specify plot box screen coordinates */
  int nplots = context->plot_param.nplots;

  if (context->plot_param.axes_type == AXES_3d)
    {
      rect->x = context->plot_param_3d.origin[1] + context->plot_param_3d.axis1[1];
      rect->width = context->plot_param_3d.axis2[1] - context->plot_param_3d.axis1[1];
      
      rect->y = context->plot_param_3d.origin[2] - context->plot_param_3d.axis3[2];
      rect->height = - context->plot_param_3d.axis1[2] - context->plot_param_3d.axis2[2] + context->plot_param_3d.axis3[2];
    }
  else if (context->plot_param.axes_type == AXES_polar)
    {
      double xorigin, yorigin, radius;
      polar_get_geometry_from_coords (width, height, &xorigin, &yorigin, &radius);
      rect->x = xorigin - radius;
      rect->width = 2 * radius;
      rect->y = yorigin - radius;
      rect->height = 2 * radius;
     }
  else
    {
      gboolean flag = FALSE;
      if (context->plot_param.axes_type == AXES_linear)
	{
	  for (int iplot=1; iplot<=nplots; iplot++)
	    {
	      const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];
	      if ( the_plot->plot_type == PLOT_color ||
		   (the_plot->plot_type == PLOT_contour &&
		    the_plot->styleflags == STYLE_AUTO) )
		flag = TRUE;
	    }
	}

      rect->y = 0.09375 * height;
      rect->height = 0.84375 * height - rect->y;
  
      /* Plot types: contour and color */
      if (flag)
	{
	  rect->x = 0.18750 * width;
	  rect->width = 0.75000 * width - rect->x;
	}
      /* Plot types: Points(2D), Histograms, and logarithmic plots.*/
      else
	{
	  rect->x = 0.15625 * width;
	  rect->width = 0.90625 * width - rect->x;
	}
    }
}

void
get_box (const struct target *target, const struct gse_ctx *context, cairo_rectangle_t *rect)
{
  double w = target->window_width;
  double h = target->window_height;
  
  get_box_from_coords (context, w, h, rect);
}

