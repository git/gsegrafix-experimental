/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
 *
 * DrawGrid2d.c
 *
 * Draws grid lines and tick marks for linear axes of two-dimensional plots.
 *
 * Copyright 2017 John Darrington
 * Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
 * http://savannah.gnu.org/projects/gsegrafix
 *
 * This file is part of GSEGrafix, a scientific and engineering plotting program.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#include "DrawGrid2d.h"
#include "DrawGrid.h"
#include "DrawTickMarks.h"
#include "InitializePlot.h"
#include "gsegraf.h"

#include <string.h> // for strcmp

#include <math.h>

void
DrawGrid2d (struct target *target, const struct gse_ctx *context)
{
  /* Check plot_box parameter */
  if (! context->plot_param.draw_box)
    return;

  cairo_rectangle_t box;
  get_box (target, context, &box);
  
  /* Draw grid lines */
  if ( strcmp(context->plot_param.grid, "on1") == 0 ||
       strcmp(context->plot_param.grid, "on2") == 0 )
    {
      if ( context->plot_param.x_tick_marks)
	{
	  if (context->plot_param.axes_type == AXES_linear ||
	      context->plot_param.axes_type == AXES_semilogy)
	    {
	      DrawGrid (target, context, "linear",
		       box.x, box.y + box.height, box.x + box.width, box.y + box.height, box.x, box.y,
		       context->xtick_labels.nvalues, &context->xtick_labels.values[0],
		       context->xtick_labels.offset1, context->xtick_labels.offset2);
	    }
	    
	  if (context->plot_param.axes_type == AXES_semilogx ||
	      context->plot_param.axes_type == AXES_loglog)
	    {
	      DrawGrid (target, context, "log",
		       box.x, box.y + box.height, box.x + box.width, box.y + box.height, box.x, box.y,
		       context->xtick_labels.nvalues, &context->xtick_labels.values[0],
		       context->xtick_labels.offset1, context->xtick_labels.offset2);
	    }
	}

      if ( context->plot_param.y_tick_marks)
	{
	  if (context->plot_param.axes_type == AXES_linear ||
	      context->plot_param.axes_type == AXES_semilogx)
	    {
	      DrawGrid (target, context, "linear",
		       box.x, box.y + box.height, box.x, box.y, box.x + box.width, box.y + box.height,
		       context->ytick_labels.nvalues, &context->ytick_labels.values[0],
		       context->ytick_labels.offset1, context->ytick_labels.offset2);
	    }
	    
	  if (context->plot_param.axes_type == AXES_semilogy ||
	      context->plot_param.axes_type == AXES_loglog)
	    {
	      DrawGrid (target, context, "log",
		       box.x, box.y + box.height, box.x, box.y, box.x + box.width, box.y + box.height,
		       context->ytick_labels.nvalues, &context->ytick_labels.values[0],
		       context->ytick_labels.offset1, context->ytick_labels.offset2);
	    }
	}
    }

  /* Draw x-axis tick marks */
  if ( context->plot_param.x_tick_marks)
    {
      if (context->plot_param.axes_type == AXES_linear ||
	  context->plot_param.axes_type == AXES_semilogy)
	{
	  DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
			box.x, box.width, box.y + box.height, 0,
			context->xtick_labels.nvalues, &context->xtick_labels.values[0],
			context->xtick_labels.offset1, context->xtick_labels.offset2,
			90.0*deg2rad);   /* lower x-axis */

	  DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
			box.x, box.x + box.width  - box.x, box.y, 0,
			context->xtick_labels.nvalues, &context->xtick_labels.values[0],
			context->xtick_labels.offset1, context->xtick_labels.offset2,
			-90.0*deg2rad);   /* upper x-axis */
	}

      if (context->plot_param.axes_type == AXES_semilogx ||
	  context->plot_param.axes_type == AXES_loglog)
	{
	  DrawTickMarks (target, context, "log", context->minor_ticks_flag, 0,
			box.x, box.width, box.y + box.height, 0,
			context->xtick_labels.nvalues, &context->xtick_labels.values[0],
			context->xtick_labels.offset1, context->xtick_labels.offset2,
			90.0*deg2rad);   /* lower x-axis */
	   
	  DrawTickMarks (target, context, "log", context->minor_ticks_flag, 0,
			 box.x, box.width, box.y, 0,
			context->xtick_labels.nvalues, &context->xtick_labels.values[0],
			context->xtick_labels.offset1, context->xtick_labels.offset2,
			-90.0*deg2rad);   /* upper x-axis */
	}
    }

  /* Draw y-axis tick marks */
  if ( context->plot_param.y_tick_marks)
    {
      if  (context->plot_param.axes_type == AXES_linear ||
	   context->plot_param.axes_type == AXES_semilogx)
	{
	  DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
			 box.x, 0, box.y + box.height, -box.height,
			context->ytick_labels.nvalues, &context->ytick_labels.values[0],
			context->ytick_labels.offset1, context->ytick_labels.offset2,
			0.0*deg2rad);   /* left y-axis */

	  DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
			 box.x + box.width, 0, box.y + box.height, -box.height,
			context->ytick_labels.nvalues, &context->ytick_labels.values[0],
			context->ytick_labels.offset1, context->ytick_labels.offset2,
			180.0*deg2rad);   /* right y-axis */
	}

      if (context->plot_param.axes_type == AXES_semilogy ||
	  context->plot_param.axes_type == AXES_loglog)
	{
	  DrawTickMarks (target, context, "log", context->minor_ticks_flag, 0,
			 box.x, 0, box.y + box.height, -box.height,
			context->ytick_labels.nvalues, &context->ytick_labels.values[0],
			context->ytick_labels.offset1, context->ytick_labels.offset2,
			0.0*deg2rad);   /* left y-axis */

	  DrawTickMarks (target, context, "log", context->minor_ticks_flag, 0,
			 box.x + box.width, 0, box.y + box.height, -box.height,
			context->ytick_labels.nvalues, &context->ytick_labels.values[0],
			context->ytick_labels.offset1, context->ytick_labels.offset2,
			180.0*deg2rad);   /* right y-axis */
	}
    }
}
