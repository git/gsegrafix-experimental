/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*
  This file is part of GSEGrafix, a scientific and engineering plotting 
  program.

  Copyright (C) 2017 John Darrington

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DATA_ITERATOR_H
#define DATA_ITERATOR_H

#include <stdbool.h>
#include "data-iterator-private.h"

struct gse_datum;
struct gse_ctx;
struct plot_parameters;

struct sample_iterator *it_create (const struct gse_ctx *, const struct plot_parameters *plot, bool segmented);
struct sample_iterator *it_create_from_data (const struct gse_datum *data, int ndata);
struct sample_iterator *it_create_from_file (const char *filename, const char *template, int dimensions);

static inline struct sample_iterator *
it_next_segment (struct sample_iterator *in)
{
  return in->next_segment (in);
}

static inline struct sample_iterator *
it_clone (const struct sample_iterator *in)
{
  return in->clone (in);
}

static inline void
it_destroy (struct sample_iterator *it)
{
  if (!it)
    return;
  it->destroy (it);
}

static inline bool
it_last (const struct sample_iterator *it)
{
  return it->last (it);
}

static inline void
it_next (struct sample_iterator *it)
{
  it->next (it);
}

static inline const struct gse_datum *
it_get (const struct sample_iterator *it)
{
  return it->get (it);
}

#endif
