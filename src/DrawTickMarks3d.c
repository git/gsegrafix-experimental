/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* DrawTickMarks3d.c
*
* Draws tick marks and tick-mark labels for 3-dimensional plots.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GNUGraphics.
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "DrawTickMarks3d.h"
#include "DrawTickMarks.h"
#include "DrawTickLabels.h"
#include "DrawAxisLabels.h"

#include <math.h>
#include "gsegraf.h"

void
DrawTickMarks3d (struct target *target, const struct gse_ctx *context, struct tick_label_widths *tlw)
{
   int quadrant, i, nxticks, nyticks, nzticks, nticks;
   double origin[3], axis1[3], axis2[3], axis3[3],
          xmin, xmax, ymin, ymax, zmin, zmax,
     offset1, offset2;
   const double *tickvalues;
   double x1, x2, y1, y2, x11, y11, x22, y22, angle;

   /* Check plot box on */
   if (!context->plot_param.draw_box)
      return;

   /* Get quadrant */
   quadrant = context->plot_param_3d.quadrant;

   /* Get axes */
   for ( i=1; i<=3; i++ )
      {
      axis1[i-1] = context->plot_param_3d.axis1[i-1];
      axis2[i-1] = context->plot_param_3d.axis2[i-1];
      axis3[i-1] = context->plot_param_3d.axis3[i-1];
      }

   /* Get origin */
   for ( i=1; i<=3; i++ )
      origin[i-1] = context->plot_param_3d.origin[i-1];

   /* Get minimum and maximum axis values */
   nxticks = context->xtick_labels.nvalues;
   nyticks = context->ytick_labels.nvalues;
   nzticks = context->ztick_labels.nvalues;
   xmin = context->xtick_labels.values[0];
   xmax = context->xtick_labels.values[nxticks-1];
   ymin = context->ytick_labels.values[0];
   ymax = context->ytick_labels.values[nyticks-1];
   zmin = context->ztick_labels.values[0];
   zmax = context->ztick_labels.values[nzticks-1];
   xmin = xmin - context->xtick_labels.offset1;
   xmax = xmax + context->xtick_labels.offset2;
   ymin = ymin - context->ytick_labels.offset1;
   ymax = ymax + context->ytick_labels.offset2;
   zmin = zmin - context->ztick_labels.offset1;
   zmax = zmax + context->ztick_labels.offset2;


   /* Axis 1 tick-mark parameters */
   if ( quadrant == 1 )
      {
      x1 = origin[1] + axis2[1];
      y1 = origin[2] - axis2[2];
      x2 = origin[1] + axis1[1] + axis2[1];
      y2 = origin[2] - axis1[2] - axis2[2];
      nticks = nxticks;
      tickvalues = &context->xtick_labels.values[0];
      offset1 = context->xtick_labels.offset1;
      offset2 = context->xtick_labels.offset2;
      }

   else if ( quadrant == 2 )
      {
      x1 = origin[1] + axis2[1];
      y1 = origin[2] - axis2[2];
      x2 = origin[1] + axis1[1] + axis2[1];
      y2 = origin[2] - axis1[2] - axis2[2];
      nticks = nyticks;
      tickvalues = &context->ytick_labels.values[0];
      offset1 = context->ytick_labels.offset1;
      offset2 = context->ytick_labels.offset2;
      }

   else if ( quadrant == 3 )
      {
      x1 = origin[1] + axis1[1] + axis2[1];
      y1 = origin[2] - axis1[2] - axis2[2];
      x2 = origin[1] + axis2[1];
      y2 = origin[2] - axis2[2];
      nticks = nxticks;
      tickvalues = &context->xtick_labels.values[0];
      offset1 = context->xtick_labels.offset1;
      offset2 = context->xtick_labels.offset2;
      }

   else if ( quadrant == 4 )
      {
      x1 = origin[1] + axis1[1] + axis2[1];
      y1 = origin[2] - axis1[2] - axis2[2];
      x2 = origin[1] + axis2[1];
      y2 = origin[2] - axis2[2];
      nticks = nyticks;
      tickvalues = &context->ytick_labels.values[0];
      offset1 = context->ytick_labels.offset1;
      offset2 = context->ytick_labels.offset2;
      }


   /* Draw axis1 tick marks and labels */
   if ( ((quadrant == 1 || quadrant == 3) && context->plot_param.x_tick_marks) ||
        ((quadrant == 2 || quadrant == 4) && context->plot_param.y_tick_marks) )
      {
      angle = atan2(axis2[2], axis2[1]) - M_PI;
      DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
		     x1, x2 - x1, y1, y2 - y1,
                    nticks, tickvalues, offset1, offset2,
                    angle);

      if ( ((quadrant == 1 || quadrant == 3) && context->plot_param.x_tick_labels) ||
           ((quadrant == 2 || quadrant == 4) && context->plot_param.y_tick_labels) )
         {
	   tlw->width_axis1_tick_labels =
	     DrawTickLabels (target, context, "linear",
			    x1, x2 - x1, y1, y2 - y1,
			    nticks, tickvalues, offset1, offset2,
			    -context->tick_major*cos(angle), context->tick_major*sin(angle),
			    GSE_ANCHOR_NORTH_WEST);
         }

      x11 = x1 - axis2[1];
      y11 = y1 + axis2[2];
      x22 = x2 - axis2[1];
      y22 = y2 + axis2[2];
      angle = atan2(axis2[2], axis2[1]);
      DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
		     x11, x22 - x11, y11, y22 - y11,
                    nticks, tickvalues, offset1, offset2,
                    angle);

      x11 = x1 - axis2[1];
      y11 = y1 + axis2[2];
      x22 = x2 - axis2[1];
      y22 = y2 + axis2[2];
      angle = atan2(axis3[2], axis3[1]);
      DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
		     x11, x22 - x11, y11, y22 - y11,
		     nticks, tickvalues, offset1, offset2,
		     angle);

      x11 = x1 - axis2[1] + axis3[1];
      y11 = y1 + axis2[2] - axis3[2];
      x22 = x2 - axis2[1] + axis3[1];
      y22 = y2 + axis2[2] - axis3[2];
      angle = atan2(axis3[2], axis3[1]) - M_PI;
      DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
		    x11, x22 - x11, y11, y22 - y11,
		    nticks, tickvalues, offset1, offset2,
		    angle);
      }


   /* Axis 2 tick-mark parameters */
   if ( quadrant == 1 )
      {
      x1 = origin[1] + axis1[1];
      y1 = origin[2] - axis1[2];
      x2 = origin[1] + axis1[1] + axis2[1];
      y2 = origin[2] - axis1[2] - axis2[2];
      nticks = nyticks;
      tickvalues = &context->ytick_labels.values[0];
      offset1 = context->ytick_labels.offset1;
      offset2 = context->ytick_labels.offset2;
      }

   else if ( quadrant == 2 )
      {
      x1 = origin[1] + axis1[1] + axis2[1];
      y1 = origin[2] - axis1[2] - axis2[2];
      x2 = origin[1] + axis1[1];
      y2 = origin[2] - axis1[2];
      nticks = nxticks;
      tickvalues = &context->xtick_labels.values[0];
      offset1 = context->xtick_labels.offset1;
      offset2 = context->xtick_labels.offset2;
      }

   else if ( quadrant == 3 )
      {
      x1 = origin[1] + axis1[1] + axis2[1];
      y1 = origin[2] - axis1[2] - axis2[2];
      x2 = origin[1] + axis1[1];
      y2 = origin[2] - axis1[2];
      nticks = nyticks;
      tickvalues = &context->ytick_labels.values[0];
      offset1 = context->ytick_labels.offset1;
      offset2 = context->ytick_labels.offset2;
      }

   else if ( quadrant == 4 )
      {
      x1 = origin[1] + axis1[1];
      y1 = origin[2] - axis1[2];
      x2 = origin[1] + axis1[1] + axis2[1];
      y2 = origin[2] - axis1[2] - axis2[2];
      nticks = nxticks;
      tickvalues = &context->xtick_labels.values[0];
      offset1 = context->xtick_labels.offset1;
      offset2 = context->xtick_labels.offset2;
      }


   /* Draw axis2 tick marks and labels */
   if ( ((quadrant == 2 || quadrant == 4) && context->plot_param.x_tick_marks) ||
        ((quadrant == 1 || quadrant == 3) && context->plot_param.y_tick_marks) )
      {
      angle = atan2(axis1[2], axis1[1]) - M_PI;
      DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
		    x1, x2 - x1, y1, y2 - y1,
		     nticks, tickvalues, offset1, offset2,
		     angle);

      if ( ((quadrant == 2 || quadrant == 4) && context->plot_param.x_tick_labels) ||
           ((quadrant == 1 || quadrant == 3) && context->plot_param.y_tick_labels) )
         {
	   tlw->width_axis2_tick_labels =
	     DrawTickLabels (target, context, "linear",
			    x1, x2 - x1, y1, y2 - y1,
			    nticks, tickvalues, offset1, offset2,
			    -context->tick_major*cos(angle), context->tick_major*sin(angle),
			    GSE_ANCHOR_NORTH_EAST);
         }

      x11 = x1 - axis1[1];
      y11 = y1 + axis1[2];
      x22 = x2 - axis1[1];
      y22 = y2 + axis1[2];
      angle = atan2(axis1[2], axis1[1]);
      DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
		    x11, x22 - x11, y11, y22 - y11,
                    nticks, tickvalues, offset1, offset2,
                    angle);

      x11 = x1 - axis1[1];
      y11 = y1 + axis1[2];
      x22 = x2 - axis1[1];
      y22 = y2 + axis1[2];
      angle = atan2(axis3[2], axis3[1]);
      DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
		    x11, x22 - x11, y11, y22 - y11,
                    nticks, tickvalues, offset1, offset2,
                    angle);

      x11 = x1 - axis1[1] + axis3[1];
      y11 = y1 + axis1[2] - axis3[2];
      x22 = x2 - axis1[1] + axis3[1];
      y22 = y2 + axis1[2] - axis3[2];
      angle = atan2(axis3[2], axis3[1]) - M_PI;
      DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
		    x11, x22 - x11, y11, y22 - y11,
                    nticks, tickvalues, offset1, offset2,
                    angle);
      }


   /* Draw axis3 tick marks and labels */
   if ( context->plot_param.z_tick_marks )
      {
      x1 = origin[1] + axis1[1];
      y1 = origin[2] - axis1[2];
      x2 = origin[1] + axis1[1] + axis3[1];
      y2 = origin[2] - axis1[2] - axis3[2];
      angle = atan2(axis1[2], axis1[1]) - M_PI;
      DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
		     x1, x2 - x1, y1, y2 - y1,
                    context->ztick_labels.nvalues, &context->ztick_labels.values[0],
                    context->ztick_labels.offset1, context->ztick_labels.offset2,
                    angle);

      if ( context->plot_param.z_tick_labels )
         {
	   tlw->width_axis3_tick_labels =
	     DrawTickLabels (target, context, "linear",
			    x1, x2 - x1, y1, y2 - y1,
			    context->ztick_labels.nvalues, &context->ztick_labels.values[0],
			    context->ztick_labels.offset1, context->ztick_labels.offset2,
			    -context->tick_major, 0.0, GSE_ANCHOR_EAST);
         }

      x1 = origin[1];
      y1 = origin[2];
      x2 = origin[1] + axis3[1];
      y2 = origin[2] - axis3[2];
      angle = atan2(axis1[2], axis1[1]);
      DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
		    x1, x2 - x1, y1, y2 - y1,
                    context->ztick_labels.nvalues, &context->ztick_labels.values[0],
                    context->ztick_labels.offset1, context->ztick_labels.offset2,
                    angle);

      x1 = origin[1];
      y1 = origin[2];
      x2 = origin[1] + axis3[1];
      y2 = origin[2] - axis3[2];
      angle = atan2(axis2[2], axis2[1]);
      DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
		     x1, x2 - x1, y1, y2 - y1,
                    context->ztick_labels.nvalues, &context->ztick_labels.values[0],
                    context->ztick_labels.offset1, context->ztick_labels.offset2,
                    angle);

      x1 = origin[1] + axis2[1];
      y1 = origin[2] - axis2[2];
      x2 = origin[1] + axis2[1] + axis3[1];
      y2 = origin[2] - axis2[2] - axis3[2];
      angle = atan2(axis2[2], axis2[1]) - M_PI;
      DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 0,
		     x1, x2 - x1, y1, y2 - y1,
                    context->ztick_labels.nvalues, &context->ztick_labels.values[0],
                    context->ztick_labels.offset1, context->ztick_labels.offset2,
                    angle);
      }
   }

