/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
 *
 * PlotEllipses.c
 *
 * Plots ellipses.
 *
 * Copyright 2017 John Darrington
 * Copyright © 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
 * http://savannah.gnu.org/projects/gsegrafix
 *
 * This file is part of GSEGrafix, a scientific and engineering plotting program.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#include "PlotEllipses.h"
#include "InitializePlot.h"
#include "DrawLines.h"
#include <math.h>
#include "gsegraf.h"
#include "data-iterator.h"

void
PlotEllipses (struct target *target, const struct gse_ctx *context)
{
  double lower[3], upper[3];
   
  int nx = context->xtick_labels.nvalues;
  lower[0] = context->xtick_labels.values[0];
  upper[0] = context->xtick_labels.values[nx-1];
  int ny = context->ytick_labels.nvalues;
  lower[1] = context->ytick_labels.values[0];
  upper[1] = context->ytick_labels.values[ny-1];
  lower[0] = lower[0] - context->xtick_labels.offset1;
  upper[0] = upper[0] + context->xtick_labels.offset2;
  lower[1] = lower[1] - context->ytick_labels.offset1;
  upper[1] = upper[1] + context->ytick_labels.offset2;

  cairo_rectangle_t box;
  get_box (target, context, &box);
  
  double xscale = box.width / (upper[0] - lower[0]);
  double yscale = box.height / (upper[1] - lower[1]);

  for (int i = 0; i < context->n_ellipses; ++i)
    {
      const struct shape_spec *es = context->ellipses + i;
      struct gse_datum data[361];
      const int npts = 361;
      /* Calculate ellipse centered at offset point;   */
      /* equations of ellipse:                         */
      /*    x^2/a^2 + y^2/b^2 = 1                      */
      /*    x = a*cos(t), y = b*sin(t), 0 <= t <= 2*pi */
      double a = es->width/2.0;    /* semi-major axis */
      double b = es->height/2.0;   /* semi-minor axis */

      double R[4];
      /* Calculate rotation-matrix elements; */
      /* R elements labeled: [ 0 1; 2 3 ]    */
      R[0] =  cos(es->angle);
      R[1] =  sin(es->angle);
      R[2] = -sin(es->angle);
      R[3] =  cos(es->angle);

      /* Calculate rotated ellipse */
      for (int i=1; i<=npts; i++ )
	{
	  double t = 2.0*M_PI*(i - 1)/(npts - 1);
	  data[i-1].x = es->x0 + (R[0]*a*cos(t) + R[1]*b*sin(t));
	  data[i-1].y = es->y0 - (R[2]*a*cos(t) + R[3]*b*sin(t));
	}

      struct sample_iterator *it = it_create_from_data (data, npts);
      DrawLines2d (target, context, 
		   it,
		   lower, upper, 
		   xscale, yscale,
		   es->line_color, es->line_width, es->texture);
      it_destroy (it);
    }
}
