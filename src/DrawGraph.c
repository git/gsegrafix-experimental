/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* DrawGraph.c
*
* Draws graph.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "PolarPlot.h"
#include "PlotData3d.h"
#include "DrawLabels3d.h"
#include "DrawTickMarks3d.h"
#include "DrawGrid3d.h"
#include "Initialize3d.h"
#include "AxesEqual.h"
#include "DrawBackgroundImage.h"
#include "DrawGrid2d.h"
#include "PlotRectangles.h"
#include "PlotEllipses.h"
#include "PlotData2d.h"
#include "PlotLines.h"
#include "PlotSymbols.h"
#include "DrawTickLabels2d.h"
#include "DrawAxisLabels.h"
#include "DrawLegend.h"
#include "DrawText.h"
#include "DrawImage.h"
#include "DrawDateTime.h"
#include "InitializePlot.h"

#include "gsegraf.h"
#include "gsegrafix.h"

#define _(X) X

static void
DrawCartesian (struct target *target, struct gse_ctx *context)
{
  GError *err = NULL;
  if (!DrawBackgroundImage (target, context, &err))
    {
      g_message (_("Cannot render background image: %s"), err->message);
      g_clear_error (&err);
    }
  
  DrawGrid2d (target, context);
  PlotData2d (target, context);
  if ( context->plot_param.axes_type == AXES_linear )
    {
      PlotRectangles (target, context);
      PlotEllipses (target, context);
    }
  PlotLines (target, context);
  PlotSymbols (target, context);

  struct tick_label_widths tlw;
  DrawTickLabels2d (target, context, &tlw);
  DrawAxisLabels (target, context, &tlw);
}

static void
DrawCartesian3d (struct target *target, struct gse_ctx *context)
{
  Initialize3d (target, context);
  DrawGrid3d (target, context);
  struct tick_label_widths tlw;
  DrawTickMarks3d (target, context, &tlw);
  DrawLabels3d (target, context, &tlw);
  PlotData3d (target, context);
  PlotLines (target, context);
  PlotSymbols (target, context);
}


static void
DrawPolar (struct target *target, struct gse_ctx *context)
{
  /* Draw Plot */
  GError *err = NULL;
  if (!DrawBackgroundImage (target, context, &err))
    {
      g_message (_("Cannot render background image: %s"), err->message);
      g_clear_error (&err);
    }

  PolarPlot (target, context);
}

void
DrawGraph (cairo_t *cr, int width, int height, struct gse_ctx *context)
{
  struct target target;
  target.cr = cr;
  target.window_width = width;
  target.window_height = height;

  cairo_set_source_rgb (cr, 1.0, 1.0, 1.0);
  cairo_paint (cr);

   /* Adjust axes */
  if ( context->plot_param.axes_type == AXES_linear &&
       context->plot_param.equal_axes)
    AxesEqual (&target, context);
  
  /* Draw graph */
  if (context->plot_param.axes_type == AXES_linear ||
      context->plot_param.axes_type == AXES_semilogx ||
      context->plot_param.axes_type == AXES_semilogy ||
      context->plot_param.axes_type == AXES_loglog)
    {
      DrawCartesian (&target, context);
    }
  else if (context->plot_param.axes_type == AXES_polar)
    {
      DrawPolar (&target, context);
    }
  else if (context->plot_param.axes_type == AXES_3d)
    {
      /* Draw Plot */
      DrawCartesian3d (&target, context);
    }
  DrawLegend(&target, context);
  DrawText (&target, context);
  DrawImage(&target, context);
  DrawDateTime (&target, context);
}

