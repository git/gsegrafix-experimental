/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* DrawImage.c
*
* Draws image from image file.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "DrawImage.h"
#include "GetWindowCoords.h"

#include "gsegraf.h"

#include "gse-cairo.h"
#include "ReadParamFile.h"

void
DrawImage (struct target *target, const struct gse_ctx *context)
{
  for (int i = 0; i < context->n_images; ++i)
    {
      const struct image_spec *is = context->images + i;

      if (is->pixbuf == NULL)
	continue;

      double xanchor = -1;
      double yanchor = -1;
      cairo_rectangle_t box;
      get_box (target, context, &box);
  
      switch (is->coords_ref)
	{
	case GSE_COORDS_ABS:
	  {
	    double plot_coords[3];
	    double window_coords[2];
	    plot_coords[0] = is->x;
	    plot_coords[1] = is->y;
	    plot_coords[2] = is->z;
	    GetWindowCoords (target, context, plot_coords, window_coords);
	    xanchor = window_coords[0];
	    yanchor = window_coords[1];
	  }
	  break;
	case GSE_COORDS_REL:
	  xanchor = (1.0 - is->x)*box.x + is->x*(box.x + box.width);
	  yanchor = (1.0 - is->y)*(box.y + box.height) + is->y*box.y;
	  break;
	default:
	  g_assert_not_reached ();
	}

      /* Check image is within plot box for absolute coordinates */
      if ( is->coords_ref == GSE_COORDS_ABS &&
	   (xanchor < 0 || yanchor < 0) )
	return;

      gse_cairo_render_pixbuf (target->cr, is->pixbuf, xanchor, yanchor, is->anchor);
    }
}
