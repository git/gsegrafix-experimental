/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*
  This file is part of GSEGrafix, a scientific and engineering plotting 
  program.

  Copyright (C) 2017 John Darrington

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/* Misc.c */
#include <stddef.h>
#include <glib.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <cairo.h>

struct gse_ctx;

void *   xmalloc ( size_t size );
void *   xzalloc (size_t size);
double   min ( int n,  const double *x );
double   max ( int n,  const double *x );
void multiply_mv (const double *matrix, double *vector);
void multiply_mm (const double *matrix1, double *matrix2);
void     interp1 ( int n, int ni, double *x, double *y, double *xi, double *yi );
void     interp2 ( int nx, int ny, int ni, double *x, double *y, double *z,
                   double *xi, double *yi, double *zi );
double   interp_rect ( double x1, double x2, double y1, double y2, double xi );
guint32  interp_color_1 (const struct gse_ctx *, double fraction );
guint32  interp_color_2 (const struct gse_ctx *, double fraction );
void find_indices (int index1, int index2, const double *x, double target,
		   int *out1, int *out2);

void     put_pixel ( GdkPixbuf *pixbuf, int i, int j, guint32 color );



static inline void
maximize (double *target, double ref)
{
  if (*target < ref)
    *target = ref;
}


static inline void
minimize (double *target, double ref)
{
  if (*target > ref)
    *target = ref;
}
