/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* DrawLines.c
*
* Contains functions:
*    DrawLines2d
*    DrawLinesPolar
*    DrawLines3d
*
* Functions draw solid lines connecting data points.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "DrawLines.h"
#include "Clip.h"
#include "DrawSymbols.h"
#include "Misc.h"

#include <math.h>
#include "gsegraf.h"
#include "InitializePlot.h"

#include "data-iterator.h"

void
DrawLines2d (struct target *target, const struct gse_ctx *context,
	     struct sample_iterator *it,
	     const double *lower, const double *upper,
	     double xscale, double yscale,
	     guint32 color, unsigned int line_width, enum line_texture texture)
{
  cairo_save (target->cr);
  
  double dashes[4]; int n_dashes;
  texture_to_dash (context, texture, dashes, &n_dashes);
  cairo_set_dash (target->cr,  dashes, n_dashes, 0);
  cairo_set_line_width (target->cr, line_width);
  gse_cairo_set_source_rgba (target->cr, color);
  
  /* Draw continuous lines */
  struct sample_iterator *it_2 = NULL;
  for (; !it_last (it); it_next (it))
    {
      cairo_rectangle_t box;
      get_box (target, context, &box);

      if (it_2)
	{
	  double line_coords[4];
	  line_coords[0] = it_get (it_2)->x;
	  line_coords[1] = it_get (it_2)->y;
	  it_next (it_2);
	  line_coords[2] = it_get (it_2)->x;
	  line_coords[3] = it_get (it_2)->y;
	  if (Clip2d (lower, upper, line_coords))
            {
	      if (! cairo_has_current_point (target->cr))
		cairo_move_to (target->cr,
			       box.x + (line_coords[0] - lower[0])*xscale,
			       (box.y + box.height) - (line_coords[1] - lower[1])*yscale);
	      
	      cairo_line_to (target->cr,
			     box.x + (line_coords[2] - lower[0])*xscale,
			     (box.y + box.height) - (line_coords[3] - lower[1])*yscale);
            }
	  else
	    {
	      /* The line is clipped out.  Break it here. */
	      cairo_stroke (target->cr);
	    }
	}
      it_destroy (it_2);
      it_2 = it_clone (it);
    }
  cairo_stroke (target->cr);
  cairo_restore (target->cr);
  it_destroy (it_2);
}


void
DrawLinesPolar (struct target *target, const struct gse_ctx *context,
		struct sample_iterator *it,
		double xorigin, double yorigin,
		double rmin, double rmax, double rscale,
		guint32 color, unsigned int line_width, enum line_texture texture)
{
  cairo_save (target->cr);
  double dashes[4]; int n_dashes;
  texture_to_dash (context, texture, dashes, &n_dashes);
  cairo_set_dash (target->cr,  dashes, n_dashes, 0);
  cairo_set_line_width (target->cr, line_width);
  gse_cairo_set_source_rgba (target->cr, color);
  
  struct sample_iterator *it_2 = NULL;
  for (; !it_last (it); it_next (it))
    {
      if (it_2)
	{
	  double line_coords[4];
	  line_coords[0] = it_get (it_2)->x;
	  line_coords[1] = it_get (it_2)->y;
	  it_next (it_2);
	  line_coords[2] = it_get (it_2)->x;
	  line_coords[3] = it_get (it_2)->y;
	  if ( ClipPolar(rmin, rmax, &line_coords[0]) )
	    {
	      double theta1 = line_coords[0];
	      double r1     = line_coords[1];
	      double theta2 = line_coords[2];
	      double r2     = line_coords[3];

	      if (! cairo_has_current_point (target->cr))
		{
		  cairo_move_to (target->cr,
				 xorigin + (r1 - rmin)*cos(theta1)*rscale,
				 yorigin - (r1 - rmin)*sin(theta1)*rscale);
		}
	      
	      cairo_line_to (target->cr,
			     xorigin + (r2 - rmin)*cos(theta2)*rscale,
			     yorigin - (r2 - rmin)*sin(theta2)*rscale);
	    }
	  else
	    {
	      /* The line is clipped out.  Break it here. */
	      cairo_stroke (target->cr);
	    }
	}
      it_destroy (it_2);
      it_2 = it_clone (it);
    }
  cairo_stroke (target->cr);
  cairo_restore (target->cr);
  it_destroy (it_2);
}


void
DrawLines3d (struct target *target, const struct gse_ctx *context, 
	     struct sample_iterator *it,
	     const double *origin, const double *Ryz,
	     const double *lower, const double *upper,
	     double xscale, double yscale, double zscale, guint32 color,
	     unsigned int line_width, enum line_texture texture)

{
  /* Draw continuous lines */
  struct sample_iterator *it_2 = NULL;
  for (; !it_last (it); it_next (it))
    {
      if (it_2)
	{
	  double line_coords[6];
	  line_coords[0] = it_get (it_2)->x;
	  line_coords[1] = it_get (it_2)->y;
	  line_coords[2] = it_get (it_2)->z;
	  it_next (it_2);
	  line_coords[3] = it_get (it_2)->x;
	  line_coords[4] = it_get (it_2)->y;
	  line_coords[5] = it_get (it_2)->z;
	  if ( Clip3d (lower, upper, line_coords))
            {
	      double r[3];

	      GseCairoPoints *points = gse_cairo_points_new (2);

	      r[0] = (line_coords[0] - lower[0])*xscale;
	      r[1] = (line_coords[1] - lower[1])*yscale;
	      r[2] = (line_coords[2] - lower[2])*zscale;
	      multiply_mv (Ryz, r);
	      points->coords[0] = origin[1] + r[1];
	      points->coords[1] = origin[2] - r[2];

	      r[0] = (line_coords[3] - lower[0])*xscale;
	      r[1] = (line_coords[4] - lower[1])*yscale;
	      r[2] = (line_coords[5] - lower[2])*zscale;
	      multiply_mv (Ryz, r);
	      points->coords[2] = origin[1] + r[1];
	      points->coords[3] = origin[2] - r[2];

	      DrawLine (target, context, points, color, line_width, texture);
	      gse_cairo_points_unref(points);
            }
	}

      it_destroy (it_2);
      it_2 = it_clone (it);
    }
  it_destroy (it_2);
}
