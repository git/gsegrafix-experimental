/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*
  This file is part of GSEGrafix, a scientific and engineering plotting 
  program.

  Copyright (C) 2017 John Darrington

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <cairo.h>
#include "gse-cairo.h"
#include <pango/pango.h>
#include <pango/pangocairo.h>

#include <gdk/gdk.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include <math.h>
#include <stdlib.h>

#include "data-iterator.h"

void
get_shift_for_anchor (enum GseAnchorType anchor, double width, double height,
     double *xshift, double *yshift)
{
  switch (anchor)
    {
    case GSE_ANCHOR_NORTH:
    case GSE_ANCHOR_SOUTH:
    case GSE_ANCHOR_CENTER:
	*xshift = -width / 2.0;
      break;
    case GSE_ANCHOR_WEST:
    case GSE_ANCHOR_NORTH_WEST:
    case GSE_ANCHOR_SOUTH_WEST:
      *xshift = 0;
      break;
    case GSE_ANCHOR_EAST:
    case GSE_ANCHOR_NORTH_EAST:
    case GSE_ANCHOR_SOUTH_EAST:
      *xshift = -width;
      break;
    default:
      g_critical ("Unknown anchor value %d", anchor);
      break;
    }
  switch (anchor)
    {
    case GSE_ANCHOR_EAST:
    case GSE_ANCHOR_WEST:
    case GSE_ANCHOR_CENTER:
      *yshift = -height / 2.0;
      break;
    case GSE_ANCHOR_NORTH:
    case GSE_ANCHOR_NORTH_EAST:
    case GSE_ANCHOR_NORTH_WEST:
      *yshift = 0;
      break;
    case GSE_ANCHOR_SOUTH:
    case GSE_ANCHOR_SOUTH_EAST:
    case GSE_ANCHOR_SOUTH_WEST:
      *yshift = -height;
      break;
    default:
      g_critical ("Unknown anchor value %d", anchor);
      break;
    }
}

void
gse_cairo_render_pixbuf (cairo_t *cr, GdkPixbuf *pixbuf,
			 double x, double y, enum GseAnchorType anchor)
{
  double xshift = 0, yshift = 0;
  get_shift_for_anchor (anchor, gdk_pixbuf_get_width (pixbuf),
       gdk_pixbuf_get_height (pixbuf), &xshift, &yshift);

  gdk_cairo_set_source_pixbuf (cr,
			       pixbuf,
			       x + xshift, y + yshift);
  cairo_paint (cr);
}


PangoLayout *
gse_cairo_create_layout (cairo_t *cr,
			 const char *text, int len,
			 const PangoFontDescription *font_desc,
			 PangoRectangle *rect)
{
  PangoLayout *layout = pango_cairo_create_layout (cr);

  pango_layout_set_text (layout, text, len);
  pango_layout_set_font_description (layout, font_desc);
  pango_layout_get_extents (layout, NULL, rect);

  return layout;
}

void
gse_cairo_render_layout (cairo_t *cr, PangoLayout *layout,
			 const PangoRectangle *rect,
			 double x, double y,
			 enum GseAnchorType anchor,
			 guint32 color)
{
  gse_cairo_set_source_rgba (cr, color);

  cairo_save  (cr);
  cairo_new_path (cr);

  cairo_translate (cr, x, y);
  gdouble angle = 0;
  cairo_rotate (cr, angle);
  cairo_translate (cr, -rect->width / 2.0 / PANGO_SCALE,
		   -rect->height / 2.0 / PANGO_SCALE);

  double xshift, yshift;
  get_shift_for_anchor (anchor,
			rect->width * cos (angle),
			rect->height * cos (angle),
			&xshift, &yshift);

  xshift += rect->width * cos (angle) / 2.0;
  yshift += rect->height * cos (angle) / 2.0;
  
  xshift /= PANGO_SCALE;
  yshift /= PANGO_SCALE;

  cairo_translate (cr, xshift, yshift);

  pango_cairo_show_layout (cr, layout);

  cairo_restore (cr);
}

			 
void
gse_cairo_render_text_with_extents (cairo_t *cr, const char *text, int x, int y,
				    enum GseAnchorType anchor,
				    guint32 color,
				    const PangoFontDescription *font_desc,
				    gdouble angle,
				    double *x1, double *y1, double *x2, double *y2)
{
  PangoLayout *layout = pango_cairo_create_layout (cr);

  gse_cairo_set_source_rgba (cr, color);

  cairo_save  (cr);
  cairo_new_path (cr);

  pango_layout_set_text (layout, text, -1);
  pango_layout_set_font_description (layout, font_desc);
  PangoRectangle rect;
  pango_layout_get_extents (layout, NULL, &rect);

  cairo_translate (cr, x, y);
  
  cairo_rotate (cr, angle);
  cairo_translate (cr, - rect.width / 2.0 /  PANGO_SCALE,
		   - rect.height / 2.0 / PANGO_SCALE);

  double xshift, yshift;
  get_shift_for_anchor (anchor,
			rect.width * cos (angle),
			rect.height * cos (angle),
			&xshift, &yshift);

  xshift += rect.width * cos (angle) / 2.0;
  yshift += rect.height * cos (angle) / 2.0;
  
  xshift /= PANGO_SCALE;
  yshift /= PANGO_SCALE;

  cairo_translate (cr, xshift, yshift);

  pango_cairo_show_layout (cr, layout);


  /* FIXME: These extents are wrong if angle != 0 */
  if (x1)
    *x1 = rect.x  / (double) PANGO_SCALE;
  if (y1)
    *y1 = rect.y  / (double) PANGO_SCALE;
  if (x2)
    *x2 = (rect.x + rect.width)  / (double) PANGO_SCALE;
  if (y2)
    *y2 = (rect.y + rect.height)  / (double) PANGO_SCALE;
  
  g_object_unref (layout);
  cairo_restore (cr);
}




void
gse_cairo_render_text (cairo_t *cr, const char *text, int x, int y,
		       enum GseAnchorType anchor,
		       guint32 color,
		       const PangoFontDescription *font_desc)
{
  gse_cairo_render_text_with_extents (cr, text, x, y,
				      anchor, color,
				      font_desc, 0,
				      0, 0, 0, 0);
}


/* 
   Render an open line defined by the array POINTS.  N_POINTS is the size
   of the array.  LINEWIDTH is the width of the line and COLOR is its color.
 */
void
gse_cairo_render_line (cairo_t *cr, GseCairoPoints *points, int n_points,
		       gint linewidth,
		       guint32 color)
{
  cairo_new_path (cr);
  cairo_set_line_width (cr, linewidth);
	  
  gse_cairo_set_source_rgba (cr, color);
	     
  int x;
  for (x = 0; x < n_points; ++x)
    {
      cairo_line_to (cr,
		     points->coords[x * 2],
		     points->coords[x * 2 + 1]);
    }
  cairo_stroke (cr);
}


/* 
   Render a closed polygon defined by the array POINTS.  N_POINTS is the size
   of the array (ie: the number of corners).  LINEWIDTH is the width of the outline.
   OUTLINE_COLOR is the color of the outline and FILL_COLOR the color of the 
   interior.
 */
void
gse_cairo_render_polygon (cairo_t *cr, GseCairoPoints *points, int n_points,
			  gint linewidth,
			  guint32 outline_color,
			  guint32 fill_color)
{
  cairo_new_path (cr);
  cairo_set_line_width (cr, linewidth);
	  
  int x;
  for (x = 0; x < n_points; ++x)
    {
      cairo_line_to (cr,
		     points->coords[x * 2],
		     points->coords[x * 2 + 1]);
      
    }
  cairo_close_path (cr);

  gse_cairo_set_source_rgba (cr, fill_color);
  cairo_fill_preserve (cr);

  gse_cairo_set_source_rgba (cr, outline_color);
  cairo_stroke (cr);
}

/* 
   Render a circle of radius R at the position X,Y.
   LINEWIDTH is the width of the outline.  OUTLINE_COLOR is its color.
   FILL_COLOR is the color of the interior.
*/
void
gse_cairo_render_circle (cairo_t *cr,
			 double x, double y, double r,
			 gint linewidth,
			 guint32 outline_color, guint32 fill_color)
{
  cairo_new_path (cr);
  cairo_set_line_width (cr, linewidth);
  cairo_arc (cr,  x, y, r, 0, 2 * M_PI);
  gse_cairo_set_source_rgba (cr, fill_color);
  cairo_fill_preserve (cr);
  gse_cairo_set_source_rgba (cr, outline_color);
  cairo_stroke (cr);
}



void
gse_cairo_render_ellipse (cairo_t *cr, double x, double y,
			  double width, double height,
			  gint linewidth, 
			  guint32 outline_color, guint32 fill_color)
{
  if (height <= 0 || width <= 0)
    return;

  cairo_save (cr);
  cairo_new_path (cr);
  cairo_translate (cr, x + width / 2.0, y + height / 2.0);
  cairo_scale (cr, width / 2.0, height / 2.0);
  cairo_arc (cr, 0, 0, 1.0, 0, 2 * M_PI);
  cairo_restore (cr);

  gse_cairo_set_source_rgba (cr, fill_color);
  cairo_fill_preserve (cr);
  gse_cairo_set_source_rgba (cr, outline_color);
  cairo_set_line_width (cr, linewidth);
  cairo_stroke (cr);
}

void
gse_cairo_render_rectangle (cairo_t *cr, double x, double y,
			  double width, double height,
			  gint linewidth, 
			  guint32 outline_color, guint32 fill_color)
{
  if (height <= 0 || width <= 0)
    return;

  cairo_save (cr);
  cairo_new_path (cr);
  cairo_rectangle (cr, x, y, width, height);
  cairo_restore (cr);

  gse_cairo_set_source_rgba (cr, fill_color);
  cairo_fill_preserve (cr);
  gse_cairo_set_source_rgba (cr, outline_color);
  cairo_set_line_width (cr, linewidth);
  cairo_stroke (cr);
}

void
gse_cairo_render_arrowhead (cairo_t *cr, double size,
			  guint32 fill_color)
{
  GseCairoPoints *points = gse_cairo_points_new(4);

  double height = size / 2.0;
  double width  = size / 4.0;
  points->coords[0] = 0.0;
  points->coords[1] = 0.0;
  points->coords[2] = -width;
  points->coords[3] = height;
  points->coords[4] = 0.0;
  points->coords[5] = height * 0.66;
  points->coords[6] = width;
  points->coords[7] = height;

  gse_cairo_render_polygon (cr, points, 4, 1,
			    0xFFFFFF00,
			    fill_color);

  gse_cairo_points_unref (points);
}


GseCairoPoints *
gse_cairo_points_new (int n)
{
  GseCairoPoints *ptr = g_slice_alloc (sizeof *ptr);

  ptr->num_points = n;
  ptr->coords = g_slice_alloc (sizeof (double) *  n * 2);

  return ptr;
}

void
gse_cairo_points_unref (GseCairoPoints *pts)
{
  g_slice_free1 (sizeof (double) * pts->num_points * 2, pts->coords);
  g_slice_free1 (sizeof *pts, pts);
}
