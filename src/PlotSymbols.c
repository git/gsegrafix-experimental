/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
 *
 * PlotSymbols.c
 *
 * Plots symbols.
 *
 * Copyright 2017 John Darrington
 * Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
 * http://savannah.gnu.org/projects/gsegrafix
 *
 * This file is part of GSEGrafix, a scientific and engineering plotting program.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#include "PlotSymbols.h"

#include "Misc.h"
#include <ctype.h>
#include <math.h>
#include "gsegraf.h"
#include <string.h>
#include "Initialize3d.h"
#include "PolarPlot.h"
#include "InitializePlot.h"

void
PlotSymbols (struct target *target, const struct gse_ctx *context)
{
  int nx, ny, nz;
  double origin[3], Ry[9], Ryz[9], r[3];
  double xmin, xmax, ymin, ymax, zmin, zmax, rmin, rmax;
  double xscale, yscale, zscale;
  double rscale, xorigin, yorigin, radius;
  
  /* Get minimum and maximum axis values */
  if ( context->plot_param.axes_type == AXES_linear )
    {
      nx = context->xtick_labels.nvalues;
      xmin = context->xtick_labels.values[0];
      xmax = context->xtick_labels.values[nx-1];
      xmin = xmin - context->xtick_labels.offset1;
      xmax = xmax + context->xtick_labels.offset2;
      ny = context->ytick_labels.nvalues;
      ymin = context->ytick_labels.values[0];
      ymax = context->ytick_labels.values[ny-1];
      ymin = ymin - context->ytick_labels.offset1;
      ymax = ymax + context->ytick_labels.offset2;
    }

  else if ( context->plot_param.axes_type == AXES_semilogx )
    {
      nx = context->xtick_labels.nvalues;
      xmin = floor(context->xtick_labels.values[0]);
      xmax = ceil(context->xtick_labels.values[nx-1]);
      nx = roundl(xmax - xmin + 1.0);
      ny = context->ytick_labels.nvalues;
      ymin = context->ytick_labels.values[0];
      ymax = context->ytick_labels.values[ny-1];
      ymin = ymin - context->ytick_labels.offset1;
      ymax = ymax + context->ytick_labels.offset2;
    }

  else if ( context->plot_param.axes_type == AXES_semilogy )
    {
      nx = context->xtick_labels.nvalues;
      xmin = context->xtick_labels.values[0];
      xmax = context->xtick_labels.values[nx-1];
      xmin = xmin - context->xtick_labels.offset1;
      xmax = xmax + context->xtick_labels.offset2;
      ny = context->ytick_labels.nvalues;
      ymin = floor(context->ytick_labels.values[0]);
      ymax = ceil(context->ytick_labels.values[ny-1]);
      ny = roundl(ymax - ymin + 1.0);
    }

  else if ( context->plot_param.axes_type == AXES_loglog )
    {
      nx = context->xtick_labels.nvalues;
      xmin = floor(context->xtick_labels.values[0]);
      xmax = ceil(context->xtick_labels.values[nx-1]);
      nx = roundl(xmax - xmin + 1.0);
      ny = context->ytick_labels.nvalues;
      ymin = floor(context->ytick_labels.values[0]);
      ymax = ceil(context->ytick_labels.values[ny-1]);
      ny = roundl(ymax - ymin + 1.0);
    }

  else if ( context->plot_param.axes_type == AXES_polar )
    {
      ny = context->ytick_labels.nvalues;
      rmin = context->ytick_labels.values[0];
      rmax = context->ytick_labels.values[ny-1];
      rmin = rmin - context->ytick_labels.offset1;
      rmax = rmax + context->ytick_labels.offset2;
    }

  else if ( context->plot_param.axes_type == AXES_3d )
    {
      nx = context->xtick_labels.nvalues;
      ny = context->ytick_labels.nvalues;
      nz = context->ztick_labels.nvalues;
      xmin = context->xtick_labels.values[0];
      xmax = context->xtick_labels.values[nx-1];
      ymin = context->ytick_labels.values[0];
      ymax = context->ytick_labels.values[ny-1];
      zmin = context->ztick_labels.values[0];
      zmax = context->ztick_labels.values[nz-1];
      xmin = xmin - context->xtick_labels.offset1;
      xmax = xmax + context->xtick_labels.offset2;
      ymin = ymin - context->ytick_labels.offset1;
      ymax = ymax + context->ytick_labels.offset2;
      zmin = zmin - context->ztick_labels.offset1;
      zmax = zmax + context->ztick_labels.offset2;
    }

  cairo_rectangle_t box;
  get_box (target, context, &box);

  /* Calculate axis scale factors */
  if ( context->plot_param.axes_type == AXES_linear ||
       context->plot_param.axes_type == AXES_semilogx ||
       context->plot_param.axes_type == AXES_semilogy ||
       context->plot_param.axes_type == AXES_loglog )
    {
      xscale = box.width/(xmax - xmin);
      yscale = box.height/(ymax - ymin);
    }

  else if ( context->plot_param.axes_type == AXES_polar )
    {
      polar_get_geometry (target, &xorigin, &yorigin, &radius);
      rscale = radius/(rmax - rmin);
    }

  else if ( context->plot_param.axes_type == AXES_3d )
    {
      /* Get view angles */
      double phi = context->plot_param_3d.phi;
      double theta = context->plot_param_3d.theta;

      get_origin (context, (int) (phi/90.0) + 1, origin);

      /* Calculate rotation matrices */
      phi = phi*deg2rad;
      theta = theta*deg2rad;

      Ry[0] = cos(-theta); Ry[1] = 0.0; Ry[2] = -sin(-theta);
      Ry[3] = 0.0; Ry[4] = 1.0; Ry[5] = 0.0;
      Ry[6] = sin(-theta); Ry[7] = 0.0; Ry[8] = cos(-theta);

      Ryz[0] = cos(phi); Ryz[1] = sin(phi); Ryz[2] = 0.0;
      Ryz[3] = -sin(phi); Ryz[4] = cos(phi); Ryz[5] = 0.0;
      Ryz[6] = 0.0; Ryz[7] = 0.0; Ryz[8] = 1.0;

      multiply_mm (Ry, Ryz);

      /* Get axis length */
      double axis_length = axis_length_3d (target);

      /* Calculate axis scale factors */
      xscale = axis_length/(xmax - xmin);
      yscale = axis_length/(ymax - ymin);
      zscale = axis_length/(zmax - zmin);
    }


  /* Draw symbols */
  for (int i = 0; i < context->n_symbols; ++i)
    {
      const struct symbol_spec *ss = context->symbols + i;
      double x, y;
      char *pchar;
      if ( context->plot_param.axes_type == AXES_linear ||
	   context->plot_param.axes_type == AXES_semilogx ||
	   context->plot_param.axes_type == AXES_semilogy ||
	   context->plot_param.axes_type == AXES_loglog )
	{
	  if ( xmin <= ss->symbol_coords[0] && ss->symbol_coords[0] <= xmax &&
	       ymin <= ss->symbol_coords[1] && ss->symbol_coords[1] <= ymax )
	    {
	      x = box.x + (ss->symbol_coords[0] - xmin)*xscale;
	      y = box.y + box.height - (ss->symbol_coords[1] - ymin)*yscale;
	    }
	  else
	    continue;
	}

      else if ( context->plot_param.axes_type == AXES_polar )
	{
	  if ( rmin <= ss->symbol_coords[1] && ss->symbol_coords[1] <= rmax )
	    {
	      x = xorigin + (ss->symbol_coords[1] - rmin)*cos(ss->symbol_coords[0])*rscale;
	      y = yorigin - (ss->symbol_coords[1] - rmin)*sin(ss->symbol_coords[0])*rscale;
	    }
	  else
	    continue;
	}

      else if ( context->plot_param.axes_type == AXES_3d )
	{
	  if ( xmin <= ss->symbol_coords[0] && ss->symbol_coords[0] <= xmax &&
	       ymin <= ss->symbol_coords[1] && ss->symbol_coords[1] <= ymax &&
	       zmin <= ss->symbol_coords[2] && ss->symbol_coords[2] <= zmax )
	    {
	      r[0] = (ss->symbol_coords[0] - xmin)*xscale;
	      r[1] = (ss->symbol_coords[1] - ymin)*yscale;
	      r[2] = (ss->symbol_coords[2] - zmin)*zscale;

	      multiply_mv (Ryz, r);

	      x = origin[1] + r[1];
	      y = origin[2] - r[2];
	    }
	  else
	    continue;
	}

      /* Draw symbols in symbol_string1 ("cCtTsSiIpPhH") */
      if ( (pchar = strchr(symbol_string1, ss->symbol_char)) != NULL )
	{
	  int ifunc = pchar - symbol_string1;
	  if ( isupper(*pchar) == 0 )
	    context->symbol_func1[ifunc](target, x, y, context->color_rgba[3], ss->symbol_color, ss->symbol_size);
	  else
	    context->symbol_func1[ifunc](target, x, y, ss->symbol_color, ss->symbol_color, ss->symbol_size);
	}

      /* Draw symbols in symbol_string2 ("+xra") */
      else if ( (pchar = strchr(symbol_string2, ss->symbol_char)) != NULL )
	{
	  int ifunc = pchar - symbol_string2;
	  context->symbol_func2[ifunc](target, x, y, ss->symbol_color,
				       ss->symbol_color, ss->symbol_size);
	}
    }
}
