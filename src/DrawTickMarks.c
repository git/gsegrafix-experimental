/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* DrawTickMarks.c
*
* Draws linear or logarithmic major and minor tick marks.
*
* Copyright 2017 John Darrington
* Copyright © 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "DrawTickMarks.h"
#include "Misc.h"

#include <math.h>
#include "gsegraf.h"

#include <string.h> 


#include "gse-cairo.h"

void
DrawTickMarks (struct target *target, const struct gse_ctx *context, const char *axis_type,
	       int minor_ticks_flag, int center_flag,
	       double x1_screen, double x_range, double y1_screen, double y_range,
	       int nticks, const double *tick_values, double offset1, double offset2,
	       double tick_angle )
{
   int i, j, i1, i2, increment_type, ndecades,
       n_offset1_minor_ticks, n_offset2_minor_ticks;
   double axis_min, axis_max, xscale, yscale,
          major_increment, minor_increment,
          x1_tick, y1_tick;
   GseCairoPoints *points;
   /* double x_range = x2_screen - x1_screen; */
   /* double y_range = y2_screen - y1_screen; */

   /* Correct tick_angle for screen coordinates that increase downward */
   tick_angle = -tick_angle;


   /* Draw linear-axis tick marks */
   if ( strcmp(axis_type, "linear") == 0 )
      {
	 int n_minor_ticks = 0;
	 axis_min = tick_values[0] - offset1;
      axis_max = tick_values[nticks-1] + offset2;
      xscale = (x_range)/(axis_max - axis_min);
      yscale = (y_range)/(axis_max - axis_min);

      /* Calculate minor tick-mark parameters */
      if ( minor_ticks_flag == 1 )
         {
         major_increment = (tick_values[nticks-1] - tick_values[0])/(nticks - 1);
         increment_type = roundl(major_increment/pow(10.0, floor(log10(1.001*major_increment))));

         if ( increment_type == 1 )
            n_minor_ticks = 4;
         else if ( increment_type == 2 )
            n_minor_ticks = 3;
         else if ( increment_type == 5 )
            n_minor_ticks = 4;
         minor_increment = major_increment/(n_minor_ticks + 1);
         }

      /* Draw major tick marks */
      i1 = 2;
      i2 = nticks - 1;
      if ( offset1 > 0.0 )
         i1 = 1;
      if ( offset2 > 0.0 )
         i2 = nticks;
      points = gse_cairo_points_new(2);
      for ( i=i1; i<=i2; i++ )
         {
         points->coords[0] = x1_screen + (tick_values[i-1] - axis_min)*xscale;
         points->coords[1] = y1_screen + (tick_values[i-1] - axis_min)*yscale;
         points->coords[2] = points->coords[0] + context->tick_major*cos(tick_angle);
         points->coords[3] = points->coords[1] + context->tick_major*sin(tick_angle);

         if ( center_flag == 1 )
            {
            points->coords[0] = points->coords[0] - 0.5*context->tick_major*cos(tick_angle);
            points->coords[1] = points->coords[1] - 0.5*context->tick_major*sin(tick_angle);
            points->coords[2] = points->coords[2] - 0.5*context->tick_major*cos(tick_angle);
            points->coords[3] = points->coords[3] - 0.5*context->tick_major*sin(tick_angle);
            }
	 gse_cairo_render_line (target->cr, points, 2, 1, context->canvas_fg_color);
         }

      /* Draw minor tick marks */
      if ( minor_ticks_flag == 1 )
         {
         for ( i=1; i<nticks; i++ )
            {
            x1_tick = x1_screen + (tick_values[i-1] - axis_min)*xscale;
            y1_tick = y1_screen + (tick_values[i-1] - axis_min)*yscale;

            for ( j=1; j<=n_minor_ticks; j++ )
               {
               points->coords[0] = x1_tick + j*minor_increment*xscale;
               points->coords[1] = y1_tick + j*minor_increment*yscale;
               points->coords[2] = points->coords[0] + context->tick_minor*cos(tick_angle);
               points->coords[3] = points->coords[1] + context->tick_minor*sin(tick_angle);

               if ( center_flag == 1 )
                  {
                  points->coords[0] = points->coords[0] - 0.5*context->tick_minor*cos(tick_angle);
                  points->coords[1] = points->coords[1] - 0.5*context->tick_minor*sin(tick_angle);
                  points->coords[2] = points->coords[2] - 0.5*context->tick_minor*cos(tick_angle);
                  points->coords[3] = points->coords[3] - 0.5*context->tick_minor*sin(tick_angle);
                  }

	 gse_cairo_render_line (target->cr, points, 2, 1, context->canvas_fg_color);
               }
            }
         }

      /* Draw offset minor tick marks */
      if ( minor_ticks_flag == 1 )
         {
         if ( offset1 > 0.0 )
            {
            n_offset1_minor_ticks = roundl(floor(offset1/minor_increment));
            for ( j=1; j<=n_offset1_minor_ticks; j++ )
               {
               points->coords[0] = x1_screen + (offset1 - j*minor_increment)*xscale;
               points->coords[1] = y1_screen + (offset1 - j*minor_increment)*yscale;
               points->coords[2] = points->coords[0] + context->tick_minor*cos(tick_angle);
               points->coords[3] = points->coords[1] + context->tick_minor*sin(tick_angle);

               if ( center_flag == 1 )
                  {
                  points->coords[0] = points->coords[0] - 0.5*context->tick_minor*cos(tick_angle);
                  points->coords[1] = points->coords[1] - 0.5*context->tick_minor*sin(tick_angle);
                  points->coords[2] = points->coords[2] - 0.5*context->tick_minor*cos(tick_angle);
                  points->coords[3] = points->coords[3] - 0.5*context->tick_minor*sin(tick_angle);
                  }

	       gse_cairo_render_line (target->cr, points, 2, 1, context->canvas_fg_color);
               }
            }

         if ( offset2 > 0.0 )
            {
            n_offset2_minor_ticks = roundl(floor(offset2/minor_increment));
            for ( j=1; j<=n_offset2_minor_ticks; j++ )
               {
               points->coords[0] = x1_screen + x_range - (offset2 - j*minor_increment)*xscale;
               points->coords[1] = y1_screen + y_range - (offset2 - j*minor_increment)*yscale;
               points->coords[2] = points->coords[0] + context->tick_minor*cos(tick_angle);
               points->coords[3] = points->coords[1] + context->tick_minor*sin(tick_angle);

               if ( center_flag == 1 )
                  {
                  points->coords[0] = points->coords[0] - 0.5*context->tick_minor*cos(tick_angle);
                  points->coords[1] = points->coords[1] - 0.5*context->tick_minor*sin(tick_angle);
                  points->coords[2] = points->coords[2] - 0.5*context->tick_minor*cos(tick_angle);
                  points->coords[3] = points->coords[3] - 0.5*context->tick_minor*sin(tick_angle);
                  }

	 gse_cairo_render_line (target->cr, points, 2, 1, context->canvas_fg_color);
               }
            }
         }

      gse_cairo_points_unref(points);
      }


   /* Draw logarithmic-axis tick marks */
   else if ( strcmp(axis_type, "log") == 0 )
      {
      ndecades = roundl(ceil(tick_values[nticks-1]) - floor(tick_values[0]));
      if ( ndecades <= 10 )
         nticks = ndecades + 1;

      xscale = x_range/(nticks - 1);
      yscale = y_range/(nticks - 1);

      /* Draw major tick marks */
      points = gse_cairo_points_new(2);
      for ( i=2; i<nticks; i++ )
         {
         points->coords[0] = x1_screen + (i - 1)*xscale;
         points->coords[1] = y1_screen + (i - 1)*yscale;
         points->coords[2] = points->coords[0] + context->tick_major*cos(tick_angle);
         points->coords[3] = points->coords[1] + context->tick_major*sin(tick_angle);

         if ( center_flag == 1 )
            {
            points->coords[0] = points->coords[0] - 0.5*context->tick_major*cos(tick_angle);
            points->coords[1] = points->coords[1] - 0.5*context->tick_major*sin(tick_angle);
            points->coords[2] = points->coords[2] - 0.5*context->tick_major*cos(tick_angle);
            points->coords[3] = points->coords[3] - 0.5*context->tick_major*sin(tick_angle);
            }

	 gse_cairo_render_line (target->cr, points, 2, 1, context->canvas_fg_color);
         }

      /* Draw minor tick marks */
      if ( minor_ticks_flag == 1 && ndecades <= 10 )
         for ( i=1; i<nticks; i++ )
            {
            x1_tick = x1_screen + (i - 1)*xscale;
            y1_tick = y1_screen + (i - 1)*yscale;

            for ( j=2; j<=9; j++ )
               {
               points->coords[0] = x1_tick + log10((double) j)*xscale;
               points->coords[1] = y1_tick + log10((double) j)*yscale;
               points->coords[2] = points->coords[0] + context->tick_minor*cos(tick_angle);
               points->coords[3] = points->coords[1] + context->tick_minor*sin(tick_angle);

               if ( center_flag == 1 )
                  {
                  points->coords[0] = points->coords[0] - 0.5*context->tick_minor*cos(tick_angle);
                  points->coords[1] = points->coords[1] - 0.5*context->tick_minor*sin(tick_angle);
                  points->coords[2] = points->coords[2] - 0.5*context->tick_minor*cos(tick_angle);
                  points->coords[3] = points->coords[3] - 0.5*context->tick_minor*sin(tick_angle);
                  }

	       gse_cairo_render_line (target->cr, points, 2, 1,
				      context->canvas_fg_color);
               }
            }

      gse_cairo_points_unref(points);
      }

   return;
   }

