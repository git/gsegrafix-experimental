/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* FileRead.c
*
* Contains functions:
*    FileRead
*
* Reads 3d data files.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "FileRead.h"
#include "gsegraf.h"
#include "errno.h"
#include "InitializePlot.h"
#include <string.h>
#include "Misc.h"
#include <stdlib.h>

#define _(X) X

gboolean
FileRead (struct gse_ctx *context, struct data_parser *parser, GError **err)
{
  for (int iplot=1; iplot<= context->plot_param.nplots; iplot++ )
    {
      int nx, ny;
	 
      struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];

      if ((the_plot->plot_type != PLOT_contour)
	  &&
	  (the_plot->plot_type != PLOT_color)
	  &&
	  (the_plot->plot_type != PLOT_mesh))
	continue;

      /* Open the data file */
      FILE *fptr = fopen(the_plot->filename, "r");
      if ( fptr == NULL )
	{
	  char *reason = strerror (errno);
	  g_set_error (err, parser->domain, 0,
		       _("Cannot open data file \"%s\": %s"),
		       the_plot->filename, reason);
	  return FALSE;
	}

      if ( fscanf(fptr, "%d %d", &nx, &ny) != 2 )
	{
	  g_set_error_literal (err, parser->domain, 0, _("Error reading data file"));
	  return FALSE;
	}
      the_plot->samples3d.nx = nx;
      the_plot->samples3d.ny = ny;
      the_plot->samples3d.x = xmalloc (nx * sizeof (double));
      the_plot->samples3d.y = xmalloc (ny * sizeof (double));
      the_plot->samples3d.z = xmalloc (ny * nx * sizeof (double));

      for (int i=1; i<=nx; i++ )
	{
	  double x;
	  if ( fscanf(fptr, "%lf", &x) != 1 )
	    {
	      g_set_error_literal (err, parser->domain, 0, _("Error reading data file"));
	      return FALSE;
	    }
	  the_plot->samples3d.x[i-1] = x;
	}

      for (int j=1; j<=ny; j++ )
	{
	  double y;
	  if ( fscanf(fptr, "%lf", &y) != 1 )
	    {
	      g_set_error_literal (err, parser->domain, 0, _("Error reading data file"));
	      return FALSE;
	    }
	  the_plot->samples3d.y[j-1] = y;
	}

      for (int i=1; i<=nx; i++ )
	for (int j=1; j<=ny; j++ )
	  {
	    double z;
	    if ( fscanf(fptr, "%lf", &z) != 1 )
	      {
		g_set_error_literal (err, parser->domain, 0, _("Error reading data file"));
		return FALSE;
	      }
	    the_plot->samples3d.z[ny*(i-1)+j-1] = z;
	  }
      fclose (fptr);
    }

  return TRUE;
}
