/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*
  This file is part of GSEGrafix, a scientific and engineering plotting 
  program.

  Copyright (C) 2017 John Darrington

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

struct sample_iterator
{
  const struct gse_ctx *context;
  struct sample_iterator * (*clone) (const struct sample_iterator *);
  struct sample_iterator * (*next_segment) (struct sample_iterator *);
  void (*destroy) (struct sample_iterator *);
  bool (*last) (const struct sample_iterator *);
  void (*next) (struct sample_iterator *);
  const struct gse_datum * (*get) (const struct sample_iterator *);
};
