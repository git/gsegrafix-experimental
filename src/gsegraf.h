/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* gsegraf.h
*
* Declares structures, external variables, and function prototypes for gsegraf
* executable.
*
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#ifndef GSEGRAF_H
#define GSEGRAF_H

#include <glib.h>

#include <pango/pango.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include <cairo.h>

#include "data.h"

#include "gsegrafix.h"

struct target
{
  cairo_t *cr;
  int window_height;
  int window_width;
};

extern const char symbol_string[]  ;
extern const char symbol_string1[] ;
extern const char symbol_string2[] ;
extern const char color_string[]   ;

enum {
  CENTER,
  FILL,
  SCALE,
  ZOOM
};


enum gse_coords_ref
  {
    GSE_COORDS_INVALID,
    GSE_COORDS_ABS,
    GSE_COORDS_REL
  };

enum line_texture
  {
    NONE,
    SOLID,
    DASHED,
    DOTTED
  };

#define STYLE_CHAR 1
#define STYLE_CHAR_CHAR 2
#define STYLE_HEX 3
#define STYLE_CHAR_HEX 4
#define STYLE_HEX_CHAR 5
#define STYLE_HEX_HEX 6
#define STYLE_AUTO 7
#define STYLE_BILINEAR 8
#define STYLE_NEAREST 9


/* Anchor types */
enum GseAnchorType
{
  GSE_ANCHOR_INVALID = -2,
  GSE_ANCHOR_NONE = -1,
  GSE_ANCHOR_CENTER = 0,
  GSE_ANCHOR_NORTH,
  GSE_ANCHOR_NORTH_WEST,
  GSE_ANCHOR_NORTH_EAST,
  GSE_ANCHOR_SOUTH,
  GSE_ANCHOR_SOUTH_WEST,
  GSE_ANCHOR_SOUTH_EAST,
  GSE_ANCHOR_WEST,
  GSE_ANCHOR_EAST,
};

enum axes_type
  {
    AXES_semilogx,
    AXES_semilogy,
    AXES_loglog,
    AXES_polar,
    AXES_linear,
    AXES_3d
  };

typedef struct
{
  int  nplots;
  enum axes_type axes_type;
  char grid[4];
  char minor_ticks[4];
  gboolean equal_axes;
  double axis_limits[6];
  enum GseAnchorType page_note_anchor;
  gboolean draw_box;
  gboolean x_tick_marks, y_tick_marks, z_tick_marks;
  gboolean x_tick_labels, y_tick_labels, z_tick_labels;
}
plot_param_type;


typedef struct
{
   double xmin, xmax, ymin, ymax, zmin, zmax;
}
data_min_max_type;


struct tick_labels
{
  int nvalues;
  double values[11];
  double offset1;
  double offset2;
};

typedef struct
{
  int quadrant;
  double phi, theta;
  double origin[3], axis1[3], axis2[3], axis3[3], Ryz[9];
}
plot_param_3d_type;


#define deg2rad (M_PI / 180.0)

struct data3d
{
  int nx;
  int ny;
  double *x ; // An array of nx doubles
  double *y ; // An array of ny doubles
  double *z ; // An array of nx*ny doubles, indexed x,y order
};

struct text_spec
{
  char *text;
  enum gse_coords_ref coords_ref;
  double x;
  double y;
  double z;
  enum GseAnchorType anchor;
};

struct image_spec
{
  GdkPixbuf *pixbuf;
  enum gse_coords_ref coords_ref;
  double x;
  double y;
  double z;
  enum GseAnchorType anchor;
};

struct shape_spec
{
  double x0;
  double y0;
  double width;
  double height;
  double angle; /* Angle in radians! */
  guint32 line_color;
  int line_width;
  enum line_texture texture;
};

struct legend_spec
{
  double    xlegend, ylegend, zlegend;
  enum gse_coords_ref coords_ref;
  char *legend_str;
  int nlines;
  PangoFontDescription *font;
  double font_size;
  enum GseAnchorType anchor_text;
};


struct symbol_spec
{
  double symbol_coords[3];
  int symbol_size;
  char symbol_char;
  guint32 symbol_color;
};

struct line_spec
{
  double coords[6];
  enum line_texture texture;
  guint32 line_color;
  int line_width;
};

enum plot_type
  {
    PLOT_points,
    PLOT_contour,
    PLOT_color,
    PLOT_histogram,
    PLOT_mesh
  };

enum stem_type
  {
    STEM_ON,
    STEM_VALUE,
    STEM_OFF
  };

enum bin_value
  {
    BIN_number,
    BIN_fraction,
    BIN_percent
  };


enum bin_ref
  {
    REF_mean,
    REF_zero,
    REF_integer
  };

struct plot_parameters
{
  enum plot_type plot_type;

  /* Style, colour and texture */
  unsigned int stylesizes;
  int styleflags;
  char stylechar1;
  char stylechar2;
  enum line_texture texture;
  guint32 stylecolor1, stylecolor2,  alphacolor;
  guint32  fill_colors_rgba, outline_colors_rgba;
  double zblack, zwhite;

  /* Stems */
  double stemvalues;
  enum stem_type stem_type;

  /* Histogram data */
  int ndata;
  enum bin_value bin_value;
  enum bin_ref bin_ref;
  double binmin, binmax, histmin, histmax;
  double *yhist;
  int nbins;
  double bin_widths;

  /* Meshplots, and 3D color and contour plots */
  guint32 meshcolors, contourcolors;
  struct data3d samples3d;
  int ninterp;

  /* Data parsing */
  char *filename;
  char *template;
};


struct gse_ctx
{
  plot_param_type    plot_param;

  struct tick_labels xtick_labels;
  struct tick_labels ytick_labels;
  struct tick_labels ztick_labels;

  data_min_max_type  data_min_max;
  plot_param_3d_type plot_param_3d;

  /* Declare tick-mark and dashed-line variables */
  double tick_major, tick_minor, dash, space_dash, space_dot;

  /* Declare color variables and pointers for colors specified by color characters */
  guint32 color_rgba[17];


/* Declare color variables for all colors with maximum saturation from blue to green to red */
  int n_color_spectrum_1, n_color_spectrum_2;
  guint32 color_spectrum_1[1021], color_spectrum_2[769];
  
/* Declare pango font-description pointers and font-size variables */
PangoFontDescription *font_date_time,
                     *font_text,
                     *font_tick_labels,
                     *font_axis_labels,
                     *font_title;

double font_size_date_time,

       font_size_text,
       font_size_tick_labels,
       font_size_axis_labels,
       font_size_title;

  /* Declare plot-parameter variables and pointers */
  gboolean axis_limits[6];
  int minor_ticks_flag;
  int ncontours;
  int background_image_style;

  char gridchar1, gridchar2;

  guint32 canvas_fg_color, canvas_bg_color, gridcolor;

  struct plot_parameters *plot_parameters;

  char *xlabel, *ylabel, *zlabel;
  
  char date_time[21];

  char *font_name;

  char *background_image_file;

  struct text_spec *texts;
  int n_texts;

  struct image_spec *images;
  int n_images;

  struct shape_spec *rectangles;
  int n_rectangles;

  struct shape_spec *ellipses;
  int n_ellipses;

  struct line_spec *lines;
  int n_lines;

  struct symbol_spec *symbols;
  int n_symbols;
  
  struct legend_spec *legend;
  
  char *title;

  void     ( *symbol_func1[12] ) (const struct target *, double x, double y,
				  guint32 fill_color_rgba,
				  guint32 outline_color_rgba,
                                 int size );
  
  void     ( *symbol_func2[4] ) (const struct target *, double x, double y,
				 guint32 fill_color_rgba,
				 guint32 outline_color_rgba,
				 int size );
};

static inline double
axis_length_3d (const struct target *target)
{
  return 0.4375 * target->window_height;
}

#endif
