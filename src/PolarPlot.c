/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* PolarPlot.c
*
* Calculates polar plot of input data.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "PolarPlot.h"

#include "DrawSymbols.h"
#include "DrawTickMarks.h"
#include "PlotLines.h"
#include "PlotSymbols.h"
#include "Misc.h"
#include "DrawLines.h"

#include <math.h>
#include <string.h>
#include "gsegraf.h"

#include "gse-cairo.h"

#include "data-iterator.h"
#include "DrawText.h"

static void     DrawDashedCircle (struct target *, const struct gse_ctx *context, double radius, guint32 fill_color_rgba );
static void     DrawDottedCircle (struct target *, const struct gse_ctx *context, double radius, guint32 fill_color_rgba );


void
polar_get_geometry_from_coords (double window_width, double window_height,
     double *xorigin, double *yorigin, double *radius)
{
  /* Specify plot-circle location and radius */
  *xorigin = 0.375 * window_width;
  *yorigin = 0.500 * window_height;
  *radius = 0.375 * MIN (window_width, window_height);
}

void
polar_get_geometry (const struct target *target,
     double *xorigin, double *yorigin, double *radius)
{
  double window_width  = target->window_width;
  double window_height = target->window_height;
  polar_get_geometry_from_coords (window_width, window_height, xorigin, yorigin, radius);
}

static void
DrawLineSegmentsPolar (struct target *target, const struct gse_ctx *context,
		       const struct plot_parameters *the_plot,
		       double xorigin, double yorigin, double rmin, double rmax,
		       double rscale, enum line_texture texture)
{
  int iseg = 0;
  struct sample_iterator *it = it_create (context, the_plot, true);
  for (; it ; iseg++)
    {
      DrawLinesPolar (target, context, it,
		     xorigin, yorigin, rmin, rmax, rscale,
		     the_plot->fill_colors_rgba, the_plot->stylesizes,
		     texture);
      it = it_next_segment (it);
    }
}

void
PolarPlot (struct target *target, struct gse_ctx *context)
{
   /* Declare variables */
   int i, j, ifunc, nyvalues, nrvalues, iplot, nplots, anchor;
   double rmin, rmax, xorigin, yorigin, radius, radius_grid, rscale,
          x, y, x1, y1, x2, y2, 
          theta, theta_minor, r, r1, dr, width_xtick_label;
   char string[21], *pchar;

   GseCairoPoints *points;

   polar_get_geometry (target, &xorigin, &yorigin, &radius);


   /* Specify axis minimum and maximum values */
   nyvalues = context->ytick_labels.nvalues;
   nrvalues = nyvalues;
   rmin = context->ytick_labels.values[0];
   rmax = context->ytick_labels.values[nyvalues-1];
   rmin = rmin - context->ytick_labels.offset1;
   rmax = rmax + context->ytick_labels.offset2;
   rscale = radius/(rmax - rmin);


   /* Draw grid lines */
   if ( context->plot_param.draw_box &&
        (strcmp(context->plot_param.grid, "on1") == 0 ||
         strcmp(context->plot_param.grid, "on2") == 0 ) )
      {
      /* Draw constant-theta grid lines */
      if ( context->plot_param.x_tick_marks )
         {
         points = gse_cairo_points_new(2);
         for ( i=1; i<=12; i++ )
            {
            theta = (i - 1.0)*30.0*deg2rad;
            points->coords[0] = xorigin;
            points->coords[1] = yorigin;
            points->coords[2] = xorigin + radius*cos(theta);
            points->coords[3] = yorigin - radius*sin(theta);

            if ( context->gridchar1 == 'l' )
	      DrawLine (target, context, points, context->gridcolor, 1, SOLID);
            else if ( context->gridchar1 == 'd' )
	      DrawLine (target, context, points, context->gridcolor, 1, DASHED);
            else if ( context->gridchar1 == '.' )
	      DrawLine (target, context, points, context->gridcolor, 1, DOTTED);
            }
         gse_cairo_points_unref(points);
         }


      /* Draw constant-r grid lines */
      if ( context->plot_param.y_tick_marks )
         {
	   for ( i=1; i<=nrvalues; i++ )
	     {
	       double x1 = - (context->ytick_labels.values[i-1] - rmin) * rscale;
	       double x2 =   (context->ytick_labels.values[i-1] - rmin) * rscale;
	       double y1 = - (context->ytick_labels.values[i-1] - rmin) * rscale;
	       double y2 =   (context->ytick_labels.values[i-1] - rmin) * rscale;

	       cairo_save (target->cr);

	       cairo_translate (target->cr, xorigin, yorigin);
	       
	       if ( context->gridchar1 == 'l' )
		 {
		   gse_cairo_render_ellipse (target->cr,
					     x1, y1,
					     x2 - x1,
					     y2 - y1,
					     1,
					     context->gridcolor,
					     0xFFFFFF00);
		 }
	       else if ( context->gridchar1 == 'd' )
		 {
		   radius_grid = (context->ytick_labels.values[i-1] - rmin)*rscale;
		   DrawDashedCircle (target, context, radius_grid, context->gridcolor);

		 }
	       else if ( context->gridchar1 == '.' )
		 {
		   radius_grid = (context->ytick_labels.values[i-1] - rmin)*rscale;
		   DrawDottedCircle (target, context, radius_grid, context->gridcolor);
		 }
	       cairo_restore (target->cr);	    
	     }
         }
      }


   /* Draw theta tick marks */
   if ( context->plot_param.draw_box &&
        context->plot_param.x_tick_marks )
      {
      points = gse_cairo_points_new(2);
      for ( i=1; i<=12; i++ )
         {
         theta = (i - 1)*30.0*deg2rad;
         points->coords[0] = xorigin + radius*cos(theta);
         points->coords[1] = yorigin - radius*sin(theta);
         points->coords[2] = xorigin + (radius - context->tick_major)*cos(theta);
         points->coords[3] = yorigin - (radius - context->tick_major)*sin(theta);

	 gse_cairo_render_line (target->cr, points, 2, 1, context->canvas_fg_color);

         if ( context->minor_ticks_flag == 1 )
            {
            for ( j=1; j<=5; j++ )
               {
               theta_minor = ((i - 1)*30.0 + j*5.0)*deg2rad;
               points->coords[0] = xorigin + radius*cos(theta_minor);
               points->coords[1] = yorigin - radius*sin(theta_minor);
               points->coords[2] = xorigin + (radius - context->tick_minor)*cos(theta_minor);
               points->coords[3] = yorigin - (radius - context->tick_minor)*sin(theta_minor);

	       gse_cairo_render_line (target->cr, points, 2, 1, context->canvas_fg_color);
               }
            }
         }
      gse_cairo_points_unref(points);
      }


   /* Draw r axis and tick marks */
   if ( context->plot_param.draw_box &&
        context->plot_param.y_tick_marks )
      {
      points = gse_cairo_points_new(2);

      theta = 45.0*deg2rad;
      points->coords[0] = xorigin;
      points->coords[1] = yorigin;
      points->coords[2] = xorigin + radius*cos(theta);
      points->coords[3] = yorigin - radius*sin(theta);

      gse_cairo_render_line (target->cr, points, 2, 1, context->canvas_fg_color);

      DrawTickMarks (target, context, "linear", context->minor_ticks_flag, 1,
		    xorigin, radius*cos(45.0*deg2rad), yorigin, - radius*sin(theta),
                    context->ytick_labels.nvalues, &context->ytick_labels.values[0],
                    context->ytick_labels.offset1, context->ytick_labels.offset2,
                    135.0*deg2rad);

      gse_cairo_points_unref(points);
      }


   /* Draw plot circle */
   if ( context->plot_param.draw_box )
     {
       gse_cairo_render_ellipse (target->cr,
				 xorigin - radius,
				 yorigin - radius,
				 2 * radius,
				 2 * radius,
				 2,
				 context->canvas_fg_color,
				 0xFFFFFF00);
     }


   /* Draw theta tick-mark labels */
   if ( context->plot_param.draw_box &&
        context->plot_param.x_tick_marks &&
        context->plot_param.x_tick_labels )
      {
      width_xtick_label = 0.0;
      for ( i=1; i<=12; i++ )
         {
         theta = (i - 1.0)*30.0*deg2rad;
         x = xorigin + (radius + 8.0)*cos(theta);
         y = yorigin - (radius + 8.0)*sin(theta);
         memset(string, 0, sizeof(string));
         snprintf(string, sizeof(string), "%d", (i - 1)*30);

         if ( i == 1 )
            anchor = GSE_ANCHOR_WEST;
         else if ( i == 2  || i == 3 )
            anchor = GSE_ANCHOR_SOUTH_WEST;
         else if ( i == 4 )
            anchor = GSE_ANCHOR_SOUTH;
         else if ( i == 5  || i == 6 )
            anchor = GSE_ANCHOR_SOUTH_EAST;
         else if ( i == 7 )
            anchor = GSE_ANCHOR_EAST;
         else if ( i == 8  || i == 9 )
            anchor = GSE_ANCHOR_NORTH_EAST;
         else if ( i == 10 )
            anchor = GSE_ANCHOR_NORTH;
         else if ( i == 11  || i == 12 )
            anchor = GSE_ANCHOR_NORTH_WEST;

	 gse_cairo_render_text_with_extents (target->cr, string, x, y, anchor,
					     context->canvas_fg_color,
					     context->font_tick_labels, 0,
					     &x1, &y1, &x2, &y2);

         if ( i == 1 )
            {
	      width_xtick_label = x2 - x1;
            }
         }
      }


   /* Draw theta-axis label */
   if ( context->xlabel != NULL )
     {
       if ( context->plot_param.draw_box )
         {
	   double label_offset = 20.0;
	   double theta_max = 45.0/(radius + label_offset + width_xtick_label);

	   {
	     /* Draw the arrowhead */
	     cairo_save (target->cr);
	     cairo_translate (target->cr, xorigin, yorigin);

	     cairo_translate (target->cr, 
			      +(radius + label_offset) * cos (theta_max),
			      -(radius + label_offset) * sin (theta_max));

	     cairo_rotate (target->cr,  -theta_max);

	     gse_cairo_render_arrowhead (target->cr, 25.0,
					 context->canvas_fg_color);
	     cairo_restore (target->cr);
	   }
	 
	   {
	     /* Draw the arc */
	     cairo_new_path (target->cr);
	     cairo_set_line_width (target->cr, 1);
	     cairo_arc (target->cr,  xorigin, yorigin, radius + label_offset, -theta_max, 0);
	     gse_cairo_set_source_rgba (target->cr, context->canvas_fg_color);
	     cairo_stroke (target->cr);
	   }
         }

       x = xorigin + radius + 28.0 + width_xtick_label;
       y = yorigin;
       gse_cairo_render_text (target->cr, context->xlabel, x, y, GSE_ANCHOR_WEST,
			      context->canvas_fg_color,
			      context->font_axis_labels);
     }


   /* Draw r-axis label */
   if ( context->ylabel != NULL )
     {
       theta = 45.0*deg2rad;
       if ( context->plot_param.draw_box )
         {
	   {
	     /* Draw the arrowhead */
	     cairo_save (target->cr);
	     cairo_translate (target->cr, xorigin, yorigin);

	     cairo_translate (target->cr,
			      + (radius + 60.0)*cos(theta),
			      - (radius + 60.0)*sin(theta));

	     cairo_rotate (target->cr, theta);
	     
	     gse_cairo_render_arrowhead (target->cr, 25.0,
					 context->canvas_fg_color);
	     cairo_restore (target->cr);
	   }
	   
	   {
	     GseCairoPoints *points = gse_cairo_points_new(2);

	     points->coords[0] = xorigin + (radius + 15.0)*cos(theta);
	     points->coords[1] = yorigin - (radius + 15.0)*sin(theta);
	     points->coords[2] = xorigin + (radius + 60.0)*cos(theta);
	     points->coords[3] = yorigin - (radius + 60.0)*sin(theta);

	     gse_cairo_render_line (target->cr, points, 2, 1, context->canvas_fg_color);
	 
	     gse_cairo_points_unref(points);
	   }
         }

       x = xorigin + (radius + 70.0)*cos(theta);
       y = yorigin - (radius + 70.0)*sin(theta);
       gse_cairo_render_text (target->cr, context->ylabel, x, y, GSE_ANCHOR_WEST,
			      context->canvas_fg_color, context->font_axis_labels);
     }


   /* Draw plot title */
   if ( context->title != NULL )
      {
      x = xorigin;
      if ( context->plot_param.draw_box &&
           context->plot_param.x_tick_marks &&
           context->plot_param.x_tick_labels )
         y = yorigin - radius - 8.0 - context->font_size_tick_labels - 8.0;
      else
         y = yorigin - radius - 8.0;
      gse_cairo_render_text (target->cr, context->title, x, y, GSE_ANCHOR_SOUTH,
			     context->canvas_fg_color, context->font_title);
      }


   /* Plot data */
   nplots = context->plot_param.nplots;
   for ( iplot=1; iplot<=nplots; iplot++ )
      {
	const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];

      /* Draw stem lines */
      if (the_plot->stem_type != STEM_OFF)
         {
         points = gse_cairo_points_new(2);

         /* Calculate r coordinate of stem point 1 */
	 if (the_plot->stem_type == STEM_VALUE)
            {
            if ( rmin <= the_plot->stemvalues && the_plot->stemvalues <= rmax )
               r1 = the_plot->stemvalues;
            else if ( the_plot->stemvalues < rmin )
               r1 = rmin;
            else if ( the_plot->stemvalues > rmax )
               r1 = rmax;
            }
	 else
	   r1 = rmin;

	 struct sample_iterator *it = it_create (context, the_plot, false);
	 for (; ! it_last(it); it_next (it))
	   {
	     const struct gse_datum *sample = it_get (it);
	     theta = sample->x;
	     r     = sample->y;
	     if ( r <= rmax )
               {
		 /* Calculate coordinates of stem point 1 */
		 x = xorigin + (r1 - rmin)*cos(theta)*rscale;
		 y = yorigin - (r1 - rmin)*sin(theta)*rscale;
		 points->coords[0] = x;
		 points->coords[1] = y;

		 /* Calculate coordinates of stem point 2 */
		 if ( r < rmin )
		   r = rmin;
		 x = xorigin + (r - rmin)*cos(theta)*rscale;
		 y = yorigin - (r - rmin)*sin(theta)*rscale;
		 points->coords[2] = x;
		 points->coords[3] = y;
		 DrawLine (target, context, points, the_plot->outline_colors_rgba, 1, SOLID);
               }
	   }
	 it_destroy (it);

         gse_cairo_points_unref(points);
         }

      /* Draw lines */
      if ( the_plot->texture != NONE)
	DrawLineSegmentsPolar (target, context, the_plot, xorigin, yorigin, rmin, rmax, rscale, the_plot->texture);
      
      /* Draw symbols in symbol_string1 ("cCtTsSiIpPhH") */
      else if ( (pchar = strchr(symbol_string1, the_plot->stylechar1)) != NULL )
	{
	  ifunc = pchar - symbol_string1;
	  struct sample_iterator *it = it_create (context, the_plot, false);
	  for (; ! it_last(it); it_next (it))
	    {
	      const struct gse_datum *sample = it_get (it);
	      theta = sample->x;
	      r     = sample->y;
	      if ( rmin <= r && r <= rmax )
		{
		  x = xorigin + (r - rmin)*cos(theta)*rscale;
		  y = yorigin - (r - rmin)*sin(theta)*rscale;
		  context->symbol_func1[ifunc](target, x, y,
					       the_plot->fill_colors_rgba,
					       the_plot->outline_colors_rgba,
					       the_plot->stylesizes);
		}
            }
	  it_destroy (it);
	}

      /* Draw symbols in symbol_string2 ("+xra") */
      else if ( (pchar = strchr(symbol_string2, the_plot->stylechar1)) != NULL )
	{
	  ifunc = pchar - symbol_string2;
	  struct sample_iterator *it = it_create (context, the_plot, false);
	  for (; ! it_last(it); it_next (it))
	    {
	      const struct gse_datum *sample = it_get (it);
	      theta = sample->x;
	      r     = sample->y;
	      if ( rmin <= r && r <= rmax )
		{
		  x = xorigin + (r - rmin)*cos(theta)*rscale;
		  y = yorigin - (r - rmin)*sin(theta)*rscale;
		  context->symbol_func2[ifunc](target, x, y,
					       the_plot->fill_colors_rgba,
					       the_plot->outline_colors_rgba,
					       the_plot->stylesizes);
		}
            }
	  it_destroy (it);
	}
      }


   /* Plot specified lines and symbols */
   PlotLines (target, context);
   PlotSymbols (target, context);

   /* Draw r tick-mark labels on translucent rectangles */
   if (context->plot_param.draw_box &&
        context->plot_param.y_tick_marks &&
        context->plot_param.y_tick_labels )
      {
      dr = (rmax - rmin)/(nrvalues - 1);
      for ( i=1; i<=nrvalues; i++ )
         {
         theta = 45.0 * deg2rad;
         x = xorigin + (context->ytick_labels.values[i-1] - rmin)*rscale*cos(theta);
         y = yorigin - (context->ytick_labels.values[i-1] - rmin)*rscale*sin(theta);

         memset(string, 0, sizeof(string));
         if ( fabs(context->ytick_labels.values[i-1]) < 0.01*dr )
	   snprintf(string, sizeof(string), "%1.0f", 0.0);
         else
	   snprintf(string, sizeof(string), "%g", context->ytick_labels.values[i-1]);

	 background_text (target, context, string, x - 10.0, y,
			  GSE_ANCHOR_EAST);
         }
      }
   }


static void
DrawDashedCircle (struct target *target, const struct gse_ctx *context, double radius, guint32 color)
{
  double dashes[4];
  int n = 2;
  dashes[0] = context->dash;
  dashes[1] = context->space_dash;
  cairo_save (target->cr);
  cairo_set_dash (target->cr, dashes, n, 0);

  gse_cairo_render_circle (target->cr, 0, 0, radius, 1, color, 0xFFFFFF00);
  cairo_restore (target->cr);
}


static void
DrawDottedCircle (struct target *target, const struct gse_ctx *context, double radius, guint32 color)
{
  double dashes[4];
  int n = 2;
  dashes[0] = 1;
  dashes[1] = context->space_dot;
  cairo_save (target->cr);
  cairo_set_dash (target->cr, dashes, n, 0);

  gse_cairo_render_circle (target->cr, 0, 0, radius, 2, color, 0xFFFFFF00);
  cairo_restore (target->cr);
}

