/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* DrawGrid.c
*
* Draws linear or logarithmic grid lines.
*
* Copyright 2017 John Darrington
* Copyright © 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "DrawGrid.h"
#include "DrawSymbols.h"
#include "Misc.h"

#include <math.h>
#include "gsegraf.h"

#include <string.h> // for strcmp

void DrawGrid (struct target *target, const struct gse_ctx *context, char *axis_type,
                double x11_screen, double y11_screen, double x12_screen, double y12_screen,
                double x21_screen, double y21_screen, int nticks, const double *tick_values,
                double offset1, double offset2 )
   {
   /* Declare variables */
   int i, ndecades;
   double xscale, yscale, axis_min, axis_max;
   GseCairoPoints *points;


   /* Draw linear-axis grid lines */
   if ( strcmp(axis_type, "linear") == 0 )
      {
      axis_min = tick_values[0] - offset1;
      axis_max = tick_values[nticks-1] + offset2;
      xscale = (x12_screen - x11_screen)/(axis_max - axis_min);
      yscale = (y12_screen - y11_screen)/(axis_max - axis_min);

      points = gse_cairo_points_new(2);
      for ( i=1; i<=nticks; i++ )
         {
         points->coords[0] = x11_screen + (tick_values[i-1] - axis_min)*xscale;
         points->coords[1] = y11_screen + (tick_values[i-1] - axis_min)*yscale;
         points->coords[2] = x21_screen + (tick_values[i-1] - axis_min)*xscale;
         points->coords[3] = y21_screen + (tick_values[i-1] - axis_min)*yscale;
         if ( context->gridchar1 == 'l' )
	   DrawLine (target, context, points, context->gridcolor, 1, SOLID);
         else if ( context->gridchar1 == 'd' )
	   DrawLine (target, context, points, context->gridcolor, 1, DASHED);
         else if ( context->gridchar1 == '.' )
	   DrawLine (target, context, points, context->gridcolor, 1, DOTTED);
         }
      gse_cairo_points_unref(points);
      }

   /* Draw logarithmic-axis grid lines */
   else if ( strcmp(axis_type, "log") == 0 )
      {
      ndecades = roundl(ceil(tick_values[nticks-1]) - floor(tick_values[0]));
      if ( ndecades <= 10 )
         nticks = ndecades + 1;

      xscale = (x12_screen - x11_screen)/(nticks - 1);
      yscale = (y12_screen - y11_screen)/(nticks - 1);

      points = gse_cairo_points_new(2);
      for ( i=1; i<=nticks; i++ )
         {
         points->coords[0] = x11_screen + (i - 1)*xscale;
         points->coords[1] = y11_screen + (i - 1)*yscale;
         points->coords[2] = x21_screen + (i - 1)*xscale;
         points->coords[3] = y21_screen + (i - 1)*yscale;
         if ( context->gridchar1 == 'l' )
	   DrawLine (target, context, points, context->gridcolor, 1, SOLID);
         else if ( context->gridchar1 == 'd' )
	   DrawLine (target, context, points, context->gridcolor, 1, DASHED);
         else if ( context->gridchar1 == '.' )
	   DrawLine (target, context, points, context->gridcolor, 1, DOTTED);
         }
      gse_cairo_points_unref(points);
      }

   return;
   }

