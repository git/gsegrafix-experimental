/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* ReadParamFile.c
*
* Reads plot-parameter file.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "ReadParamFile.h"
#include "InitializePlot.h"
#include "Misc.h"

#include <ctype.h>
#include <locale.h>
#include <math.h>
#include "gsegraf.h"

#include <string.h>
#include <stdlib.h>
#include <math.h> // for abs
#include <errno.h>
#include <stdarg.h>

#define _(X) X

enum line_texture
char_to_texture (char c)
{
  enum line_texture tex = NONE;
  switch (c)
    {
    case 'l':	tex = SOLID;     break;
    case 'd':	tex = DASHED;    break;
    case '.':	tex = DOTTED;    break;
    }
  return tex;
}


static const char *
get_string (struct data_parser *parser, const char *string,
	    unsigned int *i1_str, unsigned int *i2_str,
	    unsigned int *size, int flag, GError **err)
   {
   /* Declare variables */
   int i, i1, i2, j;
   unsigned int strvalue;
   char *pchar = NULL;
   const char *error_str[] =
      { "Invalid Unicode specification;\n"
        "hexadecimal format is \\xhh;\n"
        "octal format is \\ooo.",
        "Invalid Unicode specification;\n"
        "maximum value is:\n"
        "   FF hexadecimal or\n"
        "   377 octal." };


   /* Get initial string */
   free(parser->string_get);
   parser->string_get = strdup (string);

   /* Get string limits */
   i = 0;
   while ( (parser->string_get[i] == ' ' || parser->string_get[i] == '\t') && i < (int) *size )
      i++;

   if ( parser->string_get[i] == '#' || parser->string_get[i] != '\"' )
      return NULL;

   i1 = i + 1;
   if ( (pchar = strchr(&parser->string_get[i1], '\"')) == NULL )
      return NULL;
   *pchar = '\0';
   i2 = i1 + pchar - &parser->string_get[i1] - 1;


   /************************************************************************
   * Find escape sequences                                                 *
   * Used for text which may contain escape sequences.                     *
   * Not used for text which should not contain escape sequences.          *
   * Not used for file names, which can contain any character except '\0'. *
   ************************************************************************/
   if ( flag == 1 )
      for ( i=i1; i<=i2; i++ )
         {
         /* Find escaped instances of \ */
         if ( parser->string_get[i] == '\\' && parser->string_get[i+1] == '\\' )
            {
            for ( j=i; j<=i2; j++ )
               parser->string_get[j] = parser->string_get[j+1];
            i2--;
            }

          /* Find newline escape sequences \n */
         else if ( parser->string_get[i] == '\\' && parser->string_get[i+1] == 'n' )
            {
            parser->string_get[i] = '\n';
            for ( j=i+1; j<=i2; j++ )
               parser->string_get[j] = parser->string_get[j+1];
            i2--;
            }

         /* Find Unicode hexadecimal or octal escape sequences */
         else if ( parser->string_get[i] == '\\' && parser->string_get[i+1] != 'n' )
            {
            /* Check for hexadecimal or octal format */
            if ( parser->string_get[i+1] == 'x' &&
                 strchr("0123456789abcdefABCDEF", parser->string_get[i+2]) != NULL &&
                 strchr("0123456789abcdefABCDEF", parser->string_get[i+3]) != NULL )
               sscanf(&parser->string_get[i+2], "%2x", &strvalue);

            else if ( strchr("01234567", parser->string_get[i+1]) != NULL &&
                      strchr("01234567", parser->string_get[i+2]) != NULL &&
                      strchr("01234567", parser->string_get[i+3]) != NULL )
               sscanf(&parser->string_get[i+1], "%3o", &strvalue);

            else
               {
               g_set_error_literal (err, parser->domain, 0, error_str[0]);
               return NULL;
               }

            /* Check Unicode value */
            if ( strvalue > 255 )
               {
               g_set_error_literal (err, parser->domain, 0, error_str[1]);
               return NULL;
               }

            /* Modify string */
            parser->string_get[i] = strvalue;
            for ( j=i+1; j<=i2-3; j++ )
               parser->string_get[j] = parser->string_get[j+3];
            i2 = i2 - 3;
            }
         }
   parser->string_get[i2+1] = '\0';


   /* Calculate return variables */
   if (i1_str)
     *i1_str = (unsigned int) i1;
   if (i2_str)
     *i2_str = (unsigned int) i2;
   *size = (unsigned int) i2 - (unsigned int) i1 + 1;

   return &parser->string_get[i1];
   }


const char symbol_string[]  = "ld.cCtTsSiIpPhH+xra";
const char symbol_string1[] = "cCtTsSiIpPhH";       
const char symbol_string2[] = "+xra";               
const char color_string[]   = "kaswrylqbfmogtnpx";  


enum GseAnchorType
anchor_from_string (const char *str)
{
  enum GseAnchorType anchor = GSE_ANCHOR_INVALID;
  
  if ( strcmp (str, "off") == 0 )
    anchor = GSE_ANCHOR_NONE;
  else if ( strcmp (str, "center") == 0 )
    anchor = GSE_ANCHOR_CENTER;
  else if ( strcmp (str, "north") == 0 )
    anchor = GSE_ANCHOR_NORTH;
  else if ( strcmp (str, "northeast") == 0 )
    anchor = GSE_ANCHOR_NORTH_EAST;
  else if ( strcmp (str, "east") == 0 )
    anchor = GSE_ANCHOR_EAST;
  else if ( strcmp (str, "southeast") == 0 )
    anchor = GSE_ANCHOR_SOUTH_EAST;
  else if ( strcmp (str, "south") == 0 )
    anchor = GSE_ANCHOR_SOUTH;
  else if (strcmp (str, "southwest") == 0 )
    anchor = GSE_ANCHOR_SOUTH_WEST;
  else if ( strcmp (str, "west") == 0 )
    anchor = GSE_ANCHOR_WEST;
  else if ( strcmp (str, "northwest") == 0 )
    anchor = GSE_ANCHOR_NORTH_WEST;

  return anchor;
}

gboolean 
ReadParamFile (struct gse_ctx *context, struct data_parser *parser,
	       GError **err)
{
     int locale = 0;
     double legend_font_size = 14;
   /* Declare variables */
   int i, j, ch, iplot, nplots, iformat, nformat, nformats_total,
       index, index_bin_values,
       index_bin_refs, ibinwidth, index_stemflags, ibinvalue, ibinref, ininterp,
       istemflag, imeshcolor, icontourcolor, stylesize;
   unsigned int size;
   const char *string;
   char *pchar, character;

   const char *error_str[] =
      { "Missing file_name data.",
        "Missing file_format data.",
        "Invalid or missing axis_type parameter.",
        "Invalid or missing plot_type parameter.",
        "Invalid axis_limits parameter.",
        "Invalid or incomplete view direction angles.",
        "Invalid grid parameter.",
        "Invalid histogram bin width.",
        "Invalid ninterp value.",
        "Invalid contours value.",
        "Invalid or missing contour color.",
        "Invalid or missing mesh color.",
        "Invalid window-size data;\ntwo integers required.",
        "Invalid or missing date-time font size.",
        "Invalid or missing legend font size.",
        "Invalid or missing text font size.",
        "Invalid or missing tick-label font size.",
        "Invalid or missing axis-label font size.",
        "Invalid or missing title font size.",
        "Invalid x-axis limit.",
        "Invalid y-axis limit.",
        "Invalid axis limit." };
   FILE *fptr;


   /* Set font family */
   pango_font_description_set_family(context->font_date_time,   context->font_name);
   pango_font_description_set_family(context->font_text,        context->font_name);
   pango_font_description_set_family(context->font_tick_labels, context->font_name);
   pango_font_description_set_family(context->font_axis_labels, context->font_name);
   pango_font_description_set_family(context->font_title,       context->font_name);


   /* Set font sizes */
   pango_font_description_set_absolute_size(context->font_date_time,   context->font_size_date_time*PANGO_SCALE);
   pango_font_description_set_absolute_size(context->font_text,        context->font_size_text*PANGO_SCALE);
   pango_font_description_set_absolute_size(context->font_tick_labels, context->font_size_tick_labels*PANGO_SCALE);
   pango_font_description_set_absolute_size(context->font_axis_labels, context->font_size_axis_labels*PANGO_SCALE);
   pango_font_description_set_absolute_size(context->font_title,       context->font_size_title*PANGO_SCALE);


   /* Open plot-parameter file */
   if ( (fptr = fopen(parser->param_file, "r")) == NULL )
     {
       const char *diag = strerror (errno);
       g_set_error (err, parser->domain, 0,
		    _("Cannot open plot-parameter file \"%s\": %s"),
		    parser->param_file, diag);
       return FALSE;
     }


   /* Get maximum line length */
   parser->maxline = 0;
   i = 0;
   while ( (ch = fgetc(fptr)) != EOF )
      {
      if ( ch != '\n' )
         i++;
      else
         {
         if ( parser->maxline < i )
            parser->maxline = i;
         i = 0;
         }
      }
   parser->maxline++;


   /****************************************************************************
   * Increase parser->maxline for use with char *fgets(char *s, int n, FILE *stream).
   * fgets reads at most the next n-1 characters into the array s. It is desired
   * that fgets read the terminating character so that following uses of fgets
   * start with the next line in the file.
   ****************************************************************************/
   parser->maxline++;
   parser->line = xmalloc(parser->maxline*sizeof(char));


   /* Get locale information */
   rewind(fptr);
   while ( fgets(parser->line, parser->maxline, fptr) != NULL )
      {
      if ( strncmp(parser->line, "locale", 6) == 0 )
         {
	   if ( sscanf(&parser->line[6], "%d", &locale) != 1 )
            {
	      g_set_error (err, parser->domain, 0,
			   "Unable to read locale integer at position %ld.",
			   ftell (fptr));
	      return FALSE;
            }
         }
      }

   /* decimal point is a period */
   if ( locale == 1 )
      {
      if ( setlocale(LC_NUMERIC, "C" ) == NULL )
         g_set_error_literal (err, parser->domain, 0, "Error setting \"C\" locale.");
      }

   /* native locale */
   else if ( locale == 2 )
      {
      if ( setlocale(LC_NUMERIC, "" ) == NULL )
         g_set_error_literal (err, parser->domain, 0, "Error setting \"\" locale.");
      }

   /* french locale */
   else if ( locale == 3 )
      {
      if ( setlocale(LC_NUMERIC, "french" ) == NULL )
         g_set_error_literal (err, parser->domain, 0, "Error setting \"french\" locale.");
      }

   /* german locale */
   else if ( locale == 4 )
      {
      if ( setlocale(LC_NUMERIC, "german" ) == NULL )
         g_set_error_literal (err, parser->domain, 0, "Error setting \"german\" locale.");
      }


   /* Get number of file_name entries */
   iplot = 0;
   rewind(fptr);
   while ( fgets(parser->line, parser->maxline, fptr) != NULL )
      {
      if ( strncmp(parser->line, "file_name", 9) == 0 )
         iplot++;
      else if ( strncmp(parser->line, "#####", 5) == 0 )
         break;
      }
   nplots = iplot;
   context->plot_param.nplots = nplots;

   /* Get background color */
   rewind(fptr);
   while ( fgets(parser->line, parser->maxline, fptr) != NULL )
      {
      if ( strncmp(parser->line, "background_color", 16) == 0 )
         {
         size = strlen(&parser->line[16]);
         if ( (string = get_string (parser, &parser->line[16], NULL, NULL, &size, 0, err)) != NULL)
            if ( strcmp(string, "black") == 0 )
               {
               context->canvas_bg_color = 0x000000FF;   /* black */
               context->canvas_fg_color = 0xFFFFFFFF;   /* white */
               }
         }

      else if ( strncmp(parser->line, "background_image", 16) == 0 )
         {
	   unsigned int i2_str;
         size = strlen(&parser->line[16]);
         if ( (string = get_string (parser, &parser->line[16], NULL, &i2_str, &size, 0, err)) != NULL)
            {
	      context->background_image_file = strdup (string);
            }
         index = 16 + i2_str + 2;
         size = strlen(&parser->line[index]);
         if ( (string = get_string (parser, &parser->line[index], NULL, NULL, &size, 0, err)) != NULL)
            {
            if ( strcmp(string, "center") == 0 )
               context->background_image_style = CENTER;
            else if ( strcmp(string, "fill") == 0 )
               context->background_image_style = FILL;
            else if ( strcmp(string, "scale") == 0 )
               context->background_image_style = SCALE;
            else if ( strcmp(string, "zoom") == 0 )
               context->background_image_style = ZOOM;
            }
         }

      else if ( strncmp(parser->line, "#####", 5) == 0 )
         break;
      }


   context->plot_parameters  = xzalloc (nplots * sizeof (*context->plot_parameters));

   /* Set default number of contours */
   /* (2d and 3d contour plots) */
   context->ncontours = 0;

   /* Set defaults */
   for ( i=1; i<=nplots; i++ )
     {
       context->plot_parameters[i-1].bin_widths = -1.0;
       context->plot_parameters[i-1].bin_value = BIN_percent;
       context->plot_parameters[i-1].bin_ref = REF_mean;
       context->plot_parameters[i-1].stem_type = STEM_OFF;
       context->plot_parameters[i-1].ninterp = 20;
       context->plot_parameters[i-1].meshcolors = context->canvas_fg_color;
       context->plot_parameters[i-1].contourcolors = context->canvas_fg_color;
       context->plot_parameters[i-1].styleflags = -1;
       context->plot_parameters[i-1].stylechar1 = '*';
       context->plot_parameters[i-1].texture = SOLID;
       context->plot_parameters[i-1].stylechar2 = '*';
       context->plot_parameters[i-1].stylesizes = 1;
       context->plot_parameters[i-1].alphacolor = 0xFF;
       context->plot_parameters[i-1].zblack = -DBL_MAX;
       context->plot_parameters[i-1].zwhite =  DBL_MAX;
     }

   /* Read filenames */
   iplot = 0;
   rewind(fptr);
   if ( nplots > 0 )
      {
      while ( fgets(parser->line, parser->maxline, fptr) != NULL )
         {
         if ( strncmp(parser->line, "file_name", 9) == 0 )
            {
            iplot++;
	    struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];
            size = strlen(&parser->line[9]);
            if ( (string = get_string (parser, &parser->line[9], NULL, NULL, &size, 0, err)) != NULL)
               {
		 the_plot->filename = strdup (string);
               }
            }
         else if ( strncmp(parser->line, "file_format", 11) == 0 )
            {
            size = strlen(&parser->line[11]);
            if ( (string = get_string (parser, &parser->line[11], NULL, NULL, &size, 0, err)) != NULL)
               {
	       context->plot_parameters [iplot-1].template = strdup (string);
	       }
	    }
	 
         else if ( strncmp(parser->line, "#####", 5) == 0 )
            break;
         }
      }

   /* Read axis type */
   rewind(fptr);
   while ( fgets(parser->line, parser->maxline, fptr) != NULL )
      {
      if ( strncmp(parser->line, "axis_type", 9) == 0 )
         {
         size = strlen(&parser->line[9]);
         if ( (string = get_string (parser, &parser->line[9], NULL, NULL, &size, 0, err)) != NULL)
	   {
	     if (strcmp (string, "semilogy") == 0)
	       context->plot_param.axes_type = AXES_semilogy;
	     else if (strcmp (string, "semilogx") == 0)
	       context->plot_param.axes_type = AXES_semilogx;
	     else if (strcmp (string, "loglog") == 0)
	       context->plot_param.axes_type = AXES_loglog;
	     else if (strcmp (string, "linear") == 0)
	       context->plot_param.axes_type = AXES_linear;
	     else if (strcmp (string, "polar") == 0)
	       context->plot_param.axes_type = AXES_polar;
	     else if (strcmp (string, "3d") == 0)
	       context->plot_param.axes_type = AXES_3d;
	     else
	       {
		 g_set_error (err, parser->domain, 0,
			      _("Invalid or missing axis_type parameter at position %ld"),
			      ftell (fptr));
		 return FALSE;
	       }
	   }
         }

      else if ( strncmp(parser->line, "#####", 5) == 0 )
         break;
      }

   /* Read plot types */
   iplot = 0;
   int idx = 0;
   rewind(fptr);
   if ( nplots > 0 )
      {
      while ( fgets(parser->line, parser->maxline, fptr) != NULL )
         {
         if ( strncmp(parser->line, "plot_type", 9) == 0 )
            {
            iplot++;
            size = strlen(&parser->line[9]);
            if ( (string = get_string (parser, &parser->line[9], NULL, NULL, &size, 0, err)) != NULL)
               {
		 if (strcmp (string, "points") == 0)
		   context->plot_parameters[idx].plot_type = PLOT_points;
		 else if (strcmp (string, "contour") == 0)
		   context->plot_parameters[idx].plot_type = PLOT_contour;
		 else if (strcmp (string, "color") == 0)
		   context->plot_parameters[idx].plot_type = PLOT_color;
		 else if (strcmp (string, "histogram") == 0)
		   context->plot_parameters[idx].plot_type = PLOT_histogram;
		 else if (strcmp (string, "mesh") == 0)
		   context->plot_parameters[idx].plot_type = PLOT_mesh;
		 else
		   {
		     g_set_error (err, parser->domain, 0,
				  _("Unknown plot type: %s"), string);
		     return FALSE;
		   }
	       idx++;
               }
            }

         else if ( strncmp(parser->line, "#####", 5) == 0 )
            break;
         }
      }

   idx = 0;

   /* Read plot styles */
   iplot = 0;
   rewind(fptr);
   if ( nplots > 0 )
      {
      while ( fgets(parser->line, parser->maxline, fptr) != NULL )
         {
         if ( strncmp(parser->line, "plot_style", 10) == 0 )
            {
            iplot++;

	    struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];

	    
            if ( context->plot_parameters[idx].plot_type == PLOT_points ||
                 context->plot_parameters[idx].plot_type == PLOT_histogram )
               {
               if ( (sscanf(&parser->line[10], " %c 0x%x", &the_plot->stylechar1, (unsigned int *) &the_plot->stylecolor2) == 2 ||
                     sscanf(&parser->line[10], " %c 0X%x", &the_plot->stylechar1, (unsigned int *) &the_plot->stylecolor2) == 2) &&
                    isdigit(the_plot->stylechar1) == 0 )
                  {
                  /************************************************************
                  *
                  * 2d points and 3d points plot types
                  * Read symbol character, hexadecimal color, and symbol size
                  *
                  * 2d histogram
                  * Read symbol character and hexadecimal color
                  *
                  ************************************************************/
		    the_plot->styleflags = STYLE_CHAR_HEX;
                  the_plot->fill_colors_rgba    = the_plot->stylecolor2;         /* set specified fill color */
                  the_plot->outline_colors_rgba = the_plot->stylecolor2;         /* set specified outline color */
                  if ( (pchar = strchr("ctsiphb", the_plot->stylechar1)) != NULL )
                     the_plot->fill_colors_rgba = context->canvas_bg_color;             /* set fill color to background color */
                  if ( (pchar = strchr("cCtTsSiIpPhH+xra", the_plot->stylechar1)) != NULL )
                     the_plot->stylesizes = 6;                                  /* set default symbol size */
                  if ( sscanf(&parser->line[10], " %*c %*s %d", &stylesize) == 1 )
                     the_plot->stylesizes = abs(stylesize);                     /* get symbol size */
                  }

               else if ( sscanf(&parser->line[10], " %c %c", &the_plot->stylechar1, &the_plot->stylechar2) == 2 &&
                         (pchar = strchr(color_string, the_plot->stylechar2)) != NULL )
                  {
                  /************************************************************
                  *
                  * 2d points and 3d points plot types
                  * Read symbol character, color character, and symbol size
                  *
                  * 2d histogram
                  * Read symbol character and color character
                  *
                  ************************************************************/
                  the_plot->styleflags = STYLE_CHAR_CHAR;
                  index = pchar - &color_string[0];                            /* get index to color character */
                  the_plot->stylecolor2 = context->color_rgba[index];                    /* set specified color */
                  the_plot->fill_colors_rgba    = the_plot->stylecolor2;         /* set specified fill color */
                  the_plot->outline_colors_rgba = the_plot->stylecolor2;         /* set specified outline color */
                  if ( (pchar = strchr("ctsiphb", the_plot->stylechar1)) != NULL )
                     the_plot->fill_colors_rgba = context->canvas_bg_color;              /* set fill color to background color */
                  if ( (pchar = strchr("cCtTsSiIpPhH+xra", the_plot->stylechar1)) != NULL )
                     the_plot->stylesizes = 6;                                  /* set default symbol size */
                  if ( sscanf(&parser->line[10], " %*c %*c %d", &stylesize) == 1 )
                     the_plot->stylesizes = abs(stylesize);                     /* get symbol size */
                  }
               }

            else if ( context->plot_parameters[idx].plot_type == PLOT_color &&
                      context->plot_param.axes_type == AXES_linear )
               {
               size = strlen(&parser->line[10]);
               if ( (string = get_string (parser, &parser->line[10], NULL, NULL, &size, 0, err)) != NULL)
                  {
                  /************************************************************
                  *
                  * 2d color plot type
                  * Read "nearest" or "bilinear" string and two decimal numbers
                  *
                  ************************************************************/
                  if ( strcmp(string, "nearest") == 0 )
                     {
                     the_plot->styleflags = STYLE_NEAREST;
                     sscanf(&string[size+1], " %lf %lf", &the_plot->zblack, &the_plot->zwhite);
                     }
                  else if ( strcmp(string, "bilinear") == 0 )
                     {
                     the_plot->styleflags = STYLE_BILINEAR;
                     sscanf(&string[size+1], " %lf %lf", &the_plot->zblack, &the_plot->zwhite);
                     }
                  }
               }

            else if ( context->plot_parameters[idx].plot_type == PLOT_contour &&
                      context->plot_param.axes_type == AXES_linear )
               {
               size = strlen(&parser->line[10]);
               if ( (string = get_string (parser, &parser->line[10], NULL, NULL, &size, 0, err)) != NULL)
                  {
                  /************************************************************
                  *
                  * 2d contour plot type
                  * Read "auto" string and line width
                  *
                  ************************************************************/
                  if ( strcmp(string, "auto") == 0 )
                     {
		       the_plot->styleflags = STYLE_AUTO;
                     if ( sscanf(&string[size+1], " %d", &stylesize) == 1 )
                        the_plot->stylesizes = abs(stylesize);                  /* get line width */
                     }
                  }

               else if ( sscanf(&parser->line[10], " 0x%x", (unsigned int *) &the_plot->stylecolor1) == 1 ||
                         sscanf(&parser->line[10], " 0X%x", (unsigned int *) &the_plot->stylecolor1) == 1 )
                  {
                  /************************************************************
                  *
                  * 2d contour plot type
                  * Read hexadecimal color and line width
                  *
                  ************************************************************/
                  the_plot->styleflags = STYLE_HEX;
                  if ( sscanf(&parser->line[10], " %*s %d", &stylesize) == 1 )
                     the_plot->stylesizes = abs(stylesize);                     /* get line width */
                  }

               else if ( sscanf(&parser->line[10], " %c", &the_plot->stylechar1) == 1 &&
                         (pchar = strchr(color_string, the_plot->stylechar1)) != NULL )
                  {
                  /************************************************************
                  *
                  * 2d contour plot type
                  * Read color character and line width
                  *
                  ************************************************************/
                  the_plot->styleflags = STYLE_CHAR;
                  index = pchar - &color_string[0];                            /* get index to color character */
                  the_plot->stylecolor1 = context->color_rgba[index];                    /* set specified color */
                  if ( sscanf(&parser->line[10], " %*c %d", &stylesize) == 1 )
                     the_plot->stylesizes = abs(stylesize);                     /* get line width */
                  }
               }

            else if ( (context->plot_parameters[idx].plot_type == PLOT_contour ||
                       context->plot_parameters[idx].plot_type == PLOT_color ||
                       context->plot_parameters[idx].plot_type == PLOT_mesh) &&
                      context->plot_param.axes_type == AXES_3d )
               {
               size = strlen(&parser->line[10]);
               if ( (context->plot_parameters[idx].plot_type == PLOT_color ||
                     context->plot_parameters[idx].plot_type == PLOT_mesh) &&
                    (string = get_string (parser, &parser->line[10], NULL, NULL, &size, 0, err)) != NULL)
                  {
                  /************************************************************
                  *
                  * 3d color and 3d mesh plot types
                  * Read "auto" string and color alpha value
                  *
                  ************************************************************/
                  if ( strcmp(string, "auto") == 0 )
                     {
                     the_plot->styleflags = STYLE_AUTO;
                     if ( sscanf(&string[size+1], " 0x%x", (unsigned int *) &the_plot->alphacolor) == 1 ||
                          sscanf(&string[size+1], " 0X%x", (unsigned int *) &the_plot->alphacolor) == 1 )
                        {
                        if ( the_plot->alphacolor > 0xFF )                      /* check alpha value */
                           the_plot->alphacolor = 0xFF;
                        }
                     }
                  }

               else if ( sscanf(&parser->line[10], " 0x%x 0x%x", (unsigned int *) &the_plot->stylecolor1, (unsigned int *) &the_plot->stylecolor2) == 2 ||
                         sscanf(&parser->line[10], " 0X%x 0X%x", (unsigned int *) &the_plot->stylecolor1, (unsigned int *) &the_plot->stylecolor2) == 2 ||
                         sscanf(&parser->line[10], " 0x%x 0X%x", (unsigned int *) &the_plot->stylecolor1, (unsigned int *) &the_plot->stylecolor2) == 2 ||
                         sscanf(&parser->line[10], " 0X%x 0x%x", (unsigned int *) &the_plot->stylecolor1, (unsigned int *) &the_plot->stylecolor2) == 2 )
                  {
                  /************************************************************
                  *
                  * 3d contour and 3d mesh plot types
                  * Read two hexadecimal colors
                  *
                  ************************************************************/
                  the_plot->styleflags = STYLE_HEX_HEX;
                  }

               else if ( sscanf(&parser->line[10], " 0x%x %c", (unsigned int *) &the_plot->stylecolor1, &the_plot->stylechar2) == 2 ||
                         sscanf(&parser->line[10], " 0X%x %c", (unsigned int *) &the_plot->stylecolor1, &the_plot->stylechar2) == 2 )
                  {
                  /************************************************************
                  *
                  * 3d contour and 3d mesh plot types
                  * Read hexadecimal color and color character
                  *
                  ************************************************************/
                  the_plot->styleflags = STYLE_HEX_CHAR;
                  }

               else if ( sscanf(&parser->line[10], " %c 0x%x", &the_plot->stylechar1, (unsigned int *) &the_plot->stylecolor2) == 2 ||
                         sscanf(&parser->line[10], " %c 0X%x", &the_plot->stylechar1, (unsigned int *) &the_plot->stylecolor2) == 2 )
                  {
                  /************************************************************
                  *
                  * 3d contour and 3d mesh plot types
                  * Read color character and hexadecimal color
                  *
                  ************************************************************/
                  the_plot->styleflags = STYLE_CHAR_HEX;
                  }

               else if ( sscanf(&parser->line[10], " %c %c", &the_plot->stylechar1, &the_plot->stylechar2) == 2 )
                  {
                  /************************************************************
                  *
                  * 3d contour and 3d mesh plot types
                  * Read two color characters
                  *
                  ************************************************************/
                  the_plot->styleflags = STYLE_CHAR_CHAR;
                  }
               }

            /* Increment index */
	    idx++;
	    the_plot->texture = char_to_texture (the_plot->stylechar1);
            }

         else if ( strncmp(parser->line, "#####", 5) == 0 )
            break;
         }
      }


   /* Read remainder of plot-parameter file */
   ibinwidth = 0;
   ibinvalue = 0;
   ibinref = 0;
   ininterp = 0;
   istemflag = 0;
   imeshcolor = 0;
   icontourcolor = 0;
   index_bin_values = 0;
   index_bin_refs = 0;
   index_stemflags = 0;
   rewind(fptr);
   while ( fgets(parser->line, parser->maxline, fptr) != NULL )
      {
      if ( strncmp(parser->line, "axis_scale", 10) == 0 )
         {
         size = strlen(&parser->line[10]);
         if ( (string = get_string (parser, &parser->line[10], NULL, NULL, &size, 0, err)) != NULL)
	   context->plot_param.equal_axes = (strcmp (string, "equal") == 0);
         }

      else if ( strncmp(parser->line, "axis_limits", 11) == 0 )
         {
         size = strlen(parser->line);
         i = 11;
         for ( j=1; j<=6; j++ )
            {
            while ( parser->line[i] == ' ' || parser->line[i] == '\t' )
               i++;

            if ( sscanf(&parser->line[i], "%lf", &context->plot_param.axis_limits[j-1]) == 1 )
               context->axis_limits[j-1] = TRUE;
            else if ( sscanf(&parser->line[i], " %c", &character ) == 1 )
               {
               if ( character == '#' )
                  break;
               else if ( character != '*' )
                  {
                  g_set_error_literal (err, parser->domain, 0, error_str[4]);
                  return FALSE;
                  }
               }

            if ( (pchar = strchr(&parser->line[i], ' ')) == NULL )
               break;

            i = i + pchar - &parser->line[i];
            }
         }

      else if ( strncmp(parser->line, "view3d", 6) == 0 )
         {
         if ( sscanf(&parser->line[6], "%lf %lf", &context->plot_param_3d.phi, &context->plot_param_3d.theta) != 2 )
            {
            g_set_error_literal (err, parser->domain, 0, error_str[5]);
            return FALSE;
            }
         }

      else if ( strncmp(parser->line, "minor_ticks", 11) == 0 )
         {
         size = strlen(&parser->line[11]);
         if ( (string = get_string (parser, &parser->line[11], NULL, NULL, &size, 0, err)) != NULL)
            {
            strncpy(&context->plot_param.minor_ticks[0], string, 3);
            if ( strcmp(string, "on") == 0 )
               context->minor_ticks_flag = 1;
            }
         }

      else if ( strncmp(parser->line, "grid", 4) == 0 )
         {
         size = strlen(&parser->line[4]);
         if ( (string = get_string (parser, &parser->line[4], NULL, NULL, &size, 0, err)) != NULL &&
              strcmp(string, "off") == 0 )
            {
            strncpy(&context->plot_param.grid[0], string, 3);
            }
         else if ( sscanf(&parser->line[4], " %c 0x%x", &context->gridchar1, &context->gridcolor) == 2 ||
                   sscanf(&parser->line[4], " %c 0X%x", &context->gridchar1, &context->gridcolor) == 2 )
            {
            strcpy(&context->plot_param.grid[0], "on2");
            }
         else if ( sscanf(&parser->line[4], " %c %c", &context->gridchar1, &context->gridchar2) == 2 &&
                   (pchar = strchr(color_string, context->gridchar2)) != NULL )
            {
            index = pchar - &color_string[0];   /* get index to color character */
            context->gridcolor = context->color_rgba[index];      /* set specified color */
            strcpy(&context->plot_param.grid[0], "on1");
            }
         else
            {
            g_set_error_literal (err, parser->domain, 0, error_str[6]);
            return FALSE;
            }
         }

      else if ( strncmp(parser->line, "xlabel", 6) == 0 )
         {
         size = strlen(&parser->line[6]);
         if ( (string = get_string (parser, &parser->line[6], NULL, NULL, &size, 1, err)) != NULL)
            {
	    context->xlabel = strdup (string);
            }
         }

      else if ( strncmp(parser->line, "ylabel", 6) == 0 )
         {
         size = strlen(&parser->line[6]);
         if ( (string = get_string (parser, &parser->line[6], NULL, NULL, &size, 1, err)) != NULL)
            {
	      context->ylabel = xmalloc(size + 1);
            strcpy(context->ylabel, string);
            }
         }

      else if ( strncmp(parser->line, "zlabel", 6) == 0 )
         {
         size = strlen(&parser->line[6]);
         if ( (string = get_string (parser, &parser->line[6], NULL, NULL, &size, 1, err)) != NULL)
            {
	      context->zlabel = xmalloc(size + 1);
            strcpy(context->zlabel, string);
            }
         }

      else if ( strncmp(parser->line, "title", 5) == 0 )
         {
         size = strlen(&parser->line[5]);
         if ( (string = get_string (parser, &parser->line[5], NULL, NULL, &size, 1, err)) != NULL)
            {
            context->title = xmalloc(size + 1);
            strcpy(context->title, string);
            }
         }

      else if ( strncmp(parser->line, "bin_width", 9) == 0 )
         {
         ibinwidth++;
         if ( ibinwidth <= nplots )
            if ( sscanf(&parser->line[9], "%lf", &context->plot_parameters[ibinwidth-1].bin_widths) != 1 )
               {
               g_set_error_literal (err, parser->domain, 0, error_str[7]);
               return FALSE;
               }
         }

      else if ( strncmp(parser->line, "bin_value", 9) == 0 )
         {
         ibinvalue++;
         if ( ibinvalue <= nplots )
            {
            size = strlen(&parser->line[9]);
            if ( (string = get_string (parser, &parser->line[9], NULL, NULL, &size, 0, err)) != NULL)
               {
		 if (strcmp (string, "number") == 0)
		   context->plot_parameters[index_bin_values++].bin_value = BIN_number;
		 else if (strcmp (string, "fraction") == 0)
		   context->plot_parameters[index_bin_values++].bin_value = BIN_fraction;
		 else if (strcmp (string, "percent") == 0)
		   context->plot_parameters[index_bin_values++].bin_value = BIN_percent;
               }
            }
         }

      else if ( strncmp(parser->line, "bin_ref", 7) == 0 )
         {
         ibinref++;
         if ( ibinref <= nplots )
            {
            size = strlen(&parser->line[7]);
            if ( (string = get_string (parser, &parser->line[7], NULL, NULL, &size, 0, err)) != NULL)
               {
		 if (strcmp (string, "mean") == 0)
		   context->plot_parameters[index_bin_refs].bin_ref = REF_mean;
		 else if (strcmp (string, "zero") == 0)
		   context->plot_parameters[index_bin_refs].bin_ref = REF_zero;
		 else if (strcmp (string, "integers") == 0)
		   context->plot_parameters[index_bin_refs].bin_ref = REF_integer;

		 index_bin_refs++;
               }
            }
         }

      else if ( strncmp(parser->line, "ninterp", 7) == 0 )
         {
         ininterp++;
         if ( ininterp <= nplots )
            if ( sscanf(&parser->line[7], "%d", &context->plot_parameters[ininterp-1].ninterp) != 1 )
               {
               g_set_error_literal (err, parser->domain, 0, error_str[8]);
               return FALSE;
               }
         }

      else if ( strncmp(parser->line, "contours", 8) == 0 )
         {
         if ( sscanf(&parser->line[8], "%d", &context->ncontours) != 1 )
            {
            g_set_error_literal (err, parser->domain, 0, error_str[9]);
            return FALSE;
            }
         }

      else if ( strncmp(parser->line, "contour_color", 13) == 0 )
         {
         icontourcolor++;
         if ( icontourcolor <= nplots )
            {
            if ( sscanf(&parser->line[13], " 0x%x", (unsigned int *) &context->plot_parameters[icontourcolor-1].contourcolors) != 1 &&
                 sscanf(&parser->line[13], " 0X%x", (unsigned int *) &context->plot_parameters[icontourcolor-1].contourcolors) != 1 )
               {
               if ( sscanf(&parser->line[13], " %c", &character) == 1 &&
                    (pchar = strchr(color_string, character)) != NULL )
                  {
                  index = pchar - &color_string[0];                     /* get index to color character */
                  context->plot_parameters[icontourcolor-1].contourcolors = context->color_rgba[index];   /* set specified color */
                  }
               else
                  {
                  g_set_error_literal (err, parser->domain, 0, error_str[10]);
                  return FALSE;
                  }
               }
            }
         }

      else if ( strncmp(parser->line, "mesh_color", 10) == 0 )
         {
         imeshcolor++;
         if ( imeshcolor <= nplots )
            {
            if ( sscanf(&parser->line[10], " 0x%x", (unsigned int *) &context->plot_parameters[imeshcolor-1].meshcolors) != 1 &&
                 sscanf(&parser->line[10], " 0X%x", (unsigned int *) &context->plot_parameters[imeshcolor-1].meshcolors) != 1 )
               {
               if ( sscanf(&parser->line[10], " %c", &character) == 1 &&
                    (pchar = strchr(color_string, character)) != NULL )
                  {
                  index = pchar - &color_string[0];               /* get index to color character */
                  context->plot_parameters[imeshcolor-1].meshcolors = context->color_rgba[index];   /* set specified color */
                  }
               else
                  {
                  g_set_error_literal (err, parser->domain, 0, error_str[11]);
                  return FALSE;
                  }
               }
            }
         }

      else if ( strncmp(parser->line, "stems", 5) == 0 )
         {
         istemflag++;
         if ( istemflag <= nplots )
            {
            if ( sscanf(&parser->line[5], "%lf", &context->plot_parameters[istemflag-1].stemvalues) == 1 )
               {
		 context->plot_parameters[index_stemflags].stem_type = STEM_VALUE;
		 if ( context->plot_param.axes_type == AXES_semilogy ||
                    context->plot_param.axes_type == AXES_loglog )
                  {
                  if ( context->plot_parameters[istemflag-1].stemvalues != 0.0 )
                     context->plot_parameters[istemflag-1].stemvalues = log10(fabs(context->plot_parameters[istemflag-1].stemvalues));
                  else
                     context->plot_parameters[istemflag-1].stemvalues = log10(DBL_MIN);
                  }
               index_stemflags++;
               }
            else
               {
               size = strlen(&parser->line[5]);
               if ( (string = get_string (parser, &parser->line[5], NULL, NULL, &size, 0, err)) != NULL)
                  {
		    if (strcmp (string, "num") == 0)
		      context->plot_parameters[index_stemflags].stem_type = STEM_VALUE;
		    else if (strcmp (string, "on") == 0)
		      context->plot_parameters[index_stemflags].stem_type = STEM_ON;
		    else
		      context->plot_parameters[index_stemflags].stem_type = STEM_OFF;
		      
                  index_stemflags++;
                  }
               }
            }
         }

      else if ( strncmp(parser->line, "date_time", 9) == 0 )
         {
         size = strlen(&parser->line[9]);
         if ( (string = get_string (parser, &parser->line[9], NULL, NULL, &size, 0, err)) != NULL)
	   context->plot_param.page_note_anchor = anchor_from_string (string);
         }

      else if ( strncmp(parser->line, "save_close", 10) == 0 )
         {
         size = strlen(&parser->line[10]);
         if ( (string = get_string (parser, &parser->line[10], NULL, NULL, &size, 0, err)) != NULL)
            {
	      parser->save_filename  = strdup (string);
	      parser->close_after_save = true;
            }
         }

      else if ( strncmp(parser->line, "save", 4) == 0 )
         {
         size = strlen(&parser->line[4]);
         if ( (string = get_string (parser, &parser->line[4], NULL, NULL, &size, 0, err)) != NULL)
            {
	      parser->save_filename  = strdup (string);
	      parser->close_after_save = false;
            }
         }
      else if ( strncmp(parser->line, "window_size", 11) == 0 )
         {
         if ( sscanf(&parser->line[11], "%d %d", &parser->window_width, &parser->window_height ) != 2 )
            {
            g_set_error_literal (err, parser->domain, 0, error_str[12]);
            return FALSE;
            }
         }
      else if ( strncmp(parser->line, "plot_box", 8) == 0 )
         {
         size = strlen(&parser->line[8]);
         if ( (string = get_string (parser, &parser->line[8], NULL, NULL, &size, 0, err)) != NULL)
	   context->plot_param.draw_box = (strcmp (string, "on") == 0);
         }

      else if ( strncmp(parser->line, "x_tick_marks", 12) == 0 )
         {
         size = strlen(&parser->line[12]);
         if ( (string = get_string (parser, &parser->line[12], NULL, NULL, &size, 0, err)) != NULL)
	   context->plot_param.x_tick_marks = (strcmp (string, "on") == 0);
         }

      else if ( strncmp(parser->line, "y_tick_marks", 12) == 0 )
         {
         size = strlen(&parser->line[12]);
         if ( (string = get_string (parser, &parser->line[12], NULL, NULL, &size, 0, err)) != NULL)
	   context->plot_param.y_tick_marks = (strcmp (string, "on") == 0);
         }

      else if ( strncmp(parser->line, "z_tick_marks", 12) == 0 )
         {
         size = strlen(&parser->line[12]);
         if ( (string = get_string (parser, &parser->line[12], NULL, NULL, &size, 0, err)) != NULL)
	   context->plot_param.z_tick_marks = (strcmp (string, "on") == 0);
         }

      else if ( strncmp(parser->line, "x_tick_labels", 13) == 0 )
         {
         size = strlen(&parser->line[13]);
         if ( (string = get_string (parser, &parser->line[13], NULL, NULL, &size, 0, err)) != NULL)
	   context->plot_param.x_tick_labels = (strcmp (string, "on") == 0);
         }

      else if ( strncmp(parser->line, "y_tick_labels", 13) == 0 )
         {
         size = strlen(&parser->line[13]);
         if ( (string = get_string (parser, &parser->line[13], NULL, NULL, &size, 0, err)) != NULL)
	   context->plot_param.y_tick_labels = (strcmp (string, "on") == 0);
         }

      else if ( strncmp(parser->line, "z_tick_labels", 13) == 0 )
         {
         size = strlen(&parser->line[13]);
         if ( (string = get_string (parser, &parser->line[13], NULL, NULL, &size, 0, err)) != NULL)
	   context->plot_param.z_tick_labels = (strcmp (string, "on") == 0);
         }

      else if ( strncmp(parser->line, "font_name", 9) == 0 )
         {
         size = strlen(&parser->line[9]);
         if ( (string = get_string (parser, &parser->line[9], NULL, NULL, &size, 0, err)) != NULL)
            {
            free(context->font_name);
            context->font_name = xmalloc(size + 1);
            strcpy(context->font_name, string);
            }
         }

      else if ( strncmp(parser->line, "font_size_date_time", 19) == 0 )
         {
         if ( sscanf(&parser->line[19], "%lf", &context->font_size_date_time) != 1 )
            {
            g_set_error_literal (err, parser->domain, 0, error_str[13]);
            return FALSE;
            }
         }

      else if ( strncmp(parser->line, "font_size_legend", 16) == 0 )
         {
	   if ( sscanf(&parser->line[16], "%lf", &legend_font_size) != 1 )
	     {
	       g_set_error_literal (err, parser->domain, 0, error_str[14]);
	       return FALSE;
	     }
         }

      else if ( strncmp(parser->line, "font_size_text", 14) == 0 )
         {
         if ( sscanf(&parser->line[14], "%lf", &context->font_size_text) != 1 )
            {
            g_set_error_literal (err, parser->domain, 0, error_str[15]);
            return FALSE;
            }
         }

      else if ( strncmp(parser->line, "font_size_tick_labels", 21) == 0 )
         {
         if ( sscanf(&parser->line[21], "%lf", &context->font_size_tick_labels) != 1 )
            {
            g_set_error_literal (err, parser->domain, 0, error_str[16]);
            return FALSE;
            }
         }

      else if ( strncmp(parser->line, "font_size_axis_labels", 21) == 0 )
         {
         if ( sscanf(&parser->line[21], "%lf", &context->font_size_axis_labels) != 1 )
            {
            g_set_error_literal (err, parser->domain, 0, error_str[17]);
            return FALSE;
            }
         }

      else if ( strncmp(parser->line, "font_size_title", 15) == 0 )
         {
         if ( sscanf(&parser->line[15], "%lf", &context->font_size_title) != 1 )
            {
            g_set_error_literal (err, parser->domain, 0, error_str[18]);
            return FALSE;
            }
         }

      else if ( strncmp(parser->line, "#####", 5) == 0 )
         break;
      }

   /* Parse text strings: */
  rewind(fptr);
  while ( fgets(parser->line, parser->maxline, fptr) != NULL )
    {
      char * text_str = NULL;
      size_t text_capacity = 0;

      while (strncmp(parser->line, "text_string", 11) == 0)
	{
	  unsigned int size = strlen (parser->line + 11);
	  const char *string = get_string (parser, parser->line + 11, NULL, NULL, &size, 1, err);

	  if (string)
	    {
	      text_str = realloc (text_str, text_capacity + size + 2);
	      text_str[text_capacity] = '\0';
	      text_capacity += size + 1;
	    }
	  else
	    break;

	  if (text_str[0] != '\0')
	    strcat (text_str, "\n");
	  strcat (text_str, string);
	  
	  fgets(parser->line, parser->maxline, fptr);
	}

      if (text_str)
	{
	  context->texts = realloc (context->texts, sizeof (*context->texts) * ++context->n_texts);
	  struct text_spec *ts = context->texts + (context->n_texts - 1);
	  ts->text = text_str;
	  
	  int i;
	  const char *string;
	  ts->coords_ref = GSE_COORDS_INVALID;

	  /* Get text coordinates and anchor */
	  for ( i=1; i<=2; i++ )
	    {
	      if (ts->coords_ref == GSE_COORDS_INVALID)
		{
		  if (strncmp(parser->line, "text_coords_abs", 15) == 0)
		    ts->coords_ref = GSE_COORDS_ABS;
		  else if (strncmp(parser->line, "text_coords_rel", 15) == 0)
		    ts->coords_ref = GSE_COORDS_REL;
		  if (ts->coords_ref == GSE_COORDS_ABS || ts->coords_ref == GSE_COORDS_REL)
		    {
		      if (
			  !(
			    context->plot_param.axes_type != AXES_3d &&
			    (ts->coords_ref == GSE_COORDS_ABS) &&
			    sscanf(&parser->line[15], "%lf %lf", &ts->x, &ts->y) == 2
			    )
			  &&
			  !(
			    context->plot_param.axes_type == AXES_3d &&
			    (ts->coords_ref == GSE_COORDS_ABS) &&
			    sscanf(&parser->line[15], "%lf %lf %lf", &ts->x, &ts->y, &ts->z) == 3
			    )
			  &&
			  !(
			    (ts->coords_ref == GSE_COORDS_REL) &&
			    sscanf(&parser->line[15], "%lf %lf", &ts->x, &ts->y) == 2)
			  )
			{
			  g_set_error (err, parser->domain, 0,
				       _("Invalid or incomplete text coordinates for text \"%s\""), text_str);
			  free(text_str);
			  return FALSE;
			}
		    }
		}

	      else if ( strncmp(parser->line, "text_anchor", 11) == 0 )
		{
                  unsigned int size = strlen(&parser->line[12]);
                  if ( (string = get_string (parser, &parser->line[11], NULL, NULL, &size, 0, err)) != NULL)
		    ts->anchor = anchor_from_string (string);
		  else
		    {
		      g_set_error (err, parser->domain, 0,
				   _("Invalid text anchor: %s"), string);
		      free(text_str);
		      return FALSE;
		    }
		}
	      else
		{
		  g_set_error (err, parser->domain, 0,
			       _("Text coordinates or text anchor not found for text \"%s\""), text_str);
		  free(text_str);
		  return FALSE;
		}
	      fgets(parser->line, parser->maxline, fptr);
	    }
	}
	  
      else if ( strncmp(parser->line, "#####", 5) == 0 )
	break;
    }

  rewind (fptr);
  while ( fgets(parser->line, parser->maxline, fptr) != NULL )
    {
      /* Get image filename */
      if ( strncmp(parser->line, "image_filename", 14) == 0 )
	{
	  char *image_file = NULL;
	  const char *string;
	  unsigned int size = strlen(&parser->line[14]);
	  if ( (string = get_string (parser, &parser->line[14], NULL, NULL, &size, 0, err)) != NULL)
	    {
	      image_file = strdup (string);
	      fgets(parser->line, parser->maxline, fptr);
	    }

	  if (!image_file)
	    continue;
	    
	  context->images = realloc (context->images, sizeof *context->images * ++context->n_images);
	  struct image_spec *is = context->images + context->n_images - 1;
	  is->pixbuf = gdk_pixbuf_new_from_file (image_file, err);
	  if (!is->pixbuf)
	    {
	      return FALSE;
	    }
	    
	  
	  is->coords_ref = GSE_COORDS_INVALID;
	  /* Get image coordinates and anchor */
	  for (int i=1; i<=2; i++ )
	    {
	      if (is->coords_ref == GSE_COORDS_INVALID)
		{
		  if (strncmp(parser->line, "image_coords_abs", 16) == 0)
		    is->coords_ref = GSE_COORDS_ABS;
		  else if (strncmp(parser->line, "image_coords_rel", 16) == 0)
		    is->coords_ref = GSE_COORDS_REL;
		  if (is->coords_ref == GSE_COORDS_ABS || is->coords_ref == GSE_COORDS_REL)
		    {
		      if (
			  !(
			   context->plot_param.axes_type != AXES_3d &&
			   (is->coords_ref == GSE_COORDS_ABS) &&
			   sscanf(&parser->line[16], "%lf %lf", &is->x, &is->y) == 2
			   )
			  &&
			  !(
			   context->plot_param.axes_type == AXES_3d &&
			   (is->coords_ref == GSE_COORDS_ABS) &&
			   sscanf(&parser->line[16], "%lf %lf %lf", &is->x, &is->y, &is->z) == 3
			   )
			  &&
			  !(
			    (is->coords_ref == GSE_COORDS_REL) &&
			    sscanf(&parser->line[16], "%lf %lf", &is->x, &is->y) == 2)
			  )
			{
			  g_set_error (err, parser->domain, 0,
				       _("Invalid or incomplete image coordinates for image \"%s\""),
				       image_file);
			  free(image_file);
			  return FALSE;
			}
		    }
		}
		else if ( strncmp(parser->line, "image_anchor", 12) == 0 )
                  {
		    unsigned int size = strlen(&parser->line[12]);
		    if ( (string = get_string (parser, &parser->line[12], NULL, NULL, &size, 0, err)) != NULL)
		      is->anchor = anchor_from_string (string);
		    else
		      {
			g_set_error (err, parser->domain, 0, _("Invalid image anchor: %s"), string);
			free(image_file);
			return FALSE;
		      }
                  }

		else
                  {
		    g_set_error (err, parser->domain, 0,
				 _("Image coordinates or image anchor not found for image file \"%s\""), image_file);
		    free(image_file);
		    return FALSE;
                  }
	      fgets(parser->line, parser->maxline, fptr);
	    }
	}

      else if ( strncmp(parser->line, "#####", 5) == 0 )
	break;
    }
  rewind (fptr);
       {
    int i;
     int index;
     unsigned int size, nchar;
     long int file_position_1;
     const char *string;
     int nlines = 0;
     /* Get number of legend lines */


     long int file_position = ftell(fptr);
     while ( fgets(parser->line, parser->maxline, fptr) != NULL )
       {
	 if ( strncmp(parser->line, "legend_string", 13) == 0 )
	   {
	     nlines++;
	     file_position_1 = file_position;
	     while ( fgets(parser->line, parser->maxline, fptr) != NULL )
	       {
		 if ( strncmp(parser->line, "legend_string", 13) == 0 )
		   nlines++;
		 else
		   break;
	       }
	     break;
	   }
	 else if ( strncmp(parser->line, "#####", 5) == 0 )
	   break;
	 else
	   file_position = ftell(fptr);
       }
     
     if ( nlines > 0 )
       {
     context->legend = xzalloc (sizeof *context->legend);
     context->legend->font = pango_font_description_new();
     pango_font_description_set_family (context->legend->font,     context->font_name);
     pango_font_description_set_style (context->legend->font, PANGO_STYLE_NORMAL);
     pango_font_description_set_weight (context->legend->font, PANGO_WEIGHT_NORMAL);
     context->legend->font_size = legend_font_size;
     pango_font_description_set_absolute_size (context->legend->font, context->legend->font_size * PANGO_SCALE);

     context->legend->nlines = nlines;

     /* Get legend-line sizes */
     fseek(fptr, file_position_1, SEEK_SET);
     nchar = 0;
     for ( i=1; i<= context->legend->nlines; i++ )
       {
	 fgets(parser->line, parser->maxline, fptr);
	 size = strlen(&parser->line[13]);
	 if ( (string = get_string (parser, &parser->line[13], NULL, NULL, &size, 1, err)) != NULL)
	   nchar = nchar + size + 1;
       }

     /* Read legend lines */
     fseek(fptr, file_position_1, SEEK_SET);
     context->legend->legend_str = xzalloc(nchar);
     index = 0;
     for ( i=1; i<= context->legend->nlines; i++ )
       {
	 fgets(parser->line, parser->maxline, fptr);
	 size = strlen(&parser->line[13]);
	 if ( (string = get_string (parser, &parser->line[13], NULL, NULL, &size, 1, err)) != NULL)
	   {
	     strcpy(context->legend->legend_str + index, string);
	     index = index + size + 1;
	     context->legend->legend_str[index-1] = '\n';
	   }
       }
     context->legend->legend_str[index-1] = '\0';


     /* Get legend coordinates and anchor */
     context->legend->coords_ref = GSE_COORDS_INVALID;
     for ( i=1; i<=2; i++ )
       {
	 if ( fgets(parser->line, parser->maxline, fptr) != NULL )
	   {
	     if ( strncmp(parser->line, "legend_coords_abs", 17) == 0 ||
		  strncmp(parser->line, "legend_coords_rel", 17) == 0 )
	       {
		 if ( (context->plot_param.axes_type != AXES_3d &&
		       strncmp(parser->line, "legend_coords_abs", 17) == 0 &&
		       sscanf(&parser->line[17], "%lf %lf", &context->legend->xlegend, &context->legend->ylegend) == 2) ||
		      (context->plot_param.axes_type == AXES_3d &&
		       strncmp(parser->line, "legend_coords_abs", 17) == 0 &&
		       sscanf(&parser->line[17], "%lf %lf %lf", &context->legend->xlegend, &context->legend->ylegend, &context->legend->zlegend) == 3) ||
		      (strncmp(parser->line, "legend_coords_rel", 17) == 0 &&
		       sscanf(&parser->line[17], "%lf %lf", &context->legend->xlegend, &context->legend->ylegend) == 2) )
		   {
		     if (strncmp (&parser->line[14], "rel", 3) == 0)
		       context->legend->coords_ref = GSE_COORDS_REL;
		     else if (strncmp (&parser->line[14], "abs", 3) == 0)
		       context->legend->coords_ref = GSE_COORDS_ABS;
		   }
		 else
		   {
		     g_set_error_literal (err, parser->domain, 0,
					  _("Invalid or incomplete legend coordinates"));
		     free(context->legend->legend_str);
		     fclose(fptr);
		     return FALSE;
		   }
	       }

	     else if ( strncmp(parser->line, "legend_anchor", 13) == 0 )
	       {
		 size = strlen(&parser->line[13]);
		 if ( (string = get_string (parser, &parser->line[13], NULL, NULL, &size, 0, err)) != NULL)
		   {
		     context->legend->anchor_text = anchor_from_string (string);
		     if (context->legend->anchor_text <= GSE_ANCHOR_NONE)
		       {
			 g_set_error (err, parser->domain, 0, _("Invalid legend anchor: %s"), string);
			 free(context->legend->legend_str);
			 fclose(fptr);
			 return FALSE;
		       }
		   }
		 else
		   {
		     g_set_error_literal (err, parser->domain, 0,  _("Legend anchor not found."));
		     free(context->legend->legend_str);
		     fclose(fptr);
		     return FALSE;
		   }
	       }

	     else
	       {
		 g_set_error_literal (err, parser->domain, 0,
				      _("Legend coordinates or legend anchor not found"));
		 free(context->legend->legend_str);
		 fclose(fptr);
		 return FALSE;
	       }
	   }
       }
     }
       }
  
   fclose(fptr);


   /* Modify data for logarithmic axes */
   if ( context->plot_param.axes_type == AXES_semilogx )
      {
      for ( i=1; i<=2; i++ )
         if ( context->axis_limits[i-1])
            {
            if ( context->plot_param.axis_limits[i-1] <= 0.0 )
               {
               g_set_error_literal (err, parser->domain, 0, error_str[19]);
               return FALSE;
               }
            context->plot_param.axis_limits[i-1] = log10(fabs(context->plot_param.axis_limits[i-1]));
            }
      }

   else if ( context->plot_param.axes_type == AXES_semilogy )
      {
      for ( i=3; i<=4; i++ )
         if ( context->axis_limits[i-1])
            {
            if ( context->plot_param.axis_limits[i-1] <= 0.0 )
               {
               g_set_error_literal (err, parser->domain, 0, error_str[20]);
               return FALSE;
               }
            context->plot_param.axis_limits[i-1] = log10(fabs(context->plot_param.axis_limits[i-1]));
            }
      }

   else if ( context->plot_param.axes_type == AXES_loglog )
      {
      for ( i=1; i<=4; i++ )
         if ( context->axis_limits[i-1])
            {
            if ( context->plot_param.axis_limits[i-1] <= 0.0 )
               {
               g_set_error_literal (err, parser->domain, 0, error_str[21]);
               return FALSE;
               }
            context->plot_param.axis_limits[i-1] = log10(fabs(context->plot_param.axis_limits[i-1]));
            }
      }


     {
    const int ncoords = ( context->plot_param.axes_type == AXES_3d ) ? 3 : 2;
    FILE *fptr = fopen(parser->param_file, "r");
    while ( fgets(parser->line, parser->maxline, fptr) != NULL )
      {
	if ( strncmp(parser->line, "symbol_coords", 13) == 0 )
	  {
	    context->symbols = realloc (context->symbols, sizeof (*context->symbols) * ++context->n_symbols);
	    struct symbol_spec *ss = context->symbols + context->n_symbols - 1;
	    ss->symbol_size = 6;
	    if ( ncoords == 2 )
	      {
		if ( sscanf(&parser->line[13], " %lf %lf",
			    &ss->symbol_coords[0], &ss->symbol_coords[1]) != 2 )
		  {
		    g_set_error_literal (err, parser->domain, 0,
					 _("Invalid or missing symbol coordinates"));
		    return FALSE;
		  }
	      }

	    else if ( ncoords == 3 )
	      {
		if ( sscanf(&parser->line[13], " %lf %lf %lf",
			    &ss->symbol_coords[0], &ss->symbol_coords[1], &ss->symbol_coords[2]) != 3 )
		  {
		    g_set_error_literal (err, parser->domain, 0,
					 _("Invalid or missing symbol coordinates"));
		    return FALSE;
		  }
	      }

	    /* Modify symbol coordinates for logarithmic and polar axes */
	    if ( context->plot_param.axes_type == AXES_semilogx )
	      ss->symbol_coords[0] = log10(fabs(ss->symbol_coords[0]));

	    else if ( context->plot_param.axes_type == AXES_semilogy )
	      ss->symbol_coords[1] = log10(fabs(ss->symbol_coords[1]));

	    else if ( context->plot_param.axes_type == AXES_loglog )
	      {
		ss->symbol_coords[0] = log10(fabs(ss->symbol_coords[0]));
		ss->symbol_coords[1] = log10(fabs(ss->symbol_coords[1]));
	      }

	    else if ( context->plot_param.axes_type == AXES_polar )
	      ss->symbol_coords[0] = ss->symbol_coords[0]*deg2rad;

	    /* Get symbol type and color */
	    if ( fgets(parser->line, parser->maxline, fptr) != NULL )
	      {
	      if ( strncmp(parser->line, "symbol_style", 12) == 0 )
		{
		  char color_char;
		  char *pchar;
		  if ( sscanf(&parser->line[12], " %c 0x%x", &ss->symbol_char,
			      (unsigned int *) &ss->symbol_color) == 2 ||
		       sscanf(&parser->line[12], " %c 0X%x", &ss->symbol_char,
			      (unsigned int *) &ss->symbol_color) == 2 )
		    sscanf(&parser->line[12], " %*c %*s %u", &ss->symbol_size);

		  else if ( sscanf(&parser->line[12], " %c %c", &ss->symbol_char, &color_char) == 2 )
		    {

		      if ( (pchar = strchr(color_string, color_char)) == NULL )
			{
			  g_set_error_literal (err, parser->domain, 0,
					       _("Invalid symbol color character"));
			  return FALSE;
			}

		      else
			{
			  int index = pchar - &color_string[0];   /* get index to color character */
			  ss->symbol_color = context->color_rgba[index];
			  sscanf(&parser->line[12], " %*c %*c %u", &ss->symbol_size);
			}
		    }

		  else
		    {
		      g_set_error_literal (err, parser->domain, 0,
					   _("Invalid or missing symbol or color specification"));
		      return FALSE;
		    }

		  /* Check symbol type */
		  if ( (pchar = strchr("cCtTsSiIpPhH+xra", ss->symbol_char)) == NULL )
		    {
		      g_set_error_literal (err, parser->domain, 0,
					   _("Invalid symbol character"));
		      return FALSE;
		    }
		}
	      
	      else if ( strncmp(parser->line, "#####", 5) == 0 )
		break;
	      }
	  }
      }
    fclose(fptr);
  }

   {
     int ncoords = ( context->plot_param.axes_type == AXES_3d ) ? 6 : 4;
     FILE *fptr = fopen(parser->param_file, "r");
     while ( fgets(parser->line, parser->maxline, fptr) != NULL )
       {
	 /* Get line coordinates */
	 if ( strncmp(parser->line, "line_coords", 11) == 0 )
	   {
	     context->lines = realloc (context->lines, (sizeof *context->lines) * ++context->n_lines);
	     struct line_spec *ls = context->lines + context->n_lines - 1;
	     if ( ncoords == 4 )
	       {
		 if ( sscanf(&parser->line[11], " %lf %lf %lf %lf",
			     &ls->coords[0], &ls->coords[2],
			     &ls->coords[1], &ls->coords[3]) != 4 )
		   {
		     g_set_error_literal (err, parser->domain, 0,
					  _("Invalid or missing line coordinates"));
		     return FALSE;
		   }
	       }

	     else if ( ncoords == 6 )
	       {
		 if ( sscanf(&parser->line[11], " %lf %lf %lf %lf %lf %lf",
			     &ls->coords[0], &ls->coords[3],
			     &ls->coords[1], &ls->coords[4],
			     &ls->coords[2], &ls->coords[5]) != 6 )
		   {
		     g_set_error_literal (err, parser->domain, 0,
					  _("Invalid line color character"));
		     return FALSE;
		   }
	       }

	     /* Modify line coordinates for logarithmic and polar axes */
	     if ( context->plot_param.axes_type == AXES_semilogx )
	       for ( i=1; i<=3; i=i+2 )
		 ls->coords[i-1] = log10(fabs(ls->coords[i-1]));

	     else if ( context->plot_param.axes_type == AXES_semilogy )
	       for ( i=2; i<=4; i=i+2 )
		 ls->coords[i-1] = log10(fabs(ls->coords[i-1]));

	     else if ( context->plot_param.axes_type == AXES_loglog )
	       for ( i=1; i<=4; i++ )
		 ls->coords[i-1] = log10(fabs(ls->coords[i-1]));

	     else if ( context->plot_param.axes_type == AXES_polar )
	       for ( i=1; i<=3; i=i+2 )
		 ls->coords[i-1] = ls->coords[i-1]*deg2rad;

	     char line_char = '?';
	     /* Get line type and color */
	     ls->line_width = 1;
	     if ( fgets(parser->line, parser->maxline, fptr) != NULL )
	       if ( strncmp(parser->line, "line_style", 10) == 0 )
		 {
		   char *pchar;
		   char color_char;

		   if ( sscanf(&parser->line[10], " %c 0x%x", &line_char, (unsigned int *) &ls->line_color) == 2 ||
			sscanf(&parser->line[10], " %c 0X%x", &line_char, (unsigned int *) &ls->line_color) == 2 )
		     sscanf(&parser->line[10], " %*c %*s %u", &ls->line_width);

		   else if ( sscanf(&parser->line[10], " %c %c", &line_char, &color_char) == 2 )
		     {
		       if ( (pchar = strchr(color_string, color_char)) == NULL )
			 {
			   g_set_error_literal (err, parser->domain, 0,
						_("Invalid line color character."));
			   return FALSE;
			 }

		       else
			 {
			   index = pchar - &color_string[0];   /* get index to color character */
			   ls->line_color = context->color_rgba[index];
			   sscanf(parser->line, "%*s %*c %*c %u", &ls->line_width);
			 }
		     }
		   else
		     {
		       g_set_error_literal (err, parser->domain, 0,
					    _("Invalid or missing line or color specification"));
		       return FALSE;
		     }

		   /* Check line type */
		   if ( line_char != 'l' && line_char != 'd' && line_char != '.' )
		     {
		       g_set_error_literal (err, parser->domain, 0,
					    _("Invalid line character"));
		       return FALSE;
		     }
		 }

	     /* Draw line */
	     ls->texture = char_to_texture (line_char);
	   }
	 else if ( strncmp(parser->line, "#####", 5) == 0 )
	   break;
       }

     fclose(fptr);
   }

   
     {
    /* Draw ellipses */
    FILE *fptr = fopen(parser->param_file, "r");
    while ( fgets(parser->line, parser->maxline, fptr) != NULL )
      {
	/* Get ellipse shape parameters */
	if ( strncmp(parser->line, "ellipse_coords", 14) == 0 )
	  {
	    context->ellipses = realloc (context->ellipses, sizeof (*context->ellipses) * ++context->n_ellipses);
	    struct shape_spec *es = context->ellipses + context->n_ellipses - 1;
	    
	    if ( sscanf(&parser->line[14], "%lf %lf %lf %lf %lf",
			&es->x0, &es->y0, &es->width, &es->height, &es->angle) != 5 )
	      {
		g_set_error_literal (err, parser->domain, 0,
				     _("Invalid or missing ellipse coordinates"));
		return FALSE;
	      }
	    es->angle *= deg2rad;
	    es->line_width = 1;
	    if ( fgets(parser->line, parser->maxline, fptr) != NULL )
	      if ( strncmp(parser->line, "ellipse_style", 13) == 0 )
		{
		  char line_char, color_char;
		  if ( sscanf(&parser->line[13], " %c 0x%x", &line_char, (unsigned int *) &es->line_color) == 2 ||
		       sscanf(&parser->line[13], " %c 0X%x", &line_char, (unsigned int *) &es->line_color) == 2 )
		    sscanf(&parser->line[13], " %*c %*s %u", &es->line_width);

		  else if ( sscanf(&parser->line[13], " %c %c", &line_char, &color_char) == 2 )
		    {
		      const char *pchar;
		      if ( (pchar = strchr(color_string, color_char)) == NULL )
			{
			  g_set_error_literal (err, parser->domain, 0, _("Invalid ellipse color character"));
			  return FALSE;
			}
		      else
			{
			  int index = pchar - &color_string[0];   /* get index to color character */
			  es->line_color = context->color_rgba[index];
			  sscanf(&parser->line[13], " %*c %*c %u", &es->line_width);
			}
		    }

		  else
		    {
		      g_set_error_literal (err, parser->domain, 0, _("Invalid or missing ellipse line or color specification"));
		      return FALSE;
		    }

		  /* Check line character */
		  if ( line_char != 'l' && line_char != 'd' && line_char != '.' )
		    {
		      g_set_error_literal (err, parser->domain, 0, _("Invalid ellipse line character"));
		      return FALSE;
		    }
		  es->texture = char_to_texture (line_char);
		}
	  }

	else if ( strncmp(parser->line, "#####", 5) == 0 )
	  break;
      }

    fclose(fptr);
  }

     {
    FILE *fptr = fopen(parser->param_file, "r");
    while ( fgets(parser->line, parser->maxline, fptr) != NULL )
      {
	/* Get rectangle shape parameters */
	if ( strncmp(parser->line, "rect_coords", 11) == 0 )
	  {
	    context->rectangles = realloc (context->rectangles,
					   (sizeof *context->rectangles) * ++context->n_rectangles);
	    struct shape_spec *rs = context->rectangles + context->n_rectangles - 1;
	    if ( sscanf(&parser->line[11], "%lf %lf %lf %lf %lf",
			&rs->x0, &rs->y0, &rs->width, &rs->height, &rs->angle) != 5 )
	      {
		g_set_error_literal (err, parser->domain, 0,
				     _("Invalid or missing rectangle coordinates."));
		return FALSE;
	      }
	    rs->angle *= deg2rad;
	    char line_char = '?';
	    char color_char;
	    int index;
	    rs->line_width = 1;
	    if ( fgets(parser->line, parser->maxline, fptr) != NULL )
	      if ( strncmp(parser->line, "rect_style", 10) == 0 )
		{
		  if ( sscanf(&parser->line[10], " %c 0x%x", &line_char, (unsigned int *) &rs->line_color) == 2 ||
		       sscanf(&parser->line[10], " %c 0X%x", &line_char, (unsigned int *) &rs->line_color) == 2 )
		    sscanf(&parser->line[10], " %*c %*s %u", &rs->line_width);

		  else if ( sscanf(&parser->line[10], " %c %c", &line_char, &color_char) == 2 )
		    {
		      char *pchar;
		      if ( (pchar = strchr(color_string, color_char)) == NULL )
			{
			  g_set_error_literal (err, parser->domain, 0,
					       _("Invalid rectanle color character"));
			  return FALSE;
			}

		      else
			{
			  index = pchar - &color_string[0];   /* get index to color character */
			  rs->line_color = context->color_rgba[index];
			  sscanf(&parser->line[13], " %*c %*c %u", &rs->line_width);
			}
		    }

		  else
		    {
		      g_set_error_literal (err, parser->domain, 0,
					   _("Invalid or missing rectangle line or color specification"));
		      return FALSE;
		    }

		  /* Check line character */
		  if ( line_char != 'l' && line_char != 'd' && line_char != '.' )
		    {
		      g_set_error_literal (err, parser->domain, 0,
					   _("Invalid rectangle line character"));
		      return FALSE;
		    }
		}

	    rs->texture = char_to_texture (line_char);
	  }

	else if ( strncmp(parser->line, "#####", 5) == 0 )
	  break;
      }

    fclose(fptr);
  }

     {
    const int DIM = (context->plot_param.axes_type == AXES_3d) ? 3 : 2;
    /* Find minimum and maximum values of line data */
    FILE *fptr = fopen(parser->param_file, "r");
    int nlines = 0;
    while ( fgets(parser->line, parser->maxline, fptr) != NULL )
      {
	/* Get line coordinates */
	if ( strncmp(parser->line, "line_coords", 11) == 0 )
	  {
	    int i;
	    double line_coords[6];

	    nlines++;

	    if ( sscanf(parser->line, "%*s %lf %lf %lf %lf %lf %lf",
			&line_coords[0], &line_coords[1],
			&line_coords[2], &line_coords[3],
			&line_coords[4], &line_coords[5]) != DIM * 2)
	      {
		g_set_error_literal (err, parser->domain, 0,
				     _("Invalid line coordinates"));
		return FALSE;
	      }

	    /* Modify line coordinates for logarithmic and polar axes */
	    if ( context->plot_param.axes_type == AXES_semilogx )
	      for (i = 0; i < 2; ++i)
		line_coords[i] = log10(fabs(line_coords[i]));

	    else if ( context->plot_param.axes_type == AXES_semilogy )
	      for (i = 2; i < 4; ++i)
		line_coords[i] = log10(fabs(line_coords[i]));

	    else if ( context->plot_param.axes_type == AXES_loglog )
	      for (i = 0; i < 4; ++i)
		line_coords[i] = log10(fabs(line_coords[i]));

	    else if ( context->plot_param.axes_type == AXES_polar )
	      for (i = 0; i < 2; ++i)
		line_coords[i] = line_coords[i]*deg2rad;

	    /* Modify axis minimum and maximum values */
	    minimize (&context->data_min_max.xmin, line_coords[0]);
	    maximize (&context->data_min_max.xmax, line_coords[0]);
	    minimize (&context->data_min_max.xmin, line_coords[1]);
	    maximize (&context->data_min_max.xmax, line_coords[1]);

	    minimize (&context->data_min_max.ymin, line_coords[2]);
	    maximize (&context->data_min_max.ymax, line_coords[2]);
	    minimize (&context->data_min_max.ymin, line_coords[3]);
	    maximize (&context->data_min_max.ymax, line_coords[3]);

	    minimize (&context->data_min_max.zmin, line_coords[4]);
	    maximize (&context->data_min_max.zmax, line_coords[4]);
	    minimize (&context->data_min_max.zmin, line_coords[5]);
	    maximize (&context->data_min_max.zmax, line_coords[5]);
	  }

	else if ( strncmp(parser->line, "#####", 5) == 0 )
	  break;
      }
    fclose(fptr);
  }

  {
    const int DIM = (context->plot_param.axes_type == AXES_3d) ? 3 : 2;
    /* Find minimum and maximum values of symbol data */
    FILE *fptr = fopen(parser->param_file, "r");
    int nsymbols = 0;
    while ( fgets(parser->line, parser->maxline, fptr) != NULL )
      {
	/* Get symbol coordinates */
	if ( strncmp(parser->line, "symbol_coords", 13) == 0 )
	  {
	    double symbol_coords[3];

	    nsymbols++;

	    if ( sscanf(parser->line, "%*s %lf %lf %lf",
			&symbol_coords[0], &symbol_coords[1], &symbol_coords[2]) != DIM)
	      {
		g_set_error_literal (err, parser->domain, 0,
				     _("Invalid symbol coordinates"));
		return FALSE;
	      }

	    /* Modify symbol coordinates for logarithmic and polar axes */
	    if ( context->plot_param.axes_type == AXES_semilogx )
	      symbol_coords[0] = log10(fabs(symbol_coords[0]));

	    else if ( context->plot_param.axes_type == AXES_semilogy )
	      symbol_coords[1] = log10(fabs(symbol_coords[1]));

	    else if ( context->plot_param.axes_type == AXES_loglog )
	      {
		symbol_coords[0] = log10(fabs(symbol_coords[0]));
		symbol_coords[1] = log10(fabs(symbol_coords[1]));
	      }

	    else if ( context->plot_param.axes_type == AXES_polar )
	      symbol_coords[0] = symbol_coords[0]*deg2rad;

	    /* Modify axis minimum and maximum values */
	    minimize (&context->data_min_max.xmin, symbol_coords[0]);
	    maximize (&context->data_min_max.xmax, symbol_coords[0]);
	    minimize (&context->data_min_max.ymin, symbol_coords[1]);
	    maximize (&context->data_min_max.ymax, symbol_coords[1]);
	    minimize (&context->data_min_max.zmin, symbol_coords[2]);
	    maximize (&context->data_min_max.zmax, symbol_coords[2]);
	  }

	else if ( strncmp(parser->line, "#####", 5) == 0 )
	  break;
      }
    fclose(fptr);
  }
  return TRUE;
   }
