/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
 *
 * Histogram.c
 *
 * Calculates histogram of input data.
 *
* Copyright 2017 John Darrington
 * Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
 * http://savannah.gnu.org/projects/gsegrafix
 *
 * This file is part of GSEGrafix, a scientific and engineering plotting program.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#include "Histogram.h"
#include "Misc.h"
#include "DrawSymbols.h"
#include "InitializePlot.h"
#include "merge-sort.h"

#include <math.h>
#include "gsegraf.h"

#include <stdlib.h>

#include "data-iterator.h"

static inline void
advance_by (struct sample_iterator *it, int diff)
{
  while (diff-- > 0)
    it_next (it);
}

void
HistogramCalculate (struct gse_ctx *context, struct plot_parameters *plot)
{
  /* Calculate data mean and standard deviation */
  double sum = 0.0;
  int i;
  double ndata = 0;

  char *sorted_file = merge_sort (plot->filename, plot->template, 1);
  free (plot->filename);
  plot->filename = sorted_file;
  
  {
    struct sample_iterator *it = it_create (context, plot, false);
    for (; ! it_last(it); it_next (it))
      {
	const struct gse_datum *sample = it_get (it);
	sum += sample->x;
	ndata++;
      }
    it_destroy (it);
  }

  plot->ndata = ndata;
  
  double mean = sum/ndata;

  /* Calculate data quartile values */
  double q[5];
  struct sample_iterator *it = it_create (context, plot, false);
  int prev_j = 0;
  for (i=0; i < 5; i++)
    {
      int j = roundl(i*0.25*(ndata - 1));
      advance_by (it, j - prev_j);
      q[i] = it_get (it)->x;
      prev_j = j;
    }
  it_destroy (it);
  
  for (i=1; i < 5; ++i)
    {
      if (q[i-1] > q[i])
  	{
  	  g_return_if_fail ("Histogram data are not sorted.");
  	}
    }

  /* Estimate optimal histogram bin width */
  if ( plot->bin_widths <= 0.0 )
    {
      /* binwidth = 3.49*std/pow((double) ndata, 1.0/3.0); */   /* Scott */
      plot->bin_widths = 2.0*(q[3] - q[1])/pow(ndata, 1.0/3.0);   /* Freedman, Diaconis */
    }

  /* Calculate number of bins, context->binmin, and context->binmax */
  switch (plot->bin_ref)
    {
    case REF_mean:
      {
	double n1 = ceil(((mean - plot->bin_widths/2.0) - q[0])/plot->bin_widths);
	double n2 = ceil((q[4] - (mean + plot->bin_widths/2.0))/plot->bin_widths);
	plot->nbins = n1 + n2 + 1;
	plot->binmin = (mean - plot->bin_widths/2.0) - n1*plot->bin_widths;
	plot->binmax = (mean + plot->bin_widths/2.0) + n2*plot->bin_widths;
      }
      break;
    case REF_zero:
      if (q[4] <= 0)
	return;
      plot->nbins = ceil(q[4]/plot->bin_widths);
      plot->binmin = 0.0;
      plot->binmax = plot->nbins*plot->bin_widths;
      break;
    case REF_integer:
      plot->bin_widths = 1.0;
      plot->nbins = roundl(q[4]) - roundl(q[0]) + 1.0;
      plot->binmin = (double) roundl(q[0]) - 0.5;
      plot->binmax = (double) roundl(q[4]) + 0.5;
      break;
    }

  /* Calculate number of samples in each bin */
  plot->yhist = xmalloc(plot->nbins*sizeof(double));
  for ( i=1; i<=plot->nbins; i++ )
    plot->yhist[i-1] = 0.0;
  int j = 0;
  double bin2;
  double prev_sample = -DBL_MAX;
  it = it_create (context, plot, false);
  prev_j = j;
  for ( i=1; i<=plot->nbins; i++ )
    {
      double bin1 = plot->binmin + (i - 1.0)/(plot->nbins - 1.0)*(plot->binmax - plot->binmin - plot->bin_widths);
      bin2 = bin1 + plot->bin_widths;

      if (j >= ndata)
	break;
      advance_by (it, j - prev_j);
      double this_sample = it_get(it)->x;
      if (this_sample < prev_sample)
	{
	  g_return_if_fail ("Histogram data are not sorted.");
	}
      prev_j = j;
      while ( (j < ndata) && (bin1 <= this_sample) && (this_sample < bin2) )
	{
	  advance_by (it, j - prev_j);
	  this_sample = it_get(it)->x;
	  if (this_sample < prev_sample)
	    {
	      g_return_if_fail ("Histogram data are not sorted.");
	    }
	  plot->yhist[i-1] = plot->yhist[i-1] + 1.0;
	  prev_j = j;
	  j++;
	  prev_sample = this_sample;
	}
      prev_sample = this_sample;
    }
  advance_by (it, j - prev_j);

  if (! it_last (it))
    {
      if (it_get (it)->x >= bin2 )
	plot->yhist[plot->nbins-1] = plot->yhist[plot->nbins-1] + 1.0;
    }

  it_destroy (it);

  
  plot->histmin = 0.0;

  switch (plot->bin_value)
    {
    case BIN_number:
      plot->histmax = max(plot->nbins, plot->yhist);
      break;
    case BIN_fraction:
      plot->histmax = max(plot->nbins, plot->yhist)/ndata;
      break;
    case BIN_percent:
      plot->histmax = 100.0*max(plot->nbins, plot->yhist)/ndata;
      break;
    }
}

void
PlotHistogram (struct target *target, const struct gse_ctx *context, int iplot)
{
  struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];

  /* Specify axis minimum and maximum values */
  int nx = context->xtick_labels.nvalues;
  int ny = context->ytick_labels.nvalues;
  double xmin = context->xtick_labels.values[0];
  double xmax = context->xtick_labels.values[nx-1];
  double ymin = context->ytick_labels.values[0];
  double ymax = context->ytick_labels.values[ny-1];
  xmin -= context->xtick_labels.offset1;
  xmax += context->xtick_labels.offset2;
  ymin -= context->ytick_labels.offset1;
  ymax += context->ytick_labels.offset2;

  cairo_rectangle_t box;
  get_box (target, context, &box);

  /* Calculate axis scale factors */
  double xscale = box.width/(xmax - xmin);
  double yscale = box.height/(ymax - ymin);

  int i;
  /* Plot histogram */

  for ( i=1; i<=the_plot->nbins; i++ )
    {
      double y2_bar = 0;
      double y1_bar = 0.0;
      switch (the_plot->bin_value)
	{
	case  BIN_number:
	  y2_bar = the_plot->yhist[i-1];
	  break;
	case BIN_fraction:
	  y2_bar = the_plot->yhist[i-1]/the_plot->ndata;
	  break;
	case BIN_percent:
	  y2_bar = 100.0*the_plot->yhist[i-1]/the_plot->ndata;
	  break;
	}

      if ( y1_bar >= ymax || y2_bar <= ymin )
	continue;
      minimize (&ymin, y1_bar);
      maximize (&ymax, y2_bar);

      double y1 = (box.y + box.height) - (y1_bar - ymin) * yscale;
      double y2 = (box.y + box.height) - (y2_bar - ymin) * yscale;

      if ( the_plot->stylechar1 == 'b' || the_plot->stylechar1 == 'B' )
	{
	  /* Draw bars */
	  double x1_bar = the_plot->binmin + (i - 1.0)/(the_plot->nbins - 1.0)*(the_plot->binmax - the_plot->bin_widths - the_plot->binmin);
	  double x2_bar = x1_bar + the_plot->bin_widths;

	  if ( x1_bar >= xmax || x2_bar <= xmin )
            continue;
	  minimize (&xmin, x1_bar);
	  maximize (&xmax, x2_bar);

	  double x1 = box.x + (x1_bar - xmin)*xscale;
	  double x2 = box.x + (x2_bar - xmin)*xscale;

	  DrawBar (target, x1+1.0, y1, x2-1.0, y2, the_plot->fill_colors_rgba, the_plot->outline_colors_rgba);
	  DrawBar (target, x1, y1+1.0, x2, y2-1.0, 0xFFFFFF00, context->canvas_bg_color);
	}
      else if ( the_plot->texture == SOLID )
	{
	  /* Draw lines */
	  double xline = the_plot->binmin + (i - 1.0)/(the_plot->nbins - 1.0)*(the_plot->binmax - the_plot->bin_widths - the_plot->binmin) + the_plot->bin_widths/2.0;
	  if ( xmin < xline && xline < xmax )
            {
	      GseCairoPoints *points = gse_cairo_points_new(2);
	      points->coords[0] = box.x + (xline - xmin)*xscale;
	      points->coords[1] = y1;
	      points->coords[2] = points->coords[0];
	      points->coords[3] = y2;
	      DrawLine (target, context, points, the_plot->fill_colors_rgba, the_plot->stylesizes, SOLID);
	      gse_cairo_points_unref(points);
            }
	}
    }
}
