/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* DataMinMax.c
*
* Finds minimum and maximum values of 2d-plot data.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "DataMinMax.h"
#include "Histogram.h"
#include <float.h>
#include <math.h>
#include "gsegraf.h"


#include "Misc.h"

#include "data-iterator.h"

#define _(X) X

void
DataMinMax (struct gse_ctx *context)
{
  /* Find minimum and maximum values of data files */
  for (int iplot=1; iplot <= context->plot_param.nplots; iplot++ )
    {
      struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];

      if (the_plot->plot_type == PLOT_points)
	{
	  struct sample_iterator *it = it_create (context, the_plot, false);
	  for (; ! it_last(it); it_next (it))
	    {
	      const struct gse_datum *sample = it_get(it);
	      minimize (&context->data_min_max.xmin, sample->x);
	      maximize (&context->data_min_max.xmax, sample->x);
	      minimize (&context->data_min_max.ymin, sample->y);
	      maximize (&context->data_min_max.ymax, sample->y);
	      minimize (&context->data_min_max.zmin, sample->z);
	      maximize (&context->data_min_max.zmax, sample->z);
	    }
	  it_destroy (it);
	}

      else if ( the_plot->plot_type == PLOT_histogram )
	{
	  HistogramCalculate(context, the_plot);
	   
	  minimize (&context->data_min_max.xmin, the_plot->binmin);
	  maximize (&context->data_min_max.xmax, the_plot->binmax);
	  minimize (&context->data_min_max.ymin, the_plot->histmin);
	  maximize (&context->data_min_max.ymax, the_plot->histmax);
	}

      else if (
	       (the_plot->plot_type == PLOT_contour)
	       ||
	       (the_plot->plot_type == PLOT_color)
	       ||
	       ( the_plot->plot_type == PLOT_mesh )
	       )
	{
	  int i, j;
	  int nx = the_plot->samples3d.nx;
	  minimize (&context->data_min_max.xmin, the_plot->samples3d.x[0]);
	  maximize (&context->data_min_max.xmax, the_plot->samples3d.x[nx-1]);

	  int ny = the_plot->samples3d.ny;
	  minimize (&context->data_min_max.ymin, the_plot->samples3d.y[0]);
	  maximize (&context->data_min_max.ymax, the_plot->samples3d.y[ny-1]);

	  for ( i=1; i<=nx; i++ )
            for ( j=1; j<=ny; j++ )
	      {
		minimize (&context->data_min_max.zmin, the_plot->samples3d.z[ny*(i-1)+j-1]);
		maximize (&context->data_min_max.zmax, the_plot->samples3d.z[ny*(i-1)+j-1]);
	      }
	}
    }
}
