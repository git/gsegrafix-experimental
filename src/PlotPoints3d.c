/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* PlotPoints3d.c
*
* Plots a two-dimensional projection of three-dimensional points data.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "PlotPoints3d.h"
#include "Misc.h"
#include "DrawLines.h"
#include "DrawSymbols.h"

#include <math.h>
#include "gsegraf.h"

#include <string.h>
#include "Initialize3d.h"
#include "data-iterator.h"

static void
DrawLineSegments3d (struct target *target, const struct gse_ctx *context,
		    const struct plot_parameters *the_plot,
		    double *origin, double *Ryz,
		    const double *lower, const double *upper,
		    double xscale, double yscale, double zscale,
		    enum line_texture texture )
{
  int iseg = 0;
  struct sample_iterator *it = it_create (context, the_plot, true);
  for (; it; ++iseg)
    {
      DrawLines3d (target, context, 
		   it,
		   origin, Ryz,
		   lower, upper,
		   xscale, yscale, zscale,
		   the_plot->fill_colors_rgba, the_plot->stylesizes, texture);
      it = it_next_segment (it);
    }
}

void
PlotPoints3d (struct target *target, const struct gse_ctx *context, int iplot)
{
   /* Declare variables */
     int ifunc, nxvalues, nyvalues, nzvalues;
     double phi, theta, axis_length, origin[3], Ry[9], Ryz[9];
     double lower[3];
     double upper[3];
     double xscale, yscale, zscale, x, y;
     char *pchar;


   const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];

   /* Specify view angles */
   phi = context->plot_param_3d.phi;       /* view-direction azimuth (deg) from x axis in x-y plane */
   theta = context->plot_param_3d.theta;   /* view-direction elevation (deg) from x-y plane */

   get_origin (context, (int) (phi/90.0) + 1, origin);

   /* Calculate rotation matrices */
   phi = phi*deg2rad;
   theta = theta*deg2rad;

   Ry[0] = cos(-theta); Ry[1] = 0.0; Ry[2] = -sin(-theta);
   Ry[3] = 0.0; Ry[4] = 1.0; Ry[5] = 0.0;
   Ry[6] = sin(-theta); Ry[7] = 0.0; Ry[8] = cos(-theta);

   Ryz[0] = cos(phi); Ryz[1] = sin(phi); Ryz[2] = 0.0;
   Ryz[3] = -sin(phi); Ryz[4] = cos(phi); Ryz[5] = 0.0;
   Ryz[6] = 0.0; Ryz[7] = 0.0; Ryz[8] = 1.0;

   multiply_mm (Ry, Ryz);

   /* Specify axis length */
   axis_length = axis_length_3d (target);

   /* Specify minimum and maximum axis values */
   nxvalues = context->xtick_labels.nvalues;
   nyvalues = context->ytick_labels.nvalues;
   nzvalues = context->ztick_labels.nvalues;
   lower[0] = context->xtick_labels.values[0];
   upper[0] = context->xtick_labels.values[nxvalues-1];
   lower[1] = context->ytick_labels.values[0];
   upper[1] = context->ytick_labels.values[nyvalues-1];
   lower[2] = context->ztick_labels.values[0];
   upper[2] = context->ztick_labels.values[nzvalues-1];
   lower[0] = lower[0] - context->xtick_labels.offset1;
   upper[0] = upper[0] + context->xtick_labels.offset2;
   lower[1] = lower[1] - context->ytick_labels.offset1;
   upper[1] = upper[1] + context->ytick_labels.offset2;
   lower[2] = lower[2] - context->ztick_labels.offset1;
   upper[2] = upper[2] + context->ztick_labels.offset2;


   /* Calculate axis scale factors */
   xscale = axis_length/(upper[0] - lower[0]);
   yscale = axis_length/(upper[1] - lower[1]);
   zscale = axis_length/(upper[2] - lower[2]);


   /* Draw stem lines */
   if (the_plot->stem_type != STEM_OFF)
      {
	double stemz;
      /* Calculate z coordinate of stem point 1 */
	if (the_plot->stem_type == STEM_VALUE)
         {
         if ( lower[2] <= the_plot->stemvalues && the_plot->stemvalues <= upper[2] )
            stemz = (the_plot->stemvalues - lower[2])*zscale;
         else if ( the_plot->stemvalues < lower[2] )
            stemz = 0.0;
         else if ( the_plot->stemvalues > upper[2] )
            stemz = axis_length;
         }
      else
	stemz = 0.0;


      struct sample_iterator *it = it_create (context, the_plot, false);
      for (; ! it_last(it); it_next (it))
	{
	  const struct gse_datum *sample = it_get(it);

	  if ( lower[0] <= sample->x && sample->x <= upper[0] &&
	       lower[1] <= sample->y && sample->y <= upper[1] &&
	       lower[2] <= sample->z && sample->z <= upper[2] )
            {
	      double rstem[3];
	      double r[3];
	      GseCairoPoints *points = gse_cairo_points_new(2);
	  
	      /* Calculate coordinates of stem point 1 */
	      rstem[0] = (sample->x - lower[0])*xscale;
	      rstem[1] = (sample->y - lower[1])*yscale;
	      rstem[2] = stemz;

	      multiply_mv (Ryz, rstem);

	      x = origin[1] + rstem[1];
	      y = origin[2] - rstem[2];
	      points->coords[0] = x;
	      points->coords[1] = y;

	      /* Calculate coordinates of stem point 2 */
	      r[0] = (sample->x - lower[0])*xscale;
	      r[1] = (sample->y - lower[1])*yscale;
	      r[2] = (sample->z - lower[2])*zscale;
	      multiply_mv (Ryz, r);

	      x = origin[1] + r[1];
	      y = origin[2] - r[2];
	      points->coords[2] = x;
	      points->coords[3] = y;

	      DrawLine (target, context, points, the_plot->outline_colors_rgba, 1, SOLID);
	      gse_cairo_points_unref(points);
            }
	}
      it_destroy (it);
      }

   /* Draw lines */
   if ( the_plot->texture != NONE)
     DrawLineSegments3d (target, context, the_plot, &origin[0], &Ryz[0],
			 lower, upper,
                         xscale, yscale, zscale, the_plot->texture);

   /* Draw symbols in symbol_string1 ("cCtTsSiIpPhH") */
   else if ( (pchar = strchr(symbol_string1, the_plot->stylechar1)) != NULL )
      {
      ifunc = pchar - symbol_string1;
      
      struct sample_iterator *it = it_create (context, the_plot, false);
      for (; ! it_last(it); it_next (it))
	{
	  const struct gse_datum *sample = it_get(it);
	  if ( lower[0] <= sample->x && sample->x <= upper[0] &&
	       lower[1] <= sample->y && sample->y <= upper[1] &&
	       lower[2] <= sample->z && sample->z <= upper[2] )
            {
	      double r[3];
	      r[0] = (sample->x - lower[0])*xscale;
	      r[1] = (sample->y - lower[1])*yscale;
	      r[2] = (sample->z - lower[2])*zscale;

	      multiply_mv (Ryz, r);

	      x = origin[1] + r[1];
	      y = origin[2] - r[2];
	      context->symbol_func1[ifunc](target, x, y, the_plot->fill_colors_rgba,
					   the_plot->outline_colors_rgba,
					   the_plot->stylesizes);
            }
	}
      it_destroy (it);
      }

   /* Draw symbols in symbol_string2 ("+xra") */
   else if ( (pchar = strchr(symbol_string2, the_plot->stylechar1)) != NULL )
     {
       ifunc = pchar - symbol_string2;
       struct sample_iterator *it = it_create (context, the_plot, false);
       for (; ! it_last(it); it_next (it))
	 {
	   const struct gse_datum *sample = it_get(it);

	   if ( lower[0] <= sample->x && sample->x <= upper[0] &&
		lower[1] <= sample->y && sample->y <= upper[1] &&
		lower[2] <= sample->z && sample->z <= upper[2] )
	     {
	       double r[3];
	       r[0] = (sample->x - lower[0]) * xscale;
	       r[1] = (sample->y - lower[1]) * yscale;
	       r[2] = (sample->z - lower[2]) * zscale;

	       multiply_mv (Ryz, r);

	       x = origin[1] + r[1];
	       y = origin[2] - r[2];
	       context->symbol_func2[ifunc](target, x, y,
					    the_plot->fill_colors_rgba,
					    the_plot->outline_colors_rgba,
					    the_plot->stylesizes);
	     }
	 }
       it_destroy (it);
     }
   }


