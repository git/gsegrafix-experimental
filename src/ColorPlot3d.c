/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* ColorPlot3d.c
*
* Plots three-dimensional contour information as a function of color.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "ColorPlot3d.h"
#include "Misc.h"
#include <math.h>
#include "gsegraf.h"
#include "InitializePlot.h"
#include <stdlib.h>  // for free
#include "Initialize3d.h"
#include "gse-cairo.h"

void
ColorPlot3d (struct target *target, const struct gse_ctx *context, int iplot, int nx, int ny )
{
   int nxvalues, nyvalues, nzvalues, quadrant,
       i, j, k, i11, i22, j11, j22, nxdata, nydata, nx_interp, ny_interp,
       ipixel, jpixel, index_colors, index_zi;
   guint32 *colors;
   double xmin, xmax, ymin, ymax, zmin, zmax,
          xscale, yscale, zscale, axis_length, origin[3],
     *xi, *yi, *zi, dx, dy;
   const double *Ryz;
   double  r[3], zscale2, fraction;
   GdkPixbuf *pixbuf_color;

   const struct plot_parameters *the_plot = &context->plot_parameters[iplot-1];
	   
   cairo_rectangle_t box;
   get_box (target, context, &box);

   int width  = roundl (box.width);
   int height = roundl (box.height);

   /* Get minimum and maximum axis values */
   nxvalues = context->xtick_labels.nvalues;
   nyvalues = context->ytick_labels.nvalues;
   nzvalues = context->ztick_labels.nvalues;
   xmin = context->xtick_labels.values[0];
   xmax = context->xtick_labels.values[nxvalues-1];
   ymin = context->ytick_labels.values[0];
   ymax = context->ytick_labels.values[nyvalues-1];
   zmin = context->ztick_labels.values[0];
   zmax = context->ztick_labels.values[nzvalues-1];
   xmin = xmin - context->xtick_labels.offset1;
   xmax = xmax + context->xtick_labels.offset2;
   ymin = ymin - context->ytick_labels.offset1;
   ymax = ymax + context->ytick_labels.offset2;
   zmin = zmin - context->ztick_labels.offset1;
   zmax = zmax + context->ztick_labels.offset2;


   /* Calculate axis scale factors */
   axis_length = axis_length_3d (target);
   xscale = axis_length/(xmax - xmin);
   yscale = axis_length/(ymax - ymin);
   zscale = axis_length/(zmax - zmin);

   quadrant = context->plot_param_3d.quadrant;
   get_origin (context, context->plot_param_3d.quadrant, origin);

   /* Find data indices in range */
   for ( i=1; i<=nx; i++ )
     if ( xmin <= the_plot->samples3d.x[i-1] )
       {
         i11 = i;
         break;
       }

   for ( i=nx; i>=1; i-- )
      if ( the_plot->samples3d.x[i-1] <= xmax )
	{
	  i22 = i;
	  break;
	}
   
   for ( j=1; j<=ny; j++ )
      if ( ymin <= the_plot->samples3d.y[j-1] )
	{
	  j11 = j;
	  break;
	}
   
   for ( j=ny; j>=1; j-- )
      if ( the_plot->samples3d.y[j-1] <= ymax )
	{
	  j22 = j;
	  break;
	}


   /* Get interpolated values of x and y */
   nxdata = i22 - i11 + 1;
   nx_interp = the_plot->ninterp*(nxdata - 1) + 1;
   nydata = j22 - j11 + 1;
   ny_interp = the_plot->ninterp*(nydata - 1) + 1;
   xi = xzalloc(nx_interp*sizeof(double));
   yi = xzalloc(ny_interp*sizeof(double));
   zi = xzalloc(nx_interp*ny_interp*sizeof(double));

   xi[0] = the_plot->samples3d.x[i11-1];
   for ( i=i11; i<i22; i++ )
      {
      dx = (the_plot->samples3d.x[i] - the_plot->samples3d.x[i-1])/the_plot->ninterp;
      for ( k=1; k<=the_plot->ninterp; k++ )
         xi[the_plot->ninterp*(i-i11)+k] = the_plot->samples3d.x[i-1] + k*dx;
      }

   yi[0] = the_plot->samples3d.y[j11-1];
   for ( j=j11; j<j22; j++ )
      {
      dy = (the_plot->samples3d.y[j] - the_plot->samples3d.y[j-1])/the_plot->ninterp;
      for ( k=0; k<the_plot->ninterp; k++ )
         yi[the_plot->ninterp*(j-j11)+k] = the_plot->samples3d.y[j-1] + k*dy;
      }


   /* Get interpolated values of z (bilinear interpolation) */
   for ( i=1; i<=nx_interp; i++ )
      for ( j=1; j<=ny_interp; j++ )
         interp2(nx, ny, 1, &the_plot->samples3d.x[0], &the_plot->samples3d.y[0], &the_plot->samples3d.z[0],
                 &xi[i-1], &yi[j-1], &zi[ny_interp*(i-1)+(j-1)]);


   /* Create color array */
   colors = xmalloc(width*height*sizeof(unsigned int));
   for ( i=1; i<=width; i++ )
      for ( j=1; j<=height; j++ )
         colors[height*(i-1)+(j-1)] = 0xFFFFFF00;   /* transparent */


   /* Calculate plot colors */
   Ryz = &context->plot_param_3d.Ryz[0];
   zscale2 = 1.0/(zmax - zmin);
   if ( quadrant == 1 || quadrant == 4 )
      for ( i=1; i<=nx_interp; i++ )
         for ( j=1; j<=ny_interp; j++ )
            {
            r[0] = (xi[i-1] - xmin)*xscale;
            r[1] = (yi[j-1] - ymin)*yscale;
            r[2] = (zi[ny_interp*(i-1)+(j-1)] - zmin)*zscale;
            multiply_mv (Ryz, r);
            ipixel = roundl(origin[1] + r[1] - box.x);
            jpixel = roundl(origin[2] - r[2] - box.y);

            index_colors = height*(ipixel - 1) + (jpixel - 1);
            if ( index_colors < 0 || index_colors > width*height )
               continue;

            index_zi = ny_interp*(i-1)+(j-1);
            fraction = (zi[index_zi] - zmin)*zscale2;
            if ( 0.0 <= fraction && fraction <= 1.0  )
	      colors[index_colors] = interp_color_1(context, fraction) - 0xFF + the_plot->alphacolor;
            }

   else if ( quadrant == 2 || quadrant == 3 )
      for ( i=nx_interp; i>1; i-- )
         for ( j=1; j<=ny_interp; j++ )
            {
            r[0] = (xi[i-1] - xmin)*xscale;
            r[1] = (yi[j-1] - ymin)*yscale;
            r[2] = (zi[ny_interp*(i-1)+(j-1)] - zmin)*zscale;
	    multiply_mv(Ryz, r);
            ipixel = roundl(origin[1] + r[1] - box.x);
            jpixel = roundl(origin[2] - r[2] - box.y);

            index_colors = height*(ipixel - 1) + (jpixel - 1);
            if ( index_colors < 0 || index_colors > width*height )
               continue;

            index_zi = ny_interp*(i-1)+(j-1);
            fraction = (zi[index_zi] - zmin)*zscale2;
            if ( 0.0 <= fraction && fraction <= 1.0  )
	      colors[index_colors] = interp_color_1(context, fraction) - 0xFF + the_plot->alphacolor;
            }


   /* Create pixbuf */
   pixbuf_color = gdk_pixbuf_new(GDK_COLORSPACE_RGB, TRUE, 8, width, height);
   gdk_pixbuf_fill(pixbuf_color, 0xFFFFFF00);   /* transparent */


   /* Specify pixbuf colors */
   if ( quadrant == 1 )
      for ( i=1; i<=width; i++ )
         for ( j=1; j<=height; j++ )
            put_pixel(pixbuf_color, i-1, j-1, colors[height*(i-1)+(j-1)]);
   else if ( quadrant == 2 )
      for ( i=width; i>=1; i-- )
         for ( j=1; j<=height; j++ )
            put_pixel(pixbuf_color, i-1, j-1, colors[height*(i-1)+(j-1)]);
   else if ( quadrant == 3 )
      for ( i=width; i>=1; i-- )
         for ( j=height; j>=1; j-- )
            put_pixel(pixbuf_color, i-1, j-1, colors[height*(i-1)+(j-1)]);
   else if ( quadrant == 4 )
      for ( i=1; i<=width; i++ )
         for ( j=height; j>=1; j-- )
            put_pixel(pixbuf_color, i-1, j-1, colors[height*(i-1)+(j-1)]);


   gse_cairo_render_pixbuf (target->cr, pixbuf_color,
			    (box.x + (box.x + box.width))/2.0,
			    (box.y + (box.y + box.height))/2.0,
			    GSE_ANCHOR_CENTER);


   g_object_unref(pixbuf_color);


   /* Free memory */
   free(xi);
   free(yi);
   free(zi);
   free(colors);

   return;
   }

