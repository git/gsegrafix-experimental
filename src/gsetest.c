/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*
  This file is part of GSEGrafix, a scientific and engineering plotting 
  program.

  Copyright (C) 2017 John Darrington

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <config.h>
#include <gtk/gtk.h>
#include <cairo.h>

#include "gsegrafix.h"

static gboolean version = FALSE;
static gboolean test_mode = FALSE;

gboolean
draw (GtkWidget *w, cairo_t *cr, gpointer ud)
{
  struct gse_ctx *context = ud;

  DrawGraph (cr,
	     gtk_widget_get_allocated_width (w),
	     gtk_widget_get_allocated_height (w),
	     context);

  if (test_mode)
    {
      gtk_main_quit ();
    }
  
  return FALSE;
}

static GOptionEntry entries[] =
{
  { "test", 't', 0, G_OPTION_ARG_NONE, &test_mode, "Show a single frame and exit", NULL },
  { "version", 'v', 0, G_OPTION_ARG_NONE, &version, "Show version and exit", NULL },
  { NULL }
};

static void
rudimentary_message_handler (const char *msg, gpointer aux)
{
  g_print ("%s\n", msg);
}


int
main (int argc, char **argv)
{
  GError *error = NULL;
  GOptionContext *optctx = g_option_context_new ("- test gsegrafix library");
  g_option_context_add_main_entries (optctx, entries, NULL);
  if (!g_option_context_parse (optctx, &argc, &argv, &error))
    {
      g_print ("option parsing failed: %s\n", error->message);
      return 1;
    }
  g_option_context_free (optctx);

  if (version)
    {
      g_print ("Version: %s\n", PACKAGE_VERSION);
      return 0;
    }

  char *filename = argv[argc - 1];

  gtk_init (&argc, &argv);
  
  GtkWidget *window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
    
  GtkWidget *drawing_area = gtk_drawing_area_new ();

  gtk_container_add (GTK_CONTAINER (window), drawing_area);

  struct gse_ctx *context =
    InitializePlot (filename, NULL, rudimentary_message_handler, NULL);
  
  g_signal_connect (drawing_area, "draw", G_CALLBACK (draw), context);
  
  gtk_widget_show_all (window);
  
  gtk_main ();

  gse_context_destroy (context);
  
  return 0;
}
