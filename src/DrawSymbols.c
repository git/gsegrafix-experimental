/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* DrawSymbols.c
*
* Contains functions:
*    DrawLine
*    DrawDashedLine
*    DrawDottedLine
*    DrawCircle
*    DrawTriangle
*    DrawSquare
*    DrawDiamond
*    DrawPentagon
*    DrawHexagon
*    DrawPlus
*    DrawX
*    DrawStar
*    DrawAsterisk
*    DrawBar
*    DrawMesh
*    DrawContour
*    DrawColorPlot
*
* Functions draw lines and plot symbols.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "DrawLines.h"
#include "Misc.h"
#include "DrawSymbols.h"

#include <math.h>
#include "gsegraf.h"

#include <stdlib.h> // for free

#include "gse-cairo.h"

void
texture_to_dash (const struct gse_ctx *context, enum line_texture texture,
		 double *dash, int *n_dash)
{
  switch (texture)
    {
    case SOLID:
      *n_dash = 0;
      break;
    case DASHED:
      dash[0] = context->dash;
      dash[1] = context->space_dash;
      *n_dash = 2;
      break;
    case DOTTED:
      dash[0] = 1;
      dash[1] = context->space_dot;
      *n_dash = 2;
      break;
    default:
      g_assert_not_reached ();
    }
}

#define SQR(X) ((X)*(X))

void
DrawLine (struct target *target, const struct gse_ctx *context, GseCairoPoints *points, guint32 color,
	  unsigned int line_width, enum line_texture texture)
{
  double dashes[4];
  int n = 0;
  texture_to_dash (context, texture, dashes, &n);

  if (texture == DOTTED)
    {
      int i = 0;
      DrawCircle (target,
		  points->coords[0],
		  points->coords[1],
		  color, color, line_width);
      
      for (i = 1; i < points->num_points; ++i)
	{
	  double x0 = points->coords[(i-1) * 2];
	  double y0 = points->coords[(i-1) * 2 + 1];
	  
	  double distance =
	    sqrt (SQR (points->coords[i * 2] - x0) +
	  	  SQR (points->coords[i * 2 + 1] - y0));

	  double n_periods = round (distance / (context->space_dot + line_width));
	  for (int j = 1; j < n_periods; ++j)
	    {
	      double x = x0 + (points->coords[i * 2] - x0) * j / n_periods;
	      double y = y0 + (points->coords[i * 2 + 1] - y0) * j / n_periods;

	      DrawCircle (target,
			  x, y,
			  color, color, line_width);
	    }
	}
    }
  else
    {
      cairo_save (target->cr);
      cairo_set_dash (target->cr, dashes, n, 0);
      gse_cairo_render_line (target->cr, points, points->num_points, line_width,
			     color);
      cairo_restore (target->cr);
    }
}

void
DrawCircle (const struct target *target, double x, double y, guint32 fill_color_rgba,
	    guint32 outline_color_rgba, int size )
{
  g_return_if_fail (size > 0);
  gse_cairo_render_circle (target->cr, x, y, 0.5 * size, 1, outline_color_rgba,
		       fill_color_rgba);
}


void
DrawTriangle (const struct target *target, double x, double y, guint32 fill_color_rgba,
	      guint32 outline_color_rgba, int size)
{
  /* Draw triangle */
  g_return_if_fail (size > 0);

  double r = 0.666666667*size;
  double dx = 0.577350269*size;
  double dy = 0.333333333*size;
  GseCairoPoints *points = gse_cairo_points_new(3);
  points->coords[0] = x;
  points->coords[1] = y - r;
  points->coords[2] = x - dx;
  points->coords[3] = y + dy;
  points->coords[4] = x + dx;
  points->coords[5] = y + dy;

  gse_cairo_render_polygon (target->cr, points, 3, 1,
			    outline_color_rgba,
			    fill_color_rgba);

  gse_cairo_points_unref(points);
}

void
DrawSquare (const struct target *target, double x, double y, guint32 fill_color,
	    guint32 outline_color, int size)
{
  /* Draw square */
  g_return_if_fail (size > 0);

  gse_cairo_render_rectangle (target->cr, 
			      x - 0.5 * size,
			      y - 0.5 * size,
			      size,
			      size,
			      1,
			      outline_color,
			      fill_color);
}



void
DrawDiamond (const struct target *target, double x, double y, guint32 fill_color_rgba,
	     guint32 outline_color_rgba, int size )
{
  /* Draw diamond */
  g_return_if_fail (size > 0);

  double   r = 0.707106781*size;
  GseCairoPoints *      points = gse_cairo_points_new(4);
  points->coords[0] = x;
  points->coords[1] = y - r;
  points->coords[2] = x - r;
  points->coords[3] = y;
  points->coords[4] = x;
  points->coords[5] = y + r;
  points->coords[6] = x + r;
  points->coords[7] = y;

  gse_cairo_render_polygon (target->cr, points, 4, 1,
			    outline_color_rgba,
			    fill_color_rgba);
      
  gse_cairo_points_unref(points);
}


void
DrawPentagon (const struct target *target, double x, double y, guint32 fill_color_rgba,
	      guint32 outline_color_rgba, int size )
{
  /* Draw pentagon */
  g_return_if_fail (size > 0);

  double r = 0.618033988*size;
  double dx1 = 0.587785252*size;
  double dy1 = 0.190983005*size;
  double dx2 = 0.363271264*size;
  double dy2 = 0.5*size;

  GseCairoPoints *points;

  points = gse_cairo_points_new(5);
  points->coords[0] = x;
  points->coords[1] = y - r;
  points->coords[2] = x - dx1;
  points->coords[3] = y - dy1;
  points->coords[4] = x - dx2;
  points->coords[5] = y + dy2;
  points->coords[6] = x + dx2;
  points->coords[7] = y + dy2;
  points->coords[8] = x + dx1;
  points->coords[9] = y - dy1;

  gse_cairo_render_polygon (target->cr, points, 5, 1,
			    outline_color_rgba,
			    fill_color_rgba);
      
  gse_cairo_points_unref(points);
}


void
DrawHexagon (const struct target *target, double x, double y, guint32 fill_color_rgba,
	     guint32 outline_color_rgba, int size )
{
  /* Draw hexagon */
  g_return_if_fail (size > 0);

  double r = 0.577350269*size;
  double dx = 0.288675134*size;
  double dy = 0.5*size;

  GseCairoPoints *points = gse_cairo_points_new(6);
  points->coords[0]  = x - dx;
  points->coords[1]  = y - dy;
  points->coords[2]  = x - r;
  points->coords[3]  = y;
  points->coords[4]  = x - dx;
  points->coords[5]  = y + dy;
  points->coords[6]  = x + dx;
  points->coords[7]  = y + dy;
  points->coords[8]  = x + r;
  points->coords[9]  = y;
  points->coords[10] = x + dx;
  points->coords[11] = y - dy;

  gse_cairo_render_polygon (target->cr, points, 6, 1,
			    outline_color_rgba,
			    fill_color_rgba);
      
  gse_cairo_points_unref(points);
}


void
DrawPlus (const struct target *target, double x, double y, guint32 fill_color_rgba,
	  guint32 dummy,  int size )
{
  /* Draw plus sign */
  g_return_if_fail (size > 0);
   
  double r = 0.707106781*size;
  GseCairoPoints *points = gse_cairo_points_new(2);
  points->coords[0] = x;
  points->coords[1] = y - r;
  points->coords[2] = x;
  points->coords[3] = y + r;

  gse_cairo_render_line (target->cr, points, 2, 1, fill_color_rgba);

  points->coords[0] = x - r;
  points->coords[1] = y;
  points->coords[2] = x + r;
  points->coords[3] = y;

  gse_cairo_render_line (target->cr, points, 2, 1, fill_color_rgba);

  gse_cairo_points_unref(points);
}


void
DrawX (const struct target *target, double x, double y, guint32 fill_color_rgba,
       guint32 dummy, int size )
{
  g_return_if_fail (size > 0);
  /* Draw x */

  double dx = 0.5*size;
  double dy = 0.5*size;

  GseCairoPoints *points = gse_cairo_points_new(2);
  points->coords[0] = x - dx;
  points->coords[1] = y - dy;
  points->coords[2] = x + dx;
  points->coords[3] = y + dy;
  gse_cairo_render_line (target->cr, points, 2, 1, fill_color_rgba);
      
  points->coords[0] = x + dx;
  points->coords[1] = y - dy;
  points->coords[2] = x - dx;
  points->coords[3] = y + dy;
  gse_cairo_render_line (target->cr, points, 2, 1, fill_color_rgba);

  gse_cairo_points_unref(points);
}


void
DrawStar (const struct target *target, double x, double y, guint32 fill_color_rgba,
	  guint32 dummy, int size )
{
  g_return_if_fail (size > 0);
  /* Draw star */

  double r = 0.618033988*size;
  double      dx1 = 0.587785252*size;
  double      dy1 = 0.190983005*size;
  double      dx2 = 0.363271264*size;
  double      dy2 = 0.5*size;

  GseCairoPoints *points = gse_cairo_points_new(3);
  points->coords[0] = x;
  points->coords[1] = y - r;
  points->coords[2] = x;
  points->coords[3] = y;
  points->coords[4] = x - dx2;
  points->coords[5] = y + dy2;

  gse_cairo_render_line (target->cr, points, 3, 1, fill_color_rgba);

      
  points->coords[0] = x;
  points->coords[1] = y - r;
  points->coords[2] = x;
  points->coords[3] = y;
  points->coords[4] = x + dx2;
  points->coords[5] = y + dy2;

  gse_cairo_render_line (target->cr, points, 3, 1, fill_color_rgba);

      
  points->coords[0] = x - dx1;
  points->coords[1] = y - dy1;
  points->coords[2] = x;
  points->coords[3] = y;
  points->coords[4] = x + dx1;
  points->coords[5] = y - dy1;

  gse_cairo_render_line (target->cr, points, 3, 1, fill_color_rgba);

  gse_cairo_points_unref(points);
}


void
DrawAsterisk (const struct target *target, double x, double y, guint32 fill_color_rgba,
	      guint32 color, int size )
{
  g_return_if_fail (size > 0);
  /* Draw asterisk */

  double r = 0.577350269*size;
  double dx = 0.5*size;
  double dy = 0.288675134*size;

  GseCairoPoints *points = gse_cairo_points_new(2);
  points->coords[0] = x;
  points->coords[1] = y - r;
  points->coords[2] = x;
  points->coords[3] = y + r;
      
  gse_cairo_render_line (target->cr, points, 2, 1, fill_color_rgba);
      
  points->coords[0] = x + dx;
  points->coords[1] = y + dy;
  points->coords[2] = x - dx;
  points->coords[3] = y - dy;

  gse_cairo_render_line (target->cr, points, 2, 1, fill_color_rgba);

      
  points->coords[0] = x - dx;
  points->coords[1] = y + dy;
  points->coords[2] = x + dx;
  points->coords[3] = y - dy;

  gse_cairo_render_line (target->cr, points, 2, 1, fill_color_rgba);

  gse_cairo_points_unref(points);
}


void
DrawBar (const struct target *target, double x1, double y1, double x2, double y2,
	 guint32 fill_color, guint32 outline_color )
{
  /* Note: y1 >= y2 !! Hence the arguments are in the other
     way around to what you might expect! */
  gse_cairo_render_rectangle (target->cr,
			      x1, y2,
			      x2 - x1,
			      y1 - y2, 1,
			      outline_color,
			      fill_color);
}


void DrawMesh (const struct target *target, double x1, double y1, double x2, double y2,
                guint32 fill_color_rgba, guint32 outline_color_rgba, int flag )
   {
   /* Declare variables */
   int i;

   double dx[] = { 0.0, 6.0, 12.0, 18.0, 24.0, 30.0, 36.0, 42.0, 48.0, 54.0, 60.0 };
   GseCairoPoints *points;

   /* Draw rectangle */
   if (flag != STYLE_AUTO)
      {
	gse_cairo_render_rectangle (target->cr, 
				    x1,
				    y1 - 6.0,
				    x2 - x1,
				    y2 - y1 + 12.0,
				    1,
				    outline_color_rgba,
				    fill_color_rgba);
      }
   else
      {
	guint32 fill_color[] = { 0xFF000000, 0xFF800000, 0xFFFF0000, 0x80FF0000, 0x00FF0000 };
	guint32 alpha_mesh = fill_color_rgba;
	for (i = 1; i <= 5; i++)
	  {
	    fill_color[i-1] = fill_color[i-1] + alpha_mesh;
	    
	    gse_cairo_render_rectangle (target->cr, 
					x1 + dx[2*(i-1)],
					y1 - 6.0,
				        dx[2*i] - dx[2*(i-1)],
					y2 + 12.0 - y1,
					1,
					outline_color_rgba,
					fill_color[i-1]);
	  }
      }
   
   /* Draw horizontal line */
   points = gse_cairo_points_new(2);

   points->coords[0] = x1;
   points->coords[1] = y1;
   points->coords[2] = x2;
   points->coords[3] = y2;

   gse_cairo_render_line (target->cr, points, 2, 1, outline_color_rgba);


   /* Draw vertical lines */
   for ( i=2; i<=10; i++ )
      {
      points->coords[0] = x1 + dx[i-1];
      points->coords[1] = y1 - 6.0;
      points->coords[2] = x1 + dx[i-1];
      points->coords[3] = y2 + 6.0;

      gse_cairo_render_line (target->cr, points, 2, 1, outline_color_rgba);
      }

   gse_cairo_points_unref(points);
   }


void
DrawContour (struct target *target, const struct gse_ctx *context,
	     double x1, double y1, double x2, double y2,
	     guint32 fill_color_rgba, guint32 outline_color_rgba, int flag)
{
  /* Draw filled rectangle */
   if ( context->plot_param.axes_type == AXES_3d )
     {
       gse_cairo_render_rectangle (target->cr, x1, y1 - 6.0,
			     x2 - x1, y2 - y1 + 12.0,
			     1, fill_color_rgba, fill_color_rgba);
     }

   guint32 fill_color = 0xFFFFFF00;
   guint32 outline_color1;
   guint32 outline_color2;
   switch (flag)
     {
       /* 2d contour plot */
     case STYLE_CHAR:
     case STYLE_HEX:
       outline_color1 = fill_color_rgba;
       outline_color2 = fill_color_rgba;
       break;
     case STYLE_AUTO:
       outline_color1 = 0xFF8000FF;
       outline_color2 = 0xFF0000FF;
       break;

       /* 3d contour plot */
     case STYLE_CHAR_CHAR:
     case STYLE_CHAR_HEX:
     case STYLE_HEX_CHAR:
     case STYLE_HEX_HEX:
       outline_color1 = outline_color_rgba;
       outline_color2 = outline_color_rgba;
       break;
     }
   
   gse_cairo_render_ellipse (target->cr, x1, y1 - 6.0,
			     x2 - x1,
			     y2 - y2 + 12.0,
			     1,
			     outline_color1,
			     fill_color);
			     
   gse_cairo_render_ellipse (target->cr,
			     0.75 * x1 + 0.25 * x2,
			     y1 - 3.0,
			     0.5 * (x2 - x1),
   			     y2 - y1 + 6.0,
   			     1,
   			     outline_color2,
   			     fill_color);
   }


void
DrawColorPlot (struct target *target, const struct gse_ctx *context, double x1, double y1, double x2, double y2 )
   {
   int i, j, nx = 11, ny = 3;
   guint32 color;
   double x[11] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5 },
          y[3] = { -1, 0, 1 },
          z[11][3] = { { -1.9407e-02,  3.8980e-17, -1.9407e-02 },
                       {  2.0208e-01,  2.3387e-01,  2.0208e-01 },
                       {  4.6034e-01,  5.0455e-01,  4.6034e-01 },
                       {  7.0200e-01,  7.5683e-01,  7.0200e-01 },
                       {  8.7350e-01,  9.3549e-01,  8.7350e-01 },
                       {  9.3549e-01,  1.0000e+00,  9.3549e-01 },
                       {  8.7350e-01,  9.3549e-01,  8.7350e-01 },
                       {  7.0200e-01,  7.5683e-01,  7.0200e-01 },
                       {  4.6034e-01,  5.0455e-01,  4.6034e-01 },
                       {  2.0208e-01,  2.3387e-01,  2.0208e-01 },
                       { -1.9407e-02,  3.8980e-17, -1.9407e-02 } },
          xi[61], yi[13], zi[61][13], zinterp, zmin, zmax, zscale, fraction;
   GdkPixbuf *pixbuf;

   /* Normalize values of x and y */
   for ( i=1; i<=11; i++ )
      x[i-1] = x[i-1]*M_PI/5.0;
   for ( j=1; j<=3; j++ )
      y[j-1] = y[j-1]*M_PI/5.0;


   /* Calculate interpolated values of x, y, and z */
   for ( i=1; i<=61; i++ )
      xi[i-1] = x[0] + (i - 1)*(x[10] - x[0])/60;
   for ( j=1; j<=13; j++ )
      yi[j-1] = y[0] + (j - 1)*(y[2] - y[0])/12;

   for ( i=1; i<=61; i++ )
      for ( j=1; j<=13; j++ )
         interp2(nx, ny, 1, &x[0], &y[0], &z[0][0],
                 &xi[i-1], &yi[j-1], &zi[i-1][j-1]);


   /* Create pixbuf */
   pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, TRUE, 8, 61, 13);
   gdk_pixbuf_fill(pixbuf, 0xFFFFFF00);


   /* Draw color-plot symbol */
   zmin = min(793, &zi[0][0]);
   zmax = max(793, &zi[0][0]);
   zscale = 1.0/(zmax - zmin);
   for ( i=1; i<=61; i++ )
      for ( j=13; j>=1; j-- )
         {
         zinterp = zi[i-1][j-1];
         fraction = (zinterp - zmin)*zscale;
         color = interp_color_1(context, fraction);
         put_pixel(pixbuf, i-1, 13-j, color);
         }

   
   gse_cairo_render_pixbuf (target->cr, pixbuf, 
			   (x1 + x2)/2.0,
			   (y1 + y2)/2.0,
			   GSE_ANCHOR_CENTER);

			   
   g_object_unref(pixbuf);
   }

