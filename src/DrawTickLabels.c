/* This is an experimental fork of GNU GSEGrafix.  It is NOT the original work
   which can be found at http://www.gnu.org/software/gsegrafix */

/*******************************************************************************
*
* DrawTickLabels.c
*
* Draws tick-mark labels for linear and logarithmic axes.
*
* Copyright 2017 John Darrington
* Copyright � 2008, 2009, 2010, 2011, 2012 Spencer A. Buckner
* http://savannah.gnu.org/projects/gsegrafix
*
* This file is part of GSEGrafix, a scientific and engineering plotting program.
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*******************************************************************************/

#include "DrawTickLabels.h"
#include "Misc.h"

#include <math.h>
#include "gsegraf.h"
#include "gse-cairo.h"

#include <string.h> 

double
DrawTickLabels (struct target *target, const struct gse_ctx *context, const char *axis_type,
		       double x1_screen , double x_range, double y1_screen, double y_range,
		       int nticks, const double *tick_values, double offset1, double offset2,
		       double xoffset, double yoffset, int anchor )
{
   int i, ndecades, logtickvalue;
   double axis_min, axis_max, xscale, yscale,
          increment, xtick, ytick, tickvalue,
     x1, x2, y1, y2;
   char string[21];
   double width_tick_labels = 0.0;

   /* Draw linear-axis tick-mark labels */
   if ( strcmp(axis_type, "linear") == 0 )
      {
      axis_min = tick_values[0] - offset1;
      axis_max = tick_values[nticks-1] + offset2;
      xscale = x_range/(axis_max - axis_min);
      yscale = y_range/(axis_max - axis_min);

      increment = (tick_values[nticks-1] - tick_values[0])/(nticks - 1);
      for ( i=1; i<=nticks; i++ )
         {
         xtick = x1_screen + (tick_values[i-1] - axis_min)*xscale;
         ytick = y1_screen + (tick_values[i-1] - axis_min)*yscale;

         memset(string, 0, sizeof(string));
         if ( fabs(tick_values[i-1]) < 0.01*increment )
            snprintf(string, sizeof(string), "%1.0f", 0.0);
         else
            snprintf(string, sizeof(string), "%g", tick_values[i-1]);

	 gse_cairo_render_text_with_extents (target->cr,
					     string,
					     xtick + xoffset,
					     ytick + yoffset,
					     anchor,
					     context->canvas_fg_color,
					     context->font_tick_labels, 0,
					     &x1, &y1, &x2, &y2);
	 
         if (x2 - x1 > width_tick_labels)
	   width_tick_labels = x2 - x1;
         }
      }

   /* Draw logarithmic-axis tick-mark labels */
   else if ( strcmp(axis_type, "log") == 0 )
      {
      axis_min = floor(tick_values[0]);
      axis_max = ceil(tick_values[nticks-1]);
      ndecades = roundl(axis_max - axis_min);
      if ( ndecades <= 10 )
         nticks = ndecades + 1;
      else
         {
         axis_min = tick_values[0];
         axis_max = tick_values[nticks-1];
         }

      xscale = x_range/(nticks - 1);
      yscale = y_range/(nticks - 1);

      increment = (tick_values[nticks-1] - tick_values[0])/(nticks - 1);
      for ( i=1; i<=nticks; i++ )
         {
         xtick = x1_screen + (i - 1)*xscale;
         ytick = y1_screen + (i - 1)*yscale;

         logtickvalue = roundl(axis_min + (i - 1)*(axis_max - axis_min)/(nticks - 1));
         tickvalue = pow(10.0, (double) logtickvalue);
         memset(string, 0, sizeof(string));
         snprintf(string, sizeof(string), "%g", tickvalue);

	 gse_cairo_render_text_with_extents (target->cr, string,
					     xtick + xoffset,
					     ytick + yoffset,
					     anchor,
					     context->canvas_fg_color,
					     context->font_tick_labels, 0,
					     &x1, &y1, &x2, &y2);
					     
	 
         if ( x2 - x1 > width_tick_labels )
            width_tick_labels = x2 - x1;
         }
      }

   return  width_tick_labels;
   }

